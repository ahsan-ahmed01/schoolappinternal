package com.scolalabs.scola360;

import android.app.Application;
import com.geektime.rnonesignalandroid.ReactNativeOneSignalPackage;
import com.facebook.react.ReactApplication;
import com.amazonaws.RNAWSCognitoPackage;
import com.brentvatne.react.ReactVideoPackage;
import com.learnium.RNDeviceInfo.RNDeviceInfo;
import com.bugsnag.BugsnagReactNative;
import cl.json.RNSharePackage;
import cl.json.ShareApplication;
import com.learnium.RNDeviceInfo.RNDeviceInfo;
import fr.bamlab.rnimageresizer.ImageResizerPackage;
import com.google.firebase.FirebaseApp;
import com.horcrux.svg.SvgPackage;
//import com.tradle.react.UdpSocketsModule;
import com.peel.react.TcpSocketsModule;
import com.sha256lib.Sha256Package;
import com.peel.react.rnos.RNOSModule;
//import com.amazonaws.amplify.pushnotification.RNPushNotificationPackage;
import com.brentvatne.react.ReactVideoPackage;
import com.apsl.versionnumber.RNVersionNumberPackage;
import com.cmcewen.blurview.BlurViewPackage;
import com.reactnativecommunity.webview.RNCWebViewPackage;
import com.microsoft.codepush.react.CodePush;
import com.oblador.vectoricons.VectorIconsPackage;
import com.beefe.picker.PickerViewPackage;
import com.reactnative.ivpusic.imagepicker.PickerPackage;
import com.facebook.react.ReactNativeHost;
import com.facebook.react.ReactPackage;
import com.facebook.react.shell.MainReactPackage;
import com.facebook.soloader.SoLoader;
import com.RNFetchBlob.RNFetchBlobPackage;
import com.reactnativenavigation.NavigationApplication;
import com.reactnativenavigation.react.NavigationReactNativeHost;
import com.reactnativenavigation.react.ReactGateway;
import java.util.Arrays;
import java.util.List;

//public class MainApplication extends Application implements ReactApplication {
//
//  private final ReactNativeHost mReactNativeHost = new ReactNativeHost(this) {

//        @Override
//        protected String getJSBundleFile() {
//        return CodePush.getJSBundleFile();
//        }
//
//      @Override
//      protected String getJSBundleFile() {
//          return CodePush.getJSBundleFile();
//      }
//
//    @Override
//    public boolean getUseDeveloperSupport() {
//      return BuildConfig.DEBUG;
//    }
//
//
////   @Override
////   public void onCreate() {
////                 super.onCreate();
////                 Fabric.with(this, new Crashlytics());
////             }
//
//    @Override
//    protected List<ReactPackage> getPackages() {
//      return Arrays.<ReactPackage>asList(
//          new MainReactPackage(),
//            new ReactVideoPackage(),
//            new RNDeviceInfo(),
//            BugsnagReactNative.getPackage(),
//            new RNDeviceInfo(),
//            new ImageResizerPackage(),
//            new SvgPackage(),
//            new UdpSocketsModule(),
//            new TcpSocketsModule(),
//            new Sha256Package(),
//            new RNOSModule(),
//              new CodePush("2LG6cGSSheJU4IflJRfxyien9YuPr1n2fqxPm", MainApplication.this, BuildConfig.DEBUG),
//
//            new ReactVideoPackage(),
//            new ReactNativeOneSignalPackage(),
//            new RNVersionNumberPackage(),
//            new BlurViewPackage(),
//            new WebViewBridgePackage(),
//            //new CodePush("5IhpddfZO_JHzhtkeImWZoV0ua3gab342b6f-5851-41ad-9869-b3a695ec054d", MainApplication.this, BuildConfig.DEBUG),
//            new VectorIconsPackage(),
//            new PickerViewPackage(),
//            new PickerPackage(),
//              new RNFetchBlobPackage()
//
//      );
//    }
//
//    @Override
//    protected String getJSMainModuleName() {
//      return "index";
//    }
//  };
//
//  @Override
//  public ReactNativeHost getReactNativeHost() {
//    return mReactNativeHost;
//  }
//
//  @Override
//  public void onCreate() {
//    super.onCreate();
//    SoLoader.init(this, /* native exopackage */ false);
//    FirebaseApp.initializeApp(this);
//    OneSignal.startInit(this)
//            .inFocusDisplaying(OneSignal.OSInFocusDisplayOption.Notification)
//            .unsubscribeWhenNotificationsAreDisabled(true)
//            .init();
//  }
//}

public class MainApplication extends NavigationApplication {

  @Override
  protected ReactGateway createReactGateway() {
    ReactNativeHost host = new NavigationReactNativeHost(this, isDebug(), createAdditionalReactPackages()) {
      @Override
      protected String getJSMainModuleName() {
        return "index";
      }
    };
    return new ReactGateway(this, isDebug(), host);
  }

  @Override
  public boolean isDebug() {
    return BuildConfig.DEBUG;
  }

  protected List<ReactPackage> getPackages() {
    // Add additional packages you require here
    // No need to add RnnPackage and MainReactPackage
    return Arrays.<ReactPackage>asList(new MainReactPackage(), new ReactVideoPackage(), BugsnagReactNative.getPackage(),
        new RNDeviceInfo(), new RNAWSCognitoPackage(), new ImageResizerPackage(), new SvgPackage(),
        new RNCWebViewPackage(),
        // new UdpSocketsModule(),
        new TcpSocketsModule(), new Sha256Package(), new RNOSModule(),
        // new RNPushNotificationPackage(),
        new CodePush("2LG6cGSSheJU4IflJRfxyien9YuPr1n2fqxPm", MainApplication.this, BuildConfig.DEBUG),

        new ReactVideoPackage(), new ReactNativeOneSignalPackage(), new RNVersionNumberPackage(), new BlurViewPackage(),
        // new
        // CodePush("5IhpddfZO_JHzhtkeImWZoV0ua3gab342b6f-5851-41ad-9869-b3a695ec054d",
        // MainApplication.this, BuildConfig.DEBUG),
        new VectorIconsPackage(), new PickerViewPackage(), new PickerPackage(), new RNFetchBlobPackage(),
        new RNSharePackage()

    );
  }

  @Override
  public List<ReactPackage> createAdditionalReactPackages() {
    return getPackages();
  }

  @Override
  public void onCreate() {
    super.onCreate();
    SoLoader.init(this, /* native exopackage */ false);
    FirebaseApp.initializeApp(this);
    // OneSignal.startInit(this)
    // .inFocusDisplaying(OneSignal.OSInFocusDisplayOption.Notification)
    // .unsubscribeWhenNotificationsAreDisabled(true)
    // .init();
  }

}