"use strict";

import axios from "axios";
import moment from "moment";
var EnvConfig = require("../config/environment");
//import { GoogleAnalyticsTracker } from "react-native-google-analytics-bridge";
import { API } from "aws-amplify";
import * as miscutils from "../utils/misc";

function convertTo24Hour(time) {
  var hours = parseInt(time.substr(0, 2));
  if (time.indexOf("AM") != -1 && hours == 12) {
    time = time.replace("12", "0");
  }
  if (time.indexOf("PM") != -1 && hours < 12) {
    time = time.replace(hours, hours + 12);
  }
  return time.replace(/(AM|PM)/, "");
}

function getdatafromForm(formData, eventimgurl, selectedUsers, eventtype) {
  console.log(eventtype, "getdatafromForm-------------------");
  var body = {};
  body["eventname"] = formData._eventname;
  console.log(formData);
  if (formData._eventdesc !== undefined && formData.eventdesc !== null) {
    body["eventdesc"] = formData._eventdesc;
  } else {
    body["eventdesc"] = null;
  }
  var sdate = new Date(formData._eventstartDate);
  body["start_date"] = moment(sdate).format();
  //********** ADD DAY AND TIMES **************
  if (formData._alldayevent === undefined) {
    body["alldayevent"] = 1;
  } else {
    body["alldayevent"] = formData._alldayevent;
  }
  if (formData._eventstartTime !== null && formData._eventstartTime !== "") {
    var starttime = new Date(
      formData._eventstartDate + " " + formData._eventstartTime
    );
    body["start_time"] = moment(starttime).format();
  } else {
    body["start_time"] =  moment(sdate).format();
  }
  var edate = new Date(formData._eventendDate);
  body["end_date"] = moment(edate).format();
  if (formData._eventendTime !== null && formData._eventendTime !== "") {
    var endtime = new Date(
      formData._eventendDate + " " + formData._eventendTime
    );
    body["end_time"] = moment(endtime).format();
  } else {
    body["end_time"] = moment(edate).format();
  }

  //****************************************
  body["img_url"] = eventimgurl;
  var location = null;
  if (
    formData._eventlocation !== undefined &&
    formData._eventlocation !== null
  ) {
    location = formData._eventlocation;
  }
  body["location"] = location;
  if (formData._eventnotify === undefined || formData._eventnotify === true) {
    body["notifyParent"] = true;
  } else {
    body["notifyParent"] = false;
  }
  console.log(body);
  var _addAttachmentUrl = [];
  // if (formData._addAttachmentUrl.uri!==undefined) {
  //   _addAttachmentUrl = [formData._addAttachmentUrl.uri]
  // }
  
  /**** REMINDERS *****/
  var remindsameday = false;
  var remindonedaybefore = false;
  var remindtwodaybefore = false;

  if (formData._remindsameday === true) {
    remindsameday = true;
  }

  if (formData._remindsOneday === true) {
    remindonedaybefore = true;
  }

  if (formData._remindsTwoday === true) {
    remindtwodaybefore = true;
  }
  console.log(body);
  body["notifyOneDayAgo"] = remindsameday;
  body["notifyTwoDayAgo"] = remindonedaybefore;
  body["notifyThreeDayAgo"] = remindtwodaybefore;
  body["attachments"] = _addAttachmentUrl;
  body["event_color"] = "#000000";
  body["event_type"] = eventtype;
  if (formData._showParents === undefined || formData._showParents === false) {
    body["groups"] = [formData._grpSelectId];
    body["groupParentSelection"] = "group";
  } else {
    body["groups"] = null;
    var _users = Object.keys(selectedUsers);
    body["groupParentSelection"] = "parents";
    body["parents"] = _users;
  }
  console.log(body,"getdatafromForm");
  return body;
}

function getdatafromPTForm(
  formData,
  eventimgurl,
  selectedUsers,
  eventtype,
  parentTeacherSlots,
  volunteer,
  spot,
  volunteertype,
  daterangetype,
  daterange,
  selectedDays,
  slotTime,
  dates
) {
  var body = {};
  body["eventname"] = formData._eventname;
  console.log(formData);
  if (formData._eventdesc !== undefined && formData.eventdesc !== null) {
    body["eventdesc"] = formData._eventdesc;
  } else {
    body["eventdesc"] = null;
  }

  //****************************************
  body["img_url"] = eventimgurl;
  var location = null;
  if (
    formData._eventlocation !== undefined &&
    formData._eventlocation !== null
  ) {
    location = formData._eventlocation;
  }
  body["location"] = location;
  if (formData._eventnotify === undefined || formData._eventnotify === true) {
    body["notifyParent"] = true;
  } else {
    body["notifyParent"] = false;
  }
  console.log(body);
  /**** REMINDERS *****/
  var remindsameday = false;
  var remindonedaybefore = false;
  var remindtwodaybefore = false;

  if (formData._remindsameday === true) {
    remindsameday = true;
  }
  if (formData._remindsOneday === true) {
    remindonedaybefore = true;
  }
  if (formData._remindsTwoday === true) {
    remindtwodaybefore = true;
  }
  console.log(body);
  body["notifyOneDayAgo"] = remindsameday;
  body["notifyTwoDayAgo"] = remindonedaybefore;
  body["notifyThreeDayAgo"] = remindtwodaybefore;
  body["attachments"] = [];
  body["event_color"] = "#000000";
  body["event_type"] = eventtype;
  if (eventtype === 3) {
    body["parentTeacherSlots"] = parentTeacherSlots;
  }
  if (eventtype === 4) {
    body["volunteer"] = volunteer;
    body["spot"] = spot;
    body["volunteertype"] = volunteertype;
    body["daterangetype"] = daterangetype;
    body["slotTime"] = slotTime ? slotTime : [];
    body["dates"] = dates ? dates : {};
    if (volunteertype === "3") {
      body["selectedDays"] = selectedDays ? selectedDays : [];
      body["daterange"] = daterange ? daterange : {};
    }
  }
  if (formData._showParents === undefined || formData._showParents === false) {
    body["groups"] = [formData._grpSelectId];
    body["groupParentSelection"] = "group";
  } else {
    body["groups"] = null;
    var _users = Object.keys(selectedUsers);
    body["groupParentSelection"] = "parents";
    body["parents"] = _users;
  }
  console.log(body, "<-------getdatafromPTForm");
  return body;
}

export function addEvent(
  formData,
  selectedUsers,
  displayName,
  token,
  eventimgurl = "",
  groups,
  eventtype,
  eventid,
  parentTeacherSlots,
  volunteer,
  spot,
  volunteertype,
  daterangetype,
  daterange,
  selectedDays,
  slotTime,
  dates
) {
  console.log(
    eventtype,
    eventid,
    parentTeacherSlots,
    volunteer,
    spot,
    volunteertype,
    daterangetype,
    daterange,
    selectedDays,
    slotTime,
    dates,
    "eventsapi_parentTeacherSlots------------------"
  );
  if (eventtype === 1 || eventtype === 2) {
    console.log("eventsapi_getdatafromForm");
    var body = getdatafromForm(formData, eventimgurl, selectedUsers, eventtype);
  } else if (eventtype === 3 || eventtype === 4) {
    console.log("eventsapi_getdatafromPTForm");
    var body = getdatafromPTForm(
      formData,
      eventimgurl,
      selectedUsers,
      eventtype,
      parentTeacherSlots,
      volunteer,
      spot,
      volunteertype,
      daterangetype,
      daterange,
      selectedDays,
      slotTime,
      dates
    );
  }
  //body['parents'] = selectedUsers === null ? [] : selectedUsers
  console.log(body, "addEvent");
  let apiName = EnvConfig.API_NAME;
  var path = "";
  if (eventid === null) {
    path = "/events/create";
    console.log("Adding an Event");
  } else {
    path = "/events/edit";
    body["event_id"] = eventid;
    console.log("Edit an Event");
  }

  let myInit = {
    headers: {
      Authorization: token,
    },
    body: body,
    response: true,
  };
  console.log("Calling API");
  console.log(myInit);
  var response = API.post(apiName, path, myInit)
    .then((response) => {
      console.log(response);
      return response;
    })
    .catch((error) => {
      console.log(error.response);
      return error;
    });
  return response;
}

export function getUserResponses(token) {
  var usergroups = [];
  var startTime = new Date();
  // for(let grp in groups){
  //     console.log(groups[grp])
  //     usergroups.push(groups[grp]['id'])
  // }

  let apiName = EnvConfig.API_NAME;
  let path = "/events/get-user-response";

  let myInit = {
    headers: {
      Authorization: token,
    },
    body: {
      filter: "upcomming",
    },
    response: true,
  };
  console.log("Calling API");
  console.log(myInit);
  var response = API.post(apiName, path, myInit)
    .then((response) => {
      // console.log(response,"getUserResponse")
      var endtime = new Date();
      var timediff = endtime - startTime;
      miscutils.captureStat("API:events", "getUserResponses", timediff);
      return response;
    })
    .catch((error) => {
      // console.log(error.response,"getUserResponseError")
      return error;
    });
  return response;
}

export function getEventResponses(eventid, token) {
  var startTime = new Date();

  let apiName = EnvConfig.API_NAME;
  let path = "/events/get-event-response";

  let myInit = {
    headers: {
      Authorization: token,
    },
    body: {
      event_id: eventid,
    },
    response: true,
  };
  console.log("Calling API");
  console.log(myInit);
  var response = API.post(apiName, path, myInit)
    .then((response) => {
      console.log(response);
      var endtime = new Date();
      var timediff = endtime - startTime;
      miscutils.captureStat("API:events", "getEventResponses", timediff);
      return response;
    })
    .catch((error) => {
      console.log(error.response);
      return error;
    });
  return response;
}

export async function deleteVolunteerSignupforChild(
  event_id,
  child_id,
  spotId,
  token
) {
  var startTime = new Date();

  let apiName = EnvConfig.API_NAME;
  let path = "/events/child/delete";
  var body = {};
  body["eventid"] = event_id;
  body["child_id"] = child_id;
  body["spotId"] = spotId;

  let myInit = {
    headers: {
      Authorization: token,
    },
    body: body,
    response: true,
  };
  console.log("Calling API");
  console.log(myInit);
  var response = API.post(apiName, path, myInit)
    .then((response) => {
      console.log(response);
      var endtime = new Date();
      var timediff = endtime - startTime;
      miscutils.captureStat("API:events", "deleteSignupforChild", timediff);
      return response;
    })
    .catch((error) => {
      console.log(error.response);
      return error;
    });
  return response;
}

export async function deleteSignupforChild(event_id, child_id, slotId, token) {
  var startTime = new Date();

  let apiName = EnvConfig.API_NAME;
  let path = "/events/child/delete";
  var body = {};
  body["eventid"] = event_id;
  body["child_id"] = child_id;
  body["slotId"] = slotId;

  let myInit = {
    headers: {
      Authorization: token,
    },
    body: body,
    response: true,
  };
  console.log("Calling API");
  console.log(myInit);
  var response = API.post(apiName, path, myInit)
    .then((response) => {
      console.log(response);
      var endtime = new Date();
      var timediff = endtime - startTime;
      miscutils.captureStat("API:events", "deleteSignupforChild", timediff);
      return response;
    })
    .catch((error) => {
      console.log(error.response);
      return error;
    });
  return response;
}

export async function signupforChild(
  event_id,
  event_type,
  child_id,
  slotId,
  token
) {
  var startTime = new Date();

  let apiName = EnvConfig.API_NAME;
  let path = "/events/event-response";
  var body = {};
  body["eventid"] = event_id;
  body["child_id"] = child_id;
  body["slotId"] = slotId;
  body["event_type"] = event_type;
  let myInit = {
    headers: {
      Authorization: token,
    },
    body: body,
    response: true,
  };
  console.log("Calling API");
  console.log(myInit);
  var response = API.post(apiName, path, myInit)
    .then((response) => {
      console.log(response);
      var endtime = new Date();
      var timediff = endtime - startTime;
      miscutils.captureStat("API:events", "signupforChild", timediff);
      return response;
    })
    .catch((error) => {
      console.log(error.response);
      return error;
    });
  return response;
}

export async function volunteerforEvent(
  event_id,
  event_type,
  child_id,
  spotId,
  token
) {
  var startTime = new Date();

  let apiName = EnvConfig.API_NAME;
  let path = "/events/event-response";
  var body = {};
  body["eventid"] = event_id;
  body["child_id"] = child_id;
  body["spotId"] = spotId;
  body["event_type"] = event_type;
  let myInit = {
    headers: {
      Authorization: token,
    },
    body: body,
    response: true,
  };
  console.log("Calling API");
  console.log(myInit);
  var response = API.post(apiName, path, myInit)
    .then((response) => {
      console.log(response);
      var endtime = new Date();
      var timediff = endtime - startTime;
      miscutils.captureStat("API:events", "volunteerforEvent", timediff);
      return response;
    })
    .catch((error) => {
      console.log(error.response);
      return error;
    });
  return response;
}

export async function sendUserResponse(
  event_id,
  event_type,
  attend,
  adult,
  children,
  token
) {
  var usergroups = [];
  var startTime = new Date();

  let apiName = EnvConfig.API_NAME;
  let path = "/events/event-response";
  var body = {};
  body["event_id"] = event_id;
  body["event_type"] = event_type;
  body["attend"] = attend;
  if (adult !== -1) {
    body["adult"] = adult;
  }
  if (children !== -1) {
    body["children"] = children;
  }
  let myInit = {
    headers: {
      Authorization: token,
    },
    body: body,
    response: true,
  };
  console.log("Calling API");
  console.log(myInit);
  var response = API.post(apiName, path, myInit)
    .then((response) => {
      console.log(response);
      var endtime = new Date();
      var timediff = endtime - startTime;
      miscutils.captureStat("API:events", "sendUserResponse", timediff);
      return response;
    })
    .catch((error) => {
      console.log(error.response);
      return error;
    });
  return response;
}

export function getEvents(token, groups) {
  var usergroups = [];
  var startTime = new Date();
  // for(let grp in groups){
  //     console.log(groups[grp])
  //     usergroups.push(groups[grp]['id'])
  // }

  let apiName = EnvConfig.API_NAME;
  let path = "/events/get";

  let myInit = {
    headers: {
      Authorization: token,
    },
    body: {
      groups: Object.keys(groups),
    },
    response: true,
  };
  console.log("Calling API");
  console.log(myInit);
  var response = API.post(apiName, path, myInit)
    .then((response) => {
      var endtime = new Date();
      var timediff = endtime - startTime;
      miscutils.captureStat("API:events", "getEvent", timediff);
      console.log(response, "--eventResponse--");
      return response;
    })
    .catch((error) => {
      console.log(error.response);
      return error;
    });
  return response;
}

export function deleteEvent(eventid, token) {
  let apiName = EnvConfig.API_NAME;
  let path = "/events/delete";

  let myInit = {
    headers: {
      Authorization: token,
    },
    body: {
      event_id: eventid,
    },
    response: true,
  };
  console.log("Calling API");
  console.log(myInit);
  var response = API.post(apiName, path, myInit)
    .then((response) => {
      console.log(response);
      return response;
    })
    .catch((error) => {
      console.log(error.response);
      return error;
    });
  return response;
}
