import { Auth } from "aws-amplify";

// AWS Mobile Hub Project Constants
export const authHeader = async () => {
  let session = await Auth.currentSession();
  console.log(session);
  if (session) {
    return {
      Authorization: session.idToken.jwtToken,
      "content-type": "application/json"
    };
  }
};

module.exports = { authHeader };
