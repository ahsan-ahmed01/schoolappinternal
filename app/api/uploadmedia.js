'use strict'

import axios from 'axios'
var EnvConfig = require('../config/environment')
import { RNS3 } from 'react-native-aws3'
import ImageResizer from 'react-native-image-resizer'
import {Platform} from 'react-native'
var uuid = require('react-native-uuid')
//var Fabric = require('react-native-fabric');
//var { Answers } = Fabric;
//import { GoogleAnalyticsTracker } from "react-native-google-analytics-bridge";
import * as NewsAction from '../actions/newsfeedActions'
import { Buffer } from 'buffer'
import {Storage} from 'aws-amplify'
import RNFetchBlob from 'react-native-fetch-blob';
import * as miscutils from '../utils/misc'
export function _wp_UploadPhoto (image, userid, groupid) {
  let filename = userid + '_' + uuid.v4() + '.jpg'
  console.log(filename)
  const file = {
    uri: image.path,
    type: 'image/jpeg',
    name: filename
  }
  var today = new Date()
  var dd = today.getDate()
  var mm = today.getMonth()+1
  var yyyy = today.getFullYear()

  const options = {
    keyPrefix: 'uploads/' + yyyy + '/' + mm + '/' + dd + '/',
    bucket: EnvConfig.AWS_CONFIG.bucket,
    region: EnvConfig.AWS_CONFIG.region,
    accessKey: EnvConfig.AWS_CONFIG.accessKey,
    secretKey: EnvConfig.AWS_CONFIG.secretKey,
    successActionStatus: 201
  }

  var response = RNS3.put(file, options).then(response => {
    if (response.status !== 201) {
      throw new Error('Failed to upload image to S3')
    }
    return response.body
  })
  .catch(error => {
    return error
  })
  console.log(response)
  return response
}

function readFile(filePath) {
    console.log(filePath)
    RNFetchBlob.fs.exists(filePath)
        .then((exist) => {
            console.log(`file ${exist ? '' : 'not'} exists`)
        })
    return RNFetchBlob.fs.readFile(filePath, 'base64').then(data => new Buffer(data, 'base64'));
}

function ResizeImage (image) {
    console.log('In here')
    console.log(image)
    if (Platform.OS === "ios"){
        var response = ImageResizer.createResizedImage(image.path, 1024, 768, 'JPEG', 100).then((response) => {
            console.log(response)
            return response
        });
    }else{
        var response = ImageResizer.createResizedImage(image.path, 1024, 768, 'JPEG', 100).then((response) => {
            console.log(response)
            return response
        });
    }

    return response
}

export async function UploadPhoto (image, contentType, groupid="", remotefilename = "") {
    console.log('In UploadPhoto')
    var  startTime = new Date();
    var seconds = new Date().getTime();
    //var rand = Math.floor(Math.random() * 1000000) + 1
    var ext = "." + image.path.split('.').pop() ;
    var chars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXTZabcdefghiklmnopqrstuvwxyz";
    var string_length = 12;
    var randomstring = '';
    for (var i=0; i<string_length; i++) {
        var rnum = Math.floor(Math.random() * chars.length);
        randomstring += chars.substring(rnum,rnum+1);
    }
    let filename = (groupid == "") ? seconds + '_'+ randomstring + ext :  groupid + '_' + seconds + '_'+ randomstring + ext;
    console.log(filename)
    console.log(remotefilename)
    try {


        var today = new Date()
        var dd = today.getDate()
        var mm = today.getMonth() + 1
        var yyyy = today.getFullYear()
        if (remotefilename !== ""){
            filename = remotefilename
        }
        else {
            filename = yyyy + '/' + mm + '/' + dd + '/' + filename;
        }
        console.log(image)
        if (image.mime.includes('video')){
            var filepath = image.path.replace('file:///', '')
        }else {


            var filepath = image.path.replace('file:///', '')
            var resizedImage = await ResizeImage(image)
            console.log(resizedImage)
            filepath = resizedImage.path

        }

        console.log('Putting on S3 ->' + filepath + ' to ' + filename)
        var response = await  readFile(filepath).then(buffer => {
            var response = Storage.put(filename, buffer, {
                contentType: contentType
            }).then(result => {
                console.log(result)
                miscutils.captureStat('API:UploadMedia','Upload Object',new Date()-startTime)
                return {status: 'success', data: result}
            }).catch(e => {
                console.log(e)
                return {status: 'error', error: e}
            })
            return response
        }).catch(e => {
            console.log(e)
            return {status: 'error', error: e}
        })
        return response
    }
    catch(e){
        return {status: 'error', error: e}
    }


}

export async function UploadPhotoPublic (image, remotefilename = "") {
    const options = {
            bucket: EnvConfig.AWS_CONFIG.S3_PROFILE_BUCKET,
            region: EnvConfig.AWS_CONFIG.S3_BUCKET_REGION,
            accessKey: EnvConfig.AWS_CONFIG.S3_PROFILE_ACCESS_KEY,
            secretKey: EnvConfig.AWS_CONFIG.S3_PROFILE_SECRET_KEY,
            successActionStatus: 201
        }
        console.log(remotefilename)
        if (remotefilename !== ''){
            var filename = remotefilename
        }else {

            var filename = 'Chat/Chat_' + Date.now() + '.jpg'
        }
        const file = {
            uri: image.path,
            type: 'image/jpeg',
            name: filename
        }
        console.log(file)
        var response = RNS3.put(file, options).then(response => {
            if (response.status !== 201) {
                throw new Error('Failed to upload image to S3')
            }
            var url = EnvConfig.AWS_CONFIG.S3_PROFILE_URL + filename
            console.log(url)
            return url
        })
            .catch(error => {
                return error
            })
        console.log(response)
        return response

}

// export function UploadPhotoforChat (image, userid) {
//   let filename = 'chat_' + userid + '_' + uuid.v4() + '.jpg'
//   console.log(filename)
//   const file = {
//     uri: image.path,
//     type: 'image/jpeg',
//     name: filename
//   }
//   var today = new Date()
//   var dd = today.getDate()
//   var mm = today.getMonth()+1
//   var yyyy = today.getFullYear()
//
//   const options = {
//     keyPrefix: 'uploads/' + yyyy + '/' + mm + '/' + dd + '/',
//     bucket: EnvConfig.AWS_CONFIG.bucket,
//     region: EnvConfig.AWS_CONFIG.region,
//     accessKey: EnvConfig.AWS_CONFIG.accessKey,
//     secretKey: EnvConfig.AWS_CONFIG.secretKey,
//     successActionStatus: 201
//   }
//
//   var response = RNS3.put(file, options).then(response => {
//     if (response.status !== 201) {
//       throw new Error('Failed to upload image to S3')
//     }
//     return response.body
//   })
//   .catch(error => {
//     return error
//   })
//   console.log(response)
//   return response
// }

export function UploadPhotoOldViaWP (image, token, callbackfunc) {
  console.log('in Action')
  console.log(image)
    var photo = {
    uri: image.path,
    type: 'image/jpeg',
    name: 'photo.jpg'
  }
  var headers = {
    headers: {
      'Authorization': 'Bearer ' + token,
        'Content-Type': 'multipart/FormData'
    }
  }
  var data = {
    'image': photo
  }
  // var headers = new Headers()
  // headers.append('Authorization', 'Bearer ' + token)
  // headers.append('Content-Type', 'multipart/FormData')
  console.log(EnvConfig.UPLOAD_PHOTO)
  console.log(headers)
    console.log(photo)
  var body = new FormData()
  body.append('image', photo)
  var  startTime = new Date();
    //let tracker = new GoogleAnalyticsTracker(EnvConfig.GOOGLE_ANALYTICS_TRACKERID);
    var axiosinstance = axios.create();
    axiosinstance.defaults.timeout = 300000;
    var response = axiosinstance
            .post(EnvConfig.UPLOAD_PHOTO,
            body, headers
            ).then(function (response) {
              var  endTime = new Date();
              var timeDiff = endTime - startTime

              console.log('Image Time taken : ' + timeDiff)
              // tracker.trackTiming("News", timeDiff, {
              //     name: "ImageUpload"
              // });
              response["timeDiff"] = timeDiff
              return response
            })
            .catch(function (error) {
              return error
            })
  console.log(response)
  return response

//   var response = fetch(EnvConfig.UPLOAD_PHOTO, {
//     method: 'POST',
//     headers,
//     body
//   }).then(function (response) {
//     console.log('Success !!')
//     return response
//   })
//  .catch(function (error) {
//    console.log('Error !')
//    return error
//  })
}

export function UploadVideoOldViaWP (image, token, callbackfunc) {
    console.log('in Action')
    console.log(image)
    //if image.
    var ext = image.filename.split('.').pop();
    console.log('Extension->' + ext)
    var video = {
        uri: image.path,
        type: 'video/' + ext,
        name: image.filename
    }
    var headers = {
        headers: {
            'Authorization': 'Bearer ' + token,
            'Content-Type': 'multipart/FormData'
        }
    }
    // var headers = new Headers()
    // headers.append('Authorization', 'Bearer ' + token)
    // headers.append('Content-Type', 'multipart/FormData')
    console.log(EnvConfig.UPLOAD_VIDEO)
    console.log(headers)
    console.log(video)
    var body = new FormData()
    body.append('video', video)
    var  startTime = new Date();
    //let tracker = new GoogleAnalyticsTracker(EnvConfig.GOOGLE_ANALYTICS_TRACKERID);
    var axiosinstance = axios.create();
    axiosinstance.defaults.timeout = 600000;
    var response = axiosinstance
        .post(EnvConfig.UPLOAD_VIDEO,
            body, headers
        ).then(function (response) {
            var  endTime = new Date();
            var timeDiff = endTime - startTime

            console.log('Image Time taken : ' + timeDiff)
            // tracker.trackTiming("News", timeDiff, {
            //     name: "VideoUpload"
            // });
            response["timeDiff"] = timeDiff
            return response
        })
        .catch(function (error) {
            return error
        })
    console.log(response)
    return response

//   var response = fetch(EnvConfig.UPLOAD_PHOTO, {
//     method: 'POST',
//     headers,
//     body
//   }).then(function (response) {
//     console.log('Success !!')
//     return response
//   })
//  .catch(function (error) {
//    console.log('Error !')
//    return error
//  })
}