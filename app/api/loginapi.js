'use strict'

import axios from 'axios'

var EnvConfig = require('../config/environment')
var bugsnag = require('../config/bugsnag')
import {Auth, API} from 'aws-amplify'
import * as miscutils from '../utils/misc'
function setHeader() {
    return Auth.currentSession().then((data) => {
        console.log('In Then')
        console.log(data)
        return {
            'Authorization': data.getIdToken().getJwtToken(),
            'content-type': 'application/json'
        }
    })

}

export const latestSession = async () => {
    try {
        let session = await Auth.currentSession()
        console.log(session)
        if (session){
            return session;
        }
    } catch(e) {
        console.log(e)
        return false;
    }
}


export function authenticateuserapi(actionpayload) {
    var username = actionpayload.user
    var password = actionpayload.password
    var startTime = new Date();
    console.log("in API")
    bugsnag.setUser('-1', 'NotKnown', actionpayload.user);
    return Auth.signIn(username, password)
        .then(user => {
            var endtime = new Date();
            var timediff = endtime - startTime
            console.log('Time to get authenticateuserapi: ' + timediff)
            miscutils.captureStat('API:login','authenticateuser',timediff)
            console.log('In Then .....')
            console.log(user)
            if (user.challengeName === "NEW_PASSWORD_REQUIRED") {
                bugsnag.leaveBreadcrumb('ERROR NEW_PASSWORD_REQUIRED', {type: 'loginapi'});
                throw new Error('NEW_PASSWORD_REQUIRED')
            } else {
                bugsnag.leaveBreadcrumb('Logged in', {type: 'loginapi'});
                return user
            }

        })
        .catch(err => {
                console.log('In Catch .....')
                bugsnag.leaveBreadcrumb('Login Error - ', {type: 'loginapi'});
                console.log(err)

                bugsnag.notify(new Error(err.message));
                console.log(err)
                throw (err)
            }
        )
}

export function getAuthObj() {
    return Auth.currentCredentials().then(cred => {
        console.log(cred)
        var authobj = Auth.essentialCredentials(cred)
        console.log(cred)
        return authobj
    }).catch(err => {
            console.log('In Catch .....')
            bugsnag.leaveBreadcrumb('get Auth Error - ', {type: 'loginapi'});
            bugsnag.notify(new Error(err.message));
            throw (err)
        }
    )

}

export function _WP_authenticateuserapi(actionpayload) {
    console.log(actionpayload)
    var response = axios
        .post(EnvConfig.AUTH_URL, {
            username: actionpayload.user,
            password: actionpayload.password
        })
        .then((response) => {
                return response
            }
        )
    return response
}

export function getuserapi(token) {
    var startTime = new Date();
    let apiName = EnvConfig.API_NAME
    let path = '/user/me'
    let myInit = {
        headers: {
            'Authorization': token
        },
        response: true
    }
    console.log('Calling API')
    console.log(myInit)
    var response = API.get(apiName, path, myInit).then(response => {
        var endtime = new Date();
        var timediff = endtime - startTime
        console.log('Time to get Adminme: ' + timediff)
        miscutils.captureStat('API:login','getuserapi',timediff)
        bugsnag.leaveBreadcrumb('Adminme:' + timediff, {type: 'loginapi'});
        console.log(response)
        return response
    }).catch(error => {
        console.log(error)
        bugsnag.notify(new Error(error.response));

        return error
    })
    return response
}

export function getuserchildrenapi(token) {
    var startTime = new Date();
    let apiName = EnvConfig.API_NAME
    let path = '/user/me/children'
    let myInit = {
        headers: {
            'Authorization': token
        },
        response: true
    }
    console.log('Calling API')
    var response = API.get(apiName, path, myInit).then(response => {
        var endtime = new Date();
        var timediff = endtime - startTime
        miscutils.captureStat('API:login','getuserchildren',timediff)
        console.log('Time to get childrenapi: ' + timediff)
        console.log(response)
        return response
    }).catch(error => {
        console.log(error.response)
        return error
    })
    return response
}


export function getFireBaseConfig(token) {
    let apiName = EnvConfig.API_NAME
    var startTime = new Date();
    let path = '/config/firebase'
    let myInit = {
        headers: {
            'Authorization': token
        },
        response: true
    }
    console.log(myInit)
    var response = API.get(apiName, path, myInit).then(response => {
        var endtime = new Date();
        var timediff = endtime - startTime
        miscutils.captureStat('API:login','firebaseconfig',timediff)
        console.log('Time to get firebaseconfig: ' + timediff)
        console.log(response)
        return response
    }).catch(error => {
        console.log(error)

        return error
    })
    return response
}

export function updateUserProfilePicture(url, oldurl, token) {

    let apiName = EnvConfig.API_NAME
    let path = '/user/me/updatepicture'
    let myInit = {
        headers: {
            'Authorization': token
        }, body: {
            'profile_url': url,
            'old_profile_url': oldurl
        },
        response: true
    }
    console.log(myInit)
    var response = API.post(apiName, path, myInit).then(response => {
        console.log(response)
        return response
    }).catch(error => {
        console.log(error.response)
        return error
    })
    return response
}

export function getUserCache(token) {
    var startTime = new Date();
    let apiName = EnvConfig.API_NAME
    let path = '/config/usercache'
    let myInit = {
        headers: {
            'Authorization': token,
        },
        response: true
    }
    console.log(myInit)
    var response = API.get(apiName, path, myInit).then(response => {
        var endtime = new Date();
        var timediff = endtime - startTime
        miscutils.captureStat('API:login','getUserCache',timediff)
        console.log('Time to get UserCache: ' + timediff)
        bugsnag.leaveBreadcrumb('User-Cache:' + timediff, {type: 'loginapi'});
        console.log(response)
        return response
    }).catch(error => {
        bugsnag.notify(new Error(error.response))
        console.log(error.response)
        return error
    })
    return response
}

export function getParentofCache(token) {
    var startTime = new Date();
    let apiName = EnvConfig.API_NAME
    let path = '/config/parentofcache'
    let myInit = {
        headers: {
            'Authorization': token
        },
        response: true
    }
    console.log(myInit)
    var response = API.get(apiName, path, myInit).then(response => {
        var endtime = new Date();
        var timediff = endtime - startTime
        miscutils.captureStat('API:login','getParentofCache',timediff)
        console.log('Time to get ParentOfCache: ' + timediff)
        console.log(response)
        bugsnag.leaveBreadcrumb('ParentOf-Cache:' + timediff, {type: 'loginapi'});

        return response
    }).catch(error => {
        bugsnag.notify(new Error(error.response))
        console.log(error.response)
        return error
    })
    return response
}


export function _WP_getuserapi(token) {
    console.log('in action')
    var headers = {
        headers: {
            'Authorization': 'Bearer ' + token
        }
    }
    console.log('Getting user details from ' + EnvConfig.USER_DETAIL_URL)
    var response = axios
        .get(EnvConfig.USER_DETAIL_URL, headers)
        .then((response) => {
            return response
        })
    return response
}
