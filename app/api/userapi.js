'use strict'

import axios from 'axios'
var EnvConfig = require('../config/environment')
import { Auth, API } from 'aws-amplify'
import * as loginapi from './loginapi'

export function _wp_sendFeedback (feedback, data, token) {
    var headers = {
        headers: {
            'Authorization': 'Bearer ' + token
        }
    }
    data['feedback'] = feedback
    var response = axios
        .post(EnvConfig.SEND_FEEDBACK_URL, data, headers)
        .then((response) => {
            console.log(response)

            return response
        })
        .catch((err) => {
        console.log(err)
    })
    console.log(response)
    return response
}

export function sendFeedback (feedback, data, token) {
    console.log('IN SEND FEEDBACK API !!!')
    var  startTime = new Date();
    let apiName = EnvConfig.API_NAME
    let path = '/misc/feedback'

    var data = {
        headers: {
            'Authorization' : token
        },
        body : {
            'feedback': feedback,
        },
        response: true
    }

    console.log(data)
    var response = API.post(apiName, path, data).then(response => {
        console.log(response)
        var  endtime = new Date();
        var timediff = endtime - startTime
        console.log('Time to Send Feedback : ' + timediff)
        return response
    }).catch(error => {
        console.log(error.response)
        return error
    })
    console.log(response)
    return response
}

export  function addupdateChildAuthorizedUser(user, childid, pinCode, token) {
    var  startTime = new Date();
    let apiName = EnvConfig.API_NAME
    let path = '/user/addupdatepin'

    var data = {
        headers: {
            'Authorization' : token
        },
        body : {
            'user' : user,
            'childid' : childid,
            'pin' : pinCode,

        },
        response: true
    }

    console.log(data)
    var response = API.post(apiName, path, data).then(response => {
        console.log(response)
        var  endtime = new Date();
        var timediff = endtime - startTime
        console.log('Time to Add Authorized User: ' + timediff)
        return response
    }).catch(error => {
        console.log(error.response)
        return error
    })
    console.log(response)
    return response
}

export  function deleteChildAuthorizedUser(user, childid, pinCode, token) {
    var  startTime = new Date();
    let apiName = EnvConfig.API_NAME
    let path = '/user/deleteAuthUser'

    var data = {
        headers: {
            'Authorization' : token
        },
        body : {
            'user' : user,
            'childid' : childid,
            'pin' : pinCode,

        },
        response: true
    }

    console.log(data)
    var response = API.post(apiName, path, data).then(response => {
        console.log(response)
        var  endtime = new Date();
        var timediff = endtime - startTime
        console.log('Time to Add Authorized User: ' + timediff)
        return response
    }).catch(error => {
        console.log(error.response)
        return error
    })
    console.log(response)
    return response
}

export  function  getResetVerson() {
    var  startTime = new Date();
    let apiName = EnvConfig.API_NAME
    let path = '/user/resetversion'


    return Auth.currentSession().then((data) => {
        var data = {
            headers: {
                'Authorization' : data.getIdToken().getJwtToken(),
            },
            response: true
        }
    
        console.log(data)
        var response = API.get(apiName, path, data).then(response => {
            console.log(response)
            var  endtime = new Date();
            var timediff = endtime - startTime
            console.log('Time to Get Reset Version: ' + timediff)
            return response
        }).catch(error => {
            console.log(error.response)
            return error
        })
        console.log(response)
        return response
    })
    
}

export  function  updateChildProfilePicture(url,childid, token) {
    var  startTime = new Date();
    console.log('IN API')
    let apiName = EnvConfig.API_NAME
    let path = '/user/updatechildphoto'

    var data = {
        headers: {
            'Authorization' : token
        },
        body : {
            'url' : url,
            'childid' : childid

        },
        response: true
    }

    console.log(data)
    var response = API.post(apiName, path, data).then(response => {
        console.log(response)
        var  endtime = new Date();
        var timediff = endtime - startTime
        console.log('Time to Add Authorized User: ' + timediff)
        return response
    }).catch(error => {
        console.log(error.response)
        return error
    })
    console.log(response)
    return response
}