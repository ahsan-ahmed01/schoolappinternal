"use strict";

import * as firebase from "firebase";
import * as miscutils from "../utils/misc";
var EnvConfig = require("../config/environment");
import { sha256 } from "react-native-sha256";
export function Init(config) {
  console.log("in Init");
  console.log(config);
  if (!firebase.apps.length) {
    firebase.initializeApp(config);
  } else {
    console.log("FIREBASE ALREADY INITIALIZED !!!");
  }
  console.log("After Init");
  var result = firebase
    .auth()
    .signInAnonymously()
    .catch(function(error) {
      // Handle Errors here.
      var errorCode = error.code;
      var errorMessage = error.message;
      console.log(error);
    });

  console.log("FireBase Logged in Successfully");
  console.log("AfterSign");
  console.log(result);
}

export function notifyPostinFirebase(groupid) {
  console.log("Updating Post for Group->" + groupid);

  firebase
    .database()
    .ref("/Posts/LatestPost/" + groupid)
    .set(Date.now());
}

export function DeInit(config) {
  firebase
    .auth()
    .signOut()
    .then(
      function() {
        console.log("Signed out of Firebase");
      },
      function(error) {
        console.log(error);
      }
    );
}

export function registerUser(username, userObj) {
  console.log(username);
  console.log(userObj);
  console.log(firebase.database());
  console.log(firebase.database().app);

  firebase
    .database()
    .ref("Users/" + username)
    .update({
      display_name: userObj["display_name"],
      user_email: userObj["email"],
      user_nicename: userObj["display_name"],
      profile_photo: userObj["picture"],
      parent_of: userObj["parent_of"],
      linked: userObj["linked_to"]
    })
    .catch(error => {
      console.log(error);
      console.log("Error Updating Firebase");
    });
}

export function getRoomKeyStr(users) {
  var userids = [];
  users.forEach(function(user) {
    userids.push(user.userid);
  });
  userids.sort();
  var userstr = userids.join("-");
  return userstr;
}

export function setupNewChatRoom(roomname, users, userId) {
  console.log(users);
  console.log(userId);
  var roomkeyUserStr = getRoomKeyStr(users);
  return sha256(roomkeyUserStr).then(hashKey => {
    console.log(roomkeyUserStr + "--->" + hashKey);

    console.log("room hash->" + hashKey);
    var newKey = firebase
      .database()
      .ref()
      .child("Rooms")
      .push().key;
    console.log(newKey);
    var updatedUserData = {};
    firebase
      .database()
      .ref("/Rooms/" + newKey + "/Details/")
      .set({
        name: roomname
      });
    console.log(users);
    users.forEach(function(user) {
      updatedUserData["/Users/" + user.userid + "/rooms/" + newKey] =
        user.userid === userId ? true : false;
      updatedUserData["/Rooms/" + newKey + "/Details/users/" + user.userid] = {
        display_name: user.display_name,
        profile_photo: user.profile_photo,
        username: user.username
      };
    });
    updatedUserData["/Rooms/" + newKey + "/Details/userkey"] = hashKey;
    updatedUserData["/Chats/" + hashKey] = newKey;
    firebase
      .database()
      .ref()
      .update(updatedUserData, function(error) {
        if (error) {
          console.log("Error updating data:", error);
        }
      });
    return newKey;
  });
}

export function updateRoomHashKey(oldkey, newkey, roomKey) {
  console.log("Remove Key : " + "/Chats/" + oldkey);
  firebase
    .database()
    .ref("/Chats/" + oldkey)
    .remove();
  console.log("Add Key : " + "/Chats/" + newkey);
  firebase
    .database()
    .ref("/Chats/" + newkey)
    .set(roomKey);
}

export function getChatRoomKey(userId, users, roomname = "") {
  var userids = [];
  if (users.length > 2) {
    console.log(users);
    return null;
  }
  var roomkeyUserStr = getRoomKeyStr(users);
  console.log(roomkeyUserStr);
  return sha256(roomkeyUserStr).then(hashKey => {
    var newRoomKey = firebase
      .database()
      .ref("/Chats/" + hashKey)
      .once("value", function(snapshot) {
        console.log(snapshot);
        console.log(snapshot.val());
        if (snapshot.val() !== null) {
          return snapshot;
        } else {
          return null;
        }
      })
      .then(function(retval) {
        console.log(retval);
        console.log(retval.val());
        if (retval.val() !== null) {
          console.log("val");
          return retval.val();
        } else {
          console.log("str");
          return null;
        }
      });
    console.log(newRoomKey);
    return newRoomKey;
  });
}

export function getChatRooms(userid) {
  if (userid < 0) {
    return null;
  }
  console.log("In Firebase API ~~~~");
  var roomRef = firebase.database.ref("/Users/" + userid + "/rooms");
  roomRef.once("value", function(snap) {
    console.log(snap.val());
  });
}

export function clearChatUnRead(myuserid, roomKey) {
  var clearUpdate = {};
  clearUpdate["/Users/" + myuserid + "/Notifications/Rooms/" + roomKey] = 0;
  var ref = firebase.database().ref();
  ref.update(clearUpdate);
}

export function PerformChatCleanups(authObj, userid) {
  DeleteNotificationsforUnusedRooms(userid);
}

export function deletechatroomforUser(userid, roomKey) {
  firebase
    .database()
    .ref("/Users/" + userid + "/rooms/" + roomKey)
    .remove();
  firebase
    .database()
    .ref("/Users/" + userid + "/Notifications/Rooms/" + roomKey)
    .remove();
  console.log("Deleting /Users/" + userid + "/Notifications/Rooms/" + roomKey);
}

export function DeleteNotificationsforUnusedRooms(userid) {
  var date = firebase
    .database()
    .ref("/Users/" + userid + "/Notifications/")
    .on("value", function(snapshot) {
      var data = snapshot.val();
      if (data === null) return;
      for (var key in data.Rooms) {
        firebase
          .database()
          .ref("/Users/" + userid + "/rooms/" + key)
          .on("value", function(snapshot) {
            var roomStatus = snapshot.val();
            if (roomStatus === false || roomStatus === null) {
              console.log("Deleting Chartoom for user " + key);
              deletechatroomforUser(userid, key);
            }
          });
      }
    });
}
