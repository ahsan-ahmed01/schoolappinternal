'use strict'

import axios from 'axios'

var EnvConfig = require('../config/environment')
//import {GoogleAnalyticsTracker} from "react-native-google-analytics-bridge";
import {  API } from 'aws-amplify'
import * as miscutils from '../utils/misc'
export function getNewsFeedAPI(page = 0,groups,  token, isPinned=false,
                                        isdraft=false,inreview=false, isScheduled = false, itemsperpage = EnvConfig.NEWSFEED_ITEMS_PER_PAGE) {

    //var itemsperpage = EnvConfig.NEWSFEED_ITEMS_PER_PAGE
    console.log('In NewsFeed API, Page ' + page)
    if (groups === null){
        groups = []
    }
    if (isPinned){
        page = 0
    }
    var groupsarray = []
    Object.keys(groups).forEach(function (key) {
        var groupname = groups[key].GroupName
        if (groupname === undefined){

            groupsarray.push(groups[key].id) // If coming from reducer
        }else {
            groupsarray.push(key) //if coming fron loginworkflow
        }


    })
    var startTime = new Date();
    let apiName = EnvConfig.API_NAME
    let path = '/posts'
    let myInit = {
        headers: {
            'Authorization' : token,
        },
        body: {
            'itemsperpage': itemsperpage,
            'page': page,
            'groups' : groupsarray,
            'ispinned' : isPinned,
            'draftmode' : isdraft,
            'reviewmode' : inreview,
            'isScheduled' : isScheduled
        },
        response: true
    }
    console.log('Calling API')
    console.log(myInit)
    var response = API.post(apiName, path, myInit).then(response => {
        console.log(response)
        var endTime = new Date();
        var timeDiff = endTime - startTime
        console.log('Time to get NewsFeed : ' + timeDiff)
        miscutils.captureStat('API:newsfeed','GetNews',timeDiff)
        return response
    }).catch(error => {
        console.log(error)
        return error
    })
    return response
}

export function getNewsFeedbyGroupAPI(page = 0, groupid, token) {
    console.log('Getting News for Group : ' + groupid)
    getNewsFeedAPI(page, [groupid], token )
}


export function likepostAPI(activityid, token) {
    console.log('In NewsFeed API, Like Postid ' + activityid)
    let apiName = EnvConfig.API_NAME
    let path = '/posts/like'
    let myInit = {
        headers: {
            'Authorization' : token
        },body : {
            'postid': activityid
        },
        response: true
    }
    console.log('Calling API')
    console.log(myInit)
    var response = API.post(apiName, path, myInit).then(response => {
        console.log(response)
        return response
    }).catch(error => {
        console.log(error.response)
        return error
    })
    return response
}

export function unlikepostAPI(activityid, token) {
    console.log('In NewsFeed API, UnLike Postid ' + activityid)
    let apiName = EnvConfig.API_NAME
    let path = '/posts/unlike'
    let myInit = {
        headers: {
            'Authorization' : token
        },body : {
            'postid': activityid
        },
        response: true
    }
    console.log('Calling API')
    console.log(myInit)
    var response = API.post(apiName, path, myInit).then(response => {
        console.log(response)
        return response
    }).catch(error => {
        console.log(error.response)
        return error
    })
    return response
}

export function _wp_unlikepostAPI(activityid, token) {
    console.log('IN API !!!')
    var headers = {
        headers: {
            'Authorization': 'Bearer ' + token
        }
    }
    var data = {
        'activityid': activityid
    }
    console.log('before axios')
    var response = axios
        .post(EnvConfig.UNLIKEPOST_URL, data, headers)
        .then((response) => {
            return response
        })
    return response
}

export function viewPostAPI(activityid, token) {
    console.log('In NewsFeed API, View Postid ' + activityid)
    let apiName = EnvConfig.API_NAME
    let path = '/posts/view'
    let myInit = {
        headers: {
            'Authorization' : token
        },body : {
            'postid': activityid
        },
        response: true
    }
    console.log('Calling API')
    console.log(myInit)
    var response = API.post(apiName, path, myInit).then(response => {
        console.log(response)
        return response
    }).catch(error => {
        console.log(error.response)
        return error
    })
    return response
}

export function _wp_viewPostAPI(activityid, token) {
    console.log('IN VIEW_POST_URL API !!!')
    var headers = {
        headers: {
            'Authorization': 'Bearer ' + token,
        }
    }
    var data = {
        'activityid': activityid
    }
    console.log(headers)
    console.log('before axios view post')
    var response = axios
        .post(EnvConfig.VIEW_POST_URL, data, headers)
        .then((response) => {
            return response
        })
    console.log(response)
    return response
}

