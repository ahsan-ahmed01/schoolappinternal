"use strict";

import { API } from "aws-amplify";
var EnvConfigs = require("../config/environment");
import * as miscutils from "../utils/misc";
export function validatepin(childobj, pincode, status, token) {
  console.log(status);
  var startTime = new Date();
  let apiName = EnvConfigs.API_NAME;
  let path = "/attendance/signinchild";

  var data = {
    headers: {
      Authorization: token
    },
    body: {
      childid: childobj.id,
      pin: pincode,
      status: status
    },
    response: true
  };

  console.log(data);
  var response = API.post(apiName, path, data)
    .then(response => {
      console.log(response);
      var endtime = new Date();
      var timediff = endtime - startTime;
      miscutils.captureStat("API:attendance", "AuthPin", timediff);
      console.log("Time to Authorized PIN " + timediff);
      return response;
    })
    .catch(error => {
      console.log(error.response);
      return error;
    });
  console.log(response);
  return response;
}

export function setAttendanceOverride(attendancedate, childobj, status, token) {
  console.log(status);
  var startTime = new Date();
  let apiName = EnvConfigs.API_NAME;
  let path = "/attendance/attendanceoverride";

  var data = {
    headers: {
      Authorization: token
    },
    body: {
      childid: childobj.id,
      attendancedate: attendancedate,
      status: status
    },
    response: true
  };

  console.log(data);
  var response = API.post(apiName, path, data)
    .then(response => {
      console.log(response);
      var endtime = new Date();
      var timediff = endtime - startTime;
      miscutils.captureStat("API:attendance", "SetOverrideUser", timediff);
      console.log("Time to set Override User " + timediff);
      return response;
    })
    .catch(error => {
      console.log(error.response);
      return error;
    });
  console.log(response);
  return response;
}

export function getAuthorizedUsers(childobj, token) {
  var startTime = new Date();
  let apiName = EnvConfigs.API_NAME;
  let path = "/attendance/authorizedusers";

  var data = {
    headers: {
      Authorization: token
    },
    body: {
      childid: childobj.id
    },
    response: true
  };

  console.log(data);
  var response = API.post(apiName, path, data)
    .then(response => {
      console.log(response);
      var endtime = new Date();
      var timediff = endtime - startTime;
      miscutils.captureStat("API:attendance", "getAuthorizedUsers", timediff);
      console.log("Time to get Authorized Users: " + timediff);
      return response;
    })
    .catch(error => {
      console.log(error.response);
      return error;
    });
  console.log(response);
  return response;
}

export function getLatestStatus(groups, token) {
  var startTime = new Date();
  let apiName = EnvConfigs.API_NAME;
  let path = "/attendance/getattendance";

  var data = {
    headers: {
      Authorization: token
    },
    body: {
      group_ids: groups
    },
    response: true
  };

  console.log(data);
  var response = API.post(apiName, path, data)
    .then(response => {
      console.log(response);
      var endtime = new Date();
      var timediff = endtime - startTime;
      console.log("Time to set lastest Status " + timediff);
      miscutils.captureStat("API:attendance", "LatestStatus", timediff);
      return response;
    })
    .catch(error => {
      console.log(error.response);
      return error;
    });
  console.log(response);
  return response;
}

export function setCheckoutOverride(
  attendancedate,
  groupid,
  childobj,
  overrideuser,
  token
) {
  var startTime = new Date();
  let apiName = EnvConfigs.API_NAME;
  let path = "/attendance/attendanceoverride";

  var data = {
    headers: {
      Authorization: token
    },
    body: {
      childid: childobj.id,
      attendancedate: attendancedate,
      status: EnvConfigs.ATTENDANCESTATE.CHECKEDOUT,
      pickupuser: overrideuser
    },
    response: true
  };

  console.log(data);
  var response = API.post(apiName, path, data)
    .then(response => {
      console.log(response);
      var endtime = new Date();
      var timediff = endtime - startTime;
      miscutils.captureStat("API:attendance", "SetCheckoutOverride", timediff);
      console.log("Time to set Override Checkout " + timediff);
      return response;
    })
    .catch(error => {
      console.log(error.response);
      return error;
    });
  console.log(response);
  return response;
}

export function getChildrenforGroups(groups, token) {
  var startTime = new Date();
  let apiName = EnvConfigs.API_NAME;
  let path = "/attendance/students";

  var data = {
    headers: {
      Authorization: token
    },
    body: {
      groups: groups
    },
    response: true
  };

  console.log(data);
  var response = API.post(apiName, path, data)
    .then(response => {
      console.log(response);
      var endtime = new Date();
      var timediff = endtime - startTime;
      miscutils.captureStat("API:attendance", "GetChildrenforGroups", timediff);
      console.log("Time to get Children for Groups: " + timediff);
      return response;
    })
    .catch(error => {
      console.log(error.response);
      return error;
    });
  console.log(response);
  return response;
}
