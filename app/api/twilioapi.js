'use strict'
var EnvConfig = require('../config/environment')
//import { GoogleAnalyticsTracker } from "react-native-google-analytics-bridge";
import { Auth, API } from 'aws-amplify'


class TwilioClient {
    client;
    apitoken;
    twiliotoken;
    
    constructor() {
        this.client = null;
        
    }

    getTwilioToken(){
        console.log(this.twiliotoken)
        console.log(this.client)
        return this.twiliotoken
    }
    
    InitToken(apitoken) {
        console.log(apitoken)
        this.apitoken = apitoken
        console.log(apitoken)
        this.getToken(apitoken).then((response)=> {
            if (response.status === 200){
                let twiliotoken = response.data.token
                this.twiliotoken = twiliotoken
                console.log(this.twiliotoken)
            
           
            const Chat = require('twilio-chat');
            Chat.Client.create(twiliotoken).then(client => {
    
                this.client = client
                
                this.subscribeToAllChatClientEvents()
            }).catch(error => {
                console.error(error)
            });
        }
    })
       
    }

    async getToken(apitoken){
        console.log('IN UPDATE POST API !!!')
        let apiName = EnvConfig.API_NAME
        let path = '/twiliotoken'
        var data = {
            headers: {
                'Authorization' : apitoken
            },
            body : {
                
            },
            response: true
            
        }

        console.log(data)
        var response = await API.post(apiName, path, data).then(response => {
            console.log(response)
            return response
        }).catch(error => {
            console.log(error.response)
            return error
        })
        console.log(response)
        return response
    }

    subscribeToAllChatClientEvents() {
        console.log(this.twiliotoken)
        console.log(this.client)
        // this.client.on('tokenAboutToExpire',
        //     obj => console.log('ChatClientHelper.client', 'tokenAboutToExpire', obj));
        // this.client.on('tokenExpired', obj => console.log('ChatClientHelper.client', 'tokenExpired', obj));

        // this.client.on('userSubscribed', obj => console.log('ChatClientHelper.client', 'userSubscribed', obj));
        // this.client.on('userUpdated', obj => console.log('ChatClientHelper.client', 'userUpdated', obj));
        // this.client.on('userUnsubscribed', obj => console.log('ChatClientHelper.client', 'userUnsubscribed', obj));

        // this.client.on('channelAdded', obj => console.log('ChatClientHelper.client', 'channelAdded', obj));
        // this.client.on('channelRemoved', obj => console.log('ChatClientHelper.client', 'channelRemoved', obj));
        // this.client.on('channelInvited', obj => console.log('ChatClientHelper.client', 'channelInvited', obj));
        // this.client.on('channelJoined', obj => {
        //     console.log('ChatClientHelper.client', 'channelJoined', obj);
        //     obj.getMessages(1).then(messagesPaginator => {
        //         messagesPaginator.items.forEach(message => {
        //             this.log.info('ChatClientHelper.client', obj.sid + ' last message sid ' + message.sid)
        //         })
        //     })
        // });
        // this.client.on('channelLeft', obj => console.log('ChatClientHelper.client', 'channelLeft', obj));
        // this.client.on('channelUpdated', obj => console.log('ChatClientHelper.client', 'channelUpdated', obj));

        // this.client.on('memberJoined', obj => console.log('ChatClientHelper.client', 'memberJoined', obj));
        // this.client.on('memberLeft', obj => console.log('ChatClientHelper.client', 'memberLeft', obj));
        // this.client.on('memberUpdated', obj => console.log('ChatClientHelper.client', 'memberUpdated', obj));

        // this.client.on('messageAdded', obj => console.log('ChatClientHelper.client', 'messageAdded', obj));
        // this.client.on('messageUpdated', obj => console.log('ChatClientHelper.client', 'messageUpdated', obj));
        // this.client.on('messageRemoved', obj => console.log('ChatClientHelper.client', 'messageRemoved', obj));

        // this.client.on('typingStarted', obj => console.log('ChatClientHelper.client', 'typingStarted', obj));
        // this.client.on('typingEnded', obj => console.log('ChatClientHelper.client', 'typingEnded', obj));

        // this.client.on('connectionStateChanged',
        //     obj => console.log('ChatClientHelper.client', 'connectionStateChanged', obj));

        // this.client.on('pushNotification', obj => console.log('ChatClientHelper.client', 'onPushNotification', obj));
    }

}
//const TwilioClientInstance = new TwilioClient()
//Object.freeze(TwilioClientInstance);
export default new TwilioClient();