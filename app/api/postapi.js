'use strict'

import axios from 'axios'
var EnvConfig = require('../config/environment')
//import { GoogleAnalyticsTracker } from "react-native-google-analytics-bridge";
import { Auth, API } from 'aws-amplify'
import * as firebaseapi from './firebaseapi'
var bugsnag = require('../config/bugsnag')
import * as miscutils from '../utils/misc'


export function addPost ( childids,draft_mode, review_mode,reviwer_list,pinned_post,pinned_topic,pinned_until,scheduleDateTime,groupid, groupname, postText, postImages,postVideos, token) {
    var  startTime = new Date();
    let apiName = EnvConfig.API_NAME
    //let path = '/posts/add'
    let path = '/posts/add'
    //if posted to children then dont post to group
    // if (childids.length > 0){
    //     groupid = null
    //     groupname = ""
    // }
    console.log(token)
    console.log(postVideos)
    var data = {
        headers: {
            'Authorization' : token
        },
        body : {
            'groupids': groupid,
            'groupnames' : groupname,
            'post': postText,
            'img': postImages,
            'vid' : postVideos,
            'review_users' : reviwer_list,
            "childids": childids,
            "review_mode": review_mode,
            "pinned_post": pinned_post,
            "pinned_until": pinned_until,
            "posttime": scheduleDateTime,
            "pinned_topic" : pinned_topic,
            "draft_mode" : draft_mode
        },
        response: true
    }
    
    console.log(data)
    var response = API.post(apiName, path, data).then(response => {
        console.log(response)
        var  endtime = new Date();
        var timediff = endtime - startTime
        var postid = response.data
        firebaseapi.notifyPostinFirebase(groupid)
        console.log('Time to Post : ' + timediff)
        bugsnag.leaveBreadcrumb('TimeToPost:' + timediff, {type: 'postapi'});
        miscutils.captureStat('API:post','Post',timediff)
        return response
    }).catch(error => {
        console.log(console)
        console.log(error.response)
        bugsnag.notify(error)
        return error
    })
    console.log(response)
    return response
}


export function updatePost (activityid, childids,draft_mode, review_mode,reviwer_list,pinned_post,pinned_topic,pinned_until,scheduleDateTime,groupid, groupname, postText, postImages,postVideos, token) {
    console.log('IN UPDATE POST API !!!')
    var  startTime = new Date();
    let apiName = EnvConfig.API_NAME
    let path = '/posts/edit'
    //postImages = postImages === "" ? [] : postImages.split(',')
    // var images = []
    // for (var index in postImages){
    //     console.log(postImages[index])
    //     var imgkey = postImages[index].match(/public\/(.*)/g);
    //     console.log(imgkey[0])
    //     images.push(imgkey[0])
    // }
    // console.log(images)
    // console.log(postImages)
    console.log(token)
    console.log(postVideos)
    var data = {
        headers: {
            'Authorization' : token
        },
        body : {
            'postid': activityid,
            'post': postText,
            'groupids': groupid,
            'groupnames' : groupname,
            'img': postImages,
            'vid' : postVideos === "" ? [] : postVideos.split(',') ,
            'review_users' : reviwer_list,
            "childids": childids,
            "review_mode": review_mode,
            "pinned_post": pinned_post,
            "pinned_until": pinned_until,
            "posttime": scheduleDateTime,
            "pinned_topic" : pinned_topic,
            "draft_mode" : draft_mode
        },
        response: true
    }

    console.log(data)
    var response = API.post(apiName, path, data).then(response => {
        console.log(response)
        var  endtime = new Date();
        var timediff = endtime - startTime
        console.log('Time to Post Update : ' + timediff)
        firebaseapi.notifyPostinFirebase(groupid)
        bugsnag.leaveBreadcrumb('TimeToUpdPost:' + timediff, {type: 'postapi'});
        miscutils.captureStat('API:post','UpadatePost',timediff)
        return response
    }).catch(error => {
        console.log(error.response)
        bugsnag.notify(error)

        return error
    })
    console.log(response)
    return response
}



export function deletePost (activityid,groupid, token)  {
    console.log('IN UPDATE POST API !!!')
    var  startTime = new Date();
    let apiName = EnvConfig.API_NAME
    let path = '/posts/delete'
    var data = {
        headers: {
            'Authorization' : token
        },
        body : {
            'postid': activityid
        },
        response: true
    }

    console.log(data)
    var response = API.post(apiName, path, data).then(response => {
        console.log(response)
        var  endtime = new Date();
        firebaseapi.notifyPostinFirebase(groupid)
        var timediff = endtime - startTime
        console.log('Time to Post Delete : ' + timediff)
        miscutils.captureStat('API:post','DeletePost',timediff)
        return response
    }).catch(error => {
        console.log(error.response)
        return error
    })
    console.log(response)
    return response
}


export function addComments (activityid, comments, groupid, token) {
    var  startTime = new Date();
    let apiName = EnvConfig.API_NAME
    let path = '/posts/comments/add'
    var data = {
        headers: {
            'Authorization' : token
        },
        body : {
            'postid': activityid,
            'comment': comments        },
        response: true
    }
    console.log('Add Comment ???')
    console.log(data)
    var response = API.post(apiName, path, data).then(response => {
        console.log(response)
        var  endtime = new Date();
        var timediff = endtime - startTime
        firebaseapi.notifyPostinFirebase(groupid)

        console.log('Time to Post Comment: ' + timediff)
        miscutils.captureStat('API:post','AddComment',timediff)
        return response
    }).catch(error => {
        console.log(error.response)
        return error
    })
    console.log(response)
    return response
}


export function updateComment (commentid,comments,groupid, token) {
    var  startTime = new Date();
    let apiName = EnvConfig.API_NAME
    let path = '/posts/comments/edit'
    var data = {
        headers: {
            'Authorization' : token
        },
        body : {
            'commentid' : commentid,
            'comments': comments        },
        response: true
    }

    console.log(data)
    var response = API.post(apiName, path, data).then(response => {
        console.log(response)
        var  endtime = new Date();
        var timediff = endtime - startTime
        firebaseapi.notifyPostinFirebase(groupid)

        console.log('Time to Post Comment: ' + timediff)
        miscutils.captureStat('API:post','UpdateComment',timediff)
        return response
    }).catch(error => {
        console.log(error.response)
        return error
    })
    console.log(response)
    return response
}

export function deleteComment (commentid,groupid, token) {
    var  startTime = new Date();
    let apiName = EnvConfig.API_NAME
    let path = '/posts/comments/delete'
    var data = {
        headers: {
            'Authorization' : token
        },
        body : {
            'commentid' : commentid
            },
        response: true
    }

    console.log(data)
    var response = API.post(apiName, path, data).then(response => {
        console.log(response)
        var  endtime = new Date();
        var timediff = endtime - startTime
        firebaseapi.notifyPostinFirebase(groupid)

        console.log('Time to Post Comment: ' + timediff)
        miscutils.captureStat('API:post','DeleteComment',timediff)
        return response
    }).catch(error => {
        console.log(error.response)
        return error
    })
    console.log(response)
    return response
}
export function _wp_deleteComment (commentid, token) {
  console.log('IN DELETE COMMENT !!!')
  var headers = {
    headers: {
      'Authorization': 'Bearer ' + token
    }
  }

  var response = axios
    .post(EnvConfig.DELETE_COMMENT_URL, data, headers)
    .then((response) => {
        return response
    })
  return response
}

export function _wp_uploadGroupCoverImage(photourl,groupid, token){
    var headers = {
        headers: {
            'Authorization': 'Bearer ' + token
        }
    }
    var data = {
        'imageurl': photourl,
        'groupid': groupid,
    }
    var response = axios
        .post(EnvConfig.SET_GROUP_COVER_PHOTOS_URL, data, headers)
        .then((response) => {
            console.log(response)
            return response
        })
    return response
}

export function uploadGroupImage(photourl,groupid, type, token){
    var  startTime = new Date();
    let apiName = EnvConfig.API_NAME
    let path = '/group/updatephoto'
    var data = {
        headers: {
            'Authorization' : token
        },
        body : {
            'photourl' : photourl,
            'group' : groupid,
            'phototype' : type
        },
        response: true
    }

    console.log(data)
    var response = API.post(apiName, path, data).then(response => {
        console.log(response)
        var  endtime = new Date();
        var timediff = endtime - startTime
        console.log('Time to Update Group: ' + timediff)
        return response
    }).catch(error => {
        console.log(error.response)
        return error
    })
    console.log(response)
    return response
}

export function getReviewList(groupid, token){
    var  startTime = new Date();
    let apiName = EnvConfig.API_NAME
    let path = '/group/staff'
    var data = {
        headers: {
            'Authorization' : token
        },
        body : {
            'groupid': groupid,
            },
        response: true
    }
    console.log('')
    console.log(data)
    var response = API.post(apiName, path, data).then(response => {
        console.log(response)
        var  endtime = new Date();
        var timediff = endtime - startTime
        console.log('Time to GetReviewList for groupd id ' + groupid + ' : ' + timediff)
        miscutils.captureStat('API:post','GetReviewList(' + groupid + ')',timediff)
        return response
    }).catch(error => {
        console.log(error.response)
        return error
    })
    console.log(response)
    return response
}