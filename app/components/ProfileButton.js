import React from 'react';
import {TouchableOpacity, View, Text,Dimensions,  StyleSheet, Modal, Platform, findNodeHandle} from 'react-native';

import * as rootNavActions from '../actions/rootNavActions'
import Image from 'react-native-image-progress';
import {connect} from 'react-redux'
import Ionic from 'react-native-vector-icons/Ionicons';

import {Navigation} from 'react-native-navigation';
import {ImageCache, CustomCachedImage} from "react-native-img-cache";
import { platform } from 'os';
var closeIcon = null
class ProfileButton extends React.Component {

    constructor(props){
        super(props)
        this.showModalUser = this.showModal.bind(this)
        Ionic.getImageSource('md-close', 30).then((source) => {
            closeIcon = source
        });
    }

    render() {
        console.log('RENDER->PROFILE_BUTTON')
        var ImageWidget = () => {

            if (this.props.profile_photo === '' || this.props.profile_photo === undefined ||
                this.props.profile_photo === null || this.props.profile_photo.includes('gravatar.com')) {
                var profilePhotoURI = require('../img/blankprofile.png')
                console.log(profilePhotoURI)
                return (<Image defaultSource={require('../img/blankprofile.png')} source={profilePhotoURI} style={styles.profileImage} imageStyle={{
                    width: 36,
                    height: 36,
                    borderRadius: 18,
                    borderWidth : 0.2,
                    borderColor: 'yellow'
                }}/>)

            } else {
                var profilePhotoUri = {uri: this.props.profile_photo}
                return (<CustomCachedImage
                    component={Image}
                    defaultSource={require('../img/blankprofile.png')}
                    imageStyle={{
                        width: 36,
                        height: 36,
                        borderRadius: 18,
                        borderWidth : 0.2,
                    }}
                    source={profilePhotoUri}
                    style={styles.profileImage}/>)
            }

        }
        if (platform.os === 'ios'){
            return (
           
                <TouchableOpacity onPress={this.showModalUser} style={{marginLeft : 20, paddingBottom: 14}}>
                    {ImageWidget()}
                </TouchableOpacity>)
        }else{

        return (
           
            <TouchableOpacity onPress={this.showModalUser} style={{marginLeft : 20, paddingBottom: 14, marginRight : 10}}>
                {ImageWidget()}
            </TouchableOpacity>)
        }


    }
    componentDidMount(){

    }

    showModal(){
        console.log('Showing Modal User')
        Navigation.showModal({
            stack: {
                children: [{
                    component: {
                        name: 'nav.Three60Memos.MemberDetailsScreen',
                        passProps: {
                            selfView : true,
                            fromTopMenu : true
                        },
                        options : {
                            topBar: {
                                visible : true,
                                title: {
                                    text: "My Profile",
                                    color : "Black"
                                },
                                rightButtons:[
                                    {
                                        id : 'closeprofilebutton',
                                        icon : closeIcon,
                                        color : 'black'
                                    }
                                ]

                            }
                        }
                    }
                }]
            }
        });
    }
}

var mapStateToProps = function(store){
    return {profile_photo: store.user.profile_photo};
};

var mapDispatchToProps = function(dispatch){
    return {
        showProfileModalforUser: function (user, fromTopMenu) {
            dispatch(rootNavActions.showProfileModalforUser(user, fromTopMenu))
        },
    }
};

export default connect(mapStateToProps,mapDispatchToProps)(ProfileButton)

const styles = StyleSheet.create({
    profileImage : {
        alignItems: 'center',
        justifyContent: 'center',
        flexDirection: 'column',
        width: 36,
        height: 36,
        borderColor: 'yellow',
        marginTop: 5,
        borderRadius: 18,
        borderWidth: 0.2,
        paddingBottom: 5
    }

})