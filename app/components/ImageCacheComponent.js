
import React from 'react';

import {CustomCachedImage} from "react-native-img-cache";

import Image from 'react-native-image-progress';

export default class ImageCacheComponent extends React.Component{

    constructor (props)
    {
        super(props);
    }

    render(){
        return(
            <CustomCachedImage
                component={Image}
                defaultSource={require('../../img/placeholder.png')}
                imageStyle={{
                    width: 46,
                    height: 46,
                    borderRadius: 23,

                }}
                style={this.props.imageStyle}

                source={this.props.source}

                >

            </CustomCachedImage>
        )
    }

}

