
import React from 'react';
import {TouchableOpacity, View, Text,Dimensions,  StyleSheet, Modal, Image, Platform, findNodeHandle} from 'react-native';
import * as Layout from '../constants/Layout'
import { BlurView, VibrancyView } from 'react-native-blur'

export default class CustomBlurView extends React.Component{

    constructor (props)
    {
        super(props);
    }
    componentWillReceiveProps(nextProps) {

    }

    render(){
        if (Platform.OS === 'ios'){
            return (

                <BlurView style={[styles.iosview, this.props.style]} blurType="light"
                          blurAmount={this.props.blurAmount ? this.props.blurAmount : 20}>
                    {this.props.children}
                </BlurView>
            )

        }else {

            return (<View style={styles.androidView}>
                    {this.props.children}
                </View>
            )
        }
    }


}

const styles = StyleSheet.create({
    androidView : {
        position : 'absolute',
        top : 0, left : 0, right : 0, bottom : 0,
        height : Layout.height,
        width : Layout.width,
        backgroundColor : 'white',
        opacity : 0.9
    },
    iosview : {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center',
        padding: 10,
        position: "absolute",
        top: 0, left: 0, bottom: 0, right: 0,
    }

})