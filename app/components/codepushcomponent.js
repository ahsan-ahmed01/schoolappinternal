import React from 'react'
import codePush from 'react-native-code-push'
import * as Progress from 'react-native-progress'
var EnvConfig = require('../config/environment')
import { Platform, StatusBar, StyleSheet, View, Modal, Text } from 'react-native';

export class CodePushComponent extends React.Component {

    constructor(props){
        super(props)
        this.state = {
            showModal : false,
            statusText : "Installing Update ...",
            progress : 0
        }
    }

    componentDidMount() {
        console.log('KEY->' +  EnvConfig.AWS_CONFIG.CODEPUSH_KEY)
        codePush.sync({
            deploymentKey : EnvConfig.AWS_CONFIG.CODEPUSH_KEY
        })
    }

    render () {
        if (this.state.showModal){
            return (
                <Modal>
                    <View style={{justifyContent: 'center', alignItems : 'center'}}>
                        <Text style={{marginTop:100}}>{this.state.statusText}</Text>
                        <Progress.Bar progress={this.state.progress} width={200} style={{marginTop:10}}/>
                    </View>
                </Modal>
            )
        }
        else{
            return null
        }
    }

    codePushStatusDidChange(status) {
        console.log(status)
        switch(status) {
            case codePush.SyncStatus.CHECKING_FOR_UPDATE:
                console.log("Checking for updates.");
                break;
            case codePush.SyncStatus.DOWNLOADING_PACKAGE:
                this.setState({
                    showModal : true,
                    statusText : 'Downloading Update ...'
                })
                console.log("Downloading package.");
                break;
            case codePush.SyncStatus.INSTALLING_UPDATE:
                this.setState({
                    statusText : 'Installing Update ...'
                })
                console.log("Installing update.");
                break;
            case codePush.SyncStatus.UP_TO_DATE:
                console.log("Up-to-date.");
                break;
            case codePush.SyncStatus.UPDATE_INSTALLED:
                this.setState({
                    statusText : 'Installed Update ...',
                    progress : 1
                })
                console.log("Update installed.");

                break;
        }
    }

    codePushDownloadDidProgress(progress) {
        this.setState({
            progress : (progress.receivedBytes /progress.totalBytes)
        })
        console.log(progress.receivedBytes + " of " + progress.totalBytes + " received");
    }
}

let codePushOptions = {
    updateDialog: {
        updateTitle: "You have an update",
        optionalUpdateMessage: "You have an update available. Should we download it and install it ?",
        optionalIgnoreButtonLabel: "No",
        optionalInstallButtonLabel: "Yes (Recommended)",
    },
    deploymentKey : EnvConfig.AWS_CONFIG.CODEPUSH_KEY,
    installMode: codePush.InstallMode.IMMEDIATE,
    checkFrequency : codePush.CheckFrequency.ON_APP_RESUME

}

CodePushComponent = codePush(codePushOptions)(CodePushComponent)

module.exports = CodePushComponent