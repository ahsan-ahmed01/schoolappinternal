'use strict'
var EnvConfigs = require('../config/environment')

export default function reducer (state = {
  groupname: '',
  showBusySpinner: false,
  groupLoaded: false,
  errorLoadingGroupDetails: false,
  isPhotoLoaded: false,
    cover_photo : '',
    profile_photo : '',
    users: null
}, action) {
  switch (action.type) {
    case 'GET_GROUP_DETAILS':
      {
        console.log(action.payload)
        // var group = Object.assign(action.payload.selectedGroup)
        // group['cover_photo'] =  EnvConfigs.AWS_CONFIG.S3_PROFILE_URL + 'Group/' + action.payload.selectedGroup.id + '_coverphoto.jpg'
        // group['profile_photo'] =  EnvConfigs.AWS_CONFIG.S3_PROFILE_URL + 'Group/' + action.payload.selectedGroup.id + '_profilephoto.jpg'

          return {
          ...state,
          //showBusySpinner: true,
          groupname: action.payload.selectedGroup.id
         }
      }
    case 'GET_GROUP_DETAILS_FULFILLED':
      {
        console.log(action.payload)
        return {
          ...state,
          errorLoadingGroupDetails: false,
          showBusySpinner: false,
          groupLoaded: true,
          users: action.payload.Users

        }
      }
    case 'GET_GROUP_DETAILS_REJECTED':
      {
        console.log('Error Loading Group Details')
        console.log(action.payload)
        return {
          ...state,
          errorLoadingGroupDetails: true,
          showBusySpinner: false
        }
      }
    case 'GET_GROUP_PHOTOS':
      {
        console.log('Error Loading Group Details')
        console.log(action.payload)
        return {
          ...state,
          errorLoadingGroupDetails: false,
          showBusySpinner: false,
          isPhotoLoaded: false
        }
      }
    case 'GET_GROUP_PHOTOS_FULFILLED':
      {
        console.log('Error Loading Group Details')
        console.log(action.payload)
        return {
          ...state,
          errorLoadingGroupDetails: false,
          showBusySpinner: false,
          isPhotoLoaded: true,
          photos: action.payload
        }
      }
    case 'GET_GROUP_PHOTOS_REJECTED':
      {
        console.log('Error Loading Group Details')
        console.log(action.payload)
        return {
          ...state,
          errorLoadingGroupDetails: true,
          showBusySpinner: false
        }
      }

  }
  return state
}
