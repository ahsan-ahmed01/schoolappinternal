import {REHYDRATE} from 'redux-persist/constants'
import update from 'immutability-helper'

export default function reducer(state = {
    usercache: null,
    groupcache: null,
    cred: {
        userid: '',
        password: '',
        emailaddress: ''
    },
    showBusySpinner: false,
    isLoggedin: false,
    showLoginModal: false,
    loginOpen: true,
    error: '' ,
    registererror : '',
    resettingPassword: false,
    resetCodeSuccess: false,
    changePasswordSuccess: null,
    invalidEmailAddress: false,
    logincredvalidated: false,
    storeloaded: false,
    viewScreen: 'LoginScreen',
    licenseagereed: false,
    authObj: null,
    messageStatus: '',
    FirebaseConnected: false,
    AppReady: false
}, action) {
    switch (action.type) {

        case 'SOFT_INIT_APP': {
            return {
                ...state,
                AppReady: false
            }
        }

        case 'SET_USERID': {
            console.log('setting userid')
            return {
                ...state,
                cred: {
                    ...state.cred,
                    userid: action.payload.userid.trim().toLowerCase(),
                },
                AppReady: true,
                isLoggingin: false,
                error : ''
            }
        }
        case 'SET_PASSWORD': {
            return {
                ...state,
                cred: {
                    ...state.cred,
                    password: action.payload.password.trim()
                },
                AppReady: true,
                isLoggingin: false,
                error : ''
            }
        }

        case 'SET_APPREADY': {
           
            return {
                ...state,
                AppReady: action.payload.status
            }
        }

        case 'SET_EMAIL': {
            return {
                ...state,
                cred: {
                    ...state.cred,
                    emailaddress: action.payload.emailaddress.trim().toLowerCase()

                },
                invalidEmailAddress: false,
                resetCodeSuccess: false
            }
        }
        case 'AUTHENTICATE_USER': {
            console.log(action.payload)
            return {
                ...state,
                cred: {
                    ...state.cred,
                    userid: action.payload.user,
                    password: action.payload.password,
                    userobj: action.payload.userobj
                },
                AppReady: false,
                showBusySpinner: true,
                isLoggingin: true,
                isLoggedin: false,
                logincredvalidated: false
            }
        }

        case 'USER_LOGGING_IN': {
            return {
                ...state,
                isLoggingin: true
            }
        }

        case 'REDUX_STORAGE_LOAD': {
            return {...state, logincredvalidated: false}
        }

        case 'FIRE_BASE_DONE' : {
            return {
                ...state,
                FirebaseConnected: true
            }
        }

        case 'AUTHENTICATE_FIREBASE_ISSUE' : {
            console.log('Issue in Firebase')
            console.log(action.payload)
            return {
                ...state,
                cred: {
                    ...state.cred,
                    password: ''
                },
                showBusySpinner: false,
                isLoggedin: false,
                isLoggingin: false,
                logincredvalidated: false,
                error: 'ERROR CONNECTING TO CHAT. Your Password is correct. Please contact Tech Support'
            }

        }

        case 'AUTHENTICATE_USER_REJECTED': {
            console.log('Logging in Failed')
            console.log(action.payload)
            var error = action.payload.error !== "" ? action.payload.error : "Error Logging in. Please check your password and try again."
            return {
                ...state,
                cred: {
                    ...state.cred,
                    password: ''
                },
                showBusySpinner: false,
                isLoggedin: false,
                isLoggingin: false,
                logincredvalidated: false,
                error: error
            }
        }


        case 'AUTHENTICATE_USER_FULFILLED': {
            console.log(action.payload)
            return {
                ...state,
                authObj: action.payload,
                isLoggedin: true,
                isLoggingin: false,
                error: '',
                logincredvalidated: true
            }
        }
        case 'AUTHENTICATE_USER_RESET_ERROR': {
            console.log('Resetting Loging error')
            return {
                ...state,
                showBusySpinner: false,
                isLoggedin: false
            }
        }

        case 'LOGIN_RESET_PASSWORD': {
            return {
                ...state,
                resettingPassword: true,
                resetCodeSuccess: false,
                changePasswordSuccess: null,
                messageStatus: ''
            }
        }

        case 'LOGIN_RESET_PASSWORD_FULFILLED': {
            return {
                ...state,
                resettingPassword: false,
                invalidEmailAddress: false,
                resetCodeSuccess: true,
                messageStatus: ''
            }
        }

        case 'LOGIN_RESET_PASSWORD_REJECTED' : {
            return {
                ...state,
                resettingPassword: false,
                invalidEmailAddress: true,
                resetCodeSuccess: false,
                messageStatus: action.payload.err.message
            }
        }

        case 'LOGIN_CHANGE_PASSWORD' : {
            return {
                ...state,
                resettingPassword: true,
                changePasswordSuccess: null
            }
        }

        case 'LOGIN_CHANGE_PASSWORD_FULFILLED': {
            return {
                ...state,
                cred: {
                    password: action.payload.password,
                    userid: action.payload.email
                },
                resettingPassword: false,
                changePasswordSuccess: 'SUCCESS',
                viewScreen: 'LoginScreen',
                AppReady: true,
                isLoggingin: false
            }
        }

        case 'LOGIN_CHANGE_PASSWORD_REJECTED' : {
            return {
                ...state,
                resettingPassword: false,
                changePasswordSuccess: 'ERROR'
            }
        }

        case 'CHANGE_VIEW_SCREEN': {
            return {
                ...state,
                viewScreen: action.payload.screenName
            }
        }

        case 'SHOW_LOGIN_MODAL': {
            return {
                ...state,
                showLoginModal: action.payload.modalStatus
            }
        }

        case 'SELECT_AGREEMENT': {
            return {
                ...state,
                licenseagereed: action.payload.useraction
            }
        }

        case 'USER_CACHE_LOAD': {
            return {
                ...state,

            }
        }

        case 'USER_CACHE_LOAD_REJECTED': {
            return {
                ...state,
                messageStatus: action.payload.error,
                showBusySpinner: false,
                isLoggedin: false,
                isLoggingin: false,
                logincredvalidated: false,
                error: 'Error Downloading MetaData. Please contact support !'
            }

        }

        case 'LOGIN_GENERIC_ERROR': {
            return {
                ...state,
                messageStatus: action.payload.error,
                showBusySpinner: false,
                isLoggedin: false,
                isLoggingin: false,
                logincredvalidated: false,
                error: action.payload.error
            }

        }

        case 'USER_CACHE_LOAD_FULFILLED': {
            return {
                ...state,
                usercache: action.payload.usercache.data.users,
                groupcache: action.payload.usercache.data.groups
            }
        }

        case 'UPDATE_PROFILE_PICTURE_CACHE': {
            var foo = update(state.usercache, {[action.payload.user]: {profile_photo: {$set: action.payload.picture}}})
            return {
                ...state,
                usercache: foo
            }
        }

        case REHYDRATE: {
            if (action.payload.login === undefined) {
                return {
                    ...state,
                    storeloaded: true
                }
            }
            else {

                return {
                    ...state,
                    cred: action.payload.login.cred,
                    usercache: action.payload.login.usercache,
                    groupcache: action.payload.login.groupcache,
                    storeloaded: true
                }
            }
        }

        case "REGISTER_USER_SUCCESS" : {
            return  {
                ...state,
                cred: {
                    password: action.payload.password,
                    userid: action.payload.email
                },
                resettingPassword: false,
                changePasswordSuccess: 'SUCCESS',
                viewScreen: 'LoginScreen',
                AppReady: true,
                isLoggingin: false
            }
        }

        case "REGISTER_USER_REJECTED" : {
            return  {
                ...state,
                resettingPassword: false,
                changePasswordSuccess: 'ERROR',
                registererror : action.payload.code
            }
        }

    }
    return state
}
