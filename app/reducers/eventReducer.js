"use strict";

import _ from "lodash";
import update from "immutability-helper";
import * as miscutils from "../utils/misc";

export default function reducer(
  state = {
    error: null,
    actionError: false,
    errorText: "",
    iscreatingEvent: false,
    isLoading: false,
    events: {},
    hashmap: {},
    eventnotifications: 0,
    todayYlocation: 0,
    calendarconfig: null,
    lasteventgrp: null,
    lastrefreshtime: 0,
    user: null,
  },
  action
) {
  switch (action.type) {
    case "SET_EVENTS_USER": {
      return {
        ...state,
        user: action.payload.user,
      };
    }

    case "ADD_EVENT": {
      if (action.payload.formData.grpSelect !== undefined) {
        var grpid = action.payload.formData.grpSelect;
      } else {
        var grpid = state.lasteventgrp;
      }
      return {
        ...state,
        actionError: false,
        error: null,
        iscreatingEvent: true,
        lasteventgrp: grpid,
      };
    }

    case "ADD_EVENT_REJECTED": {
      return {
        ...state,
        actionError: true,
        errorText: "Error Adding Event",
        iscreatingEvent: false,
        isLoading: false,
      };
    }

    case "CLEAR_LAST_REFRESHTIME": {
      return {
        ...state,
        lastrefreshtime: 0,
      };
    }

    case "ADD_EVENT_FULFILLED": {
      return {
        ...state,
        iscreatingEvent: false,
        actionError: false,
        isLoading: false,
      };
    }

    case "GET_EVENTS_FULFILLED_OLD": {
      console.log(action.payload);
      var responses = action.payload.useresponses;
      var events = action.payload.events.events;
      var user = state.user;
      var eventnotifications = 0;
      if (action.payload.useresponses === undefined) {
        action.payload.useresponses = [];
      }
      action.payload.useresponses.forEach((item) => {
        console.log(item);
        console.log(events[item.event_id]);
        if (events[item.event_id] !== undefined) {
          if (events[item.event_id] !== undefined) {
            events[item.event_id]["userresponse"] = item;
            if (item.event_type == 2) {
              console.log(item);
              if (
                item.user_response !== undefined &&
                item.user_response !== null &&
                item.user_response.user === user
              ) {
                events[item.event_id]["pendingflag"] = 2;
                events[item.event_id]["attend"] = item.user_response.attend;
              } else {
                eventnotifications = eventnotifications + 1;
                events[item.event_id]["pendingflag"] = 1;
              }
            }
            if (item.event_type == 3) {
              console.log(item);
              var data = {};
              item.user_response.forEach((item) => {
                data[item.slotId] = item;
              });
              events[item.event_id]["userresponse"]["slots"] = data;
            }
          }
          console.log(events[item.event_id]);
        }
      });

      return {
        ...state,
        events, // Caused Issue with Delete EVent
        hashmap: action.payload.events.hashmap, // Caused Issue with Delete EVent
        eventnotifications,
        isLoading: false,
        actionError: false,
        errorText: "",
      };
    }

    case "GET_EVENTS_FULFILLED": {
      console.log(action.payload, "GET_EVENTS_FULFILLED");
      let _events = action.payload.events;
      console.log(action.payload.events, "ahsan---->");
      console.log(_events, "_events");
      let events = {};
      let hashmap = {};

      _events.forEach((item) => {
        let _tmp = {};
        _tmp["event_name"] = item["eventname"];
        _tmp["alldayevent"] = item["alldayevent"];
        _tmp["event_end_date"] = item["end_date"];
        _tmp["event_end_time"] = item["end_time"];
        _tmp["event_id"] = item["event_id"];
        _tmp["event_start_date"] = item["start_date"];
        //******* PROCESS START DATE ***********/
        var dates = [];
        if (item["start_date"] == "0000-00-00") {
          //if type 3 or 4 get event type from slots
          if (item["event_type"] == 3) {
            dates = item["parentTeacherResponse"].map((item) => {
              return item.event_date === ""
                ? null
                : new Date(item.event_date).toISOString().split("T")[0];
            });
            _tmp["event_start_date"] = dates[0];
          }
          console.log("here");
          if (item["event_type"] == 4) {
            console.log(item, "GET_EVENTS_FULFILLED->");
            if (item.volunteerDetail && item.volunteerDetail.volunteer) {
              dates = item.volunteerDetail.volunteer.map((item) => {
                return item.event_date === ""
                  ? null
                  : new Date(item.event_date).toISOString().split("T")[0];
              });
              _tmp["event_start_date"] = dates[0];
            }
          }
          console.log(item);
          console.log(dates);
        } else {
          var dt = new Date(item["start_date"]);
          dates.push(dt.toISOString().split("T")[0]);
        }
        dates.forEach((dtstr) => {
          if (typeof hashmap[dtstr] === "object") {
            hashmap[dtstr].push(item["event_id"]);
          } else {
            hashmap[dtstr] = [item["event_id"]];
          }
        });

        _tmp["event_start_time"] = item["start_time"];
        _tmp["eventcolor"] = item["event_color"];
        _tmp["eventimgurl"] = item["img_url"];
        _tmp["eventtype"] = item["event_type"];
        _tmp["group_id"] = item["grpid"];
        _tmp["grpid"] = item["grpid"];
        _tmp["location"] = item["location"];
        _tmp["notifyusers"] = item["notifyusers"];
        _tmp["parentTeacherResponse"] = item["parentTeacherResponse"];
        _tmp["volunteerDetail"] = item["volunteerDetail"];
        _tmp["post_content"] = item["eventdesc"];
        _tmp["remindsameday"] = item["notifyOneDayAgo"];
        _tmp["remindoneday"] = item["notifyTwoDayAgo"];
        _tmp["remindtwoday"] = item["notifyThreeDayAgo"];
        _tmp["slot_dates"] = item["slot_dates"];
        _tmp["slots"] = item["slots"];
        _tmp["users"] = item["users"] === null ? [] : item["users"].split(",");
        _tmp["time_slot"] = item["time_slot"];

        events[item["event_id"]] = _tmp;
      });
      console.log(events, "events");
      let responses = action.payload.useresponses;
      //var events = action.payload.events.events
      console.log(responses, "responses");
      var user = state.user;
      var eventnotifications = 0;
      if (action.payload.useresponses === undefined) {
        action.payload.useresponses = [];
      }
      responses.forEach((item) => {
        console.log(item);
        if (events[item.event_id] !== undefined) {
          events[item.event_id]["userresponse"] = item;
          if (item.event_type == 2) {
            if (
              item.user_response !== undefined &&
              item.user_response !== null &&
              item.user_response.user === user
            ) {
              events[item.event_id]["pendingflag"] = 2;
              events[item.event_id]["attend"] = item.user_response.attend;
            } else {
              eventnotifications = eventnotifications + 1;
              events[item.event_id]["pendingflag"] = 1;
            }
          }
          if (item.event_type == 3) {
            console.log(item.user_response);
            var data = {};
            if (
              item.user_response !== undefined &&
              item.user_response !== null &&
              item.user_response.user === user
            ) {
              item.user_response.forEach((item) => {
                data[item.slotId] = item;
              });
              events[item.event_id]["userresponse"]["slots"] = data;
            }
          }
        }
        console.log(events[item.event_id]);
      });

      return {
        ...state,
        events, // Caused Issue with Delete EVent
        hashmap: hashmap, // Caused Issue with Delete EVent
        eventnotifications,
        isLoading: false,
        actionError: false,
        errorText: "",
        lastrefreshtime: new Date(),
      };
    }

    case "GET_EVENTS_REJECTED": {
      return {
        ...state,
        isLoading: false,
      };
    }

    case "GET_EVENTS": {
      return {
        ...state,
        isLoading: true,
      };
    }

    case "CLEAR_EVENTS": {
      return {
        ...state,
        events: {},
        hashmap: {},
        isLoading: true,
        eventnotifications: 0,
      };
    }

    case "SET_TODAY_LOCATION": {
      return {
        ...state,
        todayYlocation: action.payload.ylocation,
      };
    }

    case "SET_CALENDAR_CONFIG": {
      return {
        ...state,
        calendarconfig: action.payload,
      };
    }

    case "DELETE_EVENT": {
      return {
        ...state,
        isLoading: true,
      };
    }

    case "DELETE_EVENT_FULFILLED": {
      var eventid = action.payload.eventid;
      var events = _.omit(state.events, eventid);
      var keys = Object.keys(state.hashmap);
      for (var i = 0; i < keys.length; i++) {
        var eventsindex = state.hashmap[keys[i]];
        var idx = eventsindex.indexOf(eventid);
        if (idx != -1) {
          eventsindex.splice(idx, 1);
        }
        state.hashmap[keys[i]] = Object.assign(eventsindex);
      }
      return {
        ...state,
        events: events,
        actionError: false,
        errorText: "",
        isLoading: false,
      };
    }
    case "DELETE_EVENT_REJECTED": {
      return {
        ...state,
        actionError: true,
        errorText: "Error Deleting Event",
        isLoading: false,
      };
    }
  }
  return state;
}
