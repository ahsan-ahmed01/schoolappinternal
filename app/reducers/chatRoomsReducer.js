'use strict'

import _ from 'lodash'
import update from 'immutability-helper'
import {GiftedChat} from 'react-native-gifted-chat'
import * as miscutils from '../utils/misc'

export default function reducer(state = {
    rooms: {},
    isLoading: true,
    error: {},
    liveRoom: null,
    isloadingMessages: false
}, action) {
    switch (action.type) {

        case 'LOAD_MESSAGE_ROOMS2' : {
            return {
                ...state,
                rooms : action.payload.roomData,
                isLoading : false
            }
        }

        case 'LOAD_MESSAGE_ROOMS': {
            var roomkey = Object.keys(action.payload.rooms)[0]
            console.log(roomkey)
            var val = action.payload.rooms[roomkey]
            console.log(val)
            if ((state.rooms[roomkey] !== undefined) && (val.Details.lastmsgtimestamp == state.rooms[roomkey].Details.lastmsgtimestamp)){
                console.log('No New Messages found as timestamp is same')
                return {
                    ...state,
                }
            }
            var newrooms = update(state.rooms, {
                [roomkey]: {$set: val}
            })
            console.log(newrooms[roomkey])
            console.log(state.rooms[roomkey])
            //Copy Messages
            if (state.rooms[roomkey]!== undefined && state.rooms[roomkey]['Messages'] !== undefined) {
                newrooms[roomkey]['Messages'] = state.rooms[roomkey]['Messages'];
            }
            console.log('assigned ....')
            return {
                ...state,
                rooms: newrooms,
                isLoading: false,
                isloadingMessages: false
            }
        }

        case 'LOADING_MESSAGE_GROUPS': {
            if (Object.keys(state.rooms).length > 0) {
                return {
                    ...state,
                    isLoading: false
                }
            } else {
                return {
                    ...state,
                    isLoading: true
                }
            }
        }


        case 'STOP_LOADING_GROUPS': {
            return {
                ...state,
                isLoading: false
            }
        }

        case 'LOAD_CHAT_MESSAGES': {
            return {
                ...state,
                messages: action.payload.messages
            }
        }

        case 'ADD_CHAT_MESSAGE': {
            var key = action.payload.roomkey
            var msgs = action.payload.msgs
            console.log(msgs)
            console.log(state.rooms[key])
            if (state.rooms[key] !== undefined && state.rooms[key].Messages === undefined) {
                var newrooms = update(state.rooms, {
                    [key]: {'Messages': {$set: []}}
                })
            } else {
                var newrooms = Object.assign(state.rooms)
                if (state.rooms[key] !== undefined && msgs._id === state.rooms[key].Messages[0]._id) {
                    return {
                        ...state
                    }
                }
            }
            if (state.rooms[key] !== undefined ) {
                var newrooms2 = update(newrooms, {
                    [key]: {'Messages': {$unshift: [msgs]}}
                })
                console.log(newrooms2)
            }

            return {
                ...state,
                rooms: newrooms2,
                liveRoom: key
            }
        }

        case 'LOAD_PREVIOUS_CHAT_MESSAGE': {
            return {
                ...state
            }
        }

        case 'LOAD_ALL_MSGS': {
            var key = action.payload.roomkey
            var msgs = action.payload.msgs
            console.log(msgs)
            console.log(state.rooms)
            var newMessages = update(state.rooms, {
                [key]: {'Messages': {$set: []}}
            })
            newMessages = update(newMessages, {
                [key]: {'Messages': {$push: msgs}}
            })
            return {
                ...state,
                rooms: newMessages
            }
        }

        case 'CHATROOM_STOP_LISTENER': {
            return {
                ...state,
                liveRoom: null
            }
        }

        case 'CHATROOM_START_LISTENER': {
            return {
                ...state,
                liveRoom: action.payload.roomkey
            }
        }
        case 'CLEAR_ROOMS': {
            return {
                ...state,
                isLoading : true,
                //rooms: {}
            }
        }

        case 'LOAD_MORE_MESSAGES': {
            return {
                ...state,
                isloadingMessages: true
            }
        }

        case 'ADD_PREVIOUS_MESSAGES': {
            var key = action.payload.roomkey
            var msgs = action.payload.msgs
            msgs
            var newrooms = Object.assign(state.rooms)
            console.log(newrooms[key].Messages)
            newrooms[key].Messages = GiftedChat.append(msgs, newrooms[key].Messages)
            console.log(newrooms[key].Messages)
            return {
                ...state,
                rooms: newrooms,
                isloadingMessages: false
            }
        }

        case 'USER_LEFT': {
            var newrooms = state.rooms;
            delete newrooms[action.roomkey];
            return {
                ...state,
                rooms: newrooms
            }
        }
            ;

        case 'MEMBER_INVITED': {
            var actualRoom = state.rooms[action.roomkey];
            actualRoom.Details.users = {
                ...state.rooms[action.roomkey].Details.users,
                ...action.users
            };

            return {
                ...state
            };
        }
            ;

        case 'MEMBER_ADDED': {
            var actualRoom = state.rooms[action.roomkey];
            if (!actualRoom) {
                return {
                    ...state
                };
            }

            actualRoom.Details.users = {
                ...state.rooms[action.roomkey].Details.users,
                [action.userid]: action.user
            };

            return {
                ...state
            };
        }

        case 'MEMBER_REMOVED': {
            var actualRoomUsers = state.rooms[action.roomkey].Details.users;
            delete actualRoomUsers[action.userid];

            return {
                ...state
            };
        }

        case 'CHAT_LAST_MESSAGE_TS_UPDATED': {
            if (state.rooms[action.roomkey]) {
                var updatedRoom = state.rooms[action.roomkey];
                updatedRoom['Details']['lastmsgtimestamp'] = action.lastmsgtimestamp;
            }

            return {
                ...state
            };
        }
            ;



        case 'DELETE_INVALID_CHATS': {
            for (var roomKey in state.rooms){
                if (state.rooms[roomKey].visibleRoom === false)
                {
                    delete state.rooms[roomKey];
                }
            }

            return {
                ...state
            };
        }
        // case 'PICK_PHOTO_FINISED': {
        //   var { message, roomkey } = action;
        //   var currentRoom = state.rooms[roomkey];
        //   currentRoom.Messages = GiftedChat.append(state.rooms[roomkey].Messages, message);

        //   return {
        //     ...state,
        //     rooms: { ...state.rooms }
        //   };
        // };

    }
    return state
}

