'use strict'

import _ from 'lodash'
import {Dimensions} from 'react-native'

export default function reducer (state = {
    newPostModal : false,
    profileModal : false,
    showOwnProfile : false,
    user : null,
    noNetworkModal : false,
    currentTab : 'nav.Three60Memos.HomeScreen',
    prevTab : null,
    disableApp : false,
    disableText : '',
    fromTopMenu : false,
    feedbackModal : false,
    deviceWidth :  Dimensions.get('window').width,
    attendanceModal : false,
    custommenuModal : false,
    resumeTime : null
}, action) {
    switch (action.type) {
        case 'SHOW_NEW_POST': {
            return {
                ...state,
                newPostModal : true,

            }
        }

        case 'HIDE_NEW_POST': {
            return {
                ...state,
                newPostModal : false
            }
        }
        case 'SHOW_PROFILE': {
            return {
                ...state,
                profileModal : true,
                showOwnProfile : true,
                user : null
            }
        }

        case 'HIDE_PROFILE': {
            return {
                ...state,
                profileModal : false,
                showOwnProfile : false,
                fromTopMenu : false
            }
        }

        case 'SHOW_PROFILE_USER': {
            return {
                ...state,
                profileModal : true,
                showOwnProfile : false,
                user : action.payload.user,
                fromTopMenu : action.payload.fromTopMenu
            }
        }

        case 'LOSS_OF_NETWORK': {
            return {
                ...state,
                noNetworkModal: true
            }
        }

        case 'RESUME_NETWORK': {
            return {
                ...state,
                noNetworkModal: false,
                resumeTime : new Date()
            }
        }

        case 'DISABLE_APP': {
            return {
                ...state,
                disableApp: true,
                disableText : action.payload.disableText
            }
        }

        case 'ENABLE_APP': {
            return {
                ...state,
                disableApp: false,
                disableText : ''
            }
        }

        case 'SET_CURRENT_TAB':{
            return {
                ...state,
                currentTab: action.payload.tabname,
                prevTab : action.payload.prevtab
            }
        }

        case 'DEVICE_LAYOUT_CHANGED': {
            return {
                ...state,
                deviceWidth : action.payload.event.width
            }

        }

        case 'SHOW_FEEDBACK_MODAL':{
            return {
                ...state,
                feedbackModal : true
            }
        }

        case 'HIDE_FEEDBACK_MODAL':{
            return {
                ...state,
                feedbackModal : false
            }
        }

        case 'SHOW_CUSTOMMENU_MODAL':{
            return {
                ...state,
                custommenuModal : true
            }
        }

        case 'HIDE_CUSTOMMENU_MODAL':{
            return {
                ...state,
                custommenuModal : false
            }
        }

        case 'SHOW_ATTENDANCE_MODAL':{
            return {
                ...state,
                attendanceModal : true
            }
        }

        case 'HIDE_ATTENDANCE_MODAL':{
            return {
                ...state,
                attendanceModal : false
            }
        }
    }
    return state
}