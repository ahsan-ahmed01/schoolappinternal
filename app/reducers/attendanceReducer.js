'use strict';
var EnvConfigs = require('../config/environment');
import update from 'immutability-helper'

Object.extend = function (destination, source) {
    for (var property in source) {
        if (source.hasOwnProperty(property)) {
            destination[property] = source[property]
        }
    }
    return destination
};

export default function reducer(state = {
    groups: {
    },
    attendance: {},
    validatingPin: false,
    pinStatus: 0,
    authorizedUsers : {},
    loading_authorizedUsers : false,
    loading_groupChildren : false,
    attendanceerror : false

}, action) {
    switch (action.type) {

        case 'ATTENDANCE_VALIDATE_PIN_REJECTED': {
            return {
                ...state,
                validatingPin: false,
                pinStatus: -1
            }
        }

        case 'INIT_ATTENDANCE': {
            var newstate = {};
            if (!(action.payload.attendancedate in state.attendance)) {
                //console.log('Initializing Date');
                newstate = update(state.attendance, {
                    [action.payload.attendancedate]: {$set: {}}
                })
            } else {
                newstate = Object.assign(state.attendance)
            }
            ////console.log(newstate);
            for (var userid in state.groups[action.payload.groupid]) {
                var userobj = state.groups[action.payload.groupid][userid];
                userobj['status'] = EnvConfigs.ATTENDANCESTATE.UNMARKED;
                userobj['checkInStatus'] = false;
                userobj['checkOutStatus'] = false;
                if (newstate[action.payload.attendancedate][userid] === undefined) {
                    newstate = update(newstate, {
                        [action.payload.attendancedate]: {
                            [userid]: {
                                $set: userobj
                            }
                        }
                    })
                }
                //console.log(newstate)
            }
            //console.log(newstate);


            return {
                ...state,
                attendance: newstate,
                attendanceerror : false
            }
        }

        case 'ATTENDANCE_VALIDATE_PIN': {
            return {
                ...state,
                validatingPin: true,
                pinStatus: 0,
                attendanceerror : false


            }
        }
        case 'ATTENDANCE_SET_STATUS_OVERRIDE' : {
            return {
                ...state,
                validatingPin: true,
                attendanceerror : false



            }
        }

        case 'ATTENDANCE_VALIDATE_PIN_REJECTED': {
            return {
                ...state,
                validatingPin: false,
                pinStatus: -1,
                attendanceerror : false
            }
        }

        case 'ATTENDANCE_VALIDATE_PIN_FULFILLED': {
            let newstate = null
            if (action.payload.checkinMode) {
                // ******* CHECK IN *****************
                // //console.log(action.payload.attendancedate);
                // var childobj = Object.assign(action.payload.childobj);
                // childobj['status'] = EnvConfigs.ATTENDANCESTATE.CHECKEDIN;
                // childobj['checkInUser'] = action.payload.username
                // childobj['checkInTime'] = getTime();
                // childobj['checkInStatus'] = true
                //
                // //console.log(childobj);
                //  newstate = update(state.attendance, {
                //     [action.payload.attendancedate]: {
                //         [action.payload.childobj.id]: {
                //             $set: childobj
                //         }
                //     }
                // });
                newstate = update(state.attendance, {
                    [action.payload.attendancedate]: {
                        [action.payload.childobj.id]: {
                            ['checkInTime']: {
                                $set: getTime()
                            },
                            ['checkInUser']: {
                                $set: action.payload.username
                            },
                            ['checkInStatus']: {
                                $set : true
                            },
                            ['status'] : {
                                $set : EnvConfigs.ATTENDANCESTATE.CHECKEDIN
                            }

                        }
                    }
                });
            }else{
                // ******* CHECK OUT *****************
                newstate = update(state.attendance, {
                    [action.payload.attendancedate]: {
                        [action.payload.childobj.id]: {
                            ['checkOutTime']: {
                                $set: getTime()
                            },
                            ['checkOutUser']: {
                                $set: action.payload.username
                            },
                            ['checkOutStatus']: {
                                $set : true
                            },
                            ['status'] : {
                                $set : EnvConfigs.ATTENDANCESTATE.CHECKEDOUT
                            }

            }
                    }
                });
            }
            return {

                ...state,
                validatingPin: false,
                pinStatus : 1,
                attendance: newstate,
                attendanceerror : false
            }
        }

        case 'ATTENDANCE_VALIDATE_PIN_CLEAR': {
            return {
                ...state,
                validatingPin: false,
                pinStatus: 0,
                attendanceerror : false
            }
        }

        case 'ATTENDANCE_SET_STATUS_OVERRIDE_FULFILLED' : {
            //console.log('fulfilled !!!')
            const newstate = update(state.attendance, {
                    [action.payload.attendancedate]: {
                        [action.payload.childobj.id]: {
                            ['status']: {
                                $set: action.payload.status
                            },
                            ['checkInTime']: {
                                $set: getTime()
                            },
                            ['checkInUser']: {
                                $set: action.payload.username
                            },
                            ['checkInStatus']: {
                                $set : true
                            },
                        }
                    }
                });
            //console.log(newstate)
            return {
                ...state,
                validatingPin: false,
                attendance: newstate,
                attendanceerror : false
            }
        }

        case 'ATTENDANCE_AUTHORIZED_USERS': {
            //console.log(state)
            let newstate = {}
            newstate[action.payload.childobj.id] = {}

            //console.log(newstate)
            return {
                ...state,
                authorizedUsers: newstate,
                loading_authorizedUsers : true
            }
        }

        case 'ATTENDANCE_SET_STATUS_OVERRIDE_REJECTED': {
            return {
                ...state,
                attendanceerror : true
            }
        }

        case 'ATTENDANCE_AUTHORIZED_USERS_FULFILLED': {
            const newstate = update(state.authorizedUsers, {
                [action.payload.childobj.id]: {
                        $set :action.payload.authorizedUsers
                }
            });
            return {
                ...state,
                authorizedUsers: newstate,
                loading_authorizedUsers : false
            }
        }

        case 'ATTENDANCE_AUTHORIZED_USERS_REJECTED': {
            const newstate = update(state.authorizedUsers, {
                [action.payload.childobj.id]: {
                    $set :[]
                }
            });
            return {
                ...state,
                authorizedUsers: newstate,
                loading_authorizedUsers : false
            }
        }

        case 'ATTENDANCE_CHECKOUT_OVERRIDE_FULFILLED' : {
            newstate = update(state.attendance, {
                [action.payload.attendancedate]: {
                    [action.payload.childobj.id]: {
                        ['checkOutTime']: {
                            $set: getTime()
                        },
                        ['checkOutUser']: {
                            $set: action.payload.overrideuser
                        },
                        ['checkOutStatus']: {
                            $set : true
                        },
                        ['teacherOverride']: {
                            $set : 1
                        }
                        ,
                        ['status']: {
                            $set : EnvConfigs.ATTENDANCESTATE.CHECKEDOUT
                        }
                    }
                }
            });
            return {
                ...state,
                attendance: newstate
            }
        }

        case 'ATTENDANCE_CHECKOUT_OVERRIDE_REJECTED' : {
            return {
                ...state,
                attendanceerror : true
            }
        }

        case 'ATTENDANCE_INIT_LIST':{
            return {
                ...state,
                groups : {},
                loading_groupChildren : true,
                attendancerefeshing : true
            }
        }

        case 'ATTENDANCE_INIT_LIST_FULFILLED':{
            var newstate = {};
            if (!(action.payload.attendancedate in state.attendance)) {
                //console.log('Initializing Date');
                newstate = update(state.attendance, {
                    [action.payload.attendancedate]: {$set: {}}
                })
            } else {
                newstate = Object.assign(state.attendance)
            }
            //console.log(newstate);
            //console.log(action.payload.groups)
            for (var groupid in action.payload.groups) {
                //console.log(groupid)
                for (var userid in action.payload.groups[groupid]) {
                    //console.log(userid)
                    var userobj = action.payload.groups[groupid][userid];
                    userobj['status'] = EnvConfigs.ATTENDANCESTATE.UNMARKED;
                    userobj['checkInStatus'] = false;
                    userobj['checkOutStatus'] = false;
                    if (newstate[action.payload.attendancedate][userid] === undefined) {
                        newstate = update(newstate, {
                            [action.payload.attendancedate]: {
                                [userid]: {
                                    $set: userobj
                                }
                            }
                        })
                    }
                    //console.log(newstate)
                }
            }
            //console.log(newstate);


            return {
                ...state,
                groups : action.payload.groups,
                loading_groupChildren : false,
                attendance : newstate
            }
        }

        case 'ATTENDANCE_INIT_LIST_REJECTED':{

            return {
                ...state,
                groups : {},
                loading_groupChildren : false

            }
        }

        case 'ATTENDANCE_STATUS_REFRESH': {
            return {
                ...state,
                attendancerefeshing : true
            }
        }

        case 'ATTENDANCE_STATUS_REFRESH_REJECTED': {
            return {
                ...state,
                attendance : newstate,
                attendancerefeshing : false
            }
        }

        case 'ATTENDANCE_STATUS_REFRESH_FULFILLED': {
            var newstate = Object.assign(state.attendance)
            //console.log(action.payload)
            for (var userid in action.payload.statuses){
                //console.log(userid)
                if (EnvConfigs.ATTENDANCESTATE.CHECKEDIN in action.payload.statuses[userid]) {
                    if (EnvConfigs.ATTENDANCESTATE.CHECKEDOUT in action.payload.statuses[userid]){
                        newstate[action.payload.attendancedate][userid].checkOutStatus = true
                        newstate[action.payload.attendancedate][userid].checkOutUser = action.payload.statuses[userid][EnvConfigs.ATTENDANCESTATE.CHECKEDOUT]['user']
                        newstate[action.payload.attendancedate][userid].checkOutTime = action.payload.statuses[userid][EnvConfigs.ATTENDANCESTATE.CHECKEDOUT]['timestamp']
                        newstate[action.payload.attendancedate][userid].status = EnvConfigs.ATTENDANCESTATE.CHECKEDOUT

                    }else{
                        newstate[action.payload.attendancedate][userid].checkOutStatus = false
                        newstate[action.payload.attendancedate][userid].status = EnvConfigs.ATTENDANCESTATE.CHECKEDIN
                    }
                    newstate[action.payload.attendancedate][userid].checkInStatus = true
                    newstate[action.payload.attendancedate][userid].checkInUser = action.payload.statuses[userid][EnvConfigs.ATTENDANCESTATE.CHECKEDIN]['user']
                    newstate[action.payload.attendancedate][userid].checkInTime = action.payload.statuses[userid][EnvConfigs.ATTENDANCESTATE.CHECKEDIN]['timestamp']
                }
                if (EnvConfigs.ATTENDANCESTATE.UNMARKED in action.payload.statuses[userid]) {
                    newstate[action.payload.attendancedate][userid].checkOutStatus = false
                    newstate[action.payload.attendancedate][userid].checkInStatus = false
                    newstate[action.payload.attendancedate][userid].checkInUser = action.payload.statuses[userid][EnvConfigs.ATTENDANCESTATE.UNMARKED]['user']
                    newstate[action.payload.attendancedate][userid].checkInTime = action.payload.statuses[userid][EnvConfigs.ATTENDANCESTATE.UNMARKED]['timestamp']
                    newstate[action.payload.attendancedate][userid].status = EnvConfigs.ATTENDANCESTATE.UNMARKED

                }
                if (EnvConfigs.ATTENDANCESTATE.ABSENT in action.payload.statuses[userid]) {
                    newstate[action.payload.attendancedate][userid].checkOutStatus = false
                    newstate[action.payload.attendancedate][userid].checkInStatus = false
                    newstate[action.payload.attendancedate][userid].checkInUser = action.payload.statuses[userid][EnvConfigs.ATTENDANCESTATE.ABSENT]['user']
                    newstate[action.payload.attendancedate][userid].checkInTime = action.payload.statuses[userid][EnvConfigs.ATTENDANCESTATE.ABSENT]['timestamp']
                    newstate[action.payload.attendancedate][userid].status = EnvConfigs.ATTENDANCESTATE.ABSENT

                }
            }
            //console.log(newstate)
            return {
                ...state,
                attendance : newstate,
                attendancerefeshing : false
            }
        }
    }



    return state
}

function getTime() {
    var now = new Date();
    var isPM = now.getHours() >= 12;
    var min = now.getMinutes() < 10 ? '0' + now.getMinutes() : now.getMinutes()
    var secs = now.getSeconds() < 10 ? '0' + now.getSeconds() : now.getSeconds()
    var time = [now.getHours() - (isPM ? 12 : 0),
            min,
            secs || '00'].join(':') +
        (isPM ? ' PM' : ' AM');
    return time
}