"use strict";

import _ from "lodash";

export default function reducer(state = {
                                    isUploading: false,
                                    posttext: "",
                                    selectedgroupid: null,
                                    selectedgroupname: "",
                                    selectedphotos: {},
                                    selectedvideos: {},
                                    updatingPost: false,
                                    lastgrouppostid: null,
                                    studentsOfGroup: [],
                                    errorSendingPost : false
                                },
                                action) {
    switch (action.type) {

        case "UPDATE_POST_DATA": {
            return {
                ...state,
                selectedgroupid: action.payload.postdata.group_id,
                selectedgroupname: action.payload.postdata.groupname,
                selectedphotos : action.payload.postdata.images,
                selectedvideos :action.payload.postdata.videos,
                errorSendingPost : false,
                
            };
        }

        case "SELECT_PHOTOS": {
            var photos = state.selectedphotos;
            var existPhotos = Object.keys(state.selectedphotos).length;

            for (var i = 0; i < action.payload.photos.length; i++) {
                var id = existPhotos + i;
                photos[id] = action.payload.photos[i];
            }
            return {
                ...state,
                isUploading: false,
                selectedphotos: photos
            };
        }

        case "SELECT_VIDEOS": {
            var videos = {};
            for (var i = 0; i < action.payload.videos.length; i++) {
                videos[i] = action.payload.videos[i];
            }
            return {
                ...state,
                isUploading: false,
                selectedvideos: videos
            };
        }

        case 'LOAD_REVIEW_LIST': {

        }

        case "REMOVE_SELECTED_PHOTO": {
            var newphotos = _.omit(state.selectedphotos, action.payload.key);

            return {
                ...state,
                selectedphotos: newphotos
            };
        }

        case "REMOVE_SELECTED_VIDEO": {
            var newvideos = _.omit(state.selectedvideos, action.payload.key);

            return {
                ...state,
                selectedvideos: newvideos
            };
        }

        case "SET_POST_TEXT": {
            return {
                ...state,
                posttext: action.payload.posttext
            };
        }

        case "SET_GROUP_ID": {
            return {
                ...state,
                selectedgroupid: action.payload.groupid,
                selectedgroupname: action.payload.groupname
            };
        }

        case "SEND_POST_FULFILLED": {
            return {
                ...state,
                isUploading: false,
                posttext: "",
                errorSendingPost : false,
                
                lastgrouppostid: state.selectedgroupid,
                selectedgroupid: null,
                selectedphotos: {},
                selectedvideos: {}
            };
        }

        case "SEND_POST_REJECTED": {
            return {
                ...state,
                errorSendingPost : true,
                isUploading: false,
                posttext: "",
                lastgrouppostid: state.selectedgroupid,
                selectedgroupid: null,
                selectedphotos: {},
                updatingPost: false,
                selectedvideos: {}
            };
        }

        case "CLEAR_POST_REJECTED": {
            return {
                ...state,
                errorSendingPost : false,
            };
        }

        case "SEND_POST_IMAGE_REJECTED": {
            return {
                ...state,
                isUploading: false,
                posttext: "",
                lastgrouppostid: state.selectedgroupid,
                selectedgroupid: null,
                selectedphotos: {},
                updatingPost: false,
                selectedvideos: {}
            };
        }

        case "SEND_POST_VIDEO_REJECTED": {
            return {
                ...state,
                isUploading: false,
                posttext: "",
                lastgrouppostid: state.selectedgroupid,
                selectedgroupid: null,
                selectedphotos: {},
                updatingPost: false,
                selectedvideos: {}
            };
        }

        case "REMOVE_ALL_SELECTED_MEDIA": {
            return {
                ...state,
                selectedphotos: {},
                selectedvideos: {}
            }
        }

        case "UPDATE_POST": {
            
            return {
                ...state,
                updatingPost: true,
                errorSendingPost : false
            };
        }
        case "UPDATE_POST_FULFILLED": {
            
            return {
                ...state,
                updatingPost: false
            };
        }
    }

    return state;
}
