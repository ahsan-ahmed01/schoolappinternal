"use strict";
import _ from "lodash";
import update from "immutability-helper";
import * as firebaseapi from "../api/firebaseapi";

Object.extend = function (destination, source) {
  for (var property in source) {
    if (source.hasOwnProperty(property)) {
      destination[property] = source[property];
    }
  }
  return destination;
};

export default function reducer(
  state = {
    feedactivities: null,
    pinnedactivities: null,
    groupactivities: null,
    isPosting: false,
    isNewsFeedLoading: false,
    errorloading: false,
    manualRefresh: true,
    feed_currentpg: 0,
    feed_nextpg: 0,
    group_currentpg: 0,
    group_nextpg: 0,
    ipg: 1,
    imagecount: 0,
    imagesdone: 0,
    videocount: 0,
    videodone: 0,
    refreshNewFeed: false,
    poststatus: {},
    errormsg: "Error Loading News Feed ...",
    postSuccessMessage: ''
  },
  action
) {
  switch (action.type) {
    case "FAKE_NEWS_REFRESH_INDICATOR": {
      return {
        ...state,
        isNewsFeedLoading: true
      };
    }

    case "MANUAL_REFRESH": {
      return {
        ...state,
        manualRefresh: true
      };
    }

    case "LOAD_NEWSFEED_ALL": {
      if (
        action.payload.page === 0 &&
        action.payload.groupid !== undefined &&
        action.payload.groupid > 0
      ) {
        return {
          ...state,
          isNewsFeedLoading: true,
          manualRefresh: true,
          groupactivities: null,
          refreshNewFeed: false
          //activities : null
        };
      } else {
        return {
          ...state,
          isNewsFeedLoading: true,
          refreshNewFeed: false
        };
      }
    }

    case "LOAD_NEWSFEED_ALL_FULFILLED": {
      console.log(action.payload);
      var errorLoading =
        typeof action.payload.data.posts === "undefined" ||
        action.payload.data.posts.length === 0;
      if (action.payload.data.pg > 0) {
        if (action.payload.groupid === undefined) {
          var newfeedlist = Object.extend(
            state.feedactivities,
            action.payload.data.posts
          );
        } else {
          var newfeedlist = Object.extend(
            state.groupactivities,
            action.payload.data.posts
          );
        }
      } else {
        var newfeedlist = action.payload.data.posts;
      }
      if (
        action.payload.groupid === undefined ||
        action.payload.groupid === null
      ) {
        return {
          ...state,
          isNewsFeedLoading: false,
          feedactivities: newfeedlist,
          errorloading: false,
          feed_nextpg: parseInt(action.payload.data.pg) + 1,
          feed_currentpg: action.payload.data.pg,
          ipg: action.payload.data.ipg,
          manualRefresh: false,
          refreshNewFeed: false
        };
      } else {
        return {
          ...state,
          isNewsFeedLoading: false,
          groupactivities: newfeedlist,
          errorloading: false,
          group_nextpg: parseInt(action.payload.data.pg) + 1,
          group_currentpg: action.payload.data.pg,
          ipg: action.payload.data.ipg,
          manualRefresh: false,
          refreshNewFeed: false
        };
      }
    }

    case "LOAD_PINNED_NEWSFEED_ALL_FULFILLED": {
      return {
        ...state,
        pinnedactivities: action.payload.data.posts
      };
    }

    case "LOAD_NEWSFEED_ALL_REJECTED": {
      return {
        ...state,
        isNewsFeedLoading: false,
        errorloading: true,
        errormsg: "Error Loading News Feed ...",
        manualRefresh: false,
        refreshNewFeed: false
      };
    }

    case "NEWS_FEED_REFRESH": {
      return {
        ...state,
        refreshNewFeed: true
      };
    }

    case "NEWSFEED_LOADING": {
      return {
        ...state,
        isNewsFeedLoading: true
        //activities : null
      };
    }

    case "NEWSFEED_LIKE_POST": {
      var index = action.payload.activityid;
      var userid = action.payload.user;
      //console.log(newuser)
      var newactivities2 = state.groupactivities;
      var newactivities = state.feedactivities;
      var pinnedactivities = state.pinnedactivities
      //******* PINNED FEED ****************/
      if (
        state.pinnedactivities !== null &&
        state.pinnedactivities[action.payload.activityid] !== undefined
      ) {
        console.log('In PINNED ...')
        console.log(state.pinnedactivities)
        console.log(action.payload)
        var newusers = Object.assign(
          state.pinnedactivities[action.payload.activityid].likes.users
        );
        newusers.push(userid);
        console.log(newusers)
        var newlikecount =
          state.pinnedactivities[action.payload.activityid].likes.count + 1;
        var foo = update(state.pinnedactivities, {
          [index]: { likes: { selflike: { $set: true } } }
        });
        var foo2 = update(foo, {
          [index]: { likes: { count: { $set: newlikecount } } }
        });
        pinnedactivities = update(foo2, {
          [index]: { likes: { users: { $set: newusers } } }
        });
      }
      //******* MAIN FEED ****************/
      if (
        state.feedactivities !== null &&
        state.feedactivities[action.payload.activityid] !== undefined
      ) {
        console.log('In MAIN ...')

        var newusers = Object.assign(
          state.feedactivities[action.payload.activityid].likes.users
        );
        newusers.push(userid);
        var newlikecount =
          state.feedactivities[action.payload.activityid].likes.count + 1;
        var foo = update(state.feedactivities, {
          [index]: { likes: { selflike: { $set: true } } }
        });
        var foo2 = update(foo, {
          [index]: { likes: { count: { $set: newlikecount } } }
        });
        newactivities = update(foo2, {
          [index]: { likes: { users: { $set: newusers } } }
        });
      }
      //******* GROUP FEED ****************/      
      if (
        state.groupactivities !== null &&
        state.groupactivities[action.payload.activityid] !== undefined
      ) {
        console.log('In Group ...')
        var newusers = Object.assign(
          state.groupactivities[action.payload.activityid].likes.users
        );
        newusers.push(userid);
        var newlikecount =
          state.groupactivities[action.payload.activityid].likes.count + 1;
        var foo = update(state.groupactivities, {
          [index]: { likes: { selflike: { $set: true } } }
        });
        var foo2 = update(foo, {
          [index]: { likes: { count: { $set: newlikecount } } }
        });
        newactivities2 = update(foo2, {
          [index]: { likes: { users: { $set: newusers } } }
        });
      }
      console.log(action);
      return {
        ...state,
        feedactivities: newactivities,
        groupactivities: newactivities2,
        pinnedactivities: pinnedactivities
      };
    }

    case "NEWSFEED_UNLIKE_POST": {
      var userid_remove = action.payload.user;
      var index = action.payload.activityid;

      console.log(state);
      var newactivities2 = state.groupactivities;
      var newactivities = state.feedactivities;
      var pinnedactivities = state.pinnedactivities
      // ***** IN PINNED *******
      if (
        state.pinnedactivities !== null &&
        state.pinnedactivities[action.payload.activityid] !== undefined
      ) {
        console.log('IN PINNED')
        var newlikecount =
          state.pinnedactivities[action.payload.activityid].likes.count - 1;
        var foo = update(state.pinnedactivities, {
          [index]: { likes: { selflike: { $set: false } } }
        });
        var foo2 = update(foo, {
          [index]: { likes: { count: { $set: newlikecount } } }
        });
        var liked_users = foo2[index].likes.users;
        console.log(liked_users);
        var idx = liked_users.indexOf(userid_remove);

        pinnedactivities = update(foo2, {
          [index]: { likes: { users: { $splice: [[idx, 1]] } } }
        });
      }
      // *** IN MAIN ****
      if (
        state.feedactivities !== null &&
        state.feedactivities[action.payload.activityid] !== undefined
      ) {
        var newlikecount =
          state.feedactivities[action.payload.activityid].likes.count - 1;
        var foo = update(state.feedactivities, {
          [index]: { likes: { selflike: { $set: false } } }
        });
        var foo2 = update(foo, {
          [index]: { likes: { count: { $set: newlikecount } } }
        });
        var liked_users = foo2[index].likes.users;
        console.log(liked_users);
        var idx = liked_users.indexOf(userid_remove);

        newactivities = update(foo2, {
          [index]: { likes: { users: { $splice: [[idx, 1]] } } }
        });
      }
      if (
        state.groupactivities !== null &&
        state.groupactivities[action.payload.activityid] !== undefined
      ) {
        var newlikecount =
          state.groupactivities[action.payload.activityid].likes.count - 1;
        var foo = update(state.groupactivities, {
          [index]: { likes: { selflike: { $set: false } } }
        });
        var foo2 = update(foo, {
          [index]: { likes: { count: { $set: newlikecount } } }
        });
        var liked_users = foo2[index].likes.users;
        console.log(liked_users);
        var idx = liked_users.indexOf(userid_remove);
        newactivities2 = update(foo2, {
          [index]: { likes: { users: { $splice: [[idx, 1]] } } }
        });
      }
      console.log(newactivities);
      return {
        ...state,
        feedactivities: newactivities,
        groupactivities: newactivities2,
        pinnedactivities: pinnedactivities
      };
    }

    case "NEWSFEED_LIKE_ACTION_POST_REJECTED": {
      var foo3 = state.groupactivities;
      var foo2 = state.feedactivities;
      if (
        state.feedactivities !== null &&
        state.feedactivities[action.payload.activityid] !== undefined
      ) {
        var index = action.payload.activityid;
        var newlikecount =
          state.feedactivities[action.payload.activityid].likes.count - 1;
        var foo = update(state.feedactivities, {
          [index]: { likes: { selflike: { $set: false } } }
        });
        foo2 = update(foo, {
          [index]: { likes: { count: { $set: newlikecount } } }
        });
      }
      if (
        state.groupactivities !== null &&
        state.groupactivities[action.payload.activityid] !== undefined
      ) {
        var index = action.payload.activityid;
        var newlikecount =
          state.groupactivities[action.payload.activityid].likes.count - 1;
        var foo = update(state.groupactivities, {
          [index]: { likes: { selflike: { $set: false } } }
        });
        foo3 = update(foo, {
          [index]: { likes: { count: { $set: newlikecount } } }
        });
      }

      return {
        ...state,
        feedactivities: foo2,
        groupactivities: foo3,
        isNewsFeedLoading: false,
        errorloading: true,
        errormsg: "Error Updating Like/Unlike Action ..."
      };
    }

    case "NEWSFEED_UNLIKE_ACTION_POST_REJECTED": {
      var newactivities2 = state.groupactivities;
      var newactivities = state.feedactivities;
      if (
        state.feedactivities !== null &&
        state.feedactivities[action.payload.activityid] !== undefined
      ) {
        var index = action.payload.activityid;
        var newlikecount =
          state.feedactivities[action.payload.activityid].likes.count + 1;
        var foo = update(state.feedactivities, {
          [index]: { likes: { selflike: { $set: true } } }
        });
        newactivities = update(foo, {
          [index]: { likes: { count: { $set: newlikecount } } }
        });
      }
      if (
        state.groupactivities !== null &&
        state.groupactivities[action.payload.activityid] !== undefined
      ) {
        var index = action.payload.activityid;
        var newlikecount =
          state.groupactivities[action.payload.activityid].likes.count + 1;
        var foo = update(state.groupactivities, {
          [index]: { likes: { selflike: { $set: true } } }
        });
        newactivities2 = update(foo, {
          [index]: { likes: { count: { $set: newlikecount } } }
        });
      }

      return {
        ...state,
        feedactivities: newactivities,
        groupactivities: newactivities2,
        isNewsFeedLoading: false,
        errorloading: true,
        errormsg: "Error Updating Like/Unlike Action ..."
      };
    }

    case "NEWSFEED_VIEW_POST_FULFILLED": {
      var index = action.payload.activityid;
      var newcount = action.payload.response.data.data;
      var newactivities2 = state.groupactivities;
      var newactivities = state.feedactivities;
      var pinnedactivities = state.pinnedactivities

      if (
        state.pinnedactivities !== null &&
        state.pinnedactivities[index] !== undefined
      ) {
        var currentcounts = state.pinnedactivities[index].self_view_count + 1
        pinnedactivities = update(state.pinnedactivities, {
          [index]: { view_count: { $set: newcount } }
        });
        pinnedactivities = update(state.pinnedactivities, {
          [index]: { self_view_count: { $set: currentcounts } }
        });
      }
      if (
        state.feedactivities !== null &&
        state.feedactivities[index] !== undefined
      ) {
        newactivities = update(state.feedactivities, {
          [index]: { view_count: { $set: newcount } }
        });
      } else {
        newactivities: state.feedactivities;
      }
      if (
        state.groupactivities !== null &&
        state.groupactivities[index] !== undefined
      ) {
        newactivities2 = update(state.groupactivities, {
          [index]: { view_count: { $set: newcount } }
        });
      } else {
        newactivities2 = state.groupactivities;
      }
      return {
        ...state,
        feedactivities: newactivities,
        groupactivities: newactivities2,
        pinnedactivities: pinnedactivities
      };
    }

    case "NEW_POST_SENDING": {
      return {
        ...state,
        isPosting: true,
        imagecount: action.payload.photos.length,
        videocount: action.payload.videos.length
      };
    }

    case "NEW_POST_SENDING_FULFILLED": {

      return {
        ...state,
        isPosting: false,
        imagecount: 0,
        imagesdone: 0,
        videocount: 0,
        videodone: 0,
        postSuccessMessage: action.payload.postMsg
      };
    }

    case "NEW_POST_SENDING_REJECTED": {
      return {
        ...state,
        isPosting: false,
        errorloading: true,
        errormsg: "Error Sending Post. Please Try Again ...",
        imagecount: 0,
        imagesdone: 0,
        videocount: 0,
        videodone: 0
      };
    }

    case "SEND_POST_IMAGE_REJECTED": {
      return {
        ...state,
        isPosting: false,
        errorloading: true,
        errormsg: "Error Sending Post. Please Try Again ...",
        imagecount: 0,
        imagesdone: 0,
        videocount: 0,
        videodone: 0
      };
    }

    case "SEND_POST_VIDEO_REJECTED": {
      return {
        ...state,
        isPosting: false,
        errorloading: true,
        errormsg: "Error Uploading Video. Please Try Again ...",
        imagecount: 0,
        imagesdone: 0,
        videocount: 0,
        videodone: 0
      };
    }

    case "ONE_FILEUPLOAD_DONE": {
      return {
        ...state,
        imagesdone: state.imagesdone + 1
      };
    }

    case "ONE_VIDEO_FILEUPLOAD_DONE": {
      return {
        ...state,
        videodone: state.videodone + 1
      };
    }

    case "UPDATE_POST_STATUS": {
      var refreshStatus = false;
      var status = action.payload.poststatus;
      if (status === null) {
        return {
          ...state,
          refreshNewFeed: true
        };
      }
      console.log(action.payload);
      for (var idx in action.payload.groupids) {
        var groupid = parseInt(action.payload.groupids[idx]);
        //firebaseapi.notifyPostinFirebase(groupid) //REMOVE THIS AFTER INITIAL
        if (
          state.poststatus === null ||
          state.poststatus[groupid] === undefined ||
          state.poststatus[groupid] !== status[groupid]
        ) {
          console.log("New Post Detected in Group ->" + groupid);
          refreshStatus = true;
        }
      }
      return {
        ...state,
        refreshNewFeed: refreshStatus,
        poststatus: status
      };
    }
  }
  return state;
}
