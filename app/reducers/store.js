import {applyMiddleware, createStore, compose, combineReducers} from 'redux'
import {Platform, AsyncStorage} from 'react-native'
import logger from 'redux-logger';
import thunk from 'redux-thunk'
import promise from 'redux-promise-middleware'
import createSagaMiddleware from 'redux-saga'
import reducer from '.'
import authenticateusersaga from '../sagas'
import {persistStore, autoRehydrate} from 'redux-persist'
import createEngine from 'redux-storage-engine-reactnativeasyncstorage'
import * as EnvConfig from '../config/environment'
import {createBlacklistFilter} from 'redux-persist-transform-filter'
import devTools from 'remote-redux-devtools'

const sagaMiddleware = createSagaMiddleware();
import {composeWithDevTools} from 'redux-devtools-extension';


const saveSubsetBlacklistFilter = createBlacklistFilter('login',
    ['logincredvalidated', 'showLoginModal', 'storeloaded']);
const saveSubsetBlacklistFilter2 = createBlacklistFilter('newsfeed',
    ['isNewsFeedLoading', 'isPosting', 'group_currentpg', 'group_nextpg', 'feed_currentpg', 'feed_nextpg', 'imagecount', 'imagesdone', 'videocount', 'videodone', 'errormsg', 'errorloading']);
const saveSubsetBlacklistFilter3 = createBlacklistFilter('postscreen', ['selectedphotos', 'updatingPost', 'selectedvideos']);

const saveSubsetBlacklistFilter4 = createBlacklistFilter('rootNavprops', ['newPostModal', 'profileModal', 'showOwnProfile', 'noNetworkModal', 'disableApp', 'disableText','feedbackModal','attendanceModal']);

const saveSubsetBlacklistFilter5 = createBlacklistFilter('chatrooms', [ 'liveRoom', 'isLoading']);

const saveSubsetBlacklistFilter6 = createBlacklistFilter('rootNavprops', ['currentTab', 'disableApp', 'disableText', 'newPostModal', '.noNetworkModal', 'profileModal', 'showOwnProfile']);

const saveSubsetBlacklistFilter7 = createBlacklistFilter('events', ['isLoading']);

const saveSubsetBlacklistFilter8 = createBlacklistFilter('user', ['config', 'firebaseinitialized','feedbacksending','updatingChildPin', 'errorUpdatingPin','groupphotouploading','childpictureupdating']);

const saveSubsetBlacklistFilter9 = createBlacklistFilter('attendance', ['attendance', 'groups','validatingPin','pinStatus','checkInUser','authorizedUsers', ' attendanceerror']);

const saveSubsetBlacklistFilter10 = createBlacklistFilter('selectedgroup', ['isPhotoLoaded']);

const saveSubsetBlacklistFilter11 = createBlacklistFilter('memberprofile', ['isuploadingPhoto']);

const saveSubsetBlacklistFilter12 = createBlacklistFilter('briefcase', ['isLoading']);

const composeEnhancers = composeWithDevTools({

});


if (EnvConfig.ENV_INSTANCE === 'dev'){
    var middleware = composeEnhancers(applyMiddleware(promise(),
        thunk,
        logger,
        sagaMiddleware), autoRehydrate())

}else{
    var middleware = compose(applyMiddleware(promise(),
        thunk,
        sagaMiddleware), autoRehydrate())
}



const store = createStore(reducer, middleware);

persistStore(store, {
    storage: AsyncStorage, transforms: [saveSubsetBlacklistFilter,
        saveSubsetBlacklistFilter2,
        saveSubsetBlacklistFilter3,
        saveSubsetBlacklistFilter4,
        saveSubsetBlacklistFilter5,
        saveSubsetBlacklistFilter6,
        saveSubsetBlacklistFilter7,
        saveSubsetBlacklistFilter8,
        saveSubsetBlacklistFilter9,
        saveSubsetBlacklistFilter10,
        saveSubsetBlacklistFilter11,
        saveSubsetBlacklistFilter12
    ]
});
store.runSaga = sagaMiddleware.run(authenticateusersaga);

export default store
