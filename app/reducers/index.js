import { combineReducers } from 'redux'

import user from './userReducer'
import login from './loginReducer'
import selectedgroup from './groupdetailsReducer'
import memberprofile from './memberprofileReducer'
import newsfeed from './newsfeedReducers'
import postscreen from './postReducer'
import chatrooms from './chatRoomsReducer'
import memberlist from './memberListReducer'
import events from './eventReducer'
import rootNavprops from './rootNavpropsReducers'
import attendance from './attendanceReducer'
import briefcase from './briefcaseReducer'

const appReducer = combineReducers({
  login, user, selectedgroup, memberprofile, newsfeed, postscreen, chatrooms, memberlist, events, rootNavprops, attendance, briefcase
})

const rootReducer = (state, action) => {
  if (action.type === 'LOGOUT_USER') {

    state = undefined
  }
  if (action.type === 'RESET_USER') {
    state = undefined
  }
  return appReducer(state, action)
}

export default rootReducer