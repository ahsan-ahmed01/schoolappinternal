"use strict";

import _ from "lodash";
import update from "immutability-helper";

export default function reducer(
  state = {
    error: null,
    actionError: false,
    isLoading: false,
    draftfeed: [],
    scheduledfeed: [],
    reviewfeed: [],
    briefcaseNotifications: 0,
  },
  action
) {
  switch (action.type) {
    case "REFRESH_BRIEFCASE": {
      return {
        ...state,
        isLoading: true,
      };
    }

    case "REFRESH_BRIEFCASE_FULFILLED": {
      // var count =
      //   Object.keys(action.payload.draftfeed).length +
      //   Object.keys(action.payload.scheduledfeed).length +
      //   Object.keys(action.payload.reviewfeed).length;
      return {
        ...state,
        isLoading: false,
        draftfeed: action.payload.draftfeed,
        scheduledfeed: action.payload.scheduledfeed,
        reviewfeed: action.payload.reviewfeed,
        // briefcaseNotifications: count,
      };
    }

    case "REFRESH_BRIEFCASE_REJECTED": {
      return {
        ...state,
        isLoading: false,
      };
    }
  }
  return state;
}
