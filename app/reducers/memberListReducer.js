
import update from 'immutability-helper'
import _ from 'lodash'

export default function reducer (state = {
  users: [],
  selectedgroup: null,
  error: '',
  isLoading: true,
  selectedlist: {}
}, action) {
  switch (action.type) {
    case 'GET_MEMBER_DETAILS':
      {
        return {
          ...state,
          selectedgroup: action.payload.selectedGroup,
          isLoading: true,
          error: ''
        }
      }
    case 'GET_MEMBER_DETAILS_FULFILLED':
      {
        return {
          ...state,
          isLoading: false,
          users: action.payload.users
        }
      }
    case 'GET_MEMBER_DETAILS_REJECTED':
      {
        return {
          ...state,
          users: null,
          isLoading: false,
          error: action.payload.err
        }
      }

    case 'MEMBER_LIST_ADD':
      {
        var user = {}
        user['id'] = action.payload.user.id
        user['display_name'] = action.payload.user.display_name
        user['profile_photo'] = action.payload.user.profile_photo
        console.log(typeof (state.selectedlist))
        console.log(JSON.stringify(state))
        var newselectedList = Object.assign(state.selectedlist)
        console.log(newselectedList)
        console.log(action.payload.user.id)
        console.log(JSON.stringify(state.selectedlist))
        var foo = update(newselectedList, {
          [action.payload.user.id]: {$set: user}
        })
        newselectedList[action.payload.user.id] = user
        console.log(JSON.stringify(foo))
        return {
          ...state,
          selectedlist: foo
        }
      }

    case 'MEMBER_LIST_REMOVE':
      {
        var newlist = _.omit(state.selectedlist, action.payload.user.id)
        return {
          ...state,
          selectedlist: newlist
        }
      }

    case 'MEMBER_LIST_CLEAR':
      {
        return {
          ...state,
          selectedlist: {},
            isLoading : false
        }
      }

    case 'SET_SELECTED_LIST':
      {
        return {
          ...state,
          selectedlist: Object.assign(action.payload.selectedUsers)
        }
      }
  }

  return state
}
