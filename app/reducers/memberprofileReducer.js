'use strict'
export default function reducer (state = {
  id: -1,
  display_name: '',
  token: '',
  user_email: '',
  showDetails: false,
  groups: null,
  profile_photo: '',
  parent_of: '',
  tel_home: '',
  tel_mobile: '',
  isuploadingPhoto: false,
  linked: null
}, action) {
  switch (action.type) {
    case 'VIEW_PROFILE':
      {
        return {
          ...state,
          id: action.payload.userObj.id,
          display_name: action.payload.userObj.display_name,
          user_email: action.payload.userObj.user_email,
          profile_photo: action.payload.userObj.profile_photo,
          is_admin: action.payload.userObj.is_admin,
          showDetails: true,
          parent_of: action.payload.userObj.parent_of,
          tel_home: action.payload.userObj.tel_home,
          tel_mobile: action.payload.userObj.tel_mobile,
          linked: action.payload.userObj.linked
        }
      }

      case 'UPDATE_ADDL_PROFILE':
      {
          return {
              ...state,
              // user_email: action.payload.userObj.user_email,
              // profile_photo: action.payload.userObj.profile_photo,
              // is_admin: action.payload.userObj.is_admin,
              // showDetails: true,
              parent_of: action.payload.userObj.parent_of,
              // tel_home: action.payload.userObj.tel_home,
              // tel_mobile: action.payload.userObj.tel_mobile,
              // linked: action.payload.userObj.linked
          }
      }

    case 'UPLOAD_PROFILE_PHOTO':
      {
        return {
          ...state,
          isuploadingPhoto: true
        }
      }

    case 'UPLOAD_PROFILE_PHOTO_FULFILLED':
      {
        return {
          ...state,
          isuploadingPhoto: false,
          profile_photo: action.payload.body.postResponse.location
          
        }
      }

    case 'UPLOAD_PROFILE_PHOTO_REJECTED':
      {
        return {
          ...state,
          isuploadingPhoto: false
        }
      }

  }
  return state
}
