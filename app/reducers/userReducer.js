'use strict'
import update from 'immutability-helper'

export default function reducer (state = {
  username : null,
  display_name: '',
  token: '',
  user_email: '',
  showDetails: false,
  groups: null,
  profile_photo: '',
  children_array : [],  
  parent_of: '',
  tel_home: '',
  tel_mobile: '',
  linked: null,
  notifications: null,
  listenerRunning: false,
  totalnotifications: 0,
  tabname: 'newsfeed',
  groupphotohash: null,
    childpictureupdating : false,
    groupphotouploading : false,
    roles: null,
    config : null,
    children : [],
    firebaseinitialized : false,
    feedbacksending : false,
    updatingChildPin : false,
    errorUpdatingPin :  false,
     userid : null,
     reset_version : 0,
  localnotification: [] //* ** SET THIS TO DIFF AND THEN IN RENDER LET THAT SEND NOTIFICATION ***/
                       //* * YOU COULD ALSO  */
}, action) {
  switch (action.type) {

      case 'INIT_FIREBASED_FULLFILLED':
      {
          return {
              ...state,
              firebaseinitialized : true
          }
      }

    case 'GET_USER_DETAILS':
      {
        return {
          ...state,
          showDetails: false
        }
      }
    case 'GET_USER_DETAILS_REJECTED':
      {
        return {
          ...state,
          showDetails: false
        }
      }

      case 'UPDATE_CHILD_OBJS': {
          return {
              ...state,
              children : action.payload.childobjs,
              children_array : action.payload.childobjs.map(function (item) {return item.childid})
          }
      }

      case 'GET_USER_DETAILS_FULFILLED':
      {
          console.log(action.payload)
          tmphash = {}
          var tmphash2 = {}
          let i = 0
          for (key in action.payload.userdata.Groups) {

              tmphash[key] = action.payload.userdata.Groups[key].profile_photo
              var name = action.payload.userdata.Groups[key].name
              i++
          }
          console.log(action.payload.userdata.reset_version)
          console.log(action.payload.userdata)
          var photourl = action.payload.userdata['picture']
          return {
              ...state,
              reset_version : action.payload.userdata.reset_version,
              profile_photo: photourl,
              tel_home:  action.payload.userdata['phone_number'],
              roles: action.payload.userdata['role'],
              display_name : action.payload.userdata['display_name'],
              user_email : action.payload.userdata['email'],
              groups : action.payload.userdata.Groups,
              token : action.payload.token,
              username : action.payload.userdata.Username,
              userid : action.payload.userdata.userid,
              groupphotohash : tmphash,
              children : action.payload.childobjs,
              children_array : action.payload.childobjs === undefined ? [] : action.payload.childobjs.map(function (item) {return item.childid})
          }
      }

      case 'UPDATE_TOKEN' : {
        return{
            ...state,
            token : action.payload.token

        }
      }

    case 'UPDATE_PROFILE_IMAGE':
      {
        return {
          ...state,
          profile_photo: action.payload.profileimage
        }
      }

    case 'LOAD_NOTIFICATIONS':
      {
        var newNotifications = action.payload.notifications
        var count = 0
        if (newNotifications !== null) {
          for (var key in newNotifications.Rooms) {
            count += newNotifications.Rooms[key]
          }
        }
        var diff = findoutNewMessages(newNotifications, state.notifications)
        if (diff !== null) {
          var newdiff = update(state.localnotification, {$push: [diff]})
          return {
            ...state,
            localnotification: newdiff,
            notifications: newNotifications,
            totalnotifications: count
          }
        } else {
          return {
            ...state,
            notifications: newNotifications,
            totalnotifications: count
          }
        }
      }

    case 'START_NOTIFICATION_LISTENER':
      {
        return {
          ...state,
          listenerRunning: true
        }
      }

    case 'STOP_NOTIFICATION_LISTENER':
      {
        return {
          ...state,
          listenerRunning: false
        }
      }

    case 'SET_ACTIVE_TAB':
      {
        return {
          ...state,
          tabname: action.payload.tabName
        }
      }

      case 'SET_GROUP_COVER_PHOTO_FULFILLED':{
        var tmpgroups = Object.assign(state.groups)
          var groupid = action.payload.groupid
          var photoUrl = action.payload.photoUrl
          for(var index in tmpgroups){
              if (tmpgroups[index].id === groupid){
                tmpgroups[index].cover_photo = photoUrl
              }
          }
          return{
              ...state,
              groups : tmpgroups,
              groupphotouploading : false
          }
      }

      case 'SET_GROUP_PROFILE_PHOTO' : {
          return {
              ...state,
              groupphotouploading : true
          }
      }

      case 'SET_GROUP_COVER_PHOTO' : {
          return {
              ...state,
              groupphotouploading : true
          }
      }

      case 'SET_GROUP_PROFILE_PHOTO_FULFILLED':{
          console.log(state)
          var tmpgroups = Object.assign(state.groups)
          var groupid = action.payload.groupid
          var photoUrl = action.payload.photoUrl
          for(var index in tmpgroups){
              if (tmpgroups[index].id === groupid){
                  tmpgroups[index].profile_photo = photoUrl
              }
          }
          var tmphash = Object.assign(state.groupphotohash)
          tmphash[groupid] = photoUrl

          return{
              ...state,
              groups : tmpgroups,
              groupphotohash : tmphash,
              groupphotouploading : false
          }
      }
      case 'SEND_FEEDBACK': {
          return{
              ...state,
              feedbacksending : true
          }
      }

      case 'SEND_FEEDBACK_FULFILLED': {
          return{
              ...state,
              feedbacksending : false
          }
      }

      case 'SEND_FEEDBACK_REJECTED': {
          return{
              ...state,
              feedbacksending : false
          }
      }

      case 'SET_CHILD_AUTHORIZED_USER' : {
          console.log('Setting Pin')
          return {
              ...state,
              updatingChildPin : true,
              errorUpdatingPin : false
          }
      }

      case 'SET_CHILD_AUTHORIZED_USER_FULFILLED' : {
          return {
              ...state,
              updatingChildPin : false,
              errorUpdatingPin : false
          }
      }

      case 'SET_CHILD_AUTHORIZED_USER_REJECTED' : {
          return {
              ...state,
              updatingChildPin : false,
              errorUpdatingPin : true
          }
      }

      case 'DELETE_CHILD_AUTHORIZED_USER' : {
          console.log('Setting Pin')
          return {
              ...state,
              updatingChildPin : true,
              errorUpdatingPin : false
          }
      }

      case 'DELETE_CHILD_AUTHORIZED_USER_FULFILLED' : {
          return {
              ...state,
              updatingChildPin : false,
              errorUpdatingPin : false
          }
      }

      case 'DELETE_CHILD_AUTHORIZED_USER_REJECTED' : {
          return {
              ...state,
              updatingChildPin : false,
              errorUpdatingPin : true
          }
      }

      case 'UPLOAD_CHILD_PROFILE_PHOTO' : {
          return {
              ...state,
              childpictureupdating : true
          }

      }

      case 'UPLOAD_CHILD_PROFILE_PHOTO_REJECTED' : {
          return {
              ...state,
              childpictureupdating : false
          }

      }

      case 'UPLOAD_CHILD_PROFILE_PHOTO_FULFILLED' : {
          var newchildrenarray = Object.assign(state.children)
          for(var index in newchildrenarray){
              console.log('Viewing id ' + index + ' with childid ' + newchildrenarray[index].childid)
              if (newchildrenarray[index].childid === action.payload.childid){
                  console.log('Updating URL to ' + action.payload.url)
                  newchildrenarray[index].pictureurl = action.payload.url
              }
          }
          return {
              ...state,
              children : newchildrenarray,
              childpictureupdating : false
          }
      }

  }
  return state
}

function findoutNewMessages (newNotifications, oldNotifications) {
  var newMessage = null
  if (oldNotifications === null || newNotifications === null) return null

  for (var key in newNotifications.Rooms) {
    var newCount = newNotifications.Rooms[key]
    if (oldNotifications.Rooms[key] === undefined) {
      if (newMessage === null) { newMessage = {} }
      newMessage[key] = newNotifications.Rooms[key]
    } else {
      var oldCount = oldNotifications.Rooms[key]
      if (newCount > oldCount) {
        if (newMessage === null) { newMessage = {} }
        newMessage[key] = newCount - oldCount
      }
    }
  }
  console.log(newMessage)
  return newMessage
}
