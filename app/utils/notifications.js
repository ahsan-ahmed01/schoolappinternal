import {updateToken} from "../actions/userActions";

'user string'
import React, {Component} from 'react'
import {AppState, Platform, PushNotificationIOS, View, NetInfo} from 'react-native'
import {connect} from 'react-redux'
import firebase from 'firebase'
import * as userActions from '../actions/userActions'
import * as loginActions from '../actions/loginActions'
import * as rootnavActions from '../actions/rootNavActions'
import * as newsfeedActions from '../actions/newsfeedActions'
import VersionNumber from 'react-native-version-number';
import {Auth} from 'aws-amplify'
// import FCM from 'react-native-fcm'
import * as miscutils from './misc'
import OneSignal from 'react-native-onesignal'
import DropdownAlert from 'react-native-dropdownalert';
var bugsnag = require('../config/bugsnag')

//import NavigatorService from '../navigation/navigatorsvc';
import * as ChatActions from '../actions/chatActions'
import Icon from 'react-native-vector-icons/FontAwesome';
import {Navigation} from 'react-native-navigation'
var EnvConfig = require('../config/environment')
class NotificationsUtil extends Component {

    constructor (props) {
        super(props)
        this.state = {
            notificationimgSrc : null,
            lastRoomKey : null,
            notificationeventId : null

        }
        this._handleAppStateChange = this._handleAppStateChange.bind(this)
    }

    processPostNotifications(){

    }


initOneSignal(nextProps){
       //* ***** One Signal **********
       console.log(EnvConfig.AWS_CONFIG.ONE_SIGNAL_KEY)
       //OneSignal.setLogLevel(6, 0);
       OneSignal.init(EnvConfig.AWS_CONFIG.ONE_SIGNAL_KEY, {kOSSettingsKeyAutoPrompt : true})
       console.log(OneSignal)
       miscutils.captureStat('API:Notifications','Registered Signals->' + EnvConfig.AWS_CONFIG.ONE_SIGNAL_KEY,0)
       console.log('Registering Signals !')
       OneSignal.inFocusDisplaying(0) //Only for Andriod, IOS is in AppDeletegate.m
       OneSignal.addEventListener('received', this.onReceived.bind(this))
       OneSignal.addEventListener('opened', this.onOpened.bind(this))
       //OneSignal.addEventListener('registered', this.onRegistered.bind(this))
       OneSignal.addEventListener('ids', this.onIds.bind(this))
   }

  _handleAppStateChange (currentAppState) {
      if (currentAppState === 'background') {
          // this.props.stopNotifications(this.props.user.id)
      }
      if (currentAppState === 'active') {
          console.log('----APP IS ACTIVE ---')
          // if (Platform.OS === 'ios') {
          //     PushNotificationIOS.setApplicationIconBadgeNumber(0);
          //
          // }
          Auth.signIn(this.props.login.cred.userid, this.props.login.cred.password)
              .then(user => {
                  console.log(user)
                  //var updateNewsfeed = this.props.user.roles !== 'Attendance'
                  console.log(this.props.user.roles)
                  this.props.updateToken(user.signInUserSession.idToken.jwtToken, this.props.user.groups, false)

              })
              .catch(err => {
                      console.log(err)
                      return err
                  }
              )
          this._handleConnectionChange(NetInfo.isConnected)
      }
      // if (Platform.OS === 'ios') {
      //     PushNotificationIOS.setApplicationIconBadgeNumber(0);
      // }
  }
    _handleConnectionChange(connectionInfo) {
        console.log('Connection Status' + connectionInfo)
        if (connectionInfo){
            console.log('Found Network')
            bugsnag.leaveBreadcrumb('Found Network')
            this.props.hideNoNetworkModal()
        }else{
            Navigation.showModal({
                stack: {
                    children: [{
                        component: {
                            id : 'idNoNetwork',
                            name: 'nav.Three60Memos.WelcomeScreen',
                            passProps: {
                                textMessage: 'No Network Found',
                                textMessagesmall: 'You must be connected to the internet to use the App'
                            }
                        }
                    }]
                }
            })
            bugsnag.leaveBreadcrumb('No Network')
            this.props.showNoNetworkModal()
        }
    }

    componentWillReceiveProps(nextProps){
        console.log(this.props.user.firebaseinitialized)
        console.log(nextProps.user.firebaseinitialized)
        console.log(this.props)
        console.log(nextProps.user)

        if ((!this.props.user.firebaseinitialized) &&  (nextProps.user.firebaseinitialized)){
            if (nextProps.user.roles === 'attendance'){
                console.log('Not Starting  NOTIFICATIONS for attendance role!!!!!!')

            }else {
                console.log('Starting NOTIFICATIONS !!!!!!')
                this.props.startNotifications(this.props.user.userid, null)
            }
            this.startDisableAppListener()
            this.StartOtherListeners()
            if (nextProps.user.roles !== 'attendance') {
                this.initOneSignal(nextProps)
            }
        }
    }

    componentDidMount() {
        //this.props.setAppStatus(true)
        AppState.addEventListener('change', this._handleAppStateChange);
        NetInfo.isConnected.addEventListener('connectionChange', this._handleConnectionChange.bind(this));

        if (this.props.login.FirebaseConnected === false || this.props.login.AppReady === false){
            console.log('SOFT INIT APP !!!!!!')
            this.props.softinitApp(this.props.login.cred.userid, this.props.login.cred.password)
        }else{
            this.props.setAppStatus(true)
        }
        Navigation.events().registerComponentDidAppearListener((component) => {
            if (this.props.currentTab !== component.componentName) {
                console.log('Moving from Tab ' + this.props.currentTab + ' to  ' + component.componentName)
                bugsnag.leaveBreadcrumb('ChangeTab:' + component.componentName.replace('nav.Three60Memos',''))
                this.props.updateCurrentTab(component.componentName, this.props.currentTab, this.props.user.token)
            }
        })
    }
  componentWillUnMount () {

    this.props.stopNotifications(this.props.user.userid)
    AppState.removeEventListener('change', this._handleAppStateChange)
    NetInfo.isConnected.removeEventListener('connectionChange', this._handleConnectionChange.bind(this));

      OneSignal.removeEventListener('received', this.onReceived)
    OneSignal.removeEventListener('opened', this.onOpened)
    OneSignal.removeEventListener('registered', this.onRegistered)
    OneSignal.removeEventListener('ids', this.onIds)
  }

  onReceived (notification) {
    console.log('Notification received: ', notification)
    //Events ......
    if (notification.payload.additionalData.eventid !== undefined){
        console.log('In Event Notification Handler')
        this.setState({notificationimgSrc: require('../img/calendar.png')})
        this.setState({notificationeventId: notification.payload.additionalData.eventid})
        this.dropdown.alertWithType('custom',EnvConfig.NOTIFICATION_EVENT_HEADER, notification.payload.body)
    }
    //Messages ....
    if (notification.payload.additionalData.roomkey !== undefined) {
        console.log('In Message Notification Handler')
        if ((this.props.currentTab === "ChatRooms") &&
            notification.payload.additionalData.roomkey !== undefined &&
            (this.props.chatrooms.liveRoom === notification.payload.additionalData.roomkey)) {
            console.log('suupress !!!!')
            notification.shown = false
            return false
        }
        else {
            console.log('Calling Local Notification')
            //Crashylitics Bug
            if (this.props.chatrooms.rooms[notification.payload.additionalData.roomkey] !== undefined) {
                this.setState({notificationimgSrc: this.props.chatrooms.rooms[notification.payload.additionalData.roomkey].Details.users[notification.payload.additionalData.senderid].profile_photo})
            }
            this.setState({lastRoomKey: notification.payload.additionalData.roomkey})
            this.dropdown.alertWithType('custom', notification.payload.body, notification.payload.additionalData.message)

        }
    }

  }



  onOpened (openResult) {
    console.log('Message: ', openResult.notification.payload.body)
    console.log('Data: ', openResult.notification.payload.additionalData)
    console.log('isActive: ', openResult.notification.isAppInFocus)
    console.log('openResult: ', openResult)
      if (openResult.notification.payload.additionalData.roomkey !== undefined) {
          var roomKey = openResult.notification.payload.additionalData.roomkey
          // this.props.navigator.handleDeepLink({
          //     link: 'chatRooms/Three60Memos.ChatScreen/BYROOMKEY/' + roomKey
          // })
      }
  }
    onClose(data) {
      if (data.action === 'tap'){
        console.log('User Tapped !!!')
          console.log(data)
          if (data.title === EnvConfig.NOTIFICATION_EVENT_HEADER){
              //NavigatorService.navigate('Event', {eventid: this.state.notificationeventId})
          }else{
              //NavigatorService.navigate('Chat', {roomKey: this.state.lastRoomKey})
          }

      }
    }

  onRegistered (notifData) {
    console.log('Device had been registered for push notifications!', notifData)
  }

  onIds (device) {
    console.log('Device info: ', device)
  }

  render () {
      console.log('RENDER->NOTIFICATION')
      this.updateChatBadgeCount()
      this.updateEventBadgeCount()
      this.updateBriefCaseBadgeCount()
      return (
          <DropdownAlert  onClose={data => this.onClose(data)}
                          imageSrc={this.state.notificationimgSrc}
                          containerStyle={{
                              backgroundColor: 'black',
                          }}
                          closeInterval={7000}
                          ref={(ref) => this.dropdown = ref}/>
    )
  }

    getLastMsgTimestampofRoom(roomKey) {
        if (this.props.chatrooms.rooms[roomKey] !== undefined) {
            var msgs = this.props.chatrooms.rooms[roomKey].Messages
            if (msgs === undefined) {
                return 0
            } else {
                var lastts = msgs[0].orderBy
                return lastts
            }
        }
        else return 0
    }

    updateChatBadgeCount(){
        var badgetcount = this.props.user.totalnotifications == 0 ? null : this.props.user.totalnotifications.toString()
        Navigation.mergeOptions("idChatTab",
            {
                bottomTab: {
                    badge: badgetcount
                }
            })
    }

    updateEventBadgeCount(){
        console.log('Setting Badge for Events : '  +this.props.eventNotifications)
        Navigation.mergeOptions("idEventTab",
            {
                bottomTab: {
                    badge: this.props.eventNotifications == 0 ? null : this.props.eventNotifications.toString()
                }
            })
    }

    updateBriefCaseBadgeCount(){
        console.log('Setting Badge for Briefcase : '  +this.props.eventNotifications)
        Navigation.mergeOptions("idBriefcaseTab",
            {
                bottomTab: {
                    badge: this.props.briefcaseNotifications == 0 ? null : this.props.briefcaseNotifications.toString()
                }
            })
    }

    shouldComponentUpdate(nextProps, nextState) {
        if (this.props.user.totalnotifications !== nextProps.user.totalnotifications){
           console.log('Setting Badge Count')
           this.updateChatBadgeCount()
           return true
        }
        if (this.props.eventNotifications !== nextProps.eventNotifications){
            console.log('Setting Badge Count')
            this.updateEventBadgeCount()
            return true
         }
         if (this.props.briefcaseNotifications !== nextProps.briefcaseNotifications){
            console.log('Setting Badge Count')
            //this.u()
            return true
         }
        if ((this.props.noNetworkModal !== nextProps.noNetworkModal) && (nextProps.noNetworkModal == false)){
            Navigation.dismissModal('idNoNetwork');
        }
        if ((this.props.disableAppFlag !== nextProps.disableAppFlag) && (nextProps.disableAppFlag == false)){
            console.log('Closing Disable App Screen')
            Navigation.dismissModal('idDisableApp');
        }
        return false
    }

    startDisableAppListener(){
        console.log('Starting Disable Status Listener')
        firebase.database().ref('/status/').on('value', function (snapshot) {
            console.log('----- GOT STATUS -------------')
            var data = snapshot.val()
            console.log(data)
            console.log(data)
            if (data == null) {
                this.props.enableApp()
                return
            }
            console.log(data.disable === "True")
            if (data.disable === "True"){
                this.props.disableApp(data.disabletext)
                Navigation.showModal({
                    stack: {
                        children: [{
                            component: {
                                id : 'idDisableApp',
                                name: 'nav.Three60Memos.WelcomeScreen',
                                passProps: {
                                    textMessage: data.disabletext,
                                    textMessagesmall: data.disabletextdesc
                                }
                            }
                        }]
                    }
                })
            }
            else{
                this.props.enableApp()
            }


        }, this)


    }

    StartOtherListeners() {
        if (this.props.user.roles !== 'Attendance') {
            //Process Post Notifications
            firebase.database().ref('/LastActive/' + this.props.user.userid).set(new Date().toString())
            firebase.database().ref('/UserStats/' + this.props.user.userid + "/OS").set(Platform.OS)
            firebase.database().ref('/UserStats/' + this.props.user.userid + "/LastActive").set(new Date().toString())

            firebase.database().ref('/UserStats/' + this.props.user.userid + "/360Version").set(VersionNumber.appVersion + " " + VersionNumber.buildVersion)
            firebase.database().ref('/Posts/LatestPost/').on('value', function (snapshot) {
                console.log('----- GOT POSTNOTIFICATION -------------')
                console.log(snapshot.key)
                var poststatus = snapshot.val()

                console.log(poststatus)
                setTimeout(() => { this.props.updatePostStatus(poststatus, Object.keys(this.props.user.groups)),1})

            }, this)
        }
    }

// processLocalNotifications () {
  //   var count = this.props.user.localnotification.length
  //   for (var i = 0; i < count; i++) {
  //     var msg = this.props.user.localnotification[i]
  //     var roomKey = Object.keys(msg)[0]
  //     var userids = []
  //     Object.keys(this.props.chatrooms.rooms[roomKey].Details.users).forEach(function (key) {
  //       if (key !== this.props.user.id) { userids.push(key) }
  //     }, this)
  //     if (userids.length === 1) {
  //       var username = this.props.chatrooms.rooms[roomKey].Details.users[userids[0]].display_name
  //       var msg = username + ' sent you a Chat Message'
  //     } else {
  //       username = ''
  //       var msg = 'You received a Chat Message'
  //     }
  //     this.props.user.localnotification.shift()
  //     var customData = 'ROOM:' + roomKey + ':' + username
  //     if (AppState)// //
  //       { miscutils.sendLocalNotificationforRoom(msg, this.props.user.totalnotifications, customData) }
  //   }
  // }

}



var mapStateToProps = function (store) {
  return {login: store.login,
        user: store.user,
        chatrooms: store.chatrooms,
        currentTab: store.rootNavprops.currentTab,
        noNetworkModal :  store.rootNavprops.noNetworkModal,
        disableAppFlag : store.rootNavprops.disableApp,
        eventNotifications : store.events.eventnotifications,
        briefcaseNotifications : store.briefcase.briefcaseNotifications
        }
}
var mapDispatchToProps = function (dispatch) {
  return {
    startNotifications: function (userid, navProps) { dispatch(userActions.startNotifications(userid)) },
    stopNotifications: function (userid) { dispatch(userActions.stopNotifications(userid)) },
      startListener: function (roomKey, lastmsgtimestamp) {
          dispatch(chatActions.startListener(roomKey, lastmsgtimestamp))
      },
      updateToken: function (token, authobj, groups) {
          dispatch(userActions.updateToken(token, authobj, groups))
      },
      softinitApp: function (userid, password) {
          dispatch(loginActions.softinitApp(userid, password))
      },
      updateCurrentTab : function (tabname, prevtab, token) {
          dispatch(rootnavActions.updateCurrentTab(tabname, prevtab, token))
      }
      ,showNoNetworkModal: function () {
          dispatch(rootnavActions.showNoNetworkModal())

      },
      hideNoNetworkModal: function () {
          dispatch(rootnavActions.hideNoNetworkModal())

      },
      enableApp: function () {
          dispatch(rootnavActions.enableApp())
      },
      disableApp: function (disableText) {
          dispatch(rootnavActions.disableApp(disableText))
      },
      updatePostStatus : function (poststatus,groupsid) {
          dispatch(newsfeedActions.updatePostStatus(poststatus,groupsid))
      },
      setAppStatus : function (status) {
          dispatch(loginActions.setAppStatus(status))
      },



  }
}
export default connect(mapStateToProps, mapDispatchToProps)(NotificationsUtil)
