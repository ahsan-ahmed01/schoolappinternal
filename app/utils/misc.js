// import FCM from 'react-native-fcm'
import firebase from "firebase";
//import * as emojimap from './emojimap'
//import _ from 'lodash'
var EnvConfig = require("../config/environment");
import { Hub } from "aws-amplify";
export function ObjectArraytoIndexArray(ObjArray) {
  var idxArray = [];
  if (ObjArray === null) return [];
  Object.keys(ObjArray).forEach(function (key) {
    idxArray.push(ObjArray[key]);
  });
  return idxArray;
}

export function getMonthName(monthint) {
  var monthNames = [
    "January",
    "February",
    "March",
    "April",
    "May",
    "June",
    "July",
    "August",
    "September",
    "October",
    "November",
    "December",
  ];
  return monthNames[monthint - 1];
}

export function getShortMonthName(monthint) {
  var monthNames = [
    "Jan",
    "Feb",
    "Mar",
    "Apr",
    "May",
    "Jun",
    "Jul",
    "Aug",
    "Sep",
    "Oct",
    "Nov",
    "Dec",
  ];
  return monthNames[monthint - 1];
}

export function getShortDayofWeek(dayindex) {
  var days = ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"];
  return days[dayindex];
}

export function convertDatetoYYYYMMDD(dt, delimiter = "") {
  var mm = dt.getMonth() + 1; // getMonth() is zero-based
  var dd = dt.getDate();

  return [
    dt.getFullYear(),
    (mm > 9 ? "" : "0") + mm,
    (dd > 9 ? "" : "0") + dd,
  ].join(delimiter);
}

export function isEntitledtoEvent(event, userprops) {
  var groupid = event.group_id;
  //**** if Owner you are entitled
  if (userprops.role === "admin") {
    return true;
  }
  if (event.event_owner === userprops.id) {
    return true;
  }

  //*******
  var group = getGroupbyGroupid(userprops.groups, groupid);
  if (group !== null) {
    if (group.is_admin === "1" || group.is_mod === "1") {
      return true;
    }
  }

  return false;
}

export function isAdminGroup(groupid, usergroups) {
  var group = getGroupbyGroupid(usergroups, groupid);
  if (group !== null) {
    if (group.is_admin === "1" || group.is_mod === "1") {
      return true;
    }
  }
  return false;
}

export function getGroupbyGroupid(groups, groupid) {
  for (var index in groups) {
    if (groups[index].id === groupid) {
      return groups[index];
    }
  }
  return null;
}

export function LogtoFireBase(text) {
  var logRef = firebase.database().ref();
  var key = logRef.child("Logs").push().key;
  firebase
    .database()
    .ref("/Logs/" + key + "/")
    .set({
      log: text,
    });
}

// export function ConvertToEmoji (token) {
//   var emojitokens = token.split(';')
//   var emojidata = _.map(emojitokens, (emojitoken, i) => {
//     var emojitokencode = emojitoken.replace('&#x', '')
//     var sh = emojimap.getEmojiText('U+' + emojitokencode.toUpperCase())
//     if (sh !== undefined) {
//       return (<Emoji name={sh}/>)
//     } else {
//       return null
//     }
//   })
//   return emojidata
// }

export function getProfilePhoto(username) {
  var s3_url = EnvConfig.S3_PROFILE_URL + username + ".jpg";
  return s3_url;
}

export function captureStat(subject, statsdata, timetaken) {
  Hub.dispatch("stats", {
    data:
      "[" +
      new Date().toUTCString() +
      "] " +
      subject +
      "->" +
      statsdata +
      " : " +
      timetaken +
      " ms",
  });
}

export function getNewListinOrder(activities) {
  var activitymap = {};
  Object.keys(activities).forEach(function (activity) {
    activitymap[parseInt(activities[activity].row_num)] =
      activities[activity].postid;
  });
  var rownums = Object.keys(activitymap).sort(
    (a, b) => parseInt(a) - parseInt(b)
  );
  var sortedactivities = [];
  rownums.forEach(function (rownum) {
    sortedactivities.push(activitymap[rownum]);
  });
  return sortedactivities;
}

export function formatTimeinAMPM(time) {
  var date = new Date(time);
  var hours = date.getHours();
  var minutes = date.getMinutes();
  var ampm = hours >= 12 ? "PM" : "AM";
  hours = hours % 12;
  hours = hours ? hours : 12; // the hour '0' should be '12'
  minutes = minutes < 10 ? "0" + minutes : minutes;
  var strTime = hours + ":" + minutes + " " + ampm;
  console.log(date);
  return strTime;
}

// export function sendLocalNotificationforRoom (msg, badgecount, customData) {
//   console.log('---- SENDING LOCAL NOTIFICATION : ' + msg)

//   console.log('in SEND LOCAL')
//   console.log(msg)
//   console.log(badgecount)
//   console.log(FCM)
//   FCM.presentLocalNotification({
//     title: 'Chat Message',                     // as FCM payload
//     body: msg,                    // as FCM payload (required)
//     sound: 'default',                                   // as FCM payload
//     priority: 'high',                                   // as FCM payload
//     click_action: 'ACTION',                             // as FCM payload
//     badge: badgecount,                                          // as FCM payload IOS only, set 0 to clear badges
//     number: badgecount,                                         // Android only
//     ticker: 'Chat Message',                   // Android only
//     auto_cancel: true,                                  // Android only (default true)
//     large_icon: 'ic_launcher',                           // Android only
//     icon: 'ic_launcher',                                // as FCM payload, you can relace this with custom icon you put in mipmap

//     my_custom_data: customData,             // extra data you want to throw
//     lights: true,                                    // Android only, LED blinking (default false
//     show_in_foreground: true
//   })
// }

export function getUserfromCache(cache, username) {
  if (username in cache) {
    return cache[username];
  } else {
    var user = {};
    user["username"] = username;
    user["userid"] = username.replace(/./g, "_").replace(/@/g, "_");
    user["profile_photo"] = "<DEFAULT>";
    user["parent_of"] = {};
    user["display_name"] = "Alumni User";
    user["invalid_user"] = true;
    return user;
  }
}
