import React, { Component } from "react";
import {
  TouchableOpacity,
  View,
  Text,
  StyleSheet,
  Modal,
  Image,
  Platform,
  findNodeHandle,
} from "react-native";
import Icon from "react-native-vector-icons/FontAwesome";
import ActionButton from "react-native-action-button";
import * as rootNavAction from "../actions/rootNavActions";
import Entypo from "react-native-vector-icons/Entypo";
import Ionic from "react-native-vector-icons/Ionicons";
import MaterialCommunityIcons from "react-native-vector-icons/MaterialCommunityIcons";
import MaterialIcons from "react-native-vector-icons/MaterialIcons";
import { connect } from "react-redux";
import PostScreen from "../screens/post";
import { BlurView } from "react-native-blur";
import MemberDetailsScreen from "../screens/memberdetails";
import * as memberActions from "../actions/memberprofileActions";
import FeedBackScreen from "../screens/feedback";
import CustomBlurView from "../components/CustomBlurView";

var EnvConfig = require("../config/environment");
import { Navigation } from "react-native-navigation";
import FontAwesome from "react-native-vector-icons/FontAwesome";
import ProfileButton from "../components/ProfileButton";

var closeIcon = null;
var refreshIcon = null;

export class HMSActionButton extends Component {
  constructor(props) {
    super(props);
    this.state = { viewRef: null };
    Ionic.getImageSource("md-close", 30).then((source) => {
      closeIcon = source;
    });
    Ionic.getImageSource("md-refresh", 30).then((source) => {
      refreshIcon = source;
    });
    this.clickAddEvent = this.clickAddEvent.bind(this);
    this.clickShowAttendance = this.clickShowAttendance.bind(this);
    this.clickAddFeedback = this.clickAddFeedback.bind(this);
  }

  imageLoaded() {
    this.setState({ viewRef: findNodeHandle(this.refs.backgroundImage) });
  }

  shouldComponentUpdate(nextProps, nextState) {
    console.log("In Should Comp Update");
    return true;
  }

  clickAddEvent = (Event, navigate, eventtype) => () => {
    Navigation.showModal({
      stack: {
        children: [
          {
            component: {
              name: `nav.Three60Memos.${navigate}`,
              passProps: {
                eventid: null,
                eventtype,
              },
              options: {
                topBar: {
                  visible: true,
                  title: {
                    text: Event,
                    color: "white",
                  },
                  background: {
                    color: EnvConfig.MAIN_HOME_COLOR,
                  },
                  rightButtons: [
                    {
                      id: "closebutton",
                      icon: closeIcon,
                      color: "white",
                    },
                  ],
                },
              },
            },
          },
        ],
      },
    });
  };

  clickAddPost = () => {
    Navigation.showModal({
      stack: {
        children: [
          {
            component: {
              name: "nav.Three60Memos.PostScreen",
              passProps: {
                isNewPost: true,
              },
              options: {
                topBar: {
                  visible: false,
                  height: 0,
                  title: {
                    text: "New Post",
                    color: "white",
                  },
                  background: {
                    color: EnvConfig.MAIN_HOME_COLOR,
                  },
                  rightButtons: [
                    {
                      id: "closebutton",
                      icon: closeIcon,
                      color: "white",
                    },
                  ],
                },
              },
            },
          },
        ],
      },
    });
  };

  clickShowAttendance = () => {
    Navigation.showModal({
      stack: {
        children: [
          {
            component: {
              name: "nav.Three60Memos.AttendanceSummaryList",
              passProps: {},
              options: {
                topBar: {
                  visible: true,
                  title: {
                    text: "Attendance Summary",
                    color: "white",
                  },
                  background: {
                    color: EnvConfig.MAIN_HOME_COLOR,
                  },
                  backgroundColor: "#202679",
                  rightButtons: [
                    {
                      id: "closebutton",
                      icon: closeIcon,
                      color: "white",
                    },
                    {
                      id: "refreshbutton",
                      icon: refreshIcon,
                      color: "white",
                    },
                  ],
                },
              },
            },
          },
        ],
      },
    });
  };

  clickAddFeedback = () => {
    Navigation.showModal({
      stack: {
        children: [
          {
            component: {
              name: "nav.Three60Memos.FeedBackScreen",
              passProps: {},
              options: {
                topBar: {
                  visible: true,
                  title: {
                    text: "Send Feedback",
                    color: "Black",
                  },
                  rightButtons: [
                    {
                      id: "closebutton",
                      icon: closeIcon,
                      color: "black",
                    },
                  ],
                },
              },
            },
          },
        ],
      },
    });
  };
  renderPostButton = () => {
    return (
      <ActionButton.Item
        textStyle={styles.textContainerStyle}
        buttonColor="#9b59b6"
        title="Post"
        spaceBetween={10}
        onPress={this.clickAddPost}
      >
        <Entypo name="new-message" style={styles.actionButtonIcon} />
      </ActionButton.Item>
    );
  };

  renderAttendanceButton = () => {
    console.log("rendering Attendance Menu");
    return (
      <ActionButton.Item
        textStyle={styles.textContainerStyle}
        buttonColor="#22b14c"
        title="Attendance"
        onPress={this.clickShowAttendance}
      >
        <Icon name="check-square-o" style={styles.actionButtonIcon} />
      </ActionButton.Item>
    );
  };
  renderFeedBackButton = () => {
    return (
      <ActionButton.Item
        textStyle={styles.textContainerStyle}
        buttonColor="blue"
        title="Feedback"
        onPress={this.clickAddFeedback}
      >
        <Icon name="envelope" style={styles.actionButtonIcon} />
      </ActionButton.Item>
    );
  };
  renderLogoutMenu = () => {
    return (
      <ActionButton.Item
        textStyle={styles.textContainerStyle}
        buttonColor="blue"
        title="Logout"
        onPress={this.props.logoutUser.bind(this)}
      >
        <MaterialCommunityIcons name="logout" style={styles.actionButtonIcon} />
      </ActionButton.Item>
    );
  };

  renderProfileButton = () => {
    if (Platform.OS === "android") {
      return (
        <ActionButton.Item
          textStyle={styles.textContainerStyle}
          title="Profile"
          buttonColor="orange"
          onPress={this.showProfile.bind(this)}
        >
          <Entypo name="user" style={styles.actionButtonIcon} />
        </ActionButton.Item>
      );
    } else {
      return {};
    }
  };
  renderVolunteerEventMenu = () => {
    return (
      <ActionButton.Item
        textStyle={styles.textAddEventStyle}
        buttonColor="#fff"
        title="Volunteer Event"
        onPress={this.clickAddEvent(
          "Add Volunteer Event",
          "AddVolunteerEventScreen",
          4
        )}
      >
        <Ionic name="md-hand" style={[styles.actionButtonIcon,{color:"blue"}]} />
      </ActionButton.Item>
    );
  };
  renderParentTeacherEventMenu = () => {
    return (
      <ActionButton.Item
        textStyle={styles.textAddEventStyle}
        buttonColor="#fff"
        title="Parent-Teacher Event"
        onPress={this.clickAddEvent(
          "Add Parent-Teacher Event",
          "AddPTEventScreen",
          3
        )}
      >
        <Icon name="group" style={[styles.actionButtonIcon,{color:"blue"}]} />
      </ActionButton.Item>
    );
  };
  renderRsvpEventMenu = () => {
    return (
      <ActionButton.Item
        textStyle={styles.textAddEventStyle}
        buttonColor="#fff"
        title="Rsvp Event"
        onPress={this.clickAddEvent("Add Rsvp Event", "AddRsvpEventScreen", 2)}
      >
        <Icon name="vimeo" style={[styles.actionButtonIcon,{color:"blue"}]} />
      </ActionButton.Item>
    );
  };
  renderGenericEventMenu = () => {
    return (
      <ActionButton.Item
        textStyle={styles.textAddEventStyle}
        buttonColor="#fff"
        title="Event"
        onPress={this.clickAddEvent("Add Event", "AddEventScreen", 1)}
      >
        {this.addeventIcon()}
      </ActionButton.Item>
    );
  };
  addeventIcon = () => {
    return (
      <MaterialIcons
        name="event"
        size={24}
        style={{color:"blue"}}
      />
    );
  };

  addChatIcon = () => {
    return <FontAwesome name="wechat" size={20} style={{ color: "white" }} />;
  };

  busyIcon = () => {
    return (
      <FontAwesome name="hourglass-2" size={20} style={{ color: "white" }} />
    );
  };

  showProfile() {
    console.log("Showing Modal User");
    Navigation.showModal({
      stack: {
        children: [
          {
            component: {
              name: "nav.Three60Memos.MemberDetailsScreen",
              passProps: {
                selfView: true,
                fromTopMenu: true,
              },
              options: {
                topBar: {
                  visible: true,
                  title: {
                    text: "My Profile",
                    color: "Black",
                  },
                  rightButtons: [
                    {
                      id: "closeprofilebutton",
                      icon: closeIcon,
                      color: "black",
                    },
                  ],
                },
              },
            },
          },
        ],
      },
    });
  }

  render() {
    console.log("RENDER->NAVIGATION");

    switch (this.props.parent) {
      case "nav.Three60Memos.HomeScreen":
        console.log(this.props);
        console.log(this.props.isNewsFeedLoading);
        console.log(!this.props.isAppReady);
        console.log(this.props.isNewsFeedLoading || !this.props.isAppReady);
        let hideButton = this.props.isNewsFeedLoading || !this.props.isAppReady;
        var buttonIcon = hideButton === true ? this.busyIcon : null;
        return (
          <ActionButton
            renderIcon={buttonIcon}
            style={{ zIndex: 10 }}
            buttonColor="#e81e61"
            backdrop={
              <CustomBlurView style={styles.absolute} blurAmount={10} />
            }
            // offsetX={(this.props.rootNavprops.deviceWidth / 2) - 27}
            verticalOrientation="up"
          >
            {this.props.userroles !== "attendance"
              ? this.renderPostButton()
              : ""}
            {this.renderCustomMenu()}
            {this.props.userroles !== "parent"
              ? this.renderAttendanceButton()
              : ""}
            {this.props.userroles !== "attendance"
              ? this.renderProfileButton()
              : ""}
            {this.props.userroles !== "attendance"
              ? this.renderFeedBackButton()
              : ""}
            {this.props.userroles === "attendance"
              ? this.renderLogoutMenu()
              : ""}
          </ActionButton>
        );
      case "nav.Three60Memos.EventsScreen":
        if (
          this.props.userroles === "admin" ||
          this.props.userroles === "staff" ||
          this.props.userroles === "teacher"
        ) {
          return (
            // <ActionButton style={{zIndex: 10}}
            //               buttonColor="#e81e61"
            //               renderIcon={this.addeventIcon}
            //               onPress={this.clickAddEvent}
            // />
            <ActionButton
              renderIcon={buttonIcon}
              style={{ zIndex: 10 }}
              buttonColor="#e81e61"
              backdrop={
                <CustomBlurView style={styles.absolute} blurAmount={10} />
              }
              // offsetX={(this.props.rootNavprops.deviceWidth / 2) - 27}
              verticalOrientation="up"
            >
              {this.renderAddEventCustomMenu()}
              {this.props.userroles !== "attendance"
                ? this.renderVolunteerEventMenu()
                : ""}
              {this.props.userroles !== "attendance"
                ? this.renderParentTeacherEventMenu()
                : ""}
              {this.props.userroles !== "attendance"
                ? this.renderRsvpEventMenu()
                : ""}
              {this.props.userroles !== "attendance"
                ? this.renderGenericEventMenu()
                : ""}
            </ActionButton>
          );
        } else {
          return null;
        }

      case "nav.Three60Memos.ChatRoomsScreen":
        return (
          <ActionButton
            style={{ zIndex: 1000, overflow: "visible" }}
            buttonColor="#e81e61"
            elevation={5}
            zIndex={1000}
            renderIcon={this.addChatIcon}
            onPress={this.props.customClick}
          />
        );
      default:
        return null;
    }
  }
  showCustomMenuModal() {
    Navigation.showModal({
      stack: {
        children: [
          {
            component: {
              name: "nav.Three60Memos.CustomViewURL",
              passProps: {},
              options: {
                topBar: {
                  visible: true,
                  background: {
                    color: EnvConfig.MAIN_HOME_COLOR,
                  },
                  title: {
                    text: "Scandi Lunch Menu",
                    color: "white",
                  },
                  rightButtons: [
                    {
                      id: "closebutton",
                      icon: closeIcon,
                      color: "black",
                    },
                  ],
                },
              },
            },
          },
        ],
      },
    });
  }

  renderCustomMenu() {
    console.log(EnvConfig.CUSTOM_MAIN_MENU.length);
    if (EnvConfig.CUSTOM_MAIN_MENU.length > 0) {
      return (
        <ActionButton.Item
          textStyle={styles.textContainerStyle}
          buttonColor="#f1802d"
          title={EnvConfig.CUSTOM_MAIN_MENU[0].Title}
          onPress={this.showCustomMenuModal.bind(this)}
        >
          <MaterialCommunityIcons
            name={EnvConfig.CUSTOM_MAIN_MENU[0].Icon}
            style={styles.actionButtonIcon}
          />
        </ActionButton.Item>
      );
    } else {
      return "";
    }
  }

  renderAddEventCustomMenu() {
    console.log(EnvConfig.CUSTOM_MAIN_MENU.length);
    if (EnvConfig.CUSTOM_MAIN_MENU.length > 0) {
      return (
        <ActionButton.Item
          textStyle={styles.textContainerStyle}
          buttonColor="blue"
          title={EnvConfig.CUSTOM_MAIN_MENU[0].Title}
          onPress={this.showCustomMenuModal.bind(this)}
        >
          <MaterialCommunityIcons
            name={EnvConfig.CUSTOM_MAIN_MENU[0].Icon}
            style={styles.actionButtonIcon}
          />
        </ActionButton.Item>
      );
    } else {
      return "";
    }
  }
  showUrl(url) {}
}

var mapStateToProps = function (store) {
  return {
    isNewsFeedLoading: store.newsfeed.isNewsFeedLoading,
    isAppReady: store.login.AppReady,
  };
};

var mapDispatchToProps = function (dispatch) {
  return {
    // showNewPost: function () {
    //     dispatch(rootNavAction.showNewPost())
    // },
    // hideNewPost: function () {
    //     dispatch(rootNavAction.hideNewPost())
    // },
    showProfileModal: function () {
      dispatch(rootNavAction.showProfileModal());
    },
    showFeedbackModal: function () {
      dispatch(rootNavAction.showFeedbackModal());
    },
    hideProfileModal: function () {
      dispatch(rootNavAction.hideProfileModal());
    },
    showAttendance: function () {
      dispatch(rootNavAction.showAttendance());
    },
    hideAttendance: function () {
      dispatch(rootNavAction.hideAttendance());
    },
    logoutUser: function () {
      dispatch(memberActions.logoutUser());
    },
    // showCustomMenuModal: function () {
    //     dispatch(rootNavAction.showCustomMenuModal())
    // },
    hideCustomMenuModal: function () {
      dispatch(rootNavAction.hideCustomMenuModal());
    },
  };
};

var styles = StyleSheet.create({
  msgCountText: {
    fontSize: 12,
    color: "white",
    backgroundColor: "green",
    marginTop: 22,
    paddingLeft: 1,
    paddingRight: 13,
  },
  actionButtonIcon: {
    fontSize: 20,
    height: 22,
    color: "white",
  },
  absolute: {
    position: "absolute",
    top: 0,
    left: 0,
    bottom: 0,
    right: 0,
  },
  textContainerStyle: {
    width: 100,
    textAlign: "center",
  },
  textAddEventStyle: {
    width: 150,
    textAlign: "center",
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(HMSActionButton);
