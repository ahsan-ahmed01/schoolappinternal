

import React, {Component} from 'react';
import {View, Text, Platform} from 'react-native';
import Icon from 'react-native-vector-icons/Entypo';



import Colors from '../constants/Colors';
import {connect} from 'react-redux';

export  class ChatNotification extends Component {

    constructor (props)
    {
        super(props);
    }

    shouldComponentUpdate(nextProps, nextState) {
        if (this.props.user.totalnotifications !== nextProps.user.totalnotifications){
            return true
        }else{
            return false
        }
    }

    render(){
        messageCount = this.props.user.totalnotifications;
        if (messageCount > 0 && messageCount < 100) {
            if (Platform.OS === 'android') {


                return (

                    <View style={{
                        zIndex: 0,
                        bottom: 10,
                        width: 40
                    }}>
                        <Icon
                            name='chat'
                            size={28}
                            style={{top: 20}}
                            color={this.props.focused ? '#C95165' : '#77808e'}
                        />
                        <View style={{
                            right: -17,
                            flexDirection: 'row',
                            top: 0,
                            left: 10,
                            borderRadius: 7,
                            backgroundColor: 'red',
                            height: 14,
                            width: 14,
                            alignItems: 'center',
                            alignSelf: 'center',
                            justifyContent: "center",
                            zIndex: 2
                        }}>
                            <Text style={{
                                fontSize: 10,
                                color: 'white',
                                alignItems: 'center',
                                alignSelf: 'center',

                            }}>{messageCount}</Text>
                        </View>

                    </View>
                );
            } else {
                return (

                    <View style={{
                        zIndex: 0,
                        bottom : -10
                    }}>
                        <Icon
                            name='chat'
                            size={28}
                            color={this.props.focused ? '#C95165': '#77808e'}
                        />
                        <View style={{
                            right: -17,
                            flexDirection: 'row',
                            top: -25,
                            borderRadius: 7,
                            backgroundColor: 'red',
                            height: 14,
                            width: 14,
                            alignItems: 'center',
                            alignSelf: 'center',
                            justifyContent: "center",
                            zIndex: 2
                        }}>
                            <Text style={{
                                fontSize: 10,
                                color: 'white',
                                alignItems: 'center',
                                alignSelf: 'center'
                            }}>{messageCount}</Text>
                        </View>

                    </View>
                );
            }
        }
        else if (messageCount > 0) {
            return (

                <View style={{
                    zIndex: 0,
                    flex: 1,
                    bottom : 10
                }}>
                    <Icon
                        name='chat'
                        size={28}
                        style={{marginBottom: -3}}
                        color={this.props.focused ? '#C95165' : '#77808e'}
                    />
                    <View style={{
                        right: -17,
                        flexDirection: 'row',
                        top: -25,
                        borderRadius: 10,
                        backgroundColor: 'red',
                        height: 18,
                        width: 18,
                        alignItems: 'center',
                        alignSelf: 'center',
                        justifyContent: "center",
                        zIndex: 2
                    }}>
                        <Text style={{
                            fontSize: 8,
                            color: 'white',
                            alignItems: 'center',
                            alignSelf: 'center'
                        }}>{messageCount}</Text>
                    </View>

                </View>
            );
        }
        else{
            return (
                <Icon
                    name='chat'
                    size={28}
                    style={{marginBottom: -3}}
                    color={this.props.focused ? '#C95165' : '#77808e'}
                />
            )
        }
    }
}


var mapStateToProps = function (store) {
    return {user: store.user}
}

export default connect(mapStateToProps)(ChatNotification)