import React, {Component} from 'react';
import {connect} from 'react-redux'
import MainTabNavigator from './MainTabNavigator';
import * as loginActions from '../actions/loginActions'
import {
    StyleSheet,
    Text,
    TouchableOpacity,
    View,Modal,NetInfo, YellowBox
} from 'react-native';
import LoginScreen from '../screens/login'
import {commonStyle} from '../config/commonstyles'
import ActionButton from 'react-native-action-button';
import Entypo from 'react-native-vector-icons/Entypo';
import Ionic from 'react-native-vector-icons/Ionicons';
import CustomBlurView from '../components/CustomBlurView'
import MaterialIcons from 'react-native-vector-icons/MaterialIcons'
import HMSActionButton from './HMSActionButton'
import * as rootNavActions from '../actions/rootNavActions'
import NotificationsUtil from '../utils/notifications'
import firebase from 'firebase'
import * as _ from 'lodash'

class RootNavigator extends Component {

    _onLayout = event => this.props.appLayout(event.nativeEvent.layout);
    componentDidMount(){

        NetInfo.isConnected.addEventListener('connectionChange', this._handleConnectionChange.bind(this));

      if (!this.props.login.logincredvalidated) {
          if ((this.props.login.cred.userid !== '') && (this.props.login.cred.password !== '')){
              this.props.authenticate(this.props.login.cred.userid, this.props.login.cred.password)
              //this.startDisableAppListener()
          }else{
              if ((this.props.login.cred.emailaddress !== '') && (this.props.login.cred.password !== '')){
                  this.props.authenticate(this.props.login.cred.emailaddress, this.props.login.cred.password)

              }
          }
      }
      //Ignore Yellow Warnings
      this.ignoreWarnings()

  }
    constructor(props){
        super(props)
        //slowlog(this, /.*/)

    }
    _handleConnectionChange(connectionInfo) {
        console.log('Connection Status' + connectionInfo)
        if (connectionInfo){
            this.props.hideNoNetworkModal()
        }else{
            this.props.showNoNetworkModal()
        }
    }

  ignoreWarnings(){
      YellowBox.ignoreWarnings = (['Setting a timer',
          'Debugger and device',
          'Could not find image','Possible Unhandled Promise Rejection']);

  }

  renderNoNetworkModal(){
      if (this.props.rootNavprops.noNetworkModal){
          return (
              <Modal transparent={true}
                     onRequestClose={() => {
                         console.log('Modal has been closed.');
                     }}
                     animationType='slide' >
                  <CustomBlurView>
                      <View style={{flex : 1, paddingTop: 140, flexDirection:'column'}}>
                          <MaterialIcons size={30} name="network-check" style={{color : 'red',  alignSelf:'center'}}/>
                          <Text style={styles.msgText}>It seems you are not connected to the Internet. Please connect to the internet to use the App</Text>
                      </View>
                  </CustomBlurView>
              </Modal>
          )
      }
  }

  startDisableAppListener(){
      console.log('Starting Status Listener')
      firebase.database().ref('/status/status2019/').on('value', function (snapshot) {
          console.log('----- GOT STATUS -------------')
          var data = snapshot.val()
          console.log(data)
          console.log(data)
          console.log(data.disable === "True")
          if (data.disable === "True"){
              this.props.disableApp(data.disabletext)
          }
          else{
              this.props.enableApp()
          }


      }, this)
  }

    componentDidUpdate(prevProps, prevState){
      console.log(prevProps)
        console.log(this.props)
      if (prevProps.login.FirebaseConnected === false && this.props.login.FirebaseConnected === true){
          this.startDisableAppListener()
      }


    }

    componentWillUpdate(nextProps, nextState){
        console.log(_.difference(this.props, nextProps));
        console.log(_.difference(this.state, nextState));
    }

    renderdisableApp(){
      console.log('In Disable')
        console.log(this.props.rootNavprops.disableApp)
        if (this.props.rootNavprops.disableApp){
        //if (false){
          return(
          <Modal transparent={true}
                 onRequestClose={() => {
                     console.log('Modal has been closed.');
                 }}
                 animationType='slide' >
              <CustomBlurView >
                  <View style={{flex : 1, paddingTop: 140, flexDirection:'column'}}>
                      <Ionic size={40} name="md-warning" style={{color : 'red',  alignSelf:'center'}}/>
                      <Text style={styles.msgText}>{this.props.rootNavprops.disableText}</Text>
                  </View>
              </CustomBlurView>
          </Modal>)
      }
    }

  render() {
      console.log(this.props)

      if (!this.props.login.logincredvalidated) {
          console.log('Not Logged in ...')
          return (
              <View style={{flex:1}}>
                  {this.renderNoNetworkModal()}
                  {this.renderdisableApp()}
              <LoginScreen/>
              </View>
          );
      }
      else {
          return (
              <View style={{flex : 1}} onLayout={this._onLayout}>
                  {this.renderNoNetworkModal()}
                  {this.renderdisableApp()}
                  <MainTabNavigator  style={{flex : 1}} />
                  <HMSActionButton userroles={this.props.user.roles}/>

              </View>);
      }

  }

    onShowSignIn(){

    }


}

const styles = StyleSheet.create({
    container: {
        flex: 1,

    },

    actionButtonIcon: {
        fontSize: 20,
        height: 22,
        color: 'white',
    },
    msgText : {
        fontSize : 20,
        color: 'blue',
        alignContent:'center',
        padding : 20,
        textAlign : 'center'
    }
});

var mapStateToProps = function (store) {
    return {
        login: store.login,
        user: store.user,
        rootNavprops: store.rootNavprops
    };
};

var mapDispatchToProps = function (dispatch) {
    return {
        authenticate: function (username, password, userobj) {
            dispatch(loginActions.authenticateUser(username, password, userobj));
        },
        checkTokenValidity: function (token) {
            dispatch(loginActions.checkTokenValidity(token))
        },
        setLoginModalStatus: function (modalStatus) {
            dispatch(loginActions.setLoginModalStatus(modalStatus))
        },
        showNoNetworkModal: function () {
            dispatch(rootNavActions.showNoNetworkModal())

        },
        hideNoNetworkModal: function () {
            dispatch(rootNavActions.hideNoNetworkModal())

        },
        enableApp: function () {
            dispatch(rootNavActions.enableApp())
        },
        disableApp: function (disableText) {
            dispatch(rootNavActions.disableApp(disableText))
        },
                  appLayout: function (event) {
                  dispatch(rootNavActions.appLayout(event))
              }

    }
};

export default connect(mapStateToProps, mapDispatchToProps)(RootNavigator)