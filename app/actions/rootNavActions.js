'use strict'

// export function showNewPost() {
//     return {
//         type: 'SHOW_NEW_POST',
//         payload: {}
//     }
// }


export function showProfileModal() {
    return {
        type: 'SHOW_PROFILE',
        payload: {}
    }
}

// export function showProfileModalforUser(User, fromTopMenu) {
//
//     return {
//         type: 'SHOW_PROFILE_USER',
//         payload: {
//             user: User,
//             fromTopMenu : fromTopMenu
//         }
//     }
// }
//
// export function hideProfileModal() {
//     return {
//         type: 'HIDE_PROFILE',
//         payload: {}
//     }
// }

export function showNoNetworkModal() {
    return {
        type: 'LOSS_OF_NETWORK',
        payload: {}
    }
}

export function hideNoNetworkModal() {
    return {
        type: 'RESUME_NETWORK',
        payload: {}
    }
}

export function updateCurrentTab(tabname,prevtabname, token) {
    return {
        type: 'SET_CURRENT_TAB',
        payload: {
            tabname : tabname,
            prevtab : prevtabname,
            token : token
        }
    }
}

export function disableApp(disableText) {
    return {
        type: 'DISABLE_APP',
        payload: {
            disableText
        }
    }
}

export function enableApp() {
    return {
        type: 'ENABLE_APP',
        payload: {

        }
    }
}

export function appLayout(event){
    return {
        type : 'DEVICE_LAYOUT_CHANGED',
        payload : {
            event : event
        }
    }
}

export function showFeedbackModal(){
    return {
        type : 'SHOW_FEEDBACK_MODAL',
        payload : {

        }
    }
}

export function hideFeedbackModal(){
    return {
        type : 'HIDE_FEEDBACK_MODAL',
        payload : {

        }
    }
}
export function showCustomMenuModal(){
    return {
        type : 'SHOW_CUSTOMMENU_MODAL',
        payload : {

        }
    }
}

export function hideCustomMenuModal(){
    return {
        type : 'HIDE_CUSTOMMENU_MODAL',
        payload : {

        }
    }
}

export function showAttendance(){
    return {
        type : 'SHOW_ATTENDANCE_MODAL',
        payload : {

        }
    }
}

export function hideAttendance(){
    return {
        type : 'HIDE_ATTENDANCE_MODAL',
        payload : {

        }
    }
}

