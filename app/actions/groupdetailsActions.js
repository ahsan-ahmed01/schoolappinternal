'use strict'
import axios from 'axios'
var EnvConfig = require('../config/environment')
import { UploadPhoto} from '../api/uploadmedia'
import {  API } from 'aws-amplify'

export function viewGroupDetails (selectedGroup, token) {
    return function (dispatch) {
        dispatch({type: 'GET_GROUP_DETAILS', payload: {selectedGroup}})

    }
}

export function _WP_getGroupPhotos (selectedGroup, token) {
  return function (dispatch) {
    var headers = {
      headers: {
        'Authorization': 'Bearer ' + token
      },
      params: {
        'groupid': selectedGroup.id
      }
    }
    dispatch({type: 'GET_GROUP_PHOTOS', payload: {selectedGroup}})
    axios
            .get(EnvConfig.GROUP_PHOTOS_URL, headers)
            .then((response) => {
              dispatch({type: 'GET_GROUP_PHOTOS_FULFILLED', payload: response.data})
            })
            .catch((err) => {
              dispatch({type: 'GET_GROUP_PHOTOS_REJECTED', payload: err})
            })
  }
}

export function uploadCoverPhoto(image, groupid, token){
    return {
        type: 'SET_GROUP_COVER_PHOTO',
        payload: {
            image, groupid, token
        }
    }
}

export function uploadGroupProfilePhoto(image, groupid, token){
    return {
        type: 'SET_GROUP_PROFILE_PHOTO',
        payload: {
            image, groupid, token
        }
    }
}

