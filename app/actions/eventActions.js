"use strict";
import firebase from "firebase";
import * as eventsapi from "../api/eventsapi";
import { UploadPhoto } from "../api/uploadmedia";

export function addEventAction(
  formData,
  selectedUsers,
  displayName,
  token,
  eventimgurl,
  groups,
  eventtype,
  eventid = null,
  parentTeacherSlots,
  volunteer,
  spot,
  volunteertype,
  daterangetype,
  daterange,
  selectedDays,
  slotTime,
  dates
) {
  console.log(
    eventtype,
    eventid,
    parentTeacherSlots,
    volunteer,
    spot,
    volunteertype,
    daterangetype,
    daterange,
    selectedDays,
    slotTime,
    dates,
    "------eventtype---------------"
  );
  return (dispatch) => {
    dispatch({
      type: "ADD_EVENT",
      payload: {
        formData,
        selectedUsers,
        displayName,
        token,
        eventimgurl,
        groups,
        eventtype,
        eventid,
        parentTeacherSlots,
        volunteer,
        spot,
        volunteertype,
        daterangetype,
        daterange,
        selectedDays,
        slotTime,
        dates,
      },
    });
  };
}

export function setUser(user) {
  return (dispatch) => {
    dispatch({ type: "SET_EVENTS_USER", payload: { user } });
  };
}

export function getEventsAction(token, groups) {
  return (dispatch) => {
    dispatch({ type: "GET_EVENTS", payload: { token, groups } });
  };
}

export function clearEventsAction(token) {
  return (dispatch) => {
    dispatch({ type: "CLEAR_EVENTS", payload: { token } });
  };
}

export function clearLastRefreshTime() {
  return (dispatch) => {
    dispatch({ type: "CLEAR_LAST_REFRESHTIME" });
  };
}

export function setLocationforToday(ylocation) {
  return (dispatch) => {
    dispatch({ type: "SET_TODAY_LOCATION", payload: { ylocation } });
  };
}

export function getCalendarConfig() {
  return (dispatch) => {
    var calendarconfig = firebase.database().ref("/Config/Calendar/");
    calendarconfig.once("value", function (snap) {
      dispatch({ type: "SET_CALENDAR_CONFIG", payload: snap.val() });
    });
  };
}

export function deleteSignupforChild(event_id, child_id, slotId, token) {
  return (dispatch) => {
    dispatch({
      type: "DELETE_SIGNUP_CHILD_EVENT",
      payload: { event_id, child_id, slotId, token },
    });
  };
}

export function deleteEventAction(eventid, token, groups) {
  return (dispatch) => {
    dispatch({ type: "DELETE_EVENT", payload: { eventid, token, groups } });
  };
}
