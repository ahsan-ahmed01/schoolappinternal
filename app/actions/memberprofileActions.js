'use strict'
import axios from 'axios'
import {Alert} from 'react-native'
var EnvConfig = require('../config/environment')
import * as userActions from './userActions'
import * as loginActions from './loginActions'
import * as firebaseapi from '../api/firebaseapi'
import * as loginapi from '../api/loginapi'
import * as userapi from '../api/userapi'
import {Storage} from 'aws-amplify'
import RNFetchBlob from 'react-native-fetch-blob';
import { Buffer } from 'buffer'
import { RNS3 } from 'react-native-aws3'
export function viewMyProfile (userObj) {
  return {
    type: 'VIEW_PROFILE',
    payload: {
      userObj: userObj
    }
  }
}

export function _wp_uploadMyProfilePhoto (image, user, token) {
  console.log('in Action')
  console.log(image)
  return function (dispatch) {
    var photo = {
      uri: image.path,
      type: 'image/jpeg',
      name: 'photo.jpg'
    }
    var body = new FormData()
    body.append('image', photo)
    dispatch({type: 'UPLOAD_PROFILE_PHOTO', payload: {image}})
    axios
            .post(EnvConfig.PROFILE_PHOTO_URL,
            body, {
              headers: {
                'Authorization': 'Bearer ' + token,
                'Content-Type': 'multipart/FormData'
              }
            })
            .then((response) => {
              console.log(response)
              dispatch(userActions.updateProfileImage(response.body.postResponse.location))
              dispatch({type: 'UPLOAD_PROFILE_PHOTO_FULFILLED', payload: response.data})

            })
            .catch((err) => {
              dispatch({type: 'UPLOAD_PROFILE_PHOTO_REJECTED', payload: err})
            })
  }
}
function readFile(filePath) {
    console.log(filePath)
    RNFetchBlob.fs.exists(filePath)
        .then((exist) => {
            console.log(`file ${exist ? '' : 'not'} exists`)
        })
    return RNFetchBlob.fs.readFile(filePath, 'base64').then(data => new Buffer(data, 'base64'));
}

export function uploadMyProfilePhoto (image, user, oldurl, token){
    console.log(user)
    return function (dispatch) {
        const options = {
            bucket: EnvConfig.AWS_CONFIG.S3_PROFILE_BUCKET,
            region: EnvConfig.AWS_CONFIG.S3_BUCKET_REGION,
            accessKey: EnvConfig.AWS_CONFIG.S3_PROFILE_ACCESS_KEY,
            secretKey: EnvConfig.AWS_CONFIG.S3_PROFILE_SECRET_KEY,
            successActionStatus: 201
        }
        console.log(options)
        var filename = 'Profile/' + user.username + '_' +  Date.now() +  '.jpg'
        const file = {
            uri: image.path,
            type: 'image/jpeg',
            name: filename
        }
        console.log(file)
        dispatch({type: 'UPLOAD_PROFILE_PHOTO', payload: {image}})
            console.log('Putting File')
            var response = RNS3.put(file, options).then(response => {
                console.log(response)

                if (response.status !== 201) {
                    throw new Error('Failed to upload image to S3')
                }
                console.log(response)
                var url = EnvConfig.AWS_CONFIG.S3_PROFILE_URL + filename
                console.log(url)

                var response2 = loginapi.updateUserProfilePicture(url,oldurl, token)
                console.log(response2)
                dispatch(userActions.updateProfileImage(response.body.postResponse.location))
                dispatch(loginActions.updateProfilePicture(user.username, response.body.postResponse.location))
                dispatch({type: 'UPLOAD_PROFILE_PHOTO_FULFILLED', payload: response})
                return response.body
            })
                .catch(error => {
                    dispatch({type: 'UPLOAD_PROFILE_PHOTO_REJECTED', payload: error})
                    return error
                })
            console.log(response)
            return response

    }
}

export function uploadChildProfilePhoto (image, childid, token){
    return function (dispatch) {
        const options = {
            bucket: EnvConfig.AWS_CONFIG.S3_PROFILE_BUCKET,
            region: EnvConfig.AWS_CONFIG.S3_BUCKET_REGION,
            accessKey: EnvConfig.AWS_CONFIG.S3_PROFILE_ACCESS_KEY,
            secretKey: EnvConfig.AWS_CONFIG.S3_PROFILE_SECRET_KEY,
            successActionStatus: 201
        }
        console.log(options)
        var filename = 'Profile/' + childid + '_' +  Date.now() +  '.jpg'
        const file = {
            uri: image.path,
            type: 'image/jpeg',
            name: filename
        }
        console.log(file)
        dispatch({type: 'UPLOAD_CHILD_PROFILE_PHOTO', payload: {image}})
        console.log('Putting File')
        var response = RNS3.put(file, options).then(response => {
            console.log(response)

            if (response.status !== 201) {
                throw new Error('Failed to upload image to S3')
            }
            console.log(response)
            var url = EnvConfig.AWS_CONFIG.S3_PROFILE_URL + filename
            console.log(url)

            return userapi.updateChildProfilePicture(url,childid, token).then((response2) => {
                console.log(response2)
                if (response2.status === 200) {
                    console.log(response2)
                    dispatch({type: 'UPLOAD_CHILD_PROFILE_PHOTO_FULFILLED', payload: {childid, url}})
                }else{
                    Alert.alert('Error', 'Error Uploading Picture. Please try again', [{text: 'OK', onPress: () => {}}])
                }
                return response.body
            })

        })
            .catch(error => {
                Alert.alert('Error', 'Error Uploading Picture. Please try again', [{text: 'OK', onPress: () => {}}])
                dispatch({type: 'UPLOAD_CHILD_PROFILE_PHOTO_REJECTED', payload: error})
                return error
            })
        console.log(response)
        return response

    }
}

export function logoutUser (logoutfirebase : true) {
    if (logoutfirebase) {
        console.log('Logging out of Firebase')
        firebaseapi.DeInit()
    }
    return function (dispatch) {
        dispatch({type: 'LOGOUT_USER', payload: null})
    }
  }

export  function saveAuthorizedUser(name, childid, code, token) {
    return function (dispatch) {
        dispatch({type: 'SET_CHILD_AUTHORIZED_USER', payload: {name, childid, code, token}})
    }
}

export  function deleteAuthorizedUser(name, childid, code, token) {
    return function (dispatch) {
        dispatch({type: 'DELETE_CHILD_AUTHORIZED_USER', payload: {name, childid, code, token}})
    }
}

