'use strict'
import * as firebase from 'firebase'
import * as utils from '../utils/misc'
export function loadingGroups () {
  return function (dispatch) {
    dispatch({type: 'LOADING_MESSAGE_GROUPS', payload: {}})
  }
}

export function getChatGroups2 (userid) {
    return function (dispatch) {
        var roomRef = firebase.database().ref('/Users/' + userid + '/rooms')
        var starttime=Date.now()
        roomRef.on('value', function (snap) {
            
            var roomkeys = snap.val()
            
            for (var roomkey in roomkeys) {
                if (roomkeys[roomkey] !== false)
                    dispatch(getChatGroupDetails(userid, roomkey, roomkeys[roomkey]))
            }
            utils.captureStat('Func','getChatGroups', Date.now()-starttime)
            if (roomkeys === null) {
                dispatch({type: 'STOP_LOADING_GROUPS', payload:{}})
            }
        })

    }
}

async function getRoomData(roomkey){
    
    var roomRef = firebase.database().ref('/Rooms/' + roomkey + '/Details/')
    return roomRef.once('value', function (snap) {
          return snap.val()
        }
    )
}

async function getAllRoomsData(roomkeys, dispatch){
   var roomData = {}
   for (var i = 0; i < roomkeys.length; i++){
     
     var roomkey = roomkeys[i]
       
       var data = await getRoomData(roomkey)
       var tmp = {}
       tmp['key'] = roomkey
       tmp['visibleRoom'] = true
       tmp['Details'] = data.val()
       roomData[roomkey] = tmp
   }
   
    if (Object.keys(roomData).length > 0) {
        dispatch({type: 'LOAD_MESSAGE_ROOMS2', payload:{roomData}})
    }
    return roomData
   // var roomRef = firebase.database().ref('/Rooms/' + roomKey + '/Details/')

}


export function getChatGroups (userid) {
  return function (dispatch) {
      dispatch({type: 'LOADING_MESSAGE_GROUPS', payload:{}})
    var roomRef = firebase.database().ref('/Users/' + userid + '/rooms')
    var starttime=Date.now()
    roomRef.on('value', function (snap) {
      
      var roomkeys = snap.val()
      
        if (roomkeys === null){
            dispatch({type: 'LOAD_MESSAGE_ROOMS2', payload: {roomData : {}}})
        }else {
            var validkeys = []
            for (var roomkey in roomkeys) {
                if (roomkeys[roomkey] !== false) {
                    
                    validkeys.push(roomkey)
                }
            }
            
            var roomData = getAllRoomsData(validkeys, dispatch)
            
            utils.captureStat('Func', 'getChatGroups', Date.now() - starttime)
            if (roomData.length > 0) {
                dispatch({type: 'LOAD_MESSAGE_ROOMS2', payload: {roomData}})
            }
        }
    })

  }
}

export function stopListener (userid) {
  return function (dispatch) {
    var roomRef = firebase.database().ref('/Users/' + userid + '/rooms')
    roomRef.off('value')
  }
}

export function clearGroups () {
  return function (dispatch) {
    dispatch({type: 'CLEAR_ROOMS', payload: {}})
  }
}

export function deletechatroomforUser(userid, roomKey){
    firebase.database().ref('/Users/' + userid + '/rooms/' + roomKey ).remove()
    firebase.database().ref('/Users/' + userid + '/Notifications/Rooms/' + roomKey ).remove()
    
}



export function getChatGroupDetails (userid, roomKey, visibleRoom) {
  
  return(dispatch) => {
    var rooms = {}
    var roomRef = firebase.database().ref('/Rooms/' + roomKey + '/Details/')
    roomRef.once('value').then(function (item) {
      
      var val = item.val()
        if (val===null){
            //deletechatroomforUser(userid,roomKey)
            
        }
      var roomItem = {}
      roomItem['key'] = roomKey
      roomItem['visibleRoom'] = visibleRoom
      roomItem['Details'] = val
      rooms[roomKey] = roomItem
      dispatch({type: 'LOAD_MESSAGE_ROOMS', payload: {rooms: rooms}})
    })
  };
}

export function updateChatsLastMessageTS(rooms) {

  return (dispatch) => {

    Object.keys(rooms).forEach(roomkey => {
      
      if(rooms[roomkey]) {
        var roomRef = firebase.database().ref('/Rooms/' + roomkey + '/Details/')
        roomRef.on('value', function (snap) {
          var roomDetail = snap.val();
          dispatch({ type: 'CHAT_LAST_MESSAGE_TS_UPDATED', roomkey, lastmsgtimestamp: roomDetail.lastmsgtimestamp });
        })
      };

    });
  
  }

}