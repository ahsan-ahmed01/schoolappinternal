'use strict'

export function selectPhotos (photos) {
  return {
    type: 'SELECT_PHOTOS',
    payload: {
      photos
    }
  }
}

export function removeSelectedImage (key) {
  return {
    type: 'REMOVE_SELECTED_PHOTO',
    payload: {
      key
    }
  }
}

export function removeSelectedVideo (key) {
    return {
        type: 'REMOVE_SELECTED_VIDEO',
        payload: {
            key
        }
    }
}

export function setPostText (posttext) {
  return {
    type: 'SET_POST_TEXT',
    payload: {
      posttext
    }
  }
}

export function setGroupid (groupid, groupname) {
  return {
    type: 'SET_GROUP_ID',
    payload: {
      groupid, groupname
    }
  }
}

export function clearSendingPostError () {
  return {
    type: 'CLEAR_POST_REJECTED',
    payload: {
     
    }
  }
}


export function sendPost (postid, postProps,childids,draft_mode,review_mode,reviwer_list,pinned_post,pinned_topic,pinned_until,scheduleDateTime, user,postText, token) {
  if (postid === null){
        return {
        type: 'SEND_POST',
        payload: {
          postProps,childids,draft_mode,review_mode,reviwer_list,pinned_post,pinned_topic,pinned_until,scheduleDateTime, user, postText, token
        }
      }
    }else{
      return {
        type: 'UPDATE_POST',
        payload: {
          postid, postProps,childids,draft_mode,review_mode,reviwer_list,pinned_post,pinned_topic,pinned_until,scheduleDateTime, user, postText, token
        }
      }
    }
}

export function updatePost (activityid, content, imageurls, videos, groupid,token) {
  return {
    type: 'UPDATE_POST',
    payload: {
      activityid, content, imageurls,videos, token, groupid
    }
  }
}

export function deletePost (activityid, groupid, inbriefcase, usergroups, token) {
  return {
    type: 'DELETE_POST',
    payload: {
      activityid, groupid, token,inbriefcase, usergroups, 
    }
  }
}

export function addComment (activityid, comments, token, groupid) {
  return {
    type: 'ADD_COMMENT',
    payload: {
      activityid, comments, token, groupid
    }
  }
}

export function updatePostScreenwithData(postdata){
    return {
        type: 'UPDATE_POST_DATA',
        payload: {
            postdata
        }
    }
}


export function updateComment (activityid,commentid, comments, token, groupid) {
  return {
    type: 'UPDATE_COMMENT',
    payload: {
      activityid, commentid, comments, token, groupid
    }
  }
}

export function deleteComment (commentid, groupid, token) {
  return {
    type: 'DELETE_COMMENT',
    payload: {
      commentid, token, groupid
    }
  }
}

export function selectVideos(videos) {
  return {
    type : 'SELECT_VIDEOS',
      payload : {
        videos
      }
  }
}

export function removeAllSelectedMedia() {
  return {
    type : 'REMOVE_ALL_SELECTED_MEDIA',
  }
}

export function loadReviewerList(groupid) {
    return {
      type : 'LOAD_REVIEW_LIST',
        payload : {
        groupid
        }
    }
}