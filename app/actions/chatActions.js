'use strict'
import * as firebase from 'firebase'
import * as firebaseapi from '../api/firebaseapi'
import {Alert} from 'react-native'
//import RNFetchBlob from 'react-native-fetch-blob'
// //const polyfill = RNFetchBlob.polyfill
// window.XMLHttpRequest = polyfill.XMLHttpRequest
// window.Blob = polyfill.Blob

import * as chatRoomActions from './chatRoomActions'
var EnvConfig = require('../config/environment')
import {UploadPhotoPublic, UploadPhotoforChat} from '../api/uploadmedia'

var messagecallbackfn = null

export function loadMessages (roomkey) {
  return dispatch => {
    var msgRef = firebase.database().ref('/Rooms/' + roomkey + '/Messages/').orderByChild('orderBy')
    msgRef.once('value', function (snap) {
      var msgs = []
      snap.forEach(function (child) {
        msgs.push(child.val())
          
      })

      dispatch({type: 'LOAD_ALL_MSGS', payload: {msgs, roomkey}})
    })
  }
}

export function writeChatMessage (roomkey, message, users, myuserid, mydisplayname, sendNotification = true) {
  
    
    
  var msgs = message[0]
  var now = new Date().getTime()
  msgs['orderBy'] = now
  var updatedUserData = {}
  var msgkey = msgs._id
  if (msgkey.length <= 0) {
    msgkey = firebase.database().ref('/Rooms/' + roomkey + '/Messages/').push().key
  }
  updatedUserData['/Rooms/' + roomkey + '/Messages/' + msgkey] = msgs
  
    
  // update room's lastmsgtimestamp
  var roomDetailsRef = firebase.database().ref('/Rooms/' + roomkey + '/Details/')
  roomDetailsRef.update({ lastmsgtimestamp: now }, function (error) {
    
  })

  // Added Notifications to Users
  var notifyUsers = [];
  for (var user in users) {
    
    if (user !== myuserid) {
      notifyUsers.push(user)
      var ref = firebase.database().ref('/Users/' + user + '/Notifications/Rooms/' + roomkey)
      ref.transaction(function (data) {
        
          
        data = data === null ? 1 : data + 1
        return data
      })
        
        //If User has the room visibility as false. Enable it. Its for the first message to room
        updatedUserData['/Users/' + user + '/rooms/' + roomkey] = true;

    }
  }
  firebase.database().ref().update(updatedUserData, function (error) {
        if (error) {
            
        }
    })
  if (sendNotification) {
    
    var textmsg = mydisplayname + ' sent a Chat message'
    updatedUserData['/Notifications/Rooms/' + roomkey + '/' + msgkey + '/User'] = myuserid
    updatedUserData['/Notifications/Rooms/' + roomkey + '/' + msgkey + '/Title'] = textmsg
    updatedUserData['/Notifications/Rooms/' + roomkey + '/' + msgkey + '/Notify'] = notifyUsers
      updatedUserData['/Notifications/Rooms/' + roomkey + '/' + msgkey + '/Message'] = message[0].text

  }
  firebase.database().ref().update(updatedUserData, function (error) {
    if (error) {
      
    }
  })
  return dispatch => {
    dispatch({type: 'ADD_PENDING_CHAT_MESSAGE', payload: {msgs, roomkey}})
  }
}



export function startListener (roomkey, lastmsgtimestamp = 0) {
  return dispatch => {
      
      dispatch({type: 'CHATROOM_START_LISTENER', payload: {roomkey}})
    // TODO: Change to child_added so not all messages are sent !!!
    var ref = firebase.database().ref('/Rooms/' + roomkey)
    var msgRef = ref.child('Messages').orderByChild('orderBy').limitToLast(EnvConfig.MESSAGES_PER_REFRESH).startAt(lastmsgtimestamp)
    messagecallbackfn = msgRef.on('child_added', function (snap) {
      
      

      dispatch({type: 'ADD_CHAT_MESSAGE', payload: {msgs: snap.val(), roomkey}})


    })
    var usersRef = ref.child('Details').child('users')
    usersRef.on('child_added', function (newuser) {
      dispatch({ type: 'MEMBER_ADDED', roomkey, userid: newuser.key, user: newuser.val() })
    })
    usersRef.on('child_removed', function (removeduser) {
      dispatch({ type: 'MEMBER_REMOVED', roomkey, userid: removeduser.key })
    })
  }
}

export function stopListener (roomkey, lastmsgtimestamp = 0) {
    
    var ref = firebase.database().ref('/Rooms/' + roomkey)
    var msgRef = ref.child('Messages').orderByChild('orderBy').limitToLast(EnvConfig.MESSAGES_PER_REFRESH).startAt(lastmsgtimestamp)
      
    msgRef.off('child_added')
    return {type: 'CHATROOM_STOP_LISTENER', payload: {roomkey}}
}

export function getMoreMessages (roomkey, lasttimestamp) {
  return dispatch => {
    dispatch({type: 'LOAD_MORE_MESSAGES', payload: {}})
    var ref = firebase.database().ref('/Rooms/' + roomkey)
    if (lasttimestamp !== null) {
      var msgRef = ref.child('Messages').orderByChild('orderBy').limitToLast(EnvConfig.MESSAGES_PER_REFRESH + 1).endAt(lasttimestamp)
    } else {
      var msgRef = ref.child('Messages').orderByChild('orderBy').limitToLast(EnvConfig.MESSAGES_PER_REFRESH + 1)
    }
    msgRef.once('value', function (snap) {
      var msgs = []
      snap.forEach(function (childSnapshot) {
        msgs.unshift(childSnapshot.val())
      })
      msgs.shift()
      dispatch({type: 'ADD_PREVIOUS_MESSAGES', payload: { roomkey, msgs }})
    })
  }
}

export function leaveChat (userid, roomkey) {
  

  return (dispatch, getState) => {
    var { user: { display_name } } = getState()

    // add message in chat room indicating user has gone
    var removeUserMessageKey = firebase.database().ref('/Rooms/' + roomkey + '/Messages/').push().key
    firebase.database().ref('/Rooms/' + roomkey + '/Messages/' + removeUserMessageKey).set({
      _id: removeUserMessageKey,
      orderBy: new Date().getTime(),
      text: `${display_name} left the chat`,
      user: {
        _id: 'system'
      }
    })

    // Remove:
    // Users/rooms/
    var userRoomRef = firebase.database().ref('/Users/' + userid + '/rooms/')
    userRoomRef.child(roomkey).remove()

    // Users/Notifications/Rooms
    var userNotificationsRef = firebase.database().ref('/Users/' + userid + '/Notifications/Rooms')
    userNotificationsRef.child(roomkey).remove()

    // Rooms/Details/users
    var roomDetailsRef = firebase.database().ref('/Rooms/' + roomkey + '/Details')
    roomDetailsRef.child('users').once('value', function (snap) {
      var userids = Object.keys(snap.val())

      // //Chats
      // if(userids.length === 2) {
      //   var chatsRef = firebase.database().ref('/Chats');
      //   userRoomRef.child(userStr).remove();
      // }
      
      userids.sort(function (a, b) { return a - b })
      var olduserStr = userids.join('-')

        userids = userids.filter(id => { return (id !== userid) })
      userids.sort(function (a, b) { return a - b })
      var userStr = userids.join('-')
      roomDetailsRef.child('users').child(userid).remove()


        firebaseapi.updateRoomHashKey(olduserStr, userStr, roomkey)
      // Rooms/Details/userkey
      roomDetailsRef.update({ userkey: userStr }, function (error) {
        
      })
    })


    // dispatch(getChatGroups(userid));

    dispatch({ type: 'USER_LEFT', roomkey })
  }
}

export function finishPickPhotoviaAWS (path, roomkey, token) {
  return (dispatch, getState) => {
      UploadPhotoPublic(path).then((url) => {
          
          if (url !== '') {

              var {
                  user: {userid, display_name, profile_photo},
                  chatrooms: {rooms}
              } = getState()
              var users = rooms[roomkey].Details.users
              var messageId = 'temp-id-' + Math.round(Math.random() * 1000000)
              var message = {
                  _id: messageId,
                  createdAt: new Date(),
                  orderBy: new Date().getTime(),
                  user: {
                      _id: userid,
                      avatar: profile_photo,
                      name: display_name
                  },
                  image: url

              }
          }else{
            Alert.alert('Error','Error Upload Image !')
          }

    writeChatMessage(roomkey, [message], users, userid, display_name, true)
  })
}
}

export function finishPickPhotoviaAWSOld (path, roomkey, userid) {
  return (dispatch, getState) => {
    UploadPhotoforChat(path, userid).then((response) => {
      
      var {
      user: { id, display_name, profile_photo },
      chatrooms: { rooms }
    } = getState()
      var users = rooms[roomkey].Details.users
      var messageId = 'temp-id-' + Math.round(Math.random() * 1000000)
      var message = {
        _id: messageId,
        createdAt: new Date(),
        orderBy: new Date().getTime(),
        user: {
          _id: id,
          avatar: profile_photo,
          name: display_name
        },
        image: response.postResponse.location
      }

      writeChatMessage(roomkey, [message], users, id, display_name, true)
    })
  }
}

// export function finishPickPhoto (path, roomkey) {
//   return (dispatch, getState) => {
//     var {
//       user: { id, display_name, profile_photo },
//       chatrooms: { rooms }
//     } = getState()
//
//     var users = rooms[roomkey].Details.users
//     var messageId = 'temp-id-' + Math.round(Math.random() * 1000000)
//     var imageId = `${roomkey}_${id}_${messageId}`
//
//     Blob.build(RNFetchBlob.wrap(path), { type: 'image/jpeg' })
//     .then((blob) =>
//       firebase.storage()
//       .ref()
//       .child(`images/${imageId}.jpg`)
//       .put(blob, { contentType: 'image/png' })
//     )
//     .then((snapshot) => {
//       var message = {
//         _id: messageId,
//         createdAt: new Date(),
//         orderBy: new Date().getTime(),
//         user: {
//           _id: id,
//           avatar: profile_photo,
//           name: display_name
//         },
//         image: snapshot.downloadURL
//       }
//
//       writeChatMessage(roomkey, [message], users, id, display_name, true)
//
//       // dispatch({ type: 'PICK_PHOTO_FINISED', roomkey, message });
//     })
//
//     // dispatch({ type: 'PHOTO_ATTACHED' });
//   }
// }

export function inviteUsersToChat (roomkey, selectedUsers, oldselectedusers) {
  return (dispatch) => {
    var users = Object.values(selectedUsers)
    
    //convert the key from having @ and .com to the username field
    var newusers = {}
    users.forEach(user => {
      newusers[user.userid] = user
    })
    
    var oldusers = Object.keys(oldselectedusers)
    
      oldusers.sort()
    var olduserStr = oldusers.join('-')

    var allusersdict = Object.assign({}, newusers, oldselectedusers);
    
      var allusers =  Object.keys(allusersdict)
      

      allusers.sort()
    var alluserstr = allusers.join('-')

    
    users.forEach(user => {
      var { userid, display_name, profile_photo } = user

      var roomDetailsRef = firebase.database().ref('/Rooms/' + roomkey + '/Details')

      roomDetailsRef.child('users').child(userid).set({
        display_name,
        profile_photo,
        joinedTs: Date.now()
      })

      roomDetailsRef.child('users').once('value', function (snap) {
        var userStr = Object.keys(snap.val())
          .sort(function (a, b) { return a - b })
          .join('-')
        
        roomDetailsRef.update({ userkey: userStr }, function (error) {
          
        })

      })

      var userRoomsRef = firebase.database().ref('/Users/' + userid + '/rooms')
      userRoomsRef.child(roomkey).set(false)

    })
    
    
    firebaseapi.updateRoomHashKey(olduserStr, alluserstr, roomkey)

    dispatch({ type: 'MEMBER_INVITED', roomkey, users: newusers })
  }
}
