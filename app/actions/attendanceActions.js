'use strict';

export function validatepin(childobj, pincode, attendancedate,checkinMode, token) {
    return dispatch => {
        dispatch({type: 'ATTENDANCE_VALIDATE_PIN', payload: {childobj, pincode, attendancedate, checkinMode,token}})
    }
}

export function validatepinClear() {
    return dispatch => {
        dispatch({type: 'ATTENDANCE_VALIDATE_PIN_CLEAR', payload: {}})
    }
}

export function initAttendance(attendancedate, groupid) {
    return dispatch => {
        dispatch({type: 'INIT_ATTENDANCE', payload: {attendancedate, groupid}})
    }
}

export function setAttendanceoverride(attendancedate, groupid, childobj, status, token) {
    return dispatch => {
        dispatch({type: 'ATTENDANCE_SET_STATUS_OVERRIDE', payload: {attendancedate, groupid, childobj, status, token}})
    }
}

export  function checkoutOverride(attendancedate, groupid, childobj,overrideuser,token) {
    return dispatch => {
        dispatch({type: 'ATTENDANCE_CHECKOUT_OVERRIDE', payload: {attendancedate, groupid, childobj,overrideuser,token}})
    }
}

export  function getAuthorizedUsers(childobj,token) {
    return dispatch => {
        dispatch({type: 'ATTENDANCE_AUTHORIZED_USERS', payload: {childobj,token}})
    }
}

export  function  initAttendanceList(groups, attendancedate,  token) {
    return dispatch => {
        dispatch({type: 'ATTENDANCE_INIT_LIST', payload: {groups,token, attendancedate}})
    }
}

export  function  refreshAttendance(groups, attendancedate,  token) {
    return dispatch => {
        dispatch({type: 'ATTENDANCE_STATUS_REFRESH', payload: {groups,token, attendancedate}})
    }
}