'use strict'
import firebase from 'firebase'
import OneSignal from 'react-native-onesignal'
var bugsnag = require('../config/bugsnag')


export function UnregisterGroupsinOneSignal (userObj){
  // OneSignal.getTags((receivedTags) => {
  //          console.log(receivedTags);
  //          for (var key in receivedTags){
  //              if (key.startsWith('group')){
  //                  console.log('Deleting Tags ' + key)
  //                  OneSignal.deleteTag(key)
  //              }

  //          }
  //      });
       
}

export function registerGroupsinOneSignal (userObj) {
    console.log(userObj)
      if (userObj.role === "attendance")
        return null
  var groups = userObj.Groups
  var tag = {}
  if (groups === undefined){
      console.log('Could not register groups in one signal')
      bugsnag.leaveBreadcrumb('Could not register groups in one signal')
  }
  Object.keys(groups).forEach(function (key) {
      console.log('Registred Group ' + key)
      bugsnag.leaveBreadcrumb('Registred Group ' + key)
      tag['group-' + key] = true
    })
    tag['user'] = userObj.email
    tag['userid'] = userObj.userid
    tag['display_name'] = userObj.display_name
    
    console.log(tag)
    
    var response = OneSignal.sendTags(tag)
    console.log(response)


}

export function unregisterGroupsinOneSignal (userObj) {
    var groups = userObj.groups
    var tag = {}
    Object.keys(groups).forEach(function (key) {
        var groupid = groups[key].GroupName
        tag['group-' + groupid] = true
        tag['group-' + key] = true

    })
    OneSignal.deleteTag(tag)
}

export function updateProfileImage (profileimage) {
    console.log('Profile Image ->' + profileimage)
  return {
    type: 'UPDATE_PROFILE_IMAGE',
    payload: {
      profileimage
    }
  }
}

export function startNotifications (userid, props = {}) {
  return function (dispatch) {
    console.log('------- STARTING NOTIFICATION LISTENER ----------')
    firebase.database().ref('/Users/' + userid + '/Notifications/').on('value', function (snapshot) {
      console.log('----- GOT NOTIFICATION -------------')
      console.log(snapshot.val())
      var data = snapshot.val()

      dispatch({type: 'LOAD_NOTIFICATIONS', payload: {notifications: data}})
    })
  }
}

export function updateToken (token, groups, NewRefresh=true) {
    return function (dispatch) {
        dispatch({type: 'UPDATE_TOKEN_WORKFLOW', payload: {token, groups, NewRefresh}})
    }
}


export function stopNotifications (userid) {
  return function (dispatch) {
    console.log('------- STOPPING NOTIFICATION LISTENER ----------')
    firebase.database().ref('/Users/' + userid + '/Notifications/').off('value')
    dispatch({type: 'STOP_NOTIFICATIONS', payload: {}})
  }
}

export function setNavigationTab (tabName) {
  return function (dispatch) {
    dispatch({type: 'SET_ACTIVE_TAB', payload: {tabName}})
  }
}

export function InitializedFirebase() {
  return function (dispatch) {
      dispatch({type : 'INIT_FIREBASED_FULLFILLED'})
  }

}

export function sendFeedback(feedback, data, token){
  return function (dispatch) {
      dispatch({type : 'SEND_FEEDBACK', payload : {feedback, data , token}})
  }
}

export function resetUser(){
  return function (dispatch){
    dispatch({type : 'RESET_USER', payload : {}})
  }
}