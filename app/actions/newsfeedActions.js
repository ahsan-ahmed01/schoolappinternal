'use strict'

export function getNewsFeedAll (page = 0, token, authobj, groups) {
  return {
    type: 'LOAD_NEWSFEED_ALL',
    payload: {
      page, usertoken: token, authobj, groups, isGroupView  : false
    }
  }
}

export function getNewsFeedByGroup (page = 0, groupid, token) {
  return {
    type: 'LOAD_NEWSFEED_ALL',
    payload: {
      page, usertoken: token, groupid, isGroupView  : true
    }
  }
}

export function showNewFeedsisLoading(){
  return {
    type : 'NEWSFEED_LOADING',
      payload : {

      }
  }
}

export function enableNewsFeedLoadingIndicator(){
  return {
    type : 'FAKE_NEWS_REFRESH_INDICATOR',
      payload : {

      }
  }
}

export function likeapost (activityid, token, user) {
  return {
    type: 'NEWSFEED_LIKE_POST',
    payload: {
      activityid,
      token,
      user
    }
  }
}

export function unlikeapost (activityid, token, user) {
  return {
    type: 'NEWSFEED_UNLIKE_POST',
    payload: {
      activityid,
      token,
      user
    }
  }
}

export function viewpost (activityid, token) {
  return {
    type: 'NEWSFEED_VIEW_POST',
    payload: {
      activityid,
      token
    }
  }
}

export function showsendingspinner () {
  return {
    type: 'NEW_POST_SENDING',
    payload: {

    }
  }
}

export function hidesendingspinner () {
  return {
    type: 'NEW_POST_SENDING_FULFILLED',
    payload: {

    }
  }

}

export function showmanualRefresh(){
  return {
    type : 'MANUAL_REFRESH',
      payload : {

      }
  }
}

export function onefileuploaddone(){
    return function (dispatch) {
      dispatch({type : 'ONE_FILEUPLOAD_DONE', payload : {}})
    }
}

export function updatePostStatus(poststatus,groupids) {
    return function (dispatch) {
        dispatch({type : 'UPDATE_POST_STATUS', payload : {poststatus, groupids}})
    }
}




