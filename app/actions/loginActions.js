  'use strict'

import axios from 'axios'
import * as userActions from './userActions'
  import { Auth } from 'aws-amplify';

var EnvConfig = require('../config/environment')
  var bugsnag = require('../config/bugsnag')
export function authenticateUser (user, password, userobj) {
  
  

  return {
    type: 'AUTHENTICATE_USER',
    payload: {user, password, userobj}
  }
}

  export function isLoggingIn () {
      return {
          type: 'USER_LOGGING_IN',
          payload: null
      }
  }

  export function updateProfilePicture (user, picture) {
      return {
          type: 'UPDATE_PROFILE_PICTURE_CACHE',
          payload: {user, picture}
      }
  }

export function authenticatefulfilled (authobj) {
  return {
    type: 'AUTHENTICATE_USER_FULFILLED',
    payload: authobj
  }
}

export function setUserName (userid) {
  
  return {
    type: 'SET_USERID',
    payload: {
      userid
    }
  }
}

export function setPassword (password) {
  return {
    type: 'SET_PASSWORD',
    payload: {
      password
    },
      AppReady : true,
  }
}

  export function setAppStatus (status) {
      return {
          type: 'SET_APPREADY',
          payload: {
              status
          }
      }
  }

export function setEmailAddress (emailaddress) {
  return {
    type: 'SET_EMAIL',
    payload: {
      emailaddress
    }
  }
}

export function resetError () {
  return {type: 'AUTHENTICATE_USER_RESET_ERROR', payload: {}}
}

export function resetPassword(username) {
    return function (dispatch) {
        dispatch({type: 'LOGIN_RESET_PASSWORD', payload: { 'user_login': username}})
        
        Auth.forgotPassword(username)
            .then(data => {
                
                
                dispatch({type: 'LOGIN_RESET_PASSWORD_FULFILLED', payload: {data}})

            })
            .catch(err => {
                
                
                dispatch({type: 'LOGIN_RESET_PASSWORD_REJECTED', payload: {err}})
            });
    }
  }


  export function changePassword (email, reset_code, password) {
      return function (dispatch) {
          dispatch({type: 'LOGIN_CHANGE_PASSWORD', payload: {email}})
          Auth.forgotPasswordSubmit(email, reset_code, password)
              .then(data => {
                  
                  
                  dispatch({type: 'LOGIN_CHANGE_PASSWORD_FULFILLED', payload: {email, password}})

              })
              .catch(err => {
                  
                  
                  dispatch({type: 'LOGIN_CHANGE_PASSWORD_REJECTED', payload: err})
              });
      }

  }

  export function registerUser (email, reset_code, password) {
    console.log('In Register')
      return function (dispatch) {
          //dispatch({type: 'LOGIN_CHANGE_PASSWORD', payload: {email}})
          console.log('Calling Auth')
          Auth.signUp({
              username : email,
              password : password,
              attributes: {
                  email : email,
                  given_name : 'somename',
                  family_name : 'some familyname',
                  phone_number : '+12011112222',
                  picture : '<DEFAULT>',
                  nickname : ''
              },
              validationData : [{
                  "Name": "Code",
                  "Value": reset_code,
              }]
          }).then(response => {
              console.log(response)
              if (response.userConfirmed){
                  dispatch({type: 'REGISTER_USER_SUCCESS', payload: {email, password}})
              }else{
                  dispatch({type: 'REGISTER_USER_REJECTED', payload: "User Not Confirmed !"})
              }
            bugsnag.leaveBreadcrumb('Registered User : ' + email)
          }).catch(err => {
              console.log(err)
              
              bugsnag.leaveBreadcrumb('Error Registering User : ' + email)

              dispatch({type: 'REGISTER_USER_REJECTED', payload: err})
          });
      }

  }

export function changeScreen (screenName) {
  return function (dispatch) {
    dispatch({type: 'CHANGE_VIEW_SCREEN', payload: {screenName}})
  }
}

export function checkTokenValidity (token) {

}

export function setLoginModalStatus (modalStatus) {
  return function (dispatch) {
    dispatch({type: 'SHOW_LOGIN_MODAL', payload: {modalStatus}})
  }
}

  export function selectAgreement (useraction) {
      return function (dispatch) {
          dispatch({type: 'SELECT_AGREEMENT', payload: {useraction}})
      }
  }

  export function softinitApp (userid, password) {
      return function (dispatch) {
          Auth.signIn(userid, password)
              .then(user => {
                  
                  dispatch({type: 'SOFT_INIT_APP', payload: {token : user.signInUserSession.idToken.jwtToken}})
              })
              .catch(err => {
                      
                      return err
                  }
              )

      }
  }

 
