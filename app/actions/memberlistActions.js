'use strict'
import axios from 'axios'
var EnvConfig = require('../config/environment')

export function getMemberList (selectedGroup, token) {
  return function (dispatch) {
    var headers = {
      headers: {
        'Authorization': 'Bearer ' + token

      },
      params: {
        'groupid': selectedGroup
      }
    }
    dispatch({type: 'GET_MEMBER_DETAILS', payload: {selectedGroup}})
    axios
            .get(EnvConfig.GROUP_DETAIL_URL, headers)
            .then((response) => {
              dispatch({type: 'GET_MEMBER_DETAILS_FULFILLED', payload: response.data})
            })
            .catch((err) => {
              dispatch({type: 'GET_MEMBER_DETAILS_REJECTED', payload: err})
            })
  }
}

export function selectuser (user) {
  return function (dispatch) {
    dispatch({type: 'MEMBER_LIST_ADD', payload: {user}})
  }
}

export function unselectuser (user) {
  return function (dispatch) {
    dispatch({type: 'MEMBER_LIST_REMOVE', payload: {user}})
  }
}

export function clearselections () {
  return function (dispatch) {
    dispatch({type: 'MEMBER_LIST_CLEAR', payload: {}})
  }
}

export function setSelectedUsers (selectedUsers) {
  return function (dispatch) {
    dispatch({type: 'SET_SELECTED_LIST', payload: {selectedUsers}})
  }
}
