export function refreshBriefcase(usergroups) {
    return function (dispatch) {
        dispatch({type : 'REFRESH_BRIEFCASE', payload : {groups : usergroups}})
    }
  }
  