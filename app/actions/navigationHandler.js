
import firebase from 'firebase'
import * as firebaseapi from '../api/firebaseapi'
import * as userActions from '../actions/userActions'

var navigiationEventsHandler = (event, props, tabName = '') => {
  
  
  
  if (props.login !== undefined && props.login.isLoggedin === false){
    return
  }
  if (props.user !== undefined && props.user.id === -1){
    return
  }
  
  if (event.id === 'willAppear') {
    
    
    if (props.setNavigationTab !== undefined) { props.setNavigationTab(tabName) }
  }

  if (event.type === 'NavBarButtonPress') { // this is the event type for button presses
    if (event.id === 'leftmenu') {
      
      props
          .navigator
          .toggleDrawer({side: 'left', animated: true})
    }
    if (event.id === 'rightmenu') {
        // this is the same id field from the static navigatorButtons definition
      props
          .navigator
          .toggleDrawer({side: 'right', animated: true})
    }
    if (event.id === 'editpost') {
        // this is the same id field from the static navigatorButtons definition
      props
          .navigator
          .showModal({title: 'Post', screen: 'Three60Memos.PostScreen', animationType: 'slide-down'})
    }

    if (event.id === 'addevent') {
        // this is the same id field from the static navigatorButtons definition
      props
          .navigator
          .showModal({title: 'Add Event',
            screen: 'Three60Memos.AddEventScreen',
            animationType: 'slide-down',
            navigatorButtons: {
              rightButtons: [
                {
                  title: 'Save',
                  id: 'AddEvent'
                }],
              leftButtons: [
                {
                  title: 'Cancel',
                  id: 'closePost'
                }
              ]
            }})
    }
  }
  if (event.type === 'DeepLink') {
    const parts = event.link.split('/')
    
    if ((tabName === 'chatRooms') && (parts[0] === 'chatRooms')) {
      if ((parts[2] === 'BYROOMKEY') && (parts[3] !== undefined)) {
        var roomKey = parts[3]
        
        props.navigator.switchToTab({
          tabIndex: 2
        })
        props.navigator.popToRoot({
        })

        // props.loadMessages(roomKey)
        props.startListener(roomKey)
        props.navigator.push({
          screen: 'Three60Memos.ChatScreen',
          passProps: {roomKey: roomKey},
          animated: true,
          backButtonTitle: '',
          backButtonHidden: false,
          navigatorStyle: {}
        })
      } else {
        HandleOpeningChatWindow(props)
      }
    } else
    if (parts[0] === tabName) {
      props.navigator.push({
        screen: parts[1],
        animated: true,
        backButtonHidden: false,
        navigatorStyle: {},
        backButtonTitle: ''
              // navigatorButtons: {}
      })
      
    }
  }
}

function HandleOpeningChatWindow (props) {
  
  var userids = [props.memberprofile.id, props.user.id]
  userids.sort(function (a, b) { return a - b })
  var userstr = userids.join('-')

  firebase.database().ref('/Chats/' + userstr).once('value')
  .then(function (snapshot) {
    var roomKey = snapshot.val()
    
    if (roomKey === null) {
      
      var users = [{
        id: props.memberprofile.id,
        display_name: props.memberprofile.display_name,
        profile_photo: props.memberprofile.profile_photo
      },
      {
        id: props.user.id,
        display_name: props.user.display_name,
        profile_photo: props.user.profile_photo
      }
      ]
      roomKey = firebaseapi.getChatRoomKey(users, '')
    }
    OpenChatRoom(props, roomKey)
  })
}

function OpenChatRoom (props, roomKey) {
  props.navigator.switchToTab({
    tabIndex: 2
  })
  props.loadMessages(roomKey)
  props.startListener(roomKey)
  props.navigator.push({
    screen: 'Three60Memos.ChatScreen',
    title: 'Chat - ' + props.memberprofile.display_name,
    passProps: {roomKey: roomKey},
    animated: true,
    backButtonTitle: '',
    backButtonHidden: false,
    navigatorStyle: {}
  })
}

exports.navigiationEventsHandler = navigiationEventsHandler
