import { Client, Configuration } from 'bugsnag-react-native';
var EnvConfigs = require('./environment')

const config = new Configuration(EnvConfigs.AWS_CONFIG.BUGSNAG_KEY);
config.codeBundleId = EnvConfigs.BUNDLE_ID
const bugsnag = new Client(config);


module.exports = bugsnag
