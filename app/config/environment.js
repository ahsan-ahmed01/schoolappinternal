const TargetEnv = "dev";

import { Platform } from "react-native";

var _baseURL = {
  dev: "https://hudsondev3.scolalabs.com",
  uat: "",
  prod: "https://hudson360.scolalabs.com",
};

var _awsbaseURL = {
  dev: "https://36jllv9kxa.execute-api.us-east-1.amazonaws.com/dev2/",
  proddev: "https://7o0tb444l3.execute-api.us-east-2.amazonaws.com/dev",
  uat: "",
  prod: "https://hudson360.scolalabs.com",
};

var _firebaseconfig = {};

var _awsconfig = {
  dev: {
    S3_PROFILE_URL: "https://s3.amazonaws.com/hmsdev-profile-photos/",
    S3_BUCKET_ARN: "arn:aws:s3:::hmsdev-profile-photos",
    S3_PROFILE_BUCKET: "hmsdev-profile-photos",
    S3_PROFILE_ACCESS_KEY: "AKIAIESUK7B2PQQQQAIA",
    S3_PROFILE_SECRET_KEY: "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",
    S3_BUCKET_REGION: "us-east-1",
    TMP: "212",
    ONE_SIGNAL_KEY: "",
    BUGSNAG_KEY: "7bf9cb8a4bb831f654fd09c5d5d695c6",
    CODEPUSH_KEY: "",
  },
};

function getFireBaseConfig() {
  return _firebaseconfig[TargetEnv];
}

function getBaseURL() {
  return _baseURL[TargetEnv];
}

function getAWSBaseURL() {
  return _awsbaseURL[TargetEnv];
}

function getAWSConfig() {
  return _awsconfig[TargetEnv];
}

function isProducion() {
  return TargetEnv === "prod";
}

var _EnvConfigs = {
  TITLE_NAME: "News",
  BUNDLE_ID: "1.1.0",
  API_NAME: "HMS 360",
  SCHOOL_NAME: "360",
  MAIN_HOME_COLOR: "#3b479d",
  MAIN_EVENT_COLOR: "#3b479d",
  MAIN_CHAT_COLOR: "#3b479d",
  MAIN_GROUP_COLOR: "#3b479d",
  LOGO_IMAGE: require("../img/logo.png"),
  VID_IMAGE: require("../img/logo.png"),
  NEWSFEED_ITEMS_PER_PAGE: 10,
  FIREBASE_CONFIG: getFireBaseConfig(),
  STORAGE_KEY: "hms360-storage-key",
  MESSAGES_PER_REFRESH: 15,
  AWS_CONFIG: getAWSConfig(),
  HOMESCREEN_INTERVAL: 600000,
  EVENTS_INTERVAL: 300000,
  GOOGLE_ANALYTICS_TRACKERID: "UA-113957817-1",
  NOTIFICATION_EVENT_HEADER: "Event",
  ENV_INSTANCE: TargetEnv,
  ATTENDANCESTATE: {
    CHECKEDIN: "Checked In",
    CHECKEDOUT: "Checked Out",
    ABSENT: "Absent",
    TARDY: "Tardy",
    UNMARKED: "UnMarked",
  },
  CUSTOM_MAIN_MENU: [],
};

function getEnvironment() {
  return _EnvConfigs;
}

var EnvConfigs = getEnvironment();
module.exports = EnvConfigs;
