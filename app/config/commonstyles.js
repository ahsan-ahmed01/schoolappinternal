import {StyleSheet} from 'react-native'

export var commonStyle = StyleSheet.create({
  button: {
    width: 150,
    backgroundColor: '#59c3c3',
    padding: 10,
    borderRadius: 5,
    borderWidth: 0,
    marginLeft: 10,
    marginRight: 10

  },
    buttonsmall: {
        width: 75,
        backgroundColor: '#59c3c3',
        padding: 5,
        borderRadius: 5,
        borderWidth: 0,
        marginLeft: 10,
        marginRight: 10,
        borderRadius : 5

    },
  buttonText: {
    alignItems: 'center',
    color: 'white',
    textAlign: 'center'
  },
  buttonImage: {
    margin: 10
  },
  buttonAcross: {
    backgroundColor: '#59c3c3',
    paddingVertical: 12,
    marginLeft: 5,
    marginRight: 5,
    alignItems: 'center',
    paddingHorizontal: 25,
    justifyContent: 'center',
    flexDirection: 'row',
    borderRadius : 5

  },
  buttonTextAcross: {
    color: '#FFF',
    fontSize: 18
  },
      buttonview: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-around',
        alignItems : 'flex-start',
        marginTop : 10,
    },
})

export var RichTextToolBarActions = [
  'INST_IMAGE',
  'INST_LINK',
  'bold',
  'italic',
  'underline',
  'unorderedList',
  'orderedList'

]

export var RichTextToolBarActionsnoMedia = [
  'INST_LINK',
  'bold',
  'italic',
  'underline',
  'unorderedList',
  'orderedList'

]

export function getRichTextToolBarActions (){
  const texts = {}
  // texts['INST_IMAGE'] = require('../img/icon_format_media.png')
  // texts['bold'] = require('../img/icon_format_bold.png')
  // texts['italic'] = require('../img/icon_format_italic.png')
  // texts['underline'] = require('../img/icon_format_underline.png')
  // texts['unorderedList'] = require('../img/icon_format_ul.png')
  // texts['orderedListt'] = require('../img/icon_format_ol.png')
  // texts['INST_LINK'] = require('../img/icon_format_link.png')
  return texts
}

