import {takeLatest, fork, takeEvery} from "redux-saga/effects";
import * as loginworkflows from "./loginworkflows";
import * as feedworkflows from "./newsfeedworkflows";
import * as postworkflows from "./postworkflows";
import * as eventworkflows from "./eventworkflows";
import * as userworkflows from "./userworkflows";
import * as attendanceworkflows from "./attendanceworkflows";

function* authenticateusersaga() {
    yield takeLatest(
        "AUTHENTICATE_USER",
        loginworkflows.authenticateuserworkflow
    );
}

function* getNewsFeed() {
    yield takeLatest("LOAD_NEWSFEED_ALL", feedworkflows.newsfeedworkflow);
}

function* likeapost() {
    yield takeLatest("NEWSFEED_LIKE_POST", feedworkflows.likeapostworkflow);
}

function* unlikeapost() {
    yield takeLatest("NEWSFEED_UNLIKE_POST", feedworkflows.unlikeapostworkflow);
}

function* viewPost() {
    yield takeLatest("NEWSFEED_VIEW_POST", feedworkflows.viewpostworkflow);
}

function* sendPost() {
    yield takeLatest("SEND_POST", postworkflows.sendPostworkflow);
}

function* updatePost() {
    yield takeLatest("UPDATE_POST", postworkflows.updatePostworkflow);
}

function* addEvent() {
    yield takeEvery("ADD_EVENT", eventworkflows.addEventworkflow);
}

function* getEvents() {
    yield takeEvery("GET_EVENTS", eventworkflows.getEventsworkflow);
}

function* deleteEvent() {
    yield takeEvery("DELETE_EVENT", eventworkflows.deleteEventsworkflow);
}

function* deletePost() {
    yield takeEvery("DELETE_POST", postworkflows.deletepostworkflow);
}

function* addComment() {
    yield takeEvery("ADD_COMMENT", postworkflows.addcommentworkflow);
}

function* updateComment() {
    yield takeEvery("UPDATE_COMMENT", postworkflows.updatecommentworkflow);
}

function* deleteComment() {
    yield takeEvery("DELETE_COMMENT", postworkflows.deletecommentworkflow);
}

function* setTabChange() {
    yield takeLatest("SET_CURRENT_TAB", feedworkflows.setTabChangeworkflow);
}

function* setGroupCoverPhoto() {
    yield takeLatest("SET_GROUP_COVER_PHOTO", postworkflows.setGroupCoverPhoto);
}

function* setGroupProfilePhoto() {
    yield takeLatest(
        "SET_GROUP_PROFILE_PHOTO",
        postworkflows.setGroupProfilePhoto
    );
}

function* sendfeedback() {
    yield takeLatest("SEND_FEEDBACK", userworkflows.sendFeedbackworkflow);
}

function* updateTokenWorkflow() {
    yield takeLatest("UPDATE_TOKEN_WORKFLOW", userworkflows.updateToken);
}

function* addUpdateChildPin() {
    yield takeEvery(
        "SET_CHILD_AUTHORIZED_USER",
        userworkflows.updateChildAuthotizedUser
    );
}

function* deleteChildAuthorizedUser() {
    yield takeEvery(
        "DELETE_CHILD_AUTHORIZED_USER",
        userworkflows.deleteChildAuthotizedUser
    );
}

function* validateChildPin() {
    yield takeEvery(
        "ATTENDANCE_VALIDATE_PIN",
        attendanceworkflows.validatePinworkflow
    );
}

function* setAttendanceoverride() {
    yield takeEvery(
        "ATTENDANCE_SET_STATUS_OVERRIDE",
        attendanceworkflows.setAttendanceOverride
    );
}

function* getAuthorizedUers() {
    yield takeEvery(
        "ATTENDANCE_AUTHORIZED_USERS",
        attendanceworkflows.getAuthorizedUsers
    );
}

function* setCheckoutOverride() {
    yield takeEvery(
        "ATTENDANCE_CHECKOUT_OVERRIDE",
        attendanceworkflows.setCheckoutOverride
    );
}

function* initGroupChildren() {
    yield takeLatest(
        "ATTENDANCE_INIT_LIST",
        attendanceworkflows.initGroupChildren
    );
}

function* refreshAttendanceStatus() {
    yield takeLatest(
        "ATTENDANCE_STATUS_REFRESH",
        attendanceworkflows.refreshAttendanceStatus
    );
}


function* softInitApp() {
    yield takeLatest("SOFT_INIT_APP", loginworkflows.softInitWorkflow);
}

function* refreshBriefcase() {
    yield takeLatest("REFRESH_BRIEFCASE", feedworkflows.refreshBriefcaseworkflow)
}

function* deleteEventSignupforChild(){
    yield takeEvery("DELETE_SIGNUP_CHILD_EVENT", eventworkflows.deleteEventSignupforChild)
}

export default function* rootSaga() {
    yield [
        fork(authenticateusersaga),
        fork(getNewsFeed),
        fork(likeapost),
        fork(unlikeapost),
        fork(viewPost),
        fork(sendPost),
        fork(updatePost),
        fork(addEvent),
        fork(getEvents),
        fork(deleteEvent),
        fork(deletePost),
        fork(addComment),
        fork(updateComment),
        fork(deleteComment),
        fork(setTabChange),
        fork(setGroupCoverPhoto),
        fork(setGroupProfilePhoto),
        fork(sendfeedback),
        fork(updateTokenWorkflow),
        fork(addUpdateChildPin),
        fork(deleteChildAuthorizedUser),
        fork(validateChildPin),
        fork(setAttendanceoverride),
        fork(getAuthorizedUers),
        fork(setCheckoutOverride),
        fork(initGroupChildren),
        fork(refreshAttendanceStatus),
        fork(softInitApp),
        fork(refreshBriefcase),
        fork(deleteEventSignupforChild)
    ];
}
