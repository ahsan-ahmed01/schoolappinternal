"use strict";
import { call, put, all } from "redux-saga/effects";
import * as eventsapi from "../api/eventsapi";
import * as firebaseapi from "../api/firebaseapi";
import * as loginapi from "../api/loginapi";

var addEventworkflow = function* (action) {
  let session = yield call(loginapi.latestSession);
  var usertoken = session.idToken.jwtToken;

  try {
    var addeventResponse = yield call(
      eventsapi.addEvent,
      action.payload.formData,
      action.payload.selectedUsers,
      action.payload.displayName,
      usertoken,
      action.payload.eventimgurl,
      action.payload.groups,
      action.payload.eventtype,
      action.payload.eventid,
      action.payload.parentTeacherSlots,
      action.payload.volunteer,
      action.payload.spot,
      action.payload.volunteertype,
      action.payload.daterangetype,
      action.payload.daterange,
      action.payload.selectedDays,
      action.payload.slotTime,
      action.payload.dates
    );
    console.log(addeventResponse);
    if (addeventResponse.data.statusCode !== 200) {
      throw Error("Unknow Error - No Event Id Returned !");
    }
    yield put({
      type: "ADD_EVENT_FULFILLED",
      payload: { response: addeventResponse },
    });
    yield put({
      type: "CLEAR_EVENTS",
      payload: { response: addeventResponse },
    });
    var startMonth = new Date().getMonth();
    var startYear = new Date().getFullYear();
    var endYear = startMonth + 10 > 12 ? startYear + 1 : startYear;
    var endMonth =
      startMonth + 10 > 12 ? startMonth + 10 - 12 : startMonth + 12;
    yield put({
      type: "GET_EVENTS",
      payload: {
        startMonth,
        endMonth,
        startYear,
        endYear,
        token: usertoken,
        groups: action.payload.groups,
      },
    });
  } catch (addeventError) {
    yield put({
      type: "ADD_EVENT_REJECTED",
      payload: { error: addeventError },
    });
  }
};

var getEventsworkflow = function* (action) {
  let session = yield call(loginapi.latestSession);
  var usertoken = session.idToken.jwtToken;
  try {
    const [geteventResponse, useresponses] = yield all([
      call(eventsapi.getEvents, usertoken, action.payload.groups),
      call(eventsapi.getUserResponses, usertoken),
    ]);
    yield put({
      type: "GET_EVENTS_FULFILLED",
      payload: {
        events: geteventResponse.data.results,
        useresponses: useresponses.data.result,
      },
    });
    //yield put({type: 'GET_EVENTS_FULFILLED', payload: {events : geteventResponse.data.data, useresponses : useresponses.data.result}})
  } catch (error) {
    yield put({ type: "GET_EVENTS_REJECTED", payload: { error: error } });
  }
};

var deleteEventsworkflow = function* (action) {
  let session = yield call(loginapi.latestSession);
  var usertoken = session.idToken.jwtToken;
  try {
    var deleteeventResponse = yield call(
      eventsapi.deleteEvent,
      action.payload.eventid,
      usertoken
    );
    yield put({
      type: "DELETE_EVENT_FULFILLED",
      payload: deleteeventResponse.data,
    });
    //yield put({type: 'CLEAR_EVENTS', payload: { response: deleteeventResponse}})

    yield put({
      type: "GET_EVENTS",
      payload: { token: action.payload.token, groups: action.payload.groups },
    });
  } catch (error) {
    yield put({ type: "DELETE_EVENT_REJECTED", payload: { error: error } });
  }
};

var deleteEventSignupforChild = function* (action) {
  let session = yield call(loginapi.latestSession);
  var usertoken = session.idToken.jwtToken;
  try {
    var deleteeventResponse = yield call(
      eventsapi.deleteSignupforChild,
      action.payload.event_id,
      action.payload.child_id,
      action.payload.slotId,
      usertoken
    );

    yield put({
      type: "GET_EVENTS",
      payload: { token: action.payload.token, groups: action.payload.groups },
    });
  } catch (error) {
    yield put({ type: "DELETE_EVENT_REJECTED", payload: { error: error } });
  }
};

module.exports = {
  addEventworkflow,
  getEventsworkflow,
  deleteEventsworkflow,
  deleteEventSignupforChild,
};
