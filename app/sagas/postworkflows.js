import * as uploadmediaapi from "../api/uploadmedia";
import * as postapi from "../api/postapi";
import { call, put } from "redux-saga/effects";
import * as miscutils from "../utils/misc";
var bugsnag = require("../config/bugsnag");
import * as loginapi from '../api/loginapi'

var sendPostworkflow = function* (action) {
  var childids = action.payload.childids;
  var review_mode = action.payload.review_mode;
  var reviwer_list = action.payload.reviwer_list;
  var pinned_post = action.payload.pinned_post;
  var pinned_until = action.payload.pinned_until;
  var pinned_topic = action.payload.pinned_topic;
  var scheduleDateTime = action.payload.scheduleDateTime;
  var draft_mode = action.payload.draft_mode;

  var keys = Object.keys(action.payload.postProps.selectedphotos);
  var vidkeys = Object.keys(action.payload.postProps.selectedvideos);
  var postText = action.payload.postText;
  var userid = action.payload.user.id;
  console.log(action.payload)
  var groupid = action.payload.postProps.selectedgroupid;
  var groupname = action.payload.postProps.selectedgroupname;
  var photouploads = [];
  var videouploads = [];
  let session = yield call(loginapi.latestSession)
  var usertoken = session.idToken.jwtToken
  

  console.log(action.payload.postProps.selectedvideos);
  console.log(vidkeys);
  for (var key in keys) {
    var imagepath = action.payload.postProps.selectedphotos[key];
    photouploads.push(imagepath);
  }

  for (var key in vidkeys) {
    console.log(key);
    var videopath = action.payload.postProps.selectedvideos[key];
    console.log(videopath);
    videouploads.push(videopath);
  }
  bugsnag.leaveBreadcrumb("Sending Post Workflow");
  bugsnag.leaveBreadcrumb("ImgCount : " + photouploads.length);
  bugsnag.leaveBreadcrumb("VidCount : " + videouploads.length);

  console.log(videouploads);
  console.log(uploadedurls);
  try {
    yield put({
      type: "NEW_POST_SENDING",
      payload: { photos: photouploads, videos: videouploads }
    });

    //********** UPLOAD VIDEO ***************
    const vidresponses = yield videouploads.map(p => {
      bugsnag.leaveBreadcrumb("Uploading Video");
      console.log(p);
      if ("mime" in p) {
        var ext = p.mime;
      } else {
        var ext = p.filename.split(".").pop();
      }
      var contentType = "video/" + ext;
      var response = call(uploadSingleFile, p, contentType);
      console.log(response);
      return response;
    });
    console.log(vidresponses);
    var uploadedvidurls = [];
    for (var i = 0; i < vidresponses.length; i++) {
      var response = vidresponses[i];
      if (response.data.key !== "" || response.data.key !== undefined) {
        uploadedvidurls.push(response.data.key);
      }
    }

    //* ******** UPLOAD IMAGES **************
    const responses = yield photouploads.map(p => {
      bugsnag.leaveBreadcrumb("Uploading Image");
      console.log(p);
      var response = call(uploadSingleFile, p, "image/jpeg", groupid);
      return response;
    });
    var uploadedurls = [];
    console.log(responses);
    for (var i = 0; i < responses.length; i++) {
      var response = responses[i];

      if (response.data.key !== "" || response.data.key !== undefined) {
        uploadedurls.push(response.data.key);
      }
    }
    //return
    //* ********* SEND POST *******************
    bugsnag.leaveBreadcrumb("Calling Post API");
    var response = yield call(
      postapi.addPost,
      childids,
      draft_mode,
      review_mode,
      reviwer_list,
      pinned_post,
      pinned_topic,
      pinned_until,
      scheduleDateTime,
      groupid,
      groupname,
      postText,
      uploadedurls,
      uploadedvidurls,
      usertoken
    );
    var postMsg = ''
    if (scheduleDateTime !== "") postMsg = 'Your Post has been saved in the Briefcase and will be published at the scheduled time.'  
    if (review_mode) postMsg = 'Your Post has been saved in the Briefcase and sent for Review !'
    if (draft_mode) postMsg = 'Your Post has been saved in the Briefcase as a Draft !'
    bugsnag.leaveBreadcrumb("Calling Post API - DONE");
    yield put({ type: "NEW_POST_SENDING_FULFILLED", payload: {postMsg} });
    bugsnag.leaveBreadcrumb("Calling Post API - COMPLETE");
    yield put({ type: "SEND_POST_FULFILLED", payload: { response } });
    yield put({ type: 'NEWS_FEED_REFRESH', payload: {} })
    if (action.payload.user.roles !== 'parent'){
      yield put({ type: 'REFRESH_BRIEFCASE', payload: { groups: action.payload.user.groups } })
    }
    
  } catch (err) {
    yield put({
      type: "SEND_POST_REJECTED",
      payload: { response: err, errorMsg: "Error Adding Post" }
    });
    yield put({ type: "NEW_POST_SENDING_REJECTED", payload: {} });
    bugsnag.notify(err);
  }
};



function* uploadSingleFile(p, contentType, groupid) {
  try {
    const response = yield call(
      uploadmediaapi.UploadPhoto,
      p,
      contentType,
      groupid
    );
    console.log(response);
    if (response.status === "success") {
      yield put({ type: "ONE_FILEUPLOAD_DONE", payload: response.data.error });
    } else {
      console.log(response.error);
      put({ type: "ONE_FILEUPLOAD_FAILED", payload: response.error });
      return response;
    }

    return response;
  } catch (err) {
    put({ type: "ONE_FILEUPLOAD_FAILED", payload: {} });
    return err;
  }
}

// function* uploadSingleVideoFile(p, usertoken) {
//     try {
//         const response = yield call(uploadmediaapi.UploadPhoto, p, usertoken)
//         yield put({type: 'ONE_VIDEO_FILEUPLOAD_DONE', payload: {}})
//
//         return response
//     } catch (err) {
//         put({type: 'ONE_VIDEO_FILEUPLOAD_FAILED', payload: {}})
//         return err
//     }
//
// }

var updatePostworkflow = function* (action) {
  console.log(action);
  var activityid = action.payload.postid;
  var content = action.payload.postText;
  let session = yield call(loginapi.latestSession)
  var usertoken = session.idToken.jwtToken
  var imageurls = action.payload.postProps.selectedphotos;
  var groupid = action.payload.postProps.selectedgroupid;
  var videourls = action.payload.postProps.selectedvideos;
  var childids = action.payload.childids;
  var review_mode = action.payload.review_mode;
  var reviwer_list = action.payload.reviwer_list;
  var pinned_post = action.payload.pinned_post;
  var pinned_until = action.payload.pinned_until;
  var pinned_topic = action.payload.pinned_topic;
  var scheduleDateTime = action.payload.scheduleDateTime;
  var draft_mode = action.payload.draft_mode;
  var groupname = action.payload.postProps.selectedgroupname;

  var photouploads = [];
  var videouploads = [];
  console.log(imageurls);
  //***** Photos ********
  var keys = Object.keys(imageurls);
  console.log(keys);
  for (var key in keys) {
    console.log(key);
    var index = keys[key];
    var imagepath = imageurls[index];
    console.log(imagepath);
    photouploads.push(imagepath);
  }
  console.log(photouploads);
  //***** VIDOES ********
  var keys = Object.keys(videourls);
  console.log(keys);
  for (var key in keys) {
    console.log(key);
    var index = keys[key];
    var videopath = videourls[index];
    videouploads.push(videopath);
  }
  var uploadedurlsstr = videouploads.join(",");
  //var imguploadedurlsstr = photouploads.join(',')
  console.log(uploadedurlsstr);

  try {
    var response = yield call(
      postapi.updatePost,
      activityid,
      childids,
      draft_mode,
      review_mode,
      reviwer_list,
      pinned_post,
      pinned_topic,
      pinned_until,
      scheduleDateTime,
      groupid,
      groupname,
      content,
      photouploads,
      uploadedurlsstr,
      usertoken
    );
    console.log(response);
    if ((response.status === 200) && (response.data.status === 'success')) {
      yield put({ type: "UPDATE_POST_FULFILLED", payload: {} });
      yield put({ type: 'NEWS_FEED_REFRESH', payload: {} })
      if (action.payload.user.roles !== 'parent'){
        yield put({ type: 'REFRESH_BRIEFCASE', payload: { groups: action.payload.user.groups } })
      }
    } else {
      yield put({
        type: "SEND_POST_REJECTED",
        payload: { response: response, errorMsg: "Error Updating Post" }
      });
    }
  } catch (err) {
    console.log(err);
    yield put({
      type: "SEND_POST_REJECTED",
      payload: { response: err, errorMsg: "Error Updating Post" }
    });
  }
};

var deletepostworkflow = function* (action) {
  console.log(action);
  var activityid = action.payload.activityid;
  var usertoken = action.payload.token;
  var groupid = action.payload.groupid;

  try {
    var response = yield call(
      postapi.deletePost,
      activityid,
      groupid,
      usertoken
    );
    yield put({ type: 'NEWS_FEED_REFRESH', payload: {} })
    if (action.payload.inbriefcase) {
      if (action.payload.user.roles !== 'parent'){
        yield put({ type: 'REFRESH_BRIEFCASE', payload: { groups: action.payload.usergroups } })
      }
    } else {

    }
    console.log(response);
  } catch (err) {
    console.log(err);
    yield put({
      type: "DELETE_POST_REJECTED",
      payload: { response: err, errorMsg: "Error Deleting Post" }
    });
  }
};

var addcommentworkflow = function* (action) {
  console.log(action);
  var activityid = action.payload.activityid;
  var comments = action.payload.comments;
  var usertoken = action.payload.token;
  var groupid = action.payload.groupid;
  try {
    var response = yield call(
      postapi.addComments,
      activityid,
      comments,
      groupid,
      usertoken
    );
    yield put({ type: 'NEWS_FEED_REFRESH', payload: {} })
    console.log(response);
  } catch (err) {
    console.log(err);
    yield put({
      type: "ADD_COMMENTS_REJECTED",
      payload: { response: err, errorMsg: "Error Adding Comment" }
    });
  }
};

var updatecommentworkflow = function* (action) {
  console.log(action);
  var activityid = action.payload.activityid;
  var comments = action.payload.comments;
  var commentid = action.payload.commentid;
  let session = yield call(loginapi.latestSession)
  var usertoken = session.idToken.jwtToken
  var groupid = action.payload.groupid;
  try {
    var response = yield call(
      postapi.updateComment,
      commentid,
      comments,
      groupid,
      usertoken
    );
    yield put({ type: 'NEWS_FEED_REFRESH', payload: {} })
    console.log(response);
  } catch (err) {
    console.log(err);
    yield put({
      type: "UPDATE_COMMENTS_REJECTED",
      payload: { response: err, errorMsg: "Error Updating Comment" }
    });
  }
};

var deletecommentworkflow = function* (action) {
  console.log(action);
  var commentid = action.payload.commentid;
  let session = yield call(loginapi.latestSession)
  var usertoken = session.idToken.jwtToken
  var groupid = action.payload.groupid;
  try {
    var response = yield call(
      postapi.deleteComment,
      commentid,
      groupid,
      usertoken
    );
    yield put({ type: 'NEWS_FEED_REFRESH', payload: {} })

    console.log(response);
  } catch (err) {
    console.log(err);
    yield put({
      type: "DELETE_COMMENTS_REJECTED",
      payload: { response: err, errorMsg: "Error Updating Comment" }
    });
  }
};

var setGroupCoverPhoto = function* (action) {
  var groupid = action.payload.groupid;
  let session = yield call(loginapi.latestSession)
  var usertoken = session.idToken.jwtToken
  var image = action.payload.image;
  var remotefilename =
    "Group/" + groupid + "_" + Date.now() + "_coverphoto.jpg";
  try {
    var response = yield call(
      uploadmediaapi.UploadPhotoPublic,
      image,
      remotefilename
    );
    console.log(response);
    if (response !== "") {
      var photoUrl = response;
      console.log("Saving Image as Cover Photo " + photoUrl);
      var response = yield call(
        postapi.uploadGroupImage,
        photoUrl,
        groupid,
        "COVER",
        token
      );
      console.log(response);
      if (response.status === 200) {
        yield put({
          type: "SET_GROUP_COVER_PHOTO_FULFILLED",
          payload: { groupid, photoUrl }
        });
      } else {
        yield put({
          type: "SET_GROUP_COVER_PHOTO_REJECTED",
          payload: response
        });
      }
    }
  } catch (err) {
    yield put({ type: "SET_GROUP_COVER_PHOTO_REJECTED", payload: err });
  }
};

var setGroupProfilePhoto = function* (action) {
  var groupid = action.payload.groupid;
  var token = action.payload.token;
  var image = action.payload.image;
  var remotefilename =
    "Group/" + groupid + "_" + Date.now() + "_profilephoto.jpg";
  try {
    var response = yield call(
      uploadmediaapi.UploadPhotoPublic,
      image,
      remotefilename
    );
    if (response !== "") {
      var photoUrl = response;
      console.log("Saving Image as Profile Photo " + photoUrl);
      var response = yield call(
        postapi.uploadGroupImage,
        photoUrl,
        groupid,
        "PROFILE",
        token
      );
      if (response.status === 200) {
        yield put({
          type: "SET_GROUP_PROFILE_PHOTO_FULFILLED",
          payload: { groupid, photoUrl }
        });
      } else {
        yield put({
          type: "SET_GROUP_PROFILE_PHOTO_REJECTED",
          payload: response
        });
      }
    }
  } catch (err) {
    yield put({ type: "SET_GROUP_PROFILE_PHOTO_REJECTED", payload: err });
  }
};

module.exports = {
  sendPostworkflow,
  updatePostworkflow,
  deletepostworkflow,
  addcommentworkflow,
  deletecommentworkflow,
  updatecommentworkflow,
  setGroupCoverPhoto,
  setGroupProfilePhoto
};
