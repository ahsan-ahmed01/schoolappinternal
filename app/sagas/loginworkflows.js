import {call, put, all} from 'redux-saga/effects'
import {authenticatefulfilled} from '../actions/loginActions'
import * as loginapi from '../api/loginapi'
import * as firebaseapi from '../api/firebaseapi'
import TwilioClient from '../api/twilioapi'
import * as chatroomactions from '../actions/chatRoomActions'
import * as useractions from '../actions/userActions'
import * as loginactions from '../actions/loginActions'
import * as newsfeedactions from '../actions/newsfeedActions'
import * as eventactions from '../actions/eventActions'
import {delay} from 'redux-saga'
// var Fabric = require('react-native-fabric');
// var { Answers } = Fabric;
// var { Crashlytics } = Fabric;
var EnvConfig = require('../config/environment')
var bugsnag = require('../config/bugsnag')
//import { GoogleAnalyticsTracker } from "react-native-google-analytics-bridge";
import { Hub } from 'aws-amplify';
import {Auth} from 'aws-amplify'
import * as miscutils from '../utils/misc'
//import firebase from 'react-native-firebase'

function getUserDataDict(userdata, username) {
    var userdatadict = {}
    for (var i = 0; i < userdata.length; i++) {
        userdatadict[userdata[i]['Name']] = userdata[i]['Value']
    }
    userdatadict['display_name'] = userdatadict['given_name'] + ' ' + userdatadict['family_name']
    userdatadict['parent_of'] = ''
    userdatadict['linked_to'] = ''
    userdatadict['picture'] = userdatadict['picture'] === '<DEFAULT>' ? EnvConfig.AWS_CONFIG.S3_PROFILE_URL + username + '.jpg' : userdatadict['picture']
    return userdatadict;
}

var authenticateuserworkflow = function* (action) {
    console.log('+++++++ IN SAGA ++++++')
    console.log(action)
    var startTime =  new Date();
    try {
        bugsnag.leaveBreadcrumb('Initiating Login', {type: 'loginworkflow'});
        try {
            yield put(loginactions.setAppStatus(false))
            var authreponse = yield call(loginapi.authenticateuserapi, action.payload)
            console.log(authreponse)
            var usertoken = authreponse.signInUserSession.idToken.jwtToken;
            var username = authreponse.username
            bugsnag.leaveBreadcrumb('Loggedin-'+authreponse.username)
            //var firebaseconfig = yield call(loginapi.getFireBaseConfig, usertoken)
            bugsnag.leaveBreadcrumb('Starting Multiple Call')
            const [firebaseconfig,userresponse, authobj] = yield all([
                call(loginapi.getFireBaseConfig, usertoken),
                call(loginapi.getuserapi, usertoken),
                call(loginapi.getAuthObj)
            ])
            var isAttendanceRole = userresponse.data.role === "attendance"
            bugsnag.leaveBreadcrumb('Got Firebase Config', {type: 'loginworkflow'});
            firebaseapi.Init(firebaseconfig.data)
            
            // new TwilioClientInstance.InitToken()
            // console.log(TwilioClientInstance)
        
            
            bugsnag.leaveBreadcrumb('Initialized Firebase', {type: 'loginworkflow'});

            //var userresponse = yield call(loginapi.getuserapi, usertoken)
            bugsnag.leaveBreadcrumb('Got User', {type: 'loginworkflow'});
            bugsnag.setUser(action.payload.user, userresponse.data['given_name'] + ' ' + userresponse.data['family_name'], action.payload.user);
            console.log(userresponse)
            //var authobj = yield call(loginapi.getAuthObj)
            bugsnag.leaveBreadcrumb('Got User', {type: 'loginworkflow'});
            yield put(authenticatefulfilled(authobj))
            yield put(newsfeedactions.enableNewsFeedLoadingIndicator())
            console.log(authobj)
            var userid = userresponse.data.userid
            userresponse.data['display_name'] = userresponse.data['given_name'] + ' ' + userresponse.data['family_name']
            userresponse.data['parent_of'] = ''
            userresponse.data['linked_to'] = ''
            userresponse.data['picture'] = userresponse.data['picture'] === '<DEFAULT>' ? EnvConfig.AWS_CONFIG.S3_PROFILE_URL + username + '.jpg' : userresponse.data['picture']
            bugsnag.leaveBreadcrumb('Calling GetUserChildred', {type: 'loginworkflow'});
            const [childobjs,usercacheresponse,tmp, apiresponse] = yield all([
                call(loginapi.getuserchildrenapi, usertoken),
                call(loginapi.getUserCache, usertoken),
                // call(loginapi.getParentofCache, usertoken),
                call(firebaseapi.PerformChatCleanups, authreponse.data, userid)
            ])
            //var childobjs = yield call(loginapi.getuserchildrenapi, usertoken)
            bugsnag.leaveBreadcrumb('GetUserChildred SUCCESS', {type: 'loginworkflow'});
            console.log(childobjs,usercacheresponse,)
            yield put({
                type: 'GET_USER_DETAILS_FULFILLED',
                payload: {userdata: userresponse.data, token: usertoken, childobjs: childobjs.data.data}
            })
            yield put({type : 'SET_EVENTS_USER', payload : {user : username}})
            //------------------------------------------------
            //var usercacheresponse = yield call(loginapi.getUserCache, usertoken)
            bugsnag.leaveBreadcrumb('GetUserCache SUCCESS', {type: 'loginworkflow'});
            //var parentofresponse = yield call(loginapi.getParentofCache, usertoken)
            bugsnag.leaveBreadcrumb('GetParentofCache SUCCESS', {type: 'loginworkflow'});
            if ((usercacheresponse.data.users !== undefined) ) {

                bugsnag.leaveBreadcrumb('UserCachecount=' + Object.keys(usercacheresponse.data.users).length)
                //bugsnag.leaveBreadcrumb('ParentOfCachecount=' + Object.keys(parentofresponse.data.data).length)
                yield put({
                    type: 'USER_CACHE_LOAD_FULFILLED',
                    payload: {usercache: usercacheresponse}
                })
            } else {
                throw "Error_USER_CACHE"

            }

            yield put({type: 'INIT_FIREBASED_FULLFILLED', payload: null})
            //yield put({type: 'UPDATE_ADDL_PROFILE', payload: {userObj: userresponse.data}})
            console.log('before cleanup')
            //var apiresponse = yield
            console.log('PerformChatCleanups')
            yield call(useractions.registerGroupsinOneSignal, userresponse.data)
            console.log('after OneSignal')
            if (!isAttendanceRole) {
                yield put({
                    type: 'LOAD_NEWSFEED_ALL',
                    payload: {page: 0, usertoken, authobj, groups: userresponse.data.Groups}
                })
                yield put({type : 'CLEAR_EVENTS', payload : {token : usertoken}})
                yield put({type : 'GET_EVENTS', payload : {token : usertoken, groups : userresponse.data.Groups}})
                if (userresponse.data.role !== 'parent'){
                    yield put({type : 'REFRESH_BRIEFCASE', payload : {token : usertoken, groups : userresponse.data.Groups}})
                }
                if (action.payload.userobj === undefined) {
                    console.log('Introducing Delay due to First Login')
                    yield delay(3000)
                    yield call(initFirebaseActions, userresponse.data, userid)

                } else {
                    yield call(initFirebaseActions, userresponse.data, userid)

                }
            }
            yield put({type: 'FIRE_BASE_DONE', payload: {}})
            
        
            yield put(loginactions.setAppStatus(true))
            var endTime =  new Date();
            var timediff = endTime - startTime
            console.log('---------- Time for Load :' + timediff + "ms ------------")
            bugsnag.leaveBreadcrumb('MainFlowtime=' +timediff + "ms")

        } catch (usererr) {
            console.log(usererr)
            if (typeof(usererr) === Error){
                console.log(usererr)
                bugsnag.notify(usererr);
            }else{
                if (typeof(usererr) === String) {
                    console.log(userer)
                    bugsnag.leaveBreadcrumb(usererr.slice(0,30))
                    bugsnag.notify(new Error(usererr));
                }else{
                    console.log(usererr.message.slice(0,30))
                    bugsnag.leaveBreadcrumb(usererr.message)
                    bugsnag.notify(new Error(usererr.message));
                }
            }
            if (usererr.constructor.name === 'FirebaseError') {
                yield put({type: 'AUTHENTICATE_FIREBASE_ISSUE', payload: userresponse.data})
            } else {
                if (usererr === 'Error_USER_CACHE') {
                    yield put({
                        type: 'LOGIN_GENERIC_ERROR',
                        payload: {error: "Error Downloading MetaData. Please contact support !"}
                    })
                } else {
                    if (usererr.message === 'NEW_PASSWORD_REQUIRED') {
                        yield put({
                            type: 'LOGIN_GENERIC_ERROR',
                            payload: {error: "Error Logging in. Seems your account needs a new Password. Please reset your Password !"}
                        })
                    } else if (usererr.code === 'NotAuthorizedException' || usererr.code==="UserNotFoundException") {
                        yield put({
                            type: 'LOGIN_GENERIC_ERROR',
                            payload: {error: "Incorrect Username/Password. Please verify"}
                        })
                    } else {
                        bugsnag.leaveBreadcrumb('Unknown Error')
                        bugsnag.leaveBreadcrumb(usererr)
                        bugsnag.leaveBreadcrumb(usererr.message)
                        if (userresponse !== undefined) {
                            bugsnag.leaveBreadcrumb(userresponse)
                        }
                        yield put({type: 'AUTHENTICATE_USER_REJECTED', payload: userresponse.data})
                    }
                }
            }

        }
    } catch (err) {
        console.log('Error Authenticating User !')
        console.log(err)
        bugsnag.notify(err);

        // let tracker = new GoogleAnalyticsTracker(EnvConfig.GOOGLE_ANALYTICS_TRACKERID);
        // tracker.setUser(action.payload.user)
        // tracker.trackEvent("Login", "Failed", {'label': action.payload.user, 'value' : 0});
        // tracker.trackException(err.message, false);
        yield put({
            type: 'LOGIN_GENERIC_ERROR',
            payload: {error: "Error connecting to Server. Please check with ADMIN"}
        })
        yield put({type: 'AUTHENTICATE_USER_REJECTED', payload: err})
    }
}



var softInitWorkflow = function* (action){
    console.log(action)
    var startTime = new Date()
    var usertoken = action.payload.token
        
    try{
        yield put({type : 'UPDATE_TOKEN', payload : {token : usertoken}})
        const [firebaseconfig,userresponse, authobj] = yield all([
            call(loginapi.getFireBaseConfig, usertoken),
            call(loginapi.getuserapi, usertoken),
            call(loginapi.getAuthObj)
        ])
        miscutils.captureStat('SoftInit', 'FirebaseConfig-UserApi-getAuthObj',new Date() - startTime )
        console.log(userresponse)
        console.log(authobj)
        if (typeof userresponse.data !== 'object'){
            throw Exception('Error ')
        }
        useractions.UnregisterGroupsinOneSignal(userresponse.data)
        var isAttendanceRole = userresponse.data.role === "attendance"
        var snaptime = new Date()
        bugsnag.leaveBreadcrumb('Got Firebase Config', {type: 'loginworkflow'});
        firebaseapi.Init(firebaseconfig.data)
        let tc = TwilioClient
        tc.InitToken(usertoken)
            
        miscutils.captureStat('SoftInit', 'InitFireBase',new Date() - snaptime )
        bugsnag.leaveBreadcrumb('Initialized Firebase', {type: 'loginworkflow'});
        var userid = userresponse.data.userid
        var username = userresponse.data.Username
        yield put({type : 'SET_EVENTS_USER', payload : {user : username}})
        userresponse.data['display_name'] = userresponse.data['given_name'] + ' ' + userresponse.data['family_name']
        userresponse.data['parent_of'] = ''
        userresponse.data['linked_to'] = ''
        userresponse.data['picture'] = userresponse.data['picture'] === '<DEFAULT>' ? EnvConfig.AWS_CONFIG.S3_PROFILE_URL + username + '.jpg' : userresponse.data['picture']
        snaptime = new Date()
        
        const [childobjs,usercacheresponse, tmp] = yield all([
            call(loginapi.getuserchildrenapi, usertoken),
            call(loginapi.getUserCache, usertoken),
            //call(firebaseapi.PerformChatCleanups, null, userid),
            call(useractions.registerGroupsinOneSignal, userresponse.data),

        ])
        miscutils.captureStat('SoftInit', 'Second BULK API Cals',new Date() - snaptime )

        bugsnag.leaveBreadcrumb('Initialized Firebase', {type: 'loginworkflow'});
        snaptime = new Date()

        if (!isAttendanceRole) {
            yield call(initFirebaseActions, userresponse.data, userid)
        }
        miscutils.captureStat('SoftInit', 'Firebase-initFirebaseActions',new Date() - snaptime )
        snaptime = new Date()
        var snaptime1 = new Date()
        yield put({
            type: 'GET_USER_DETAILS_FULFILLED',
            payload: {userdata: userresponse.data, token: usertoken, childobjs: childobjs.data.data}
        })

        miscutils.captureStat('SoftInit', 'GET_USER_DETAILS_FULFILLED',new Date() - snaptime1 )
        bugsnag.leaveBreadcrumb('GetUserCache SUCCESS', {type: 'loginworkflow'});
        bugsnag.setUser(action.payload.user, userresponse.data['given_name'] + ' ' + userresponse.data['family_name'], action.payload.user);

        snaptime1 = new Date()
        if ((usercacheresponse.data.users !== undefined)) {

            bugsnag.leaveBreadcrumb('UserCachecount=' + Object.keys(usercacheresponse.data.users).length)
            yield put({
                type: 'USER_CACHE_LOAD_FULFILLED',
                payload: {usercache: usercacheresponse}
            })
        } else {
            throw "Error_USER_CACHE"

        }
        miscutils.captureStat('SoftInit', 'Assign Caches in Redux',new Date() - snaptime1 )

        snaptime1 = new Date()
        yield put({type: 'INIT_FIREBASED_FULLFILLED', payload: null})
        //yield put({type: 'UPDATE_ADDL_PROFILE', payload: {userObj: userresponse.data}})
        miscutils.captureStat('SoftInit', 'Misc',new Date() - snaptime1 )

        snaptime1 = new Date()

        yield put({type: 'FIRE_BASE_DONE', payload: {}})
        yield put({type : 'CLEAR_EVENTS', payload : {token : usertoken}})
        yield put({type : 'GET_EVENTS', payload : {token : usertoken, groups : userresponse.data.Groups}})
        if (userresponse.data.role !== 'parent'){
            yield put({type : 'REFRESH_BRIEFCASE', payload : {token : usertoken, groups : userresponse.data.Groups}})
        }
        yield put(loginactions.setAppStatus(true))
        var endTime = new Date()
        var timediff = endTime - startTime
        console.log('---------- Time for SoftInit :' + timediff + "ms ------------")
        bugsnag.leaveBreadcrumb('SoftInitTime=' +timediff + "ms")
        miscutils.captureStat('SoftInit', 'Misc Stuff 3',new Date() - snaptime1 )
        miscutils.captureStat('SoftInit', 'Total',timediff )
    }
    catch(err){
        bugsnag.notify(err)
        console.log(err)
        throw err
    }

}

function* initFirebaseActions( userobj, userid) {
    console.log('In Here')
    console.log(userobj)
    console.log(userid)

    var apiresponse = yield call(firebaseapi.registerUser, userid, userobj)
    console.log(apiresponse)
    yield put(chatroomactions.loadingGroups())
    yield put(chatroomactions.getChatGroups(userid))
    //yield put(eventactions.getCalendarConfig()) /* changed Calendar from remote to Local */
    yield put({type: 'DELETE_INVALID_CHATS', payload: null})
}


function RegisterUserLogintoAnswers(id, display_name, user_email) {

    // // Crashlytics.setUserName(display_name);
    // // Crashlytics.setUserEmail(user_email);
    // // Crashlytics.setUserIdentifier(id);
    // // Crashlytics.setString('Logged in',  new Date().toDateString());
    // // Crashlytics.setString('organization', '');
    // // Answers.logLogin(display_name, true);
    //
    // let tracker = new GoogleAnalyticsTracker(EnvConfig.GOOGLE_ANALYTICS_TRACKERID);
    //
    // tracker.setUser(display_name)
    // tracker.trackEvent("Login", "Successful", {'label': display_name, 'value' : parseInt(id)});
    // tracker.trackEvent("HomeScreen", "Successful", {'label': display_name, 'value' : parseInt(id)});


}

module.exports = {authenticateuserworkflow,softInitWorkflow}
