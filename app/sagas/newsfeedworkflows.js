import {call, put, all} from 'redux-saga/effects'
import * as newsfeedapi from '../api/newsfeedapi'
import * as loginapi from '../api/loginapi'
var bugsnag = require('../config/bugsnag')
import * as newsactions from '../actions/newsfeedActions'
import {Auth} from 'aws-amplify'
import * as miscutils from '../utils/misc'
function* checkforNewsFeed( action, usertoken1) {
    let session = yield call(loginapi.latestSession)
    var usertoken = session.idToken.jwtToken
    if ((action.payload.groupid === undefined) || (action.payload.groupid ===  null)) {
        var newsfeedresponse = yield call(newsfeedapi.getNewsFeedAPI, action.payload.page,action.payload.groups, usertoken  )
    } else {
        var newsfeedresponse = yield call(newsfeedapi.getNewsFeedAPI, action.payload.page, action.payload.groupid, usertoken)
    }
    return newsfeedresponse

}

function* checkforPinnedNewsFeed( action, usertoken1) {
    console.log('In Here')
    let session = yield call(loginapi.latestSession)
    var usertoken = session.idToken.jwtToken
    if ((action.payload.groupid === undefined) || (action.payload.groupid ===  null)) {
        var newsfeedresponse = yield call(newsfeedapi.getNewsFeedAPI, action.payload.page,action.payload.groups, usertoken,true,false,false,false,1000)
    }else{
        var newsfeedresponse = yield call(newsfeedapi.getNewsFeedAPI, action.payload.page,action.payload.groupid, usertoken,true,false,false,false,1000)
    }
    return newsfeedresponse

}

function* checkforDraftNewsFeed( action, usertoken1) {
    let session = yield call(loginapi.latestSession)
    var usertoken = session.idToken.jwtToken
    var newsfeedresponse = yield call(newsfeedapi.getNewsFeedAPI, action.payload.page,action.payload.groups, usertoken,false,true,false  )
    return newsfeedresponse

}

function* checkforInReviewNewsFeed( action, usertoken1) {
    let session = yield call(loginapi.latestSession)
    var usertoken = session.idToken.jwtToken
    var newsfeedresponse = yield call(newsfeedapi.getNewsFeedAPI, action.payload.page,action.payload.groups, usertoken,true,false,true  )
    return newsfeedresponse

}

var newsfeedworkflow = function* (action) {
    console.log('----------In NewsFeed Workflow !!!')
    let session = yield call(loginapi.latestSession)
    var usertoken = session.idToken.jwtToken
    console.log(usertoken)
    console.log(action)
    try {
        var startTime = new Date();
        if (action.payload.page === 0){
            console.log('Here1')
            const [newsfeedresponse,pinnednewsfeedresponse] = 
            yield all([call(checkforNewsFeed,action, usertoken),
                        call(checkforPinnedNewsFeed,action, usertoken)])
            console.log(pinnednewsfeedresponse)
            console.log(newsfeedresponse)
            if (newsfeedresponse.data.status === 'success'){
                yield put({type: 'LOAD_NEWSFEED_ALL_FULFILLED', payload: {data : newsfeedresponse.data, groupid : action.payload.groupid}})
            }
            if (pinnednewsfeedresponse.data.status === 'success'){
                yield put({type: 'LOAD_PINNED_NEWSFEED_ALL_FULFILLED', payload: {data : pinnednewsfeedresponse.data, groupid : action.payload.groupid}})
            }
        }else{
            console.log('Here2')
            const newsfeedresponse = yield call(checkforNewsFeed,action, usertoken) 
            if (newsfeedresponse.data.status === 'success'){
                yield put({type: 'LOAD_NEWSFEED_ALL_FULFILLED', payload: {data : newsfeedresponse.data, groupid : action.payload.groupid}})
            }
            
        }
        var endTime = new Date();
        var timeDiff = endTime - startTime
        miscutils.captureStat('API:newsfeedworkflowWithPinned','GetNews',timeDiff)
        console.log('got stats' + timeDiff)
        
        
        
    } catch (newserror) {
        console.log('Error Getting NewsFeed !')
        //**** IF ERROR IS DUE TO TOKEN TRY TO REFRESH TOKEN
        bugsnag.leaveBreadcrumb('Refreshing Token')
        console.log(newserror)
        // let session = yield call(loginapi.latestSession)
        // var usertoken = session.idToken.jwtToken
        // yield put({type : 'UPDATE_TOKEN', payload : {token : usertoken}})
        
        // const [newsfeedresponse,pinnednewsfeedresponse] = yield all([call(checkforNewsFeed,action, usertoken),call(checkforPinnedNewsFeed,action, usertoken)])

        // if (newsfeedresponse.data.status === 'success'){
        //     yield put({type: 'LOAD_NEWSFEED_ALL_FULFILLED', payload: {data : newsfeedresponse.data, groupid : action.payload.groupid}})
        // }else {
        //     console.log(newserror)
        //     yield put({type: 'LOAD_NEWSFEED_ALL_REJECTED', payload: newserror})
        // }
        // //********** PINNED POST ***********
        // //pinnednewsfeedresponse = yield call(checkforPinnedNewsFeed,action, usertoken)
        // if (pinnednewsfeedresponse.data.status === 'success'){
        //     yield put({type: 'LOAD_PINNED_NEWSFEED_ALL_FULFILLED', payload: {data : pinnednewsfeedresponse.data, groupid : action.payload.groupid}})
        // }else {
        //     console.log(newserror)
        //     yield put({type: 'LOAD_NEWSFEED_ALL_REJECTED', payload: newserror})
        // }
    }
}

var likeapostworkflow = function* (action) {
    try {
        let session = yield call(loginapi.latestSession)
        var usertoken = session.idToken.jwtToken
        var likeresponse = yield call(newsfeedapi.likepostAPI, action.payload.activityid, usertoken)
    } catch (likeactionerror) {
        console.log(likeactionerror)
        yield put({
            type: 'NEWSFEED_LIKE_ACTION_POST_REJECTED',
            payload: {activityid: action.payload.activityid, error: likeactionerror}
        })
    }
}

var unlikeapostworkflow = function* (action) {
    try {
        let session = yield call(loginapi.latestSession)
        var usertoken = session.idToken.jwtToken
        console.log('BEFPRE CALL !!!!')
        var likeresponse = yield call(newsfeedapi.unlikepostAPI, action.payload.activityid, usertoken)
        console.log(likeresponse)
    } catch (likeactionerror) {
        console.log('Error !!')
        console.log(likeactionerror)
        yield put({
            type: 'NEWSFEED_UNLIKE_ACTION_POST_REJECTED',
            payload: {activityid: action.payload.activityid, user: action.payload.user, error: likeactionerror}
        })
    }
}

var viewpostworkflow = function* (action) {
    try {
        console.log('BEFORE CALL !!!!')
        let session = yield call(loginapi.latestSession)
        var usertoken = session.idToken.jwtToken
        var viewpostresponse = yield call(newsfeedapi.viewPostAPI, action.payload.activityid, usertoken)
        console.log(viewpostresponse)
        yield put({
            type: 'NEWSFEED_VIEW_POST_FULFILLED',
            payload: {activityid: action.payload.activityid, response: viewpostresponse}
        })
    } catch (viewposterror) {
        console.log(viewposterror)
        console.log('Erroring Registering View Post')
    }
}

var setTabChangeworkflow = function* (action) {
    console.log(action)
    if (action.payload.tabname === "nav.Three60Memos.HomeScreen") {
        if ((action.payload.prevtab === "nav.Three60Memos.EventsScreen") ||
            (action.payload.prevtab === "nav.Three60Memos.ChatRoomsScreen") ||
            (action.payload.prevtab === "nav.Three60Memos.ChatScreen") ||
            (action.payload.prevtab === "nav.Three60Memos.GroupDetailScreen") ||
            (action.payload.prevtab === "nav.Three60Memos.ClassListScreen")||
            (action.payload.prevtab === "nav.Three60Memos.BriefcaseScreen") ) {
            try {
                console.log('Detected Home Tab')
                yield put({type: 'NEWS_FEED_REFRESH', payload: null})
            } catch (newserror) {
            }
        }
    }
}

var refreshBriefcaseworkflow = function* (action) {
    let session = yield call(loginapi.latestSession)
    var usertoken = session.idToken.jwtToken
    console.log('In Sagas, refreshing Briefcase')
    var starttime = new Date()
    try{
        const [draftresponse, scheduledresponse, reviewresponse] = yield all([call(newsfeedapi.getNewsFeedAPI, action.payload.page,action.payload.groups, usertoken,false,true,false,false, 100  ),
                call(newsfeedapi.getNewsFeedAPI, action.payload.page,action.payload.groups, usertoken,false,false,false,true, 1000  ),
                call(newsfeedapi.getNewsFeedAPI, action.payload.page,action.payload.groups, usertoken,false,false,true,false, 1000  )])
        console.log(draftresponse)
        var endtime = new Date()
       
        miscutils.captureStat('API:BriefCase','GetAllItem',new Date() - starttime)
        yield put({type  :'REFRESH_BRIEFCASE_FULFILLED', payload : {draftfeed : draftresponse.data.posts, scheduledfeed : scheduledresponse.data.posts, reviewfeed : reviewresponse.data.posts}})
    }
    catch(error){
        yield put({type  :'REFRESH_BRIEFCASE_REJECTED', payload : error})
    }
   
    
}

    module.exports = {newsfeedworkflow, likeapostworkflow, unlikeapostworkflow, viewpostworkflow, setTabChangeworkflow, refreshBriefcaseworkflow}

