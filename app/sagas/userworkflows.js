
import {call, put} from 'redux-saga/effects'
import * as userapis from '../api/userapi'
import * as loginapi from '../api/loginapi'
import * as newsaction from '../actions/newsfeedActions'

var sendFeedbackworkflow = function* (action) {
    console.log(action)

    var response =  yield call(userapis.sendFeedback, action.payload.feedback, action.payload.data, action.payload.token)
    console.log(response)
    console.log(response.data)
    if (response.data === true){
        console.log('Feedback sent')
        yield put({type: 'SEND_FEEDBACK_FULFILLED', payload: response.data})
    }else{
        yield put({type: 'SEND_FEEDBACK_REJECTED', payload: response.data})
    }
}

var updateToken = function* (action) {
    console.log(action)
    yield put({type: 'UPDATE_TOKEN', payload: {token : action.payload.token}})
    if (action.payload.groups !== null) {
        if (action.payload.NewRefresh) {
            yield put({
                type: 'LOAD_NEWSFEED_ALL',
                payload: {page: 0, usertoken: action.payload.token, groups: action.payload.groups}
            })
        }else {
            console.log('No News Refresh !!! ')
        }
    }
}

var updateChildAuthotizedUser = function* (action) {
    console.log(action)
    var response = yield call(userapis.addupdateChildAuthorizedUser, action.payload.name, action.payload.childid, action.payload.code, action.payload.token)
    console.log(response)
    if (response.data.status === "success"){
        var childobjs = yield call(loginapi.getuserchildrenapi, action.payload.token)
        console.log(childobjs)
        yield put({type: 'UPDATE_CHILD_OBJS', payload: {childobjs : childobjs.data.data}})
        yield put({type: 'SET_CHILD_AUTHORIZED_USER_FULFILLED'})
    }else {

        yield put({type: 'SET_CHILD_AUTHORIZED_USER_REJECTED'})
    }

}

var deleteChildAuthotizedUser = function* (action) {
    console.log(action)
    var response = yield call(userapis.deleteChildAuthorizedUser, action.payload.name, action.payload.childid, action.payload.code, action.payload.token)
    console.log(response)
    if (response.data.status === "success"){
        var childobjs = yield call(loginapi.getuserchildrenapi, action.payload.token)
        console.log(childobjs)
        yield put({type: 'UPDATE_CHILD_OBJS', payload: {childobjs : childobjs.data.data}})
        yield put({type: 'DELETE_CHILD_AUTHORIZED_USER_FULFILLED'})
    }else {

        yield put({type: 'DELETE_CHILD_AUTHORIZED_USER_REJECTED'})
    }

}

module.exports = {
    sendFeedbackworkflow, updateToken, updateChildAuthotizedUser, deleteChildAuthotizedUser
}