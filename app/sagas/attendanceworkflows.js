import * as attendanceapi from '../api/attendanceapi'
import {call, put} from 'redux-saga/effects'
import * as loginapi from '../api/loginapi'
var EnvConfigs = require('../config/environment');
var validatePinworkflow = function* (action) {
    let session = yield call(loginapi.latestSession)
    var usertoken = session.idToken.jwtToken

    var status = action.payload.checkinMode ? EnvConfigs.ATTENDANCESTATE.CHECKEDIN : EnvConfigs.ATTENDANCESTATE.CHECKEDOUT
    var validatepinresponse = yield call(attendanceapi.validatepin,
        action.payload.childobj,
        action.payload.pincode,
        status,
        usertoken);
    console.log(validatepinresponse);
    if (validatepinresponse.data.status === 'success') {
        var username = validatepinresponse.data.username;
        yield put({
            type: 'ATTENDANCE_VALIDATE_PIN_FULFILLED', payload: {
                childobj: action.payload.childobj,
                username: username,
                attendancedate: action.payload.attendancedate,
                checkinMode: action.payload.checkinMode
            }
        })


    } else {

        yield put({type: 'ATTENDANCE_VALIDATE_PIN_REJECTED', payload: {error: validatepinresponse.error}})
    }

};

var setAttendanceOverride = function* (action) {
    let session = yield call(loginapi.latestSession)
    var usertoken = session.idToken.jwtToken
    var response = yield call(attendanceapi.setAttendanceOverride,
        action.payload.attendancedate,
        action.payload.childobj,
        action.payload.status,
        usertoken)
    console.log(response);

    if (response.data.status === 'success') {
        var username = response.data.username;
        yield put({
            type: 'ATTENDANCE_SET_STATUS_OVERRIDE_FULFILLED', payload: {
                attendancedate: action.payload.attendancedate,
                groupid: action.payload.groupid,
                username: username,
                childobj: action.payload.childobj,
                status: action.payload.status
            }
        })
    } else {

        yield put({type: 'ATTENDANCE_SET_STATUS_OVERRIDE_REJECTED', payload: {error: response.error}})
    }
}

var getAuthorizedUsers = function* (action) {
    let session = yield call(loginapi.latestSession)
    var usertoken = session.idToken.jwtToken
    var response = yield call(attendanceapi.getAuthorizedUsers, action.payload.childobj, usertoken)
    console.log(response);
    console.log(response.data);
    console.log(response.data.status);
    if (response.data.status === 'success') {
        var usernames = response.data.data;
        yield put({
            type: 'ATTENDANCE_AUTHORIZED_USERS_FULFILLED', payload: {
                authorizedUsers : usernames, childobj : action.payload.childobj
            }
        })
    } else {

        yield put({type: 'ATTENDANCE_AUTHORIZED_USERS_REJECTED', payload: {error: response.error, childobj : action.payload.childobj}})
    }
}

var setCheckoutOverride = function* (action) {
    let session = yield call(loginapi.latestSession)
    var usertoken = session.idToken.jwtToken

    var response = yield call(attendanceapi.setCheckoutOverride, action.payload.attendancedate,
                                            action.payload.groupid,
                                            action.payload.childobj,
                                            action.payload.overrideuser,
                                            usertoken)
    console.log(response);

    if (response.data.status === 'success') {
        var usernames = response.data;
        yield put({
            type: 'ATTENDANCE_CHECKOUT_OVERRIDE_FULFILLED', payload: {
                authorizedUsers : usernames, childobj : action.payload.childobj,
                overrideuser : action.payload.overrideuser,
                attendancedate: action.payload.attendancedate,
                groupid : action.payload.groupid
            }})
        yield put({type : 'UPDATE_TOKEN', payload : {token : usertoken}})

    } else {

        yield put({type: 'ATTENDANCE_CHECKOUT_OVERRIDE_REJECTED', payload: {error: response.error, childobj : action.payload.childobj}})
    }
}

var initGroupChildren = function* (action) {
    let session = yield call(loginapi.latestSession)
    var usertoken = session.idToken.jwtToken
    var response = yield call(attendanceapi.getChildrenforGroups,
        action.payload.groups,
        usertoken)
    console.log(response);

    if (response.data.status === 'success') {
        yield put({
            type: 'ATTENDANCE_INIT_LIST_FULFILLED', payload: {
                groups : response.data.data,
                attendancedate: action.payload.attendancedate,

            }})
        yield  put({type : 'ATTENDANCE_STATUS_REFRESH', payload : {groups : action.payload.groups,
                                            token : usertoken, attendancedate: action.payload.attendancedate}})

    } else {

        yield put({type: 'ATTENDANCE_INIT_LIST_REJECTED', payload: {error: response.error}})
    }
}

var refreshAttendanceStatus = function* (action) {
    let session = yield call(loginapi.latestSession)
    var usertoken = session.idToken.jwtToken
    var response = yield call(attendanceapi.getLatestStatus,
        action.payload.groups,
        usertoken)
    console.log(response);

    if (response.data.status === 'success') {
        yield put({
            type: 'ATTENDANCE_STATUS_REFRESH_FULFILLED', payload: {
                statuses : response.data.data,
                attendancedate: action.payload.attendancedate
            }})
        yield put({type : 'UPDATE_TOKEN', payload : {token : usertoken}})

    } else {

        yield put({type: 'ATTENDANCE_STATUS_REFRESH_REJECTED', payload: {error: response.error}})
    }
}

module.exports = {validatePinworkflow, setAttendanceOverride, getAuthorizedUsers, setCheckoutOverride, initGroupChildren, refreshAttendanceStatus};