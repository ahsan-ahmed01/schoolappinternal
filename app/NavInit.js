import React, { Component } from "react";
import { Platform, AppState, AppRegistry } from "react-native";
import { Provider } from "react-redux";
import { Navigation } from "react-native-navigation";
import { registerScreens } from "./screens";
import aws_exports from "../aws-exports";
import Amplify, { Auth } from "aws-amplify";
import store from "./reducers/store";
import Entypo from "react-native-vector-icons/Entypo";
import Ionic from "react-native-vector-icons/Ionicons";
import FontAwesome from "react-native-vector-icons/FontAwesome";
import {
  setCustomView,
  setCustomTextInput,
  setCustomText,
  setCustomImage,
  setCustomTouchableOpacity,
} from "react-native-global-props";

var bugsnag = require("./config/bugsnag");

var EnvConfigs = require("./config/environment");
//********* Initialization of Application ********/
initializeApplication();
console.log("Done Initializing App");
console.log(store);
registerScreens(store, Provider);
console.log("Done Registering Screen");
var homeIcon = null;
var eventIcon = null;
var chatIcon = null;
var classIcon = null;
var actionIcon = null;
var briefcaseIcon = null;

var ScreenMode = Object.freeze({
  LoginScreen: 1,
  AppScreen: 2,
  AttendanceScreen: 3,
});

export default class App extends Component {
  constructor(props) {
    super(props);
    //console.log(props)
    store.subscribe(this.onStoreUpdate.bind(this));
    //store.dispatch(appActions.appInitialized());
    Entypo.getImageSource("home", 30).then((source) => {
      homeIcon = source;
    });
    Ionic.getImageSource("ios-calendar", 30).then((source) => {
      eventIcon = source;
    });
    Ionic.getImageSource("ios-school", 30).then((source) => {
      classIcon = source;
    });
    Entypo.getImageSource("chat", 30).then((source) => {
      chatIcon = source;
    });
    Entypo.getImageSource("circle-with-plus", 60).then((source) => {
      actionIcon = source;
    });
    FontAwesome.getImageSource("briefcase", 30).then((source) => {
      briefcaseIcon = source;
    });
    this.currentScreen = 0;
  }

  componentDidCatch(error, info) {
    //console.log('componentDidCatch called.');
    //console.log(error)
  }

  onStoreUpdate() {
    var newScreen = ScreenMode.AppScreen;

    if (Auth.user !== null && store.getState().login.isLoggedin) {
      //console.log('Found User Object is not null and user is logged in !')

      if (store.getState().user.roles === "attendance") {
        newScreen = ScreenMode.AttendanceScreen;
      } else {
        if (store.getState().user.roles === null) {
          newScreen = ScreenMode.LoginScreen;
        } else {
          newScreen = ScreenMode.AppScreen;
        }
      }
    } else {
      if (
        store.getState().login.usercache === null &&
        store.getState().login.authObj === null
      ) {
        //console.log('Found user auth obj is null and so is cache')
        newScreen = ScreenMode.LoginScreen;
      } else {
        if (store.getState().user.roles === "attendance") {
          newScreen = ScreenMode.AttendanceScreen;
        } else {
          if (store.getState().user.roles === null) {
            newScreen = ScreenMode.LoginScreen;
          } else {
            newScreen = ScreenMode.AppScreen;
          }
        }
      }
    }

    if (this.currentScreen !== newScreen) {
      this.currentScreen = newScreen;
      //console.log(newScreen)
      bugsnag.leaveBreadcrumb(
        "Changing App to Screen ID->" + newScreen.toString()
      );
      this.startApp(newScreen);
    }
  }

  startApp(mode) {
    switch (mode) {
      case ScreenMode.LoginScreen:
        Navigation.setRoot({
          root: {
            stack: {
              children: [
                {
                  component: {
                    name: "nav.Three60Memos.LoginScreen",
                    passProps: {},
                    options: {
                      topBar: {
                        visible: true,
                        title: {
                          text: "Welcome to " + EnvConfigs.SCHOOL_NAME,
                          fontFamily: "MontSerrat Bold",
                          fontSize: 16,
                          color: "black",
                        },
                      },
                    },
                  },
                },
              ],
            },
          },
        });
        return;
      case ScreenMode.AttendanceScreen:
        Navigation.setRoot({
          root: {
            stack: {
              children: [
                {
                  component: {
                    name: "nav.Three60Memos.AttendanceSummaryList",
                    passProps: {
                      text: "stack with one child",
                    },
                    options: {
                      topBar: {
                        visible: true,
                        title: {
                          text: EnvConfigs.SCHOOL_NAME + " Attendance",
                          color: "white",
                        },
                        background: {
                          color: EnvConfigs.MAIN_HOME_COLOR,
                        },
                        leftButtons: {
                          id: "logo",
                          component: {
                            name: "nav.Three60Memos.ThreeSixtyLogo",
                          },
                        },
                        rightButtons: {
                          id: "ProfileButton",
                          component: {
                            name: "nav.Three60Memos.LogoutButton",
                          },
                        },
                      },
                    },
                  },
                },
              ],
              options: {},
            },
          },
        });
        return;
      case ScreenMode.AppScreen:
        var role = store.getState().user.roles;
        //console.log(store.getState())
        //console.log(role)
        if (
          role === "admin" ||
          role === "staff" ||
          role === "teacher" ||
          role === "pta"
        ) {
          Navigation.setRoot({
            root: {
              bottomTabs: {
                id: "idBottomTabs",
                children: [
                  {
                    stack: {
                      children: [
                        {
                          component: {
                            name: "nav.Three60Memos.HomeScreen",
                            options: {
                              bottomTab: {
                                text: "Home",
                                icon: homeIcon,
                                textColor: "#77808e",
                                iconColor: "#77808e",
                                selectedTextColor: EnvConfigs.MAIN_EVENT_COLOR,
                                selectedIconColor: EnvConfigs.MAIN_EVENT_COLOR,
                              },
                              topBar: {
                                title: {
                                  text: EnvConfigs.TITLE_NAME,
                                  color: "white",
                                },
                                background: {
                                  color: EnvConfigs.MAIN_EVENT_COLOR,
                                },
                                leftButtons:
                                  Platform.OS == "android"
                                    ? null
                                    : {
                                        id: "logo",
                                        component: {
                                          name:
                                            "nav.Three60Memos.ThreeSixtyLogo",
                                        },
                                      },
                                rightButtons:
                                  Platform.OS == "android"
                                    ? null
                                    : {
                                        id: "ProfileButton",
                                        component: {
                                          name:
                                            "nav.Three60Memos.ProfileButton",
                                        },
                                      },
                              },
                            },
                          },
                        },
                      ],
                    },
                  },
                  {
                    stack: {
                      children: [
                        {
                          component: {
                            id: "idEventTab",
                            name: "nav.Three60Memos.EventsScreen",
                            options: {
                              bottomTab: {
                                text: "Events",
                                icon: eventIcon,
                                textColor: "#77808e",
                                iconColor: "#77808e",
                                selectedTextColor: EnvConfigs.MAIN_EVENT_COLOR,
                                selectedIconColor: EnvConfigs.MAIN_EVENT_COLOR,
                              },
                              topBar: {
                                title: {
                                  text: "Events",
                                  color: "white",
                                },
                                background: {
                                  color: EnvConfigs.MAIN_EVENT_COLOR,
                                },
                                rightButtons:
                                  Platform.OS == "android"
                                    ? null
                                    : {
                                        id: "ProfileButton",
                                        component: {
                                          name:
                                            "nav.Three60Memos.ProfileButton",
                                        },
                                      },
                              },
                            },
                          },
                        },
                      ],
                    },
                  },
                  {
                    stack: {
                      children: [
                        {
                          component: {
                            id: "idChatTab",
                            name: "nav.Three60Memos.ChatRoomsScreen",
                            options: {
                              bottomTab: {
                                text: "Chat",
                                icon: chatIcon,
                                textColor: "#77808e",
                                iconColor: "#77808e",
                                selectedTextColor: EnvConfigs.MAIN_CHAT_COLOR,
                                selectedIconColor: EnvConfigs.MAIN_CHAT_COLOR,
                              },
                              topBar: {
                                title: {
                                  text: "Chat",
                                  color: "white",
                                },
                                background: {
                                  color: EnvConfigs.MAIN_CHAT_COLOR,
                                },
                                rightButtons:
                                  Platform.OS == "android"
                                    ? null
                                    : {
                                        id: "ProfileButton",
                                        component: {
                                          name:
                                            "nav.Three60Memos.ProfileButton",
                                        },
                                      },
                              },
                            },
                          },
                        },
                      ],
                    },
                  },
                  {
                    stack: {
                      children: [
                        {
                          component: {
                            name: "nav.Three60Memos.ClassListScreen",
                            options: {
                              bottomTab: {
                                text: "Classes",
                                icon: classIcon,
                                textColor: "#77808e",
                                iconColor: "#77808e",
                                selectedTextColor: EnvConfigs.MAIN_GROUP_COLOR,
                                selectedIconColor: EnvConfigs.MAIN_GROUP_COLOR,
                              },
                              topBar: {
                                title: {
                                  text: "Classes",
                                  color: "white",
                                },
                                background: {
                                  color: EnvConfigs.MAIN_GROUP_COLOR,
                                },
                                rightButtons:
                                  Platform.OS == "android"
                                    ? null
                                    : {
                                        id: "ProfileButton",
                                        component: {
                                          name:
                                            "nav.Three60Memos.ProfileButton",
                                        },
                                      },
                              },
                            },
                          },
                        },
                      ],
                    },
                  },
                  {
                    stack: {
                      children: [
                        {
                          component: {
                            id: "idBriefcaseTab",
                            name: "nav.Three60Memos.BriefcaseScreen",
                            options: {
                              bottomTab: {
                                text: "Briefcase",
                                icon: briefcaseIcon,
                                textColor: "#77808e",
                                iconColor: "#77808e",
                                selectedTextColor: EnvConfigs.MAIN_GROUP_COLOR,
                                selectedIconColor: EnvConfigs.MAIN_GROUP_COLOR,
                              },
                              topBar: {
                                title: {
                                  text: "My Briefcase",
                                  color: "white",
                                },
                                background: {
                                  color: EnvConfigs.MAIN_GROUP_COLOR,
                                },
                                rightButtons:
                                  Platform.OS == "android"
                                    ? null
                                    : {
                                        id: "ProfileButton",
                                        component: {
                                          name:
                                            "nav.Three60Memos.ProfileButton",
                                        },
                                      },
                              },
                            },
                          },
                        },
                      ],
                    },
                  },
                ],
                options: {},
              },
            },
          });
        } else {
          Navigation.setRoot({
            root: {
              bottomTabs: {
                id: "idBottomTabs",
                children: [
                  {
                    stack: {
                      children: [
                        {
                          component: {
                            name: "nav.Three60Memos.HomeScreen",
                            options: {
                              bottomTab: {
                                text: "Home",
                                icon: homeIcon,
                                textColor: "#77808e",
                                iconColor: "#77808e",
                                selectedTextColor: EnvConfigs.MAIN_HOME_COLOR,
                                selectedIconColor: EnvConfigs.MAIN_HOME_COLOR,
                              },
                              topBar: {
                                title: {
                                  text: EnvConfigs.TITLE_NAME,
                                  color: "white",
                                },
                                background: {
                                  color: EnvConfigs.MAIN_HOME_COLOR,
                                },
                                leftButtons:
                                  Platform.OS == "android"
                                    ? {}
                                    : {
                                        id: "logo",
                                        component: {
                                          name:
                                            "nav.Three60Memos.ThreeSixtyLogo",
                                        },
                                      },
                                rightButtons:
                                  Platform.OS == "android"
                                    ? {}
                                    : {
                                        id: "ProfileButton",
                                        component: {
                                          name:
                                            "nav.Three60Memos.ProfileButton",
                                        },
                                      },
                              },
                            },
                          },
                        },
                      ],
                    },
                  },
                  {
                    stack: {
                      children: [
                        {
                          component: {
                            id: "idEventTab",
                            name: "nav.Three60Memos.EventsScreen",
                            options: {
                              bottomTab: {
                                text: "Events",
                                icon: eventIcon,
                                textColor: "#77808e",
                                iconColor: "#77808e",
                                selectedTextColor: EnvConfigs.MAIN_EVENT_COLOR,
                                selectedIconColor: EnvConfigs.MAIN_EVENT_COLOR,
                              },
                              topBar: {
                                title: {
                                  text: "Events",
                                  color: "white",
                                },
                                background: {
                                  color: EnvConfigs.MAIN_EVENT_COLOR,
                                },
                                rightButtons:
                                  Platform.OS == "android"
                                    ? null
                                    : {
                                        id: "ProfileButton",
                                        component: {
                                          name:
                                            "nav.Three60Memos.ProfileButton",
                                        },
                                      },
                              },
                            },
                          },
                        },
                      ],
                    },
                  },
                  {
                    stack: {
                      children: [
                        {
                          component: {
                            id: "idChatTab",
                            name: "nav.Three60Memos.ChatRoomsScreen",
                            options: {
                              bottomTab: {
                                text: "Chat",
                                icon: chatIcon,
                                textColor: "#77808e",
                                iconColor: "#77808e",
                                selectedTextColor: EnvConfigs.MAIN_CHAT_COLOR,
                                selectedIconColor: EnvConfigs.MAIN_CHAT_COLOR,
                              },
                              topBar: {
                                title: {
                                  text: "Chat",
                                  color: "white",
                                },
                                background: {
                                  color: EnvConfigs.MAIN_CHAT_COLOR,
                                },
                                rightButtons:
                                  Platform.OS == "android"
                                    ? null
                                    : {
                                        id: "ProfileButton",
                                        component: {
                                          name:
                                            "nav.Three60Memos.ProfileButton",
                                        },
                                      },
                              },
                            },
                          },
                        },
                      ],
                    },
                  },
                  {
                    stack: {
                      children: [
                        {
                          component: {
                            name: "nav.Three60Memos.ClassListScreen",
                            options: {
                              bottomTab: {
                                text: "Classes",
                                icon: classIcon,
                                textColor: "#77808e",
                                iconColor: "#77808e",
                                selectedTextColor: EnvConfigs.MAIN_GROUP_COLOR,
                                selectedIconColor: EnvConfigs.MAIN_GROUP_COLOR,
                              },
                              topBar: {
                                title: {
                                  text: "Classes",
                                  color: "white",
                                },
                                background: {
                                  color: EnvConfigs.MAIN_GROUP_COLOR,
                                },
                                rightButtons:
                                  Platform.OS == "android"
                                    ? null
                                    : {
                                        id: "ProfileButton",
                                        component: {
                                          name:
                                            "nav.Three60Memos.ProfileButton",
                                        },
                                      },
                              },
                            },
                          },
                        },
                      ],
                    },
                  },
                ],
                options: {},
              },
            },
          });
        }
        if (Platform.OS == "android") {
          Navigation.setDefaultOptions({
            bottomTabs: {
              titleDisplayMode: "alwaysShow",
              animate: true,
            },
            topBar: {
              title: {
                alignment: "center",
              },
            },
          });
        }
        return;

      default:
      //console.log("Not Root Found");
    }
  }
}

function initializeApplication() {
  Amplify.configure(aws_exports);
  //console.log('---------- AWS CONFIGURED -------------------')
  const customViewProps = {
    style: {
      fontFamily: "Helvetica Neue",
    },
  };
  setCustomView(customViewProps);
  setCustomTextInput(customViewProps);
  setCustomText(customViewProps);
  setCustomImage(customViewProps);
  setCustomTouchableOpacity(customViewProps);
}
