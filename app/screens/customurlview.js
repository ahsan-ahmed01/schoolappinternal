import {
    Platform,
    AppRegistry,
    Text,
    ScrollView,
    StyleSheet,
    View,
    TextInput,
    Alert,
    TouchableOpacity, ActivityIndicator, Image, TouchableHighlight
} from 'react-native'
import React, {Component} from 'react'
import * as rootNavAction from '../actions/rootNavActions'
import {Navigation} from 'react-native-navigation'
import {connect} from 'react-redux';
import Icon from 'react-native-vector-icons/FontAwesome';
import CustomBlurView from '../components/CustomBlurView';
import { WebView } from 'react-native';
var EnvConfig = require('../config/environment')

class CustomViewURL extends React.Component {

    navigationButtonPressed({ buttonId }) {
        console.log(buttonId)
        if (buttonId === 'closebutton'){
            Navigation.dismissAllModals()
        }
    }

    constructor(props){
        super(props)
        this.state = {
        }
        Navigation.events().bindComponent(this);

    }

    render(){



        return (

                        <WebView
                            source={{uri: EnvConfig.CUSTOM_MAIN_MENU[0].URL}}
                            style={{marginTop: 2, borderColor : 'gray', borderWidth: 1}}
                        />

        )
    }

}

const styles = StyleSheet.create({
    buttonClose:{
        flexDirection : 'row-reverse',
        paddingTop : 10,
        paddingRight : 5

    },closeButton : {
        paddingTop : (Platform.OS === 'ios') ? 10 : 0,
        color : 'black'
    },
    container: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center',
        padding: 2,
        position: "absolute",
        top: 10, left: 0, bottom: 0, right: 0,

    },
})

var mapStateToProps = function(store){
    return {user: store.user,memberprofile: store.memberprofile};
};

var mapDispatchToProps = function(dispatch){
    return {
        hideCustomMenuModal: function () {dispatch(rootNavAction.hideCustomMenuModal())}
    }
};

export default connect(mapStateToProps,mapDispatchToProps)(CustomViewURL)