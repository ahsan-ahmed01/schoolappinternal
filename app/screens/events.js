"use strict";
import {
  Text,
  StyleSheet,
  ScrollView,
  Image,
  RefreshControl,
  ListView,
  View,
  TouchableOpacity,
  ActivityIndicator,
} from "react-native";
import React, { Component, PureComponent } from "react";
import ProfileButton from "../components/ProfileButton";
import Ionic from "react-native-vector-icons/Ionicons";
var addeventimg = null;
import * as userActions from "../actions/userActions";
import { connect } from "react-redux";
import EventView from "./components/eventview";

import { Navigation } from "react-native-navigation";
import HMSActionButton from "../navigation/HMSActionButton";
var closeIcon = null;
class EventsScreen extends PureComponent {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <View style={styles.container}>
        <HMSActionButton
          userroles={this.props.userroles}
          parent={"nav.Three60Memos.EventsScreen"}
        />
        {this.ErrorBlock()}
        {this.OngoingAddEventBlock()}
        <EventView navigation={this.props.navigation} />
      </View>
    );
  }

  ErrorBlock() {
    if (this.props.events.actionError) {
      return (
        <Text style={styles.errorAction}>{this.props.events.errorText}</Text>
      );
    } else {
      return null;
    }
  }
  OngoingAddEventBlock() {
    if (this.props.events.iscreatingEvent) {
      return (
        <View style={styles.addingeventcontainer}>
          <Text style={styles.addingeventtext}>Saving Event ...</Text>
          <ActivityIndicator
            style={styles.activityaddingevent}
            animating={true}
            color="white"
          />
        </View>
      );
    } else {
      return null;
    }
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 5,
    backgroundColor: "#fff",
  },
  composeEvent: {
    paddingLeft: 10,
    paddingBottom: 10,
  },
  MenuIcon: {
    paddingRight: 10,
  },
  addingeventcontainer: {
    flexDirection: "row",
    backgroundColor: "#05B2D1",
    marginBottom: 5,
    marginTop: 2,
    paddingTop: 2,
    paddingBottom: 3,
    paddingLeft: 30,
    paddingRight: 30,
    alignSelf: "center",
    borderRadius: 5,
    justifyContent: "space-between",
  },
  addingeventtext: {
    color: "white",
    fontSize: 18,
    alignItems: "center",
  },
  activityaddingevent: {
    height: 25,
    width: 25,
    paddingLeft: 70,
    paddingTop: 2,
    paddingBottom: 2,
  },
  errorAction: {
    backgroundColor: "red",
    color: "white",
    marginBottom: 5,
    paddingTop: 2,
    paddingBottom: 2,
    paddingLeft: 20,
    paddingRight: 20,
    alignSelf: "center",
  },
});

var mapStateToProps = function (store) {
  return { events: store.events, userroles: store.user.roles };
};

var mapDispatchToProps = function (dispatch) {
  return {
    setNavigationTab: function (tabName) {
      dispatch(userActions.setNavigationTab(tabName));
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(EventsScreen);
