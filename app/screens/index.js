'use strict'

import {Navigation} from 'react-native-navigation'

import ChatRoomsScreen from './chatrooms'
import EventsScreen from './events'
import ClassListScreen from './classlist'
import LoginScreen from './login'
import HomeScreen from "./HomeScreen";
import WelcomeScreen from './WelcomeScreen'
import ProfileButton from '../components/ProfileButton'
import ThreeSixtyLogo from './components/ThreeSixtyLogo'
import MemberDetailsScreen from './memberdetails'
import ChildDetailScreen from './attendance/childdetails'
import PostDetailsScreen from './postdetail'
import CommentsScreen from './comments'
import LikedUsersScreen from './likedusers'
import AddPTEventScreen from './addPTEvent'
import AddRsvpEventScreen from "./addRsvpEvent"
import AddEventScreen from "./addEvent"
import AddVolunteerEventScreen from "./addVolunteerEvent"
import ChatScreen from './chat'
import GroupDetailScreen from './groupdetails'
import PostScreen from './post'
import FeedBackScreen from './feedback'
import AttendanceSummaryList from './attendance/attendanceSummaryList'
import LogoutButton from './components/logoutbutton'
import ModalPhotoBrowser from './modalphotobrowser'
import CustomViewURL from './customurlview'
import StatScreen from './StatScreen'
import BriefcaseScreen from './briefcase'

console.log('In Register file')
// register all screens of the app (including internal ones)
export function registerScreens (store, Provider) {
    console.log('Registering Screens')
    console.log(store)
    Navigation.registerComponentWithRedux('nav.Three60Memos.HomeScreen', () => HomeScreen, Provider,store)
    Navigation.registerComponentWithRedux('nav.Three60Memos.LoginScreen', () => LoginScreen, Provider,store)
    Navigation.registerComponentWithRedux('nav.Three60Memos.EventsScreen', () => EventsScreen, Provider, store)
    Navigation.registerComponentWithRedux('nav.Three60Memos.ChatRoomsScreen', () => ChatRoomsScreen,  Provider,store)
    Navigation.registerComponentWithRedux('nav.Three60Memos.ClassListScreen', () => ClassListScreen,  Provider,store)
    Navigation.registerComponentWithRedux('nav.Three60Memos.ProfileButton', () => ProfileButton,  Provider,store)
    Navigation.registerComponentWithRedux('nav.Three60Memos.ThreeSixtyLogo', ()=> ThreeSixtyLogo, Provider, store)
    Navigation.registerComponentWithRedux('nav.Three60Memos.MemberDetailsScreen', ()=> MemberDetailsScreen, Provider, store)
    Navigation.registerComponentWithRedux('nav.Three60Memos.ChildDetailScreen', ()=> ChildDetailScreen, Provider, store)
    Navigation.registerComponentWithRedux('nav.Three60Memos.PostDetailsScreen', ()=> PostDetailsScreen, Provider, store)
    Navigation.registerComponentWithRedux('nav.Three60Memos.CommentsScreen', ()=> CommentsScreen, Provider, store)
    Navigation.registerComponentWithRedux('nav.Three60Memos.LikedUsersScreen', ()=> LikedUsersScreen, Provider, store)
    Navigation.registerComponentWithRedux('nav.Three60Memos.AddPTEventScreen', ()=> AddPTEventScreen, Provider, store)
    Navigation.registerComponentWithRedux('nav.Three60Memos.AddRsvpEventScreen', ()=> AddRsvpEventScreen, Provider, store)
    Navigation.registerComponentWithRedux('nav.Three60Memos.AddEventScreen', ()=> AddEventScreen, Provider, store)
    Navigation.registerComponentWithRedux('nav.Three60Memos.AddVolunteerEventScreen', ()=> AddVolunteerEventScreen, Provider, store)
    Navigation.registerComponentWithRedux('nav.Three60Memos.ChatScreen', ()=> ChatScreen, Provider, store)
    Navigation.registerComponentWithRedux('nav.Three60Memos.GroupDetailScreen', ()=> GroupDetailScreen, Provider, store)
    Navigation.registerComponentWithRedux('nav.Three60Memos.PostScreen', ()=> PostScreen, Provider, store)
    Navigation.registerComponentWithRedux('nav.Three60Memos.FeedBackScreen', ()=> FeedBackScreen, Provider, store)
    Navigation.registerComponentWithRedux('nav.Three60Memos.AttendanceClass', ()=> AttendanceClass, Provider, store)
    Navigation.registerComponentWithRedux('nav.Three60Memos.AttendanceSummaryList', ()=> AttendanceSummaryList, Provider, store)
    Navigation.registerComponentWithRedux('nav.Three60Memos.LogoutButton', ()=> LogoutButton, Provider, store)
    Navigation.registerComponentWithRedux('nav.Three60Memos.CommentsScreen', ()=> CommentsScreen, Provider, store)
    Navigation.registerComponentWithRedux('nav.Three60Memos.ModalPhotoBrowser', ()=> ModalPhotoBrowser, Provider, store)
    Navigation.registerComponentWithRedux('nav.Three60Memos.CustomViewURL', ()=> CustomViewURL, Provider, store)
    Navigation.registerComponentWithRedux('nav.Three60Memos.StatScreen', ()=> StatScreen, Provider, store)
    Navigation.registerComponentWithRedux('nav.Three60Memos.BriefcaseScreen', ()=> BriefcaseScreen, Provider, store)

    Navigation.registerComponentWithRedux('nav.Three60Memos.WelcomeScreen', () => WelcomeScreen,  Provider,store)
    console.log('Done Registering Screens')
}
