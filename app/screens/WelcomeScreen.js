const React = require('react');
const { Component } = require('react');
const { View, Text, Platform, TouchableHighlight } = require('react-native');
import { connect } from 'react-redux'
const { Navigation } = require('react-native-navigation');
import {Card} from 'react-native-elements'
class WelcomeScreen extends Component {
    static get options() {
        return {
            _statusBar: {
                backgroundColor: 'transparent',
                style: 'dark',
                drawBehind: true
            },
            topBar: {
                title: {
                    text: 'My Screen'
                },
                largeTitle: {
                    visible: false,
                },
                drawBehind: true,
                visible: false,
                animate: false
            }
        };
    }

    render() {
        var msg = this.props.textMessage === undefined ? "No Message Specified" : this.props.textMessage
        var msg2 = this.props.textMessagesmall === undefined ? "No Message Specified" : this.props.textMessagesmall
        return (
            <View style={styles.root}>
                            <Card
                title={msg}>
                <Text style={{marginBottom: 10}}>
                   {msg2}
                </Text>
                </Card>
            </View>
        );
    }
}

export default connect(null, null, null, { withRef: true })(WelcomeScreen);


const styles = {
    root: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#e8e8e8',
    },
    message : {
      textAlign: 'center',
        fontSize : 32
    },
    message2 : {
        textAlign: 'center',
        fontSize : 20,
        color : 'blue'
    },
    bar: {
        flex: 1,
        marginTop : 200,
        flexDirection: 'column',
        backgroundColor: '#e8e8e8',
        height : 100,


    },
    h1: {
        fontSize: 24,
        textAlign: 'center',
        margin: 30
    },
    footer: {
        fontSize: 10,
        color: '#888',
        marginTop: 10
    }
};
