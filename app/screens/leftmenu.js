'use strict'
import {StyleSheet, View, TouchableOpacity, Image, Text} from 'react-native'
import React, {Component} from 'react'
import {connect} from 'react-redux'
import {commonStyle} from '../config/commonstyles'
import * as memberActions from '../actions/memberprofileActions'

class LeftMenuScreen extends Component {
  onClickProfile () {
    this.props.navigator.toggleDrawer({
      side: 'left',
      animated: true,
      to: 'closed'
    })
    this.props.navigator.switchToTab({
      tabIndex: 0
    })
    this.props.navigator.handleDeepLink({
      link: 'newsfeed/Three60Memos.MemberDetailsScreen'
    })
    console.log('menuclick')
    console.log(this.props)
    this.props.viewMyProfile(this.props.user)
  }

  render () {
    if (this.props.user.showDetails) {
      console.log('setting photo to : ' + this.props.user.profile_photo)
      var profilePhotoUri = {uri: this.props.user.profile_photo}
      // var profile_photo_uri = require('../../img/avtaar.png');
      return (
        <View style={styles.container}>
        <View style={styles.header}>
          <TouchableOpacity onPress={this.onClickProfile.bind(this)}>
             <Image source={profilePhotoUri} style={styles.profileImage} />
            <Text style={styles.display_name}>{this.props.user.display_name}</Text>
             </TouchableOpacity>
          </View>

             <View style={styles.body}>

            </View>
            <View style={styles.footer}>
              <TouchableOpacity activeOpacity={0.5}>
                  <View style={commonStyle.buttonAcross}>
                      <Text style={commonStyle.buttonText}>Log Out</Text>
                   </View>
          </TouchableOpacity>
            </View>
          </View>

      )
    } else {
      return (
        <View style={styles.UnAuthorizedcontainer}>
        <Text></Text>
        </View>
      )
    }
  }
}

var mapStateToProps = function (store) {
  return {user: store.user, login: store.login, memberprofle: store.memberprofle}
}

var mapDispatchToProps = function (dispatch) {
  return {
    viewMyProfile: function (userObj) { dispatch(memberActions.viewMyProfile(userObj)) }
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(LeftMenuScreen)

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#333333',
    paddingTop: 20

  },
  UnAuthorizedcontainer: {
    flex: 1,
    backgroundColor: '#F2C7D1',
    alignItems: 'center',
    justifyContent: 'center'
  },

  display_name: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
    color: 'white'

  },
  profileImage: {
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'column',
    width: 96,
    height: 96,
    borderColor: 'white',
    marginTop: 5,
    borderRadius: 10,
    borderWidth: 2,
    shadowOffset: {height: 3, width: 6},
    shadowColor: 'gray',
    shadowOpacity: 0.3,
    shadowRadius: 3
  },
  body: {
    flex: 6,
    borderTopColor: 'yellow',
    borderTopWidth: 1,
    alignItems: 'stretch'
  },
  header: {
    flex: 2,
    alignItems: 'center'
  },
  footer: {
    flex: 1,
    borderWidth: 0
  }
})
