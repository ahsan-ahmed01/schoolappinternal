'use strict'
import {Text, View,Modal,TouchableOpacity,StyleSheet, ScrollView, CameraRoll, Platform, Alert, ActivityIndicator,PermissionsAndroid
} from 'react-native'
import React, {Component} from 'react'
import Share from 'react-native-share';
import {connect} from 'react-redux'
import Gallery from 'react-native-image-gallery';
import {commonStyle} from '../config/commonstyles'
import Icon from 'react-native-vector-icons/FontAwesome';
import Permissions from 'react-native-permissions'
import {CustomCachedImage, ImageCache} from "react-native-img-cache";
import ProgressBar from 'react-native-progress/Bar';
var EnvConfig = require('../config/environment')
import RNFetchBlob from 'react-native-fetch-blob'
import Image from 'react-native-image-progress';
var DeviceInfo = require('react-native-device-info');
import {Navigation} from 'react-native-navigation'
import { platform } from 'os';
class ModalPhotoBrowser extends Component {


    constructor(props) {
        super(props);
        this.state = {
            showCommentBox: true,
            page: 0,
            showModal : true,
            msgSaved : '',
            images : null
        }
        this.saveAndroid = this.saveAndroid.bind(this)
        this.save = this.save.bind(this)
        Navigation.events().bindComponent(this);
    }

    navigationButtonPressed({ buttonId }) {
        console.log('-->' + buttonId)
        if (buttonId === 'closecommentbutton'){
            Navigation.dismissAllModals()
        }
        if (buttonId === 'savebutton'){
            this.save()
        }
        if (buttonId === 'sharebutton'){
            console.log('hre')
            var uri = this.props.photoParamObj.photos[this.state.page].photo
            RNFetchBlob
            .config({
                // add this option that makes response data to be stored as a file,
                // this is much more performant.
                fileCache : true,
                appendExt : 'jpeg',
                message : this.props.photoParamObj.photos[this.state.page].caption
            })
            .fetch('GET', uri, {
                //some headers ..
            })
                .then(async resp => {
                    var fpath = resp.path();
                    console.log('The file saved to ', fpath)
                    let options = {
                        url: fpath // (Platform.OS === 'android' ? 'file://' + filePath)
                    };
                await Share.open(options);
                // remove the image or pdf from device's storage
                await RNFS.unlink(fpath);
                });
            // console.log(uri)
            // Share.open({url : uri})
            //     .then((res) => { console.log(res) })
            //     .catch((err) => { err && console.log(err); });
        }

    }

    componentWillMount(){
        this.setState({
            images : this.getImages()
        })
    }

    getImages()
    {
        var photoNum = this.props.photoParamObj.photos.length;
        var images =[]
        console.log(this.props)
        for (var i = 0; i < photoNum; i++) {

            var url = this.props.photoParamObj.photos[i].photo

            if (Platform.OS === 'ios'){
                //var newurl = url.replace("_lowres", "")
                var source = {source : {uri :url }}
                images.push(source)

            }else{
                var source = {source : {uri :url }}
                images.push(source)
            }

        }
        console.log('GetImages')
        console.log(images)
        return images
    }

    close()
    {
        console.log('in Close')
        this.state.showModal = false;
        console.log(this.props.navigator);
        this.props.modaldismissfunc();
        console.log('Exiting Close')
    }

    save(){
        if (platform.os === 'ios'){
        console.log(this.state.page)
        Permissions.check('photo').then(response => {
            console.log(response)
            if (response === 'denied'){
                Alert.alert('Permission Error',
                    'It seems you have denied the APP permission to access the Photo Gallery. Please goto Settings and change it.')
            }
            if (response !== 'authorized'){
                Permissions.request('photo').then(response => {
                    // Returns once the user has chosen to 'allow' or to 'not allow' access
                    // Response is one of: 'authorized', 'denied', 'restricted', or 'undetermined'
                  if (response === 'authorized'){
                      this._savePhoto()
                  }
                })
            }
            else{
                this._savePhoto()
                Alert.alert('Photo Saved', 'Photo saved successfully to the Photo Gallery')
            }


        })
    }else{
       
        this.requestExternalStoragePermission().then(()=>{
            this.saveAndroid()
            Alert.alert('Photo Saved', 'Photo saved successfully ')
            console.log('Lets save in Android')
        }, this)
        
    }


    }

    requestExternalStoragePermission = async () => {
        try {
          const granted = await PermissionsAndroid.request(
            PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
            {
              title:  EnvConfig.SCHOOL_NAME + ' Storage Permission',
              message: EnvConfig.SCHOOL_NAME + ' needs access to your storage ' +
                'so you can save your photos',
            },
          );
          return granted;
        } catch (err) {
          console.error('Failed to request permission ', err);
          return null;
        }
      };

    _savePhoto(){
        var uri = this.props.photoParamObj.photos[this.state.page].photo.replace(/(\r\n|\n|\r)/gm,"").replace('http:', 'https:')
        var promise = CameraRoll.saveToCameraRoll(uri, 'photo');
        promise.then((result) => {
            console.log('save succeeded ' + result);
            this.setState({msgSaved:'Image Saved '})
        })
            .catch(function(error) {
                console.log('save failed ' + error);
            })
    }


    saveAndroid(){
        var uri = this.props.photoParamObj.photos[this.state.page].photo.replace(/(\r\n|\n|\r)/gm,"").replace('http:', 'https:')
        RNFetchBlob
            .config({
                // add this option that makes response data to be stored as a file,
                // this is much more performant.
                fileCache : true,
                appendExt : 'jpeg'
            })
            .fetch('GET', uri, {
                //some headers ..
            })
            .then((res) => {
                // the temp file path
                console.log('The file saved to ', res.path())
                var promise = CameraRoll.saveToCameraRoll(res.path());
                promise.then((result) => {
                    console.log('save succeeded ' + result);
                    this.setState({msgSaved:'Image Saved '})
                })
                    .catch(function(error) {
                        console.log('save failed ' + error);
                    })
                console.log('Saved ?')
            })
       
    }

    render() {
        let commentBox;
        console.log(this.props)
        console.log(this.state)
        var len = this.props.photoParamObj.photos[this.state.page].caption.length
        if (len < 50){
            var commentText = this.props.photoParamObj.photos[this.state.page].caption
        }
        else{
            var commentText = this.props.photoParamObj.photos[this.state.page].caption.substring(0,50) + " ...";
        }

        if(this.state.showCommentBox) {
            commentBox = (
                <View
                    style={{position: 'absolute', left: 0, right: 0, bottom: 60, height: 100, backgroundColor: '#00000066',
                         alignItems: 'center', justifyContent: 'center'}}>
                    <Text style={{color: 'white'}}>{commentText}</Text>
                </View>
            );
        }
        let closebtn = ()=>{
            if (Platform.OS === 'ios' )
            {
                return (<Icon  size={24} style={{color: 'white',
                    paddingTop : 30 ,
                    paddingLeft : 20, paddingRight : 1}} name="times-circle"></Icon>)
            }else{
                return (<Icon  size={24} style={{color: 'white',
                    paddingTop : 14,
                    paddingLeft : 20, paddingRight : 10}} name="times-circle"></Icon>)
            }
        }
        return (

            <ScrollView style={styles.mainContainer} scrollEnabled={false}>
                        <Gallery
                            imageComponent={this.renderImage}
                            errorComponent={this.renderError}
                            style={{flex: 1, marginTop : -75, backgroundColor: 'transparent',alignItems: 'flex-start'}}
                            initialPage={this.props.photoParamObj.imagesindex}
                            pageMargin={10}
                            images={this.state.images}
                            onSingleTapConfirmed={() => {
                                this.toggleCommentBox();
                            }}
                            onGalleryStateChanged={(idle) => {
                                if(!idle) {
                                    this.hideCommentBox();
                                }
                            }}
                            onPageSelected={(page) => {
                                this.setState({page});
                                console.log(page)
                                this.setState({msgSaved:''})
                            }}
                        />

                        {commentBox}
            </ScrollView>


        );

    }

    renderError () {
        return (
            <View style={{ flex: 1, backgroundColor: 'black', alignItems: 'center', justifyContent: 'center' }}>
                <Text style={{ color: 'white', fontSize: 15, fontStyle: 'italic' }}>This image cannot be displayed...</Text>
            </View>
        );
    }


    renderImage (imageProps, dimensions) {
        console.log(imageProps.source)
        console.log(imageProps)

        return (
            <CustomCachedImage
                component={Image}
                defaultSource={require('../img/placeholder.png')}
                {...imageProps}
            />
        )
        // console.log(imageProps)
        // console.log(dimensions)
        // const { width, height } = dimensions || {};
        // // Display the loader until the dimensions are available, which means the image
        // // has been loaded
        // return width && height ? (
        //     <CustomCachedImage
        //         component={Image}
        //         {...imageProps}
        //
        //     />
        // ) : (
        //     <Text>Loading Image ...</Text>
        // );
    }

    toggleCommentBox() {
        if(!this.state.showCommentBox) {
            this.setState({
                showCommentBox: true
            });
        } else {
            this.setState({
                showCommentBox: false
            });
        }
    }

    hideCommentBox() {
        if(this.state.showCommentBox) {
            this.setState({
                showCommentBox: false
            });
        }
    }

}

var mapStateToProps = function(store){
    return {
    };
};

const styles = StyleSheet.create({
    mainContainer:{
        flex : 1,
        backgroundColor : 'black'
    },
    headercontainer :{
        flex : 1
    },
    bodycontainer :{
        flex : 1
    },
    footcontainer :{
        flex : 1
    },
    closetext:{
        color : 'white',
        paddingTop : 25,
        paddingRight : 10,
        paddingLeft : 10,
        fontSize : 18,
        textAlign : 'right',
    },
    messageText:{
        color : 'yellow',
        paddingTop : 25,
        paddingRight : 10,
        paddingLeft : 10,
        fontSize : 18,
        textAlign : 'right',
    }
})

export default connect(mapStateToProps)(ModalPhotoBrowser)