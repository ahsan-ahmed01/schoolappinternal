'use strict'
import {
    Platform,
    AppRegistry,
    Text,
    StyleSheet,
    View,
    Image, ImageBackground, TouchableOpacity, ActivityIndicator, TextInput
} from 'react-native'
import React, {Component} from 'react'
import {connect} from 'react-redux'
import Icon from 'react-native-vector-icons/FontAwesome';
import * as memberActions from '../../actions/memberprofileActions'
import {commonStyle} from '../../config/commonstyles'
import Modal from 'react-native-modal'
import CodeInput from 'react-native-confirmation-code-input';

class AuthorizedUserScreen extends Component {

    constructor(props){
        super(props)
        console.log(props)
        this.state = {
            modal : false,
            modalusername : null,
            modalpin : null,
            errorText : '',
            enablesavebtn : false
        }
    }

    validateCode(code) {

        var codes = this.props.child.authorizedUsers.map((user) => {

            if ((user.name !== this.state.modalusername) || (this.props.isNew))
                return user.pin
            else
                return 0
        })
        var names = this.props.child.authorizedUsers.map((user) => {

            if ((user.name !== this.state.modalusername) || (this.props.isNew))
                return user.name
            else
                return 0
        })
        console.log(codes)
        console.log(names)
        if (codes.includes(code)) {
            this.setState({
                errorText: "Every Authorized User should have a unique code. Please enter a Unique code. ",
                enablesavebtn: false
            })
        }
        else {
            if (names.includes(this.state.modalusername)) {
                this.setState({
                    errorText: "Every Authorized User should have a unique name. Please enter a Unique name. ",
                    enablesavebtn: false

                })
            } else {
                console.log('Setting code : ' + code)
                this.setState({
                    errorText: "",
                    enablesavebtn: true,
                    modalpin: code
                })

            }
        }
    }

    saveUser(){
        if ((this.state.modalusername !== null) && (this.state.modalusername.length > 0)){
            this.refs.codeInputRef2.setState ({
                codeArr : this.state.modalpin.split('')
            })

            this.props.saveAuthorizedUser(this.state.modalusername, this.props.child.childid, this.state.modalpin, this.props.user.token)
            this.setState({
                modal : false
            })
        }


    }

    deleteUser(){
        this.props.deleteAuthorizedUser(this.state.modalusername, this.props.child.childid, this.state.modalpin, this.props.user.token)
        this.setState({
            modal : false
        })
    }

    componentWillReceiveProps(nextProps){
        if ((nextProps.user.errorUpdatingPin === true) && (this.props.user.errorUpdatingPin === false)){
            this.setState({
                errorText : "Error Updating Pin. Please try again"
            })
        }

        return true
    }

    cancel(){
        this.setState({
          modal : false,
          modalusername : null,
          modalpin : null
        })
        console.log(this.refs.codeInputRef2)
    }
    changeText(text){
        this.setState({
            modalusername : text
        })
    }

    componentDidUpdate(prevProps, prevState){
        if ((this.state.modalpin !== null) && (prevState.modalpin === null)){
            console.log('Updating !!!')
            this.refs.codeInputRef2.setState ({
                codeArr : this.state.modalpin.split('')
            })
        }


    }

    renderModal(){
        if (this.state.enablesavebtn) {
            var saveButton = () => {
                return (
                    <TouchableOpacity onPress={this.saveUser.bind(this)} disabled={!this.state.enablesavebtn}>
                        <View style={[commonStyle.button, {width: 120}]}>
                            <Text style={commonStyle.buttonText}>Save
                            </Text>
                        </View></TouchableOpacity>)
            }
        }else{
            var saveButton = () => {
                return (
                <TouchableOpacity onPress={this.saveUser.bind(this)} disabled={!this.state.enablesavebtn}>
                    <View style={[commonStyle.button, {backgroundColor : 'gray', width : 120}]}>
                        <Text style={commonStyle.buttonText}>Save
                        </Text>
                    </View></TouchableOpacity>)
            }
        }

        var deleteButton = () => {
            if (!this.props.isNew){
                return (
                    <TouchableOpacity style={{paddingTop : 5, paddingLeft : 10}} onPress={this.deleteUser.bind(this)}>
                        <Icon name="trash" size={20}/>
                    </TouchableOpacity>
                )
            }else{
                return null
            }
        }
        return (
            <Modal isVisible={this.state.modal} style={{}}>
                <View style={{height : 250,marginBottom : 200,borderRadius : 10,marginTop : 20,backgroundColor : 'white'}}>
                    <View style={styles.row}>
                        <Text style={styles.labelName}>Name</Text>
                        <TextInput style={styles.textInputstyle} autoFocus={true} defaultValue={this.state.modalusername} editable={this.props.isNew}
                                   maxLength = {40} onChangeText={this.changeText.bind(this)}>

                        </TextInput>
                    </View>
                    <View style={styles.row}>
                        <Text style={styles.labelPinname}>Pin</Text>
                        <CodeInput
                            ref="codeInputRef2"
                            keyboardType="numeric"
                            codeLength={4}
                            className='border-circle'
                            autoFocus={false}
                            activeColor='black'
                            inactiveColor='gray'
                            codeInputStyle={{ fontWeight: '800' }}
                            containerStyle={styles.pinContainer}
                            onFulfill={(code) => this.validateCode(code)}
                        />

                    </View>
                    <Text style={styles.errortext}>{this.state.errorText}</Text>
                    <View style={[{padding : 10, height : 70, flexDirection: 'row'}]}>
                        {saveButton()}
                        <TouchableOpacity onPress={this.cancel.bind(this)}>
                            <View style={[commonStyle.button,  {width : 120,backgroundColor: 'red',}]}>
                                <Text style={commonStyle.buttonText}>Cancel
                                </Text>
                            </View></TouchableOpacity>
                        {deleteButton()}
                    </View>
                </View>
            </Modal>

        )
    }

    viewUser(){
        this.setState({
            modalusername : this.props.authorizeduser.name,
            modalpin : this.props.authorizeduser.pin,
            modal : true
        })

    }

    render(){
        if (this.props.isNew){
            return(
                <View>
                <TouchableOpacity onPress={()=>{
                    this.setState({
                        modal : true
                    })
                }}>
                    <Text style={styles.newuser}>Add User</Text>
                </TouchableOpacity>
                    {this.renderModal()}
                </View>

            )

        }
        else{
            return(
                <TouchableOpacity onPress={this.viewUser.bind(this)}>
                <View style={styles.container}>
                    <Text style={styles.namelabel}>{this.props.authorizeduser.name}</Text>
                    <View style={{flexDirection : 'row'}}>
                        <Text style={styles.pinlabelText}>Pin : </Text>
                     <Text style={styles.pinlabel}>{this.props.authorizeduser.pin}</Text>
                        <Icon name={'angle-right'} size={32}/>
                    </View>
                    {this.renderModal()}
                </View>
                </TouchableOpacity>
                    )
        }
    }

}

const styles = StyleSheet.create({
    container: {
       height : 40,
        padding : 5,
        flexDirection: 'row',
        justifyContent: 'space-between',
        borderBottomWidth: 1,
        borderBottomColor : 'darkgray',

    },
    namelabel : {
        paddingTop : 4,
        fontSize : 18
    },
    pinlabel : {
        flexDirection: 'row',
        color : 'blue',
        fontSize : 20,
        paddingTop : 4,
        paddingRight : 5,

    },
    pinlabelText : {
        paddingTop : 8,
    },
    newuser : {
        padding: 5,
        fontSize : 16,
        color : 'blue',
        textAlign : 'center'
    },
    row : {
        padding : 10,
        flexDirection : 'row',
        justifyContent : 'space-between',
        marginTop: 5
    },
    textInputstyle : {
        width : 250,
        height: 50,
        borderBottomColor : 'gray',
        borderBottomWidth: 1
    },
    labelName : {
        paddingTop : 5
    },
    labelPinname : {
        paddingTop : 20
    },
    errortext : {
        color : 'red',
        padding : 5,
        textAlign: 'center'
    }

})

var mapStateToProps = function (store) {
    return {user: store.user}
}

var mapDispatchToProps = function (dispatch) {
    return {
        saveAuthorizedUser: function (name,childid, pincode, token) {
            dispatch(memberActions.saveAuthorizedUser(name,childid, pincode, token))
        },
        deleteAuthorizedUser: function (name,childid, pincode, token) {
            dispatch(memberActions.deleteAuthorizedUser(name,childid, pincode, token))
        },

    }
}
export default connect(mapStateToProps, mapDispatchToProps)(AuthorizedUserScreen)