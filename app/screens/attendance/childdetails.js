'use strict'
import {
    Platform,
    AppRegistry,
    Text,
    StyleSheet,
    View,
    Image, ImageBackground, TouchableOpacity, ActivityIndicator, Alert, ScrollView
} from 'react-native'
import React, {Component} from 'react'
import Spinner from 'react-native-loading-spinner-overlay'
import {connect} from 'react-redux'
import Icon from 'react-native-vector-icons/FontAwesome';
import ImagePicker from 'react-native-image-crop-picker';
import * as memberActions from '../../actions/memberprofileActions'
import {commonStyle} from '../../config/commonstyles'
import {CustomCachedImage} from "react-native-img-cache";
import * as Layout from '../../constants/Layout'
import AuthorizedUserScreen from './authorizeduser'
import { Card } from 'react-native-elements'
import Modal from 'react-native-modal'
import {
    Form,
    Separator, InputField, LinkField,
    SwitchField, PickerField, DatePickerField, TimePickerField
} from 'react-native-form-generator';

class ChildDetailScreen extends Component {

    constructor(props) {
        super(props)
        console.log(this.state)
        console.log(this.props)
        this.state  = {
            setPin : false,
            firstEntry : null,
            childdetails : props.childdetails
        }

    }

    componentWillReceiveProps(nextProps){
        if ((this.props.user.updatingChildPin === true) && (nextProps.user.updatingChildPin === false)){
            nextProps.user.children.forEach((child)=>{
                if (child.childid === this.props.childdetails.childid){
                    this.setState({
                        childdetails: child
                    })
                    console.log('Setting child')
                    console.log(child)
                }
            })

        }

        return true
    }

    onClickCamera() {
        console.log('Camera Clicked !')
        ImagePicker.openPicker({
            width: 300,
            height: 400,
            cropping: true,
            smartAlbums : [ 'UserLibrary','PhotoStream',  'RecentlyAdded', 'SelfPortraits', 'Screenshots','Generic']
        }).then(image => {
            console.log(image)
            console.log(this.props)
            this.props.uploadChildProfilePhoto(image, this.props.childdetails.childid,  this.props.user.token);
            //#TODO : Update all avataars for firebase chats
        });
    }

    renderCameraIcon() {
        return (
                <TouchableOpacity onPress={this.onClickCamera.bind(this)}>
                    <Icon name="camera" style={styles.cameraicon} size={24}/>
                </TouchableOpacity>

            )
    }

    getChildPictureURL(){
        var url = require('../../img/child.png')
        for (var index in this.props.user.children){
            console.log(this.props.user.children[index].childid + '<--->' + this.props.childdetails.childid)
            if (this.props.user.children[index].childid === this.props.childdetails.childid){
                if (this.props.user.children[index].pictureurl !== '<DEFAULT>'){
                    url = {uri : this.props.user.children[index].pictureurl}
                }
                console.log(url)
                break;
            }
        }
        return url
    }

    render() {
        var childuri = this.getChildPictureURL()
        var childname = this.props.childdetails.firstname + ' ' + this.props.childdetails.lastname
            console.log(childuri)
            console.log('RENDER->CHILD DETAIL')
            return (
                <ScrollView style={styles.container}>

                    <Modal isVisible={this.state.setPin}>
                        <View style={styles.modalcontainer}>
                            <Text style={styles.checkinLabel}>Set Checkin/Checkout PIN for {childname}</Text>
                            <Text style={styles.pinverificationlabel}>{this.state.EntryText}</Text>

                        </View>
                        <Spinner visible={this.props.user.updatingChildPin } textContent={'Saving Pin ...'}
                                 textStyle={{color: '#FFF'}}/>


                    </Modal>
                    <Spinner visible={this.props.user.childpictureupdating } textContent={'Saving Photo ...'}
                             textStyle={{color: '#FFF'}}/>
                    <View style={styles.imgcontainer}>
                        <View style={{flex : 1}}>
                            <ImageBackground defaultSource={require('../../img/child.png')}
                                             imageStyle={{resizeMode : 'cover'}}
                                             source={childuri} style={styles.headerimage}>
                                {this.renderCameraIcon()}
                            </ImageBackground>

                        </View>

                    </View>
                    {/* <Form style={styles.form}
                          ref='childform'
                          onChange={this.handleFormChange.bind(this)}>
                        <Separator label={"Details"}/>
                        <InputField
                            ref='Name'
                            editable={false}
                            style={{color: 'black'}}
                            label='Name'
                            placeholder='<None>'
                            value={childname}
                        />
                        <InputField
                            ref='Allergies'
                            editable={false}
                            style={{color: 'black', height: 100}}
                            label='Allergies'
                            placeholder='<None>'
                            value={this.props.childdetails.allergies}
                        />
                        <InputField
                            ref='dob'
                            editable={false}
                            style={{color: 'black'}}
                            label='DOB'
                            placeholder='<None>'
                            value={this.props.childdetails.dob}
                        />
                        <Separator label={"Authorized Pickup/Dropoff Users"}/>
                    </Form> */}
                    <Card title={childname}>
                        <View style={{flexDirection : 'row', justifyContent : 'space-between'}}>
                            <Text style={{fontSize : 16}}>DOB</Text>
                            <Text style={{fontSize : 16, color : 'blue'}}>{this.props.childdetails.dob}</Text>
                        </View>
                        <View style={{flexDirection : 'row', justifyContent : 'space-between'}}>
                            <Text style={{fontSize : 16}}>Allergies</Text>
                            <Text style={{fontSize : 16, color : 'blue'}}>{this.props.childdetails.allergies}</Text>
                        </View>
                    </Card>
                {this.renderAuthorizedUsers()}


                </ScrollView>
            )
    }
    renderAuthorizedUsers(){
        var users = this.state.childdetails.authorizedUsers.map((user, i)=>{
            return(
            <AuthorizedUserScreen key={i} child={this.props.childdetails} isNew={false} authorizeduser={user}>{user.name}</AuthorizedUserScreen>)
        })
        return (
            <Card title={'Authorized Users'}>
                {users}
                <AuthorizedUserScreen child={this.props.childdetails} isNew={true}/>
            </Card>
        )
    }

    handleFormChange(formData) {

        this.setState({formData: formData})

    }

}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    modalcontainer : {
     backgroundColor : 'white', flex: 1, padding : 10
    },
    imgcontainer: {
        justifyContent: 'center',
        flexDirection: 'row'
    },
    cameraicon : {
        color: 'white',
        opacity: 0.8,

    },
    headerimage : {
        flexGrow: 1,
        width: undefined,
        height : 200,
        justifyContent: 'center',
        alignItems: 'center',
    },
    childimage: {
        width: 100,
        height: 100,
        justifyContent: 'center',
        borderWidth: 1,
        borderColor: 'black',
        alignContent: 'center'
    },
    backButton: {
        flexDirection: 'row',
        padding: 20,
    },

    childnametext: {
        fontWeight: 'bold',
        fontSize: 20,
        color: 'darkgray',
        justifyContent: 'center',
        alignContent: 'center'
    },
    pinContainer : {

    },
    pinverificationlabel : {
        color : 'black',
        textAlign: 'center'
    },

    checkinLabel : {
        paddingVertical: 50,
        paddingHorizontal: 20,
        fontSize: 20,
        textAlign : 'center',
        color : 'black'
    },
    form: {
        ...Platform.select({
            'ios': {
                backgroundColor: 'lightblue'
            }
        })

    },

})

var mapStateToProps = function (store) {
    return {user: store.user}
}

var mapDispatchToProps = function (dispatch) {
    return {
        uploadChildProfilePhoto: function (image, childid, token) {
            dispatch(memberActions.uploadChildProfilePhoto(image, childid, token))
        },
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(ChildDetailScreen)