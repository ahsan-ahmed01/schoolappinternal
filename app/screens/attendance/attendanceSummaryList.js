'use strict';
import {
    Platform,
    AppRegistry,
    Text,
    StyleSheet,
    View,
    Image, ImageBackground, TouchableOpacity, ActivityIndicator, TextInput, FlatList
} from 'react-native'
import React, {Component} from 'react'
import {connect} from 'react-redux'
import * as attendanceactions from '../../actions/attendanceActions'
import {Navigation} from 'react-native-navigation'
var EnvConfig = require('../../config/environment')

Date.prototype.yyyymmdd = function () {
    var mm = this.getMonth() + 1; // getMonth() is zero-based
    var dd = this.getDate();

    return [this.getFullYear(),
        (mm > 9 ? '' : '0') + mm,
        (dd > 9 ? '' : '0') + dd
    ].join('');
};

class AttendanceSummaryList extends Component {

    constructor(props) {
        super(props);
        var attendancedate = new Date().yyyymmdd();
        this.state = {
            attendancedate: attendancedate
        }
        Navigation.events().bindComponent(this);
    }

    navigationButtonPressed({buttonId}) {
        console.log(buttonId)
        if (buttonId === 'closebutton') {
            Navigation.dismissAllModals()
        }
        if (buttonId === 'refreshbutton') {
            console.log('Refreshing Screen')
            if (Object.keys(this.props._usergroups).length > 0) {
                let groups = []
                for (var key in this.props._usergroups) {
                    groups.push(parseInt(key))
                }
                this.props.refreshAttendance(groups, new Date().yyyymmdd(), this.props.token)
            }
        }
    }

    componentWillMount() {
        if (Object.keys(this.props._usergroups).length > 0) {
            let groups = []
            for (var key in this.props._usergroups) {
                groups.push(parseInt(key))
            }
            console.log(groups)
            this.props.initAttendanceList(groups, this.state.attendancedate, this.props.token)
        }
    }

    _keyExtractor = (item, index) => item;


    render() {
       return (<Text>Attendance</Text>)
    }

    shouldComponentUpdate(nextProps, nextState) {
        return (nextProps._usergroups.length !== this.props._usergroups.length || nextProps.attendancerefeshing !== this.props.attendancerefeshing);

    }

   
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white',


    },
    loadingdata: {
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 100
    },
    loadingtext: {
        fontSize: 20,
        color: 'darkgray'
    }

});

var mapStateToProps = function (store) {
    console.log(store)
    return {
        attendance: store.attendance,
        _usergroups: store.user.groups !== null ? Object.keys(store.user.groups).reduce(function (filtered, key) {
            var group = store.user.groups[key]
            if (group.attendancemode !== 0)
                filtered[key] = group
            return filtered
        }, {}) : [],
        attendancerefeshing :store.attendance.attendancerefeshing,
        token: store.user.token
    }
};

var mapDispatchToProps = function (dispatch) {
    return {
        initAttendanceList: function (groups, attendancedate, token) {
            dispatch(attendanceactions.initAttendanceList(groups, attendancedate, token))
        },
        refreshAttendance: function (groups, attendancedate, token) {
            dispatch(attendanceactions.refreshAttendance(groups, attendancedate, token))
        }

    }
};
export default connect(mapStateToProps, mapDispatchToProps)(AttendanceSummaryList)