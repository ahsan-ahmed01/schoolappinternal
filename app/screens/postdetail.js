'use strict'
import {
  Platform,
  AppRegistry,
  Text,
  StyleSheet,
  ScrollView,
  Image, Dimensions,
  View, TouchableOpacity
} from 'react-native'
import React, { Component } from 'react'
import * as userActions from '../actions/userActions'
import * as newsfeedaction from '../actions/newsfeedActions'
import { Card } from 'react-native-elements'
import { connect } from 'react-redux'
import { Navigation } from 'react-native-navigation';

//import {navigiationEventsHandler} from '../actions/navigationHandler'

class PostDetailsScreen extends Component {

  constructor(props) {
    super(props)
    console.log(props)
  }
  render() {
    
    return null
  }

}

var mapStateToProps = function (store) {
  return {
    newsfeed: store.newsfeed,
    rootNavprops: store.rootNavprops,
    usergroup: store.user.groups,
    usertoken: store.user.token,
    briefcase : store.briefcase
  }

}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 5,
    backgroundColor: 'lightgray'
  },
  errorLoading: {
    backgroundColor: 'red',
    color: 'white',
    marginBottom: 5,
    paddingTop: 2,
    paddingBottom: 2,
    paddingLeft: 20,
    paddingRight: 20,
    alignSelf: 'center'

  },
  centering: {
    alignItems: 'center',
    justifyContent: 'center',
    padding: 8
  }
})

var mapDispatchToProps = function (dispatch) {
  return {
    setNavigationTab: function (tabName) { dispatch(userActions.setNavigationTab(tabName)) },
    getNewsFeedByGroup: function (page, groupid, token) {
      dispatch(newsfeedaction.getNewsFeedByGroup(page, groupid, token))
    },
    showNewFeedsisLoading: function () {
      dispatch(newsfeedaction.showNewFeedsisLoading())
    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(PostDetailsScreen)
