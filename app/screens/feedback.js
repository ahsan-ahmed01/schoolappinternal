import {
    Platform,
    AppRegistry,
    Text,
    ScrollView,
    StyleSheet,
    View,
    TextInput,
    Alert,
    TouchableOpacity, ActivityIndicator, Image, TouchableHighlight
} from 'react-native'
import React, {Component} from 'react'
import * as rootNavAction from '../actions/rootNavActions'
import CustomBlurView from '../components/CustomBlurView'
import {connect} from 'react-redux';
import Icon from 'react-native-vector-icons/FontAwesome';
import {commonStyle}  from '../config/commonstyles'
import codePush from "react-native-code-push";
import VersionNumber from 'react-native-version-number';
import DeviceInfo from 'react-native-device-info'
import {Navigation} from 'react-native-navigation'

import * as userActions from '../actions/userActions'
import Spinner from 'react-native-loading-spinner-overlay'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
class FeedBackScreen extends React.Component {

    constructor(props){
        super(props)
        this.state = {
            feedbackText : '',
            label : ''
        }
        Navigation.events().bindComponent(this);
    }

    navigationButtonPressed({ buttonId }) {
        console.log(buttonId)
        if (buttonId === 'closebutton'){
            Navigation.dismissAllModals()
        }
    }

    componentDidMount(){
        codePush.getUpdateMetadata().then((metadata) =>{
            console.log(metadata)
            if ((metadata !== null) && (metadata !== undefined))  {
                this.setState({
                    label: metadata.label,
                    version: metadata.appVersion,
                    description: metadata.description
                });
            }
        });

    }


    componentDidUpdate(prevProps, prevState){
        if ((prevProps.user.feedbacksending === true) && (this.props.user.feedbacksending === false)){
                this.props.hideFeedbackModal()
        }
    }

    render(){



        return (
            <View style={{flex : 1, marginTop : 20, padding :5}}>
               <ScrollView>
                    <Spinner visible={this.props.user.feedbacksending} textContent={'Sending Feedback'} textStyle={{color: '#FFF'}} />
                    <View style={styles.container}>
                        <Text style={{paddingTop: 5}}>We are always looking to enhance the app that will support your school community .
                        </Text>
                        <Text style={{paddingTop: 10}}>If you have any suggestions or questions about 360 please write to us.</Text>
                         <Text style={{paddingTop: 10, color : 'purple'}}>Thank you and have a wonderful day. </Text>

                        <TextInput
                            onChangeText={this.onChangeText.bind(this)}
                            style={styles.postText}
                            multiline={true}
                            numberOfLines={3}
                            autoFocus={true}/>
                        <TouchableOpacity

                            style={[commonStyle.buttonAcross, {marginTop : 10}]} onPress={this.sendFeedBackClick.bind(this)}>
                            <Text style={{color : 'white'}}>Send</Text>
                        </TouchableOpacity>
                    </View>
                    </ScrollView>
            </View>

        )
    }

    onChangeText(text){
        if (text.length > 0){
            this.setState({
                feedbackText : text
            })
        }
    }

    sendFeedBackClick(){
        if (this.state.feedbackText.length <= 0){
            Alert.alert('Error', 'Please enter some feedback before sending.')
        }
        else{
            var versionString = VersionNumber.appVersion + " " + VersionNumber.buildVersion + " (" + this.state.label + ') '
            var data = {}
            data['carrier'] = DeviceInfo.getCarrier()
            data['freedisk'] = DeviceInfo.getFreeDiskStorage()
            data['brand'] = DeviceInfo.getBrand()
            data['model'] = DeviceInfo.getModel()
            data['manufacturer'] = DeviceInfo.getManufacturer()
            data['osversion'] = Platform.Version
            data['platform'] = Platform.OS
            data['softwareversion'] = versionString
            console.log(data)
            this.props.sendFeedback(this.state.feedbackText, data, this.props.user.token)

            Navigation.dismissAllModals()
            Alert.alert('Feedback Sent', 'Thank you for your feedback !')
        }

    }

}

const styles = StyleSheet.create({
    blurcontainer : {
        flex: 1,
        borderTopWidth: 1,
        borderRadius : 25,
        borderTopLeftRadius : 25,
        borderTopRightRadius : .5,
        borderBottomLeftRadius : 0.5,
        borderBottomRightRadius : 0.5,
        padding : 3
    },
    buttonClose:{
        flexDirection : 'row-reverse',
        paddingLeft : 15,
        paddingTop: 2,
        paddingBottom:  2

    },
    container :{
        flex : 1,
        paddingBottom: 20,
        paddingLeft: 10,
        paddingRight: 10
    },
    postText :{
        flex : 1,
        borderColor: 'gray',
        borderWidth: 1,
        marginTop : 10,
        height : 150,
        paddingBottom : 10
    },
    centering: {
        alignItems: 'center',
        justifyContent: 'center',
        padding: 8
    },
})

var mapStateToProps = function(store){
    return {user: store.user,memberprofile: store.memberprofile};
};

var mapDispatchToProps = function(dispatch){
    return {
        hideFeedbackModal: function () {dispatch(rootNavAction.hideFeedbackModal())},
        sendFeedback : function (feedback, data, token) {
            dispatch(userActions.sendFeedback(feedback, data, token))
        }
    }
};

export default connect(mapStateToProps,mapDispatchToProps)(FeedBackScreen)