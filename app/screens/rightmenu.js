'use strict'
import {Text, View, StyleSheet, ScrollView,  TouchableOpacity} from 'react-native'
import React, {Component} from 'react'
import {connect} from 'react-redux'
import * as groupdetailactions from '../actions/groupdetailsActions'
import {commonStyle} from '../config/commonstyles'
import * as memberActions from '../actions/memberprofileActions'
import Image from 'react-native-image-progress';
import codePush from "react-native-code-push";
import {ImageCache, CustomCachedImage} from "react-native-img-cache";

import VersionNumber from 'react-native-version-number';
class RightMenuScreen extends Component {

  constructor(props){
    super(props)
      this.state = {
        label : '',
          version : '',
          description: ''
      }
  }

  onClickGroup (selectedgroup) {
    console.log('in on click')
    console.log(this.props.navigator)
    this.props.navigator.toggleDrawer({
      side: 'right',
      animated: true,
      to: 'closed'
    })
    this.props.navigator.switchToTab({
      tabIndex: 0
    })
    this.props.navigator.handleDeepLink({
      link: 'newsfeed/Three60Memos.GroupDetailScreen'
    })

    this.props.viewGroupDetails(selectedgroup, this.props.user.token)
    this.props.getGroupPhotos(selectedgroup, this.props.user.token)
  }

    componentDidMount(){
        codePush.getUpdateMetadata().then((metadata) =>{
            if ((metadata !== null) && (metadata !== undefined)) {
              this.setState({
                  label: metadata.label,
                  version: metadata.appVersion,
                  description: metadata.description
              });
          }
        });

    }

  onClickProfile () {
    // this.props.navigator.toggleDrawer({
      //   side: 'right',
      //   animated: true,
      //   to: 'closed'
      // })
      // this.props.navigator.switchToTab({
      //   tabIndex: 0
      // })
      // this.props.navigator.handleDeepLink({
      //   link: 'newsfeed/Three60Memos.MemberDetailsScreen'
      // })
      // console.log('menuclick')
    console.log(this.props)
    this.props.viewMyProfile(this.props.user)
  }

  render () {
    if (this.props.user.showDetails) {
      console.log(this.props)
      // var GroupList = this.props.user.groups.map(function (groups, i) {
      //   if (groups.profile_photo == '') {
      //     var groupprofilePhotoURI = require('../img/blankprofile.png')
      //   } else {
      //     groupprofilePhotoURI = {uri: groups.profile_photo.replace('http:', 'https:')}
      //   }
      //
      //   return (
      //                         <TouchableOpacity key={groups.id} style={styles.groupMenuItem} onPress={this.onClickGroup.bind(this, groups)}>
      //                           <CustomCachedImage
      //                               component={Image}
      //                               style={styles.groupImage} source={groupprofilePhotoURI}></CustomCachedImage>
      //                           <Text style={styles.groupText}>{groups.name}</Text>
      //                       </TouchableOpacity>
      //   )
      // }, this)

      var ImageWidget = () => {

          if (this.props.user.profile_photo === '' ||
              this.props.user.profile_photo === null || this.props.user.profile_photo.includes('gravatar.com')) {
              console.log(this.props.user.profile_photo)
              var profilePhotoURI = require('../img/blankprofile.png')
              console.log(profilePhotoURI)
              return (<Image source={profilePhotoURI} style={styles.profileImage} />)

          } else {
              var profilePhotoUri = {uri: this.props.user.profile_photo}
              return (<CustomCachedImage
              component={Image} source={profilePhotoUri} style={styles.profileImage} />)
          }

      }
      var versionString = VersionNumber.appVersion + " " + VersionNumber.buildVersion + " (" +   this.state.label + ') '
       var CPversionString = this.state.label + ' ' + this.state.version + ' ' + this.state.description
      return (
        <View style={styles.container}>
          <View style={styles.header}>
            <TouchableOpacity onPress={this.onClickProfile.bind(this)}>
                {ImageWidget()}
              <Text style={styles.display_name}>{this.props.user.display_name}</Text>
            </TouchableOpacity>
          </View>

             <View style={styles.body}>
                 <ScrollView>
                      {/*{GroupList}*/}
                  </ScrollView>
            </View>
          <Text style={styles.poweredby}>Powered by 360 Memos</Text>
          <Text style={styles.version}>{versionString}</Text>
          <View style={styles.footer}>
          <TouchableOpacity activeOpacity={0.5} onPress={this.logoutUser.bind(this)}>
            <View style={commonStyle.buttonAcross}>
              <Text style={commonStyle.buttonText}>Log Out</Text>
            </View>
          </TouchableOpacity>
        </View>

        </View>
      )
    } else {
      return (
        <View style={styles.UnAuthorizedcontainer}>
        <Text></Text>
        </View>
      )
    }
  }

  logoutUser () {
      ImageCache.get().clear();
    this.props.logoutUser()
  }
}

var mapStateToProps = function (store) {
  return {user: store.user, login: store.login}
}

var mapDispatchToProps = function (dispatch) {
  return {
    viewGroupDetails: function (groupid, token) { dispatch(groupdetailactions.viewGroupDetails(groupid, token)) },
    getGroupPhotos: function (groupid, token) { dispatch(groupdetailactions.getGroupPhotos(groupid, token)) },
    logoutUser: function () { dispatch(memberActions.logoutUser()) },
    viewMyProfile: function (userObj) { dispatch(memberActions.viewMyProfile(userObj)) }

  }
}

export default connect(mapStateToProps, mapDispatchToProps)(RightMenuScreen)

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#333333',
    paddingTop: 20
  },
  UnAuthorizedcontainer: {
    flex: 1,
    backgroundColor: '#F2C7D1',
    alignItems: 'center',
    justifyContent: 'center'
  },
  display_name: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
    color: 'white'

  },
  profileImage: {
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'column',
    width: 96,
    height: 96,
    borderColor: 'white',
    marginTop: 5,
    borderRadius: 10,
    borderWidth: 2,
    shadowOffset: {height: 3, width: 6},
    shadowColor: 'gray',
    shadowOpacity: 0.3,
    shadowRadius: 3
  },

  body: {
    flex: 6,

    alignItems: 'stretch'
  },
    poweredby:{
      alignSelf: 'center',
        color : 'yellow',
        paddingBottom: 30,
        fontSize : 20
    },
    version: {
        alignSelf: 'center',
        color : 'white',
        paddingBottom: 30,
        fontSize : 10
    },
  header: {
    flex: 2,
    alignItems: 'center'
  },
  footer: {
    flex: 1,
    borderWidth: 0
  },
  groupMenuItem: {
    flexDirection: 'row',
    borderBottomWidth: 0.5,
    borderBottomColor: 'lightgray',

    padding: 10
  },
  groupImage: {
    width: 32,
    height: 32,
    borderRadius: 10,
    borderWidth: 0.5,
    borderColor: 'yellow'
  },
  groupText: {
    textAlign: 'center',
    color: 'white',
    textAlignVertical: 'center',
    fontSize: 18,
    paddingLeft: 5,
    paddingTop: 3
  }
})
