'use strict'

import {Platform, AppRegistry, Text, ActivityIndicator, View, StyleSheet, TouchableOpacity, ScrollView, FlatList} from 'react-native'
import React, {Component} from 'react'
import {connect} from 'react-redux'
import Icon from 'react-native-vector-icons/FontAwesome';
import Ionic from 'react-native-vector-icons/Ionicons';
import * as memberActions from '../actions/memberprofileActions'
import * as rootNavActions from '../actions/rootNavActions'
import {CustomCachedImage} from "react-native-img-cache";
var closeIcon = null
import Image from 'react-native-image-progress';
import {Navigation} from 'react-native-navigation'
import * as miscutils from '../utils/misc'

class GroupMemberList extends Component
{
  constructor (props) {
    super(props)
    console.log(this.props)
      Ionic.getImageSource('md-close', 30).then((source) => {
          closeIcon = source
      });
  }
  renderAdminStar () {
    return (<View style={styles.adminstarcontainer}>
                                    <Icon name="star" style={styles.adminstar} size={20} color="#900" />
                  </View>)
  }

  onClickMember (props, event) {
      Navigation.showModal({
          stack: {
              children: [{
                  component: {
                      name: 'nav.Three60Memos.MemberDetailsScreen',
                      passProps: {
                          selfView : false,
                          fromTopMenu : false,
                          userprofile : miscutils.getUserfromCache(this.props.login.usercache,event)
                      },
                      options : {
                          topBar: {
                              visible : true,
                              title: {
                                  text: "Profile",
                                  color : "Black"
                              },
                              rightButtons:[
                                  {
                                      id : 'closeprofilebutton',
                                      icon : closeIcon,
                                      color : 'black'
                                  }
                              ]

                          }
                      }
                  }
              }]
          }
      });

  }
    _keyExtractor = (item, index) => item;

    render() {
        console.log('member render')
        console.log(this.props)
        console.log(this.props.login.groupcache[this.props.selectedgroup.id])
        if (this.props.login.groupcache[this.props.selectedgroup.id] ) {
            return (
                <FlatList
                    data={this.props.login.groupcache[this.props.selectedgroup.id].sort()}
                    keyExtractor={this._keyExtractor}
                    renderItem={({item}) => {
                        console.log(item)
                        var userObj = miscutils.getUserfromCache(this.props.login.usercache,item)
                        var profile_photo = userObj === undefined ? null : userObj.profile_photo
                        console.log(profile_photo)
                        if (profile_photo === '<DEFAULT>' || profile_photo === ""|| profile_photo === null ) {
                            var profilePhotoURI = require('../img/blankprofile.png')
                            var ImgComp = <Image
                                style={styles.memberImage}
                                source={profilePhotoURI}
                                imageStyle={
                                    {
                                        width : 45,
                                        height : 45,
                                        resizeMode : 'stretch',
                                        borderRadius : 23
                                    }
                                }
                            />

                        } else {
                            console.log(item)
                            var profilePhotoURI = {uri: profile_photo.replace('http:', 'https:')}
                            var ImgComp =  <CustomCachedImage style={styles.memberImage}
                                                              component={Image}
                                                              defaultSource={require('../img/blankprofile.png')}
                                                              imageStyle={
                                                                  {
                                                                      width : 45,
                                                                      height : 45,
                                                                      resizeMode : 'stretch',
                                                                      borderRadius : 23,
                                                                  }
                                                              }
                                                              source={profilePhotoURI}></CustomCachedImage>
                        }
                        if ((item.is_admin === 1) || (item.is_mod === 1)) {
                            var adminStar = this.renderAdminStar()
                        } else {
                            var adminStar = <View/>
                        }
                        console.log(item)
                        console.log(this.props.selectedgroup.groupname)
                        var parentof = userObj.parent_of !== undefined && userObj.parent_of[this.props.selectedgroup.id] !== undefined ?
                            userObj.parent_of[this.props.selectedgroup.id] : ""
                        return (
                            <TouchableOpacity key={item.id} style={styles.memberlist} onPress={() => this.onClickMember(this.props, item)}>
                                {ImgComp}
                              <View style={{paddingLeft : 20}}>
                                <Text style={styles.memberText}>{userObj.display_name}</Text>
                                <Text style={[styles.memberText, styles.memberTextParentof]}>{parentof}</Text>
                              </View>
                                {adminStar}
                            </TouchableOpacity>
                        )
                    }}
                >

                </FlatList>
            )
        }
        else{
            return (<View>
              <ActivityIndicator
                  style={styles.centering}
                  animating={true}
                  color="#0000ff"
                  size="large"/>
            </View>)
        }
    }


  render2 () {
    console.log('member render')
    console.log(this.props)
    if (this.props.selectedgroup.groupLoaded) {
      var MemberList = this.props.selectedgroup.users.map(function (user, i) {
        console.log(user.profile_photo)
        if (user.profile_photo === '' || user.profile_photo === null || user.profile_photo.includes('gravatar.com')) {
          var profilePhotoURI = require('../img/blankprofile.png')
          var ImgComp = <Image
                            style={styles.memberImage}
                            source={profilePhotoURI}
                            imageStyle={
                                {
                                  width : 45,
                                  height : 45,
                                  resizeMode : 'stretch',
                                  borderRadius : 23
                                }
                            }
                        />

        } else {
          console.log(user)
          var profilePhotoURI = {uri: user.profile_photo.replace('http:', 'https:')}
            var ImgComp =  <CustomCachedImage style={styles.memberImage}
                                              component={Image}
                                              defaultSource={require('../img/blankprofile.png')}
                                              imageStyle={
                                                  {
                                                      width : 45,
                                                      height : 45,
                                                      resizeMode : 'stretch',
                                                      borderRadius : 23,
                                                                                                        }
                                              }
                                              source={profilePhotoURI}></CustomCachedImage>
        }
        if ((user.is_admin === 1) || (user.is_mod === 1)) {
          var adminStar = this.renderAdminStar()
        } else {
          var adminStar = <View/>
        }

        return (
              <TouchableOpacity key={user.id} style={styles.memberlist} onPress={() => this.onClickMember(this.props, user)}>
                  {ImgComp}
                  <View style={{paddingLeft : 20}}>
                  <Text style={styles.memberText}>{user.display_name}</Text>
                    <Text style={[styles.memberText, styles.memberTextParentof]}>{user.parent_of}</Text>
                  </View>
                  {adminStar}
              </TouchableOpacity>
        )
      }, this)
      return (
                <ScrollView>
                {MemberList}
                </ScrollView>
      )
    } else {
      return (<View>
                <ActivityIndicator
            style={styles.centering}
            animating={true} 
            color="#0000ff"
            size="large"/>
            </View>)
    }
  }
}
var mapStateToProps = function (store) {
  return {login: store.login,
    user: store.user
  }
}
const styles = StyleSheet.create({

  memberImage: {
    width: 45,
    height: 45,
    borderRadius: 22,
    borderWidth: 0.1,
    flexDirection: 'row',
    marginLeft: 5,
    paddingLeft: 2,
    paddingRight: 2
  },
  memberText: {
    color: 'black',
    textAlignVertical: 'center',
    fontSize: 18,
    paddingLeft: 5,
    paddingTop: 3
  },
  memberlist: {
    flexDirection: 'row',
    paddingTop: 5,
    paddingBottom: 5,
    margin: 5,
    backgroundColor: '#f1f1f1',
    height: 60,
    alignItems: 'stretch',
    elevation: 5,
    shadowOffset: {height: 2, width: 4},
    shadowColor: 'black',
    shadowOpacity: 0.3,
    shadowRadius: 3
  },
  memberTextParentof: {
    fontSize: 10
  },
  adminstarcontainer: {
    flex: 1,
    alignItems: 'flex-end',
    justifyContent: 'center',
    paddingRight: 10
  }
})

var mapDispatchToProps = function (dispatch) {
  return {
     viewMyProfile: function (userObj) { dispatch(memberActions.viewMyProfile(userObj)) },
      showProfileModalforUser: function (user,fromTopMenu) { dispatch(rootNavActions.showProfileModalforUser(user,fromTopMenu))

    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(GroupMemberList)
