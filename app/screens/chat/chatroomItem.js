'use strict'
import {
    Text,
    StyleSheet,
    RefreshControl, ListView,
    View, TouchableOpacity, ActivityIndicator
} from 'react-native'
import React, {Component} from 'react'
import {connect} from 'react-redux'
import {commonStyle} from '../../config/commonstyles'
import * as miscutils from '../../utils/misc'
import * as eventactions from '../../actions/eventActions'
import {CustomCachedImage} from "react-native-img-cache"
import Icon from 'react-native-vector-icons/FontAwesome';
import Image from 'react-native-image-progress'
import {Navigation} from 'react-native-navigation'
import Ionic from 'react-native-vector-icons/Ionicons';
var EnvConfig = require('../../config/environment')
class ChatRoomItem extends Component {

    constructor(props){
        super(props)

    }

    shouldComponentUpdate(nextProps, nextState) {
        if ((this.props.roomdetails.Details.lastmsgtimestamp === nextProps.roomdetails.Details.lastmsgtimestamp) &&
            (this.props.notifications === nextProps.notifications)){
            return false
        }
        return true
    }

    getUseridNames(roomdetailItem, userids) {
        var count = userids.length
        var maxcounttoshow = 3
        if (count < 3) {
            var maxcounttoshow = count
        }
        var userstr = ""
        for (var i = 0; i < maxcounttoshow; i++) {
            if (i === (maxcounttoshow - 1)) //TODO: make it better
            {
                userstr = userstr + roomdetailItem.Details.users[userids[i]].display_name
            } else {
                userstr = userstr + roomdetailItem.Details.users[userids[i]].display_name + ","
            }
        }
        if (count > 3) {
            userstr = userstr + " and Others"
        }
        return userstr
    }

    render(){
        console.log('RENDER->ROOM->' + this.props.roomdetails.key)
        console.log(this.props)
        if (this.props.roomdetails.Details.users === undefined) {
            return null
        }
        var userids = []
        var roomdetailItem = this.props.roomdetails
        Object.keys(roomdetailItem.Details.users).forEach(function (key) {
            if (key !== this.props.userid)
                userids.push(roomdetailItem.Details.users[key])
        }, this)
        if (userids.length == 0){
            return null
        }
        console.log(userids)
        if (userids.length == 1){
            var name = userids[0].display_name
            var name2 = ""
            var userobj = miscutils.getUserfromCache(this.props.usercache, userids[0].username)
            var photourl = userobj.profile_photo
        } else{
            var name = "GroupChat"
            var name2 = this.getUseridNames(roomdetailItem, userids)
            var photourl = ""
        }
        console.log(name)
        if (photourl == '') {
            var profilePhotoURI = require('../../img/groupchat.png')
            var imagecomp = <Image imageStyle={{
                width: 46,
                height: 46,
                borderRadius: 23,

            }}
                                   style={styles.memberImage} source={profilePhotoURI}></Image>
        } else {
            console.log(photourl)
            if (photourl.includes('gravatar.com') || photourl === "<DEFAULT>"){
                var profilePhotoURI = require('../../img/blankprofile.png')
                var imagecomp = <Image style={styles.memberImage}
                                       imageStyle={{
                                           width: 46,
                                           height: 46,
                                           borderRadius: 23,

                                       }}
                                       source={profilePhotoURI}></Image>
            }else {

                var profilePhotoURI = {uri: photourl.replace('http:', 'https:')}
                var imagecomp = <CustomCachedImage
                    component={Image}
                    imageStyle={{
                        width: 46,
                        height: 46,
                        borderRadius: 23,

                    }}
                    defaultSource={require('../../img/blankprofile.png')}
                    style={styles.memberImage}
                    source={profilePhotoURI}></CustomCachedImage>
            }
        }
        console.log(photourl)
        console.log(profilePhotoURI)
       

        return (
            <TouchableOpacity key={ this.props.roomdetails.key} style={styles.container}
                              onPress={this.onEnterChatNav.bind(this, this.props.roomdetails.key, name)}>
                <View style={styles.messageRoomContainer}>
                    {imagecomp}
                    <View style={styles.textLayout}>

                        <Text style={styles.messageRoomText}>{name}</Text>
                        <Text style={styles.messageRoomText2}>{name2}</Text>
                    </View>
                    <View style={styles.arrowcontainer}>
                        <Icon name="angle-right" style={styles.rightImage} size={32}></Icon>
                        {this.renderUnreaditems()}
                    </View>
                </View>
            </TouchableOpacity>
        )
    }

    onEnterChat2(){

    }

    onEnterChatNav(roomKey, username) {
        var title = username


        Navigation.push(this.props.componentId, {
            component: {
                name: 'nav.Three60Memos.ChatScreen',
                passProps: {
                    roomKey, title
                },
                options: {
                    topBar: {
                        title: {
                            text:title,
                            color: 'white'
                        },
                        background: {
                            color: EnvConfig.MAIN_CHAT_COLOR
                        },
                        backButton: {
                            visible: true,
                            color: 'white',
                            showTitle: false
                        },
                    }
                }
            }
        });

    }



    getUseridNames(roomdetailItem, userids) {
        var count = userids.length
        var maxcounttoshow = 2
        if (count < 2) {
            var maxcounttoshow = count
        }
        var userstr = ""
        for (var i = 0; i < maxcounttoshow; i++) {
            console.log(userids[i])
            if (i === (maxcounttoshow - 1)) //TODO: make it better
            {
                userstr = userstr +  userids[i].display_name
            } else {
                userstr = userstr +  userids[i].display_name + ","
            }
        }
        if (count > 2) {
            userstr = userstr + " and Others"
        }
        return userstr
    }

    renderUnreaditems()
    {
        console.log(this.props.notifications)
        if ((this.props.notifications !== 0) && (this.props.notifications !== null)) {
            return (
                <TouchableOpacity style={styles.counterContainer}>

                    <Text style={styles.countertext}>{this.props.notifications}</Text>
                </TouchableOpacity>
            )
        }
        else
            return null
    }

}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white',
    },
    messageRoomContainer: {
        flexDirection: 'row',
        borderColor: '#c3c3c3',
        borderBottomWidth: 1,
        padding: 5
    },
    countertext: {
        fontWeight: 'bold',
        color: 'white',
        alignSelf: 'center'
    },
    counterContainer: {
        backgroundColor: '#107dac',
        justifyContent: 'center',
        borderRadius: 30,
        height: 30,
        width: 30,
        marginRight: 20,
        marginTop: 10
    },
    textLayout: {
        flexDirection: 'column'
    },
    messageRoomText: {
        paddingLeft: 7,
        fontWeight: 'normal'
    },
    messageRoomText2: {
        paddingLeft: 7,
        paddingTop : 4,
        fontStyle: 'italic',
        fontSize : 12,
        color : 'darkgray'
    },
    arrowcontainer: {
        flex: 1,
        flexDirection: 'row-reverse'
    },
    memberImage: {
        width: 46,
        height: 46,
        borderRadius: 23,
        flexDirection: 'row',
        marginLeft: 5
    },
    rightImage: {
        color: 'darkgray',
        paddingTop: 7
    },
})

var mapStateToProps = function (store, ownProps) {
    return {
        userid : store.user.userid,
        usercache: store.login.usercache,
        notifications : store.user.notifications === null ? null : store.user.notifications.Rooms[ownProps.roomdetails.key] === undefined ?
            0 : store.user.notifications.Rooms[ownProps.roomdetails.key]
    }
}

var mapDispatchToProps = function (dispatch) {
    return {}
}

export default connect(mapStateToProps, mapDispatchToProps)(ChatRoomItem)
