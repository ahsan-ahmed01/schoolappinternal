'use strict'
import {Text, View,Modal,TouchableOpacity,StyleSheet
  } from 'react-native'
import React, {Component} from 'react'

//import {navigiationEventsHandler} from '../actions/navigationHandler'
//import PhotoBrowser from 'react-native-photo-browser';
import {connect} from 'react-redux'
import Gallery from 'react-native-image-gallery';
//TODO: Right Menu stops working !!!
class GroupPhotoBrowserScreen extends Component {


constructor(props) {
    super(props);
    this.state = {
      showCommentBox: true,
      page: 0,
     showModal : true
    }
}

getImages()
{
  var photoNum = this.props.navigation.state.params.paramObj.photos.length;
  var images =[]
  console.log(this.props)
  for (var i = 0; i < photoNum; i++) {
      source = {source : {uri : this.props.navigation.state.params.paramObj.photos[i].photo}}
    images.push(source)
  }
  console.log('GetImages')
  console.log(images)
  return images
}

close()
{
  console.log('in Close')
  this.state.showModal = false;
  console.log(this.props.navigator);
  this.props.navigator.dismissAllModals({});
  console.log('Exiting Close')
}

render() {
    let commentBox;
    if(this.state.showCommentBox) {
      commentBox = (
        <View
          style={{position: 'absolute', left: 0, right: 0, bottom: 0, height: 100, backgroundColor: '#00000066', padding: 10, alignItems: 'center', justifyContent: 'center'}}>
          <Text style={{color: 'white'}}>{this.props.navigation.state.params.paramObj.photos[this.state.page].caption}</Text>
        </View>
      );
    }

    return (

      <View style={styles.mainContainer}>

        <View style={styles.headercontainer}>
        
        </View>
        <View style={styles.bodycontainer}/>
        <View style={styles.footercontainer}/>

        <Modal onRequestClose={() => {
            console.log('Modal has been closed.');
        }}
          transparent={true}
          visible={this.state.showModal} >
          <View style={{flex: 1}}>
          <TouchableOpacity onPress={this.close.bind(this)} >
              <Text style={styles.closetext}  onPress={this.close.bind(this)}>Close</Text>
        </TouchableOpacity> 
            <Gallery
              style={{flex: 1, backgroundColor: 'transparent'}}
              initialPage={this.props.navigation.state.params.paramObj.imagesindex}
              pageMargin={10}
              images={this.getImages()}
              onSingleTapConfirmed={() => {
          this.toggleCommentBox();
        }}
              onGalleryStateChanged={(idle) => {
          if(!idle) {
            this.hideCommentBox();
          }
        }}
              onPageSelected={(page) => {
           this.setState({page});
        }}
            />

            {commentBox}
          </View>
        </Modal>
      </View>


    );
      
  }
  toggleCommentBox() {
    if(!this.state.showCommentBox) {
      this.setState({
        showCommentBox: true
      });
    } else {
      this.setState({
        showCommentBox: false
      });
    }
  }

  hideCommentBox() {
    if(this.state.showCommentBox) {
      this.setState({
        showCommentBox: false
      });
    }
  }

}

var mapStateToProps = function(store){
    return {login: store.login, 
    user : store.user,
    selectedgroup : store.selectedgroup
  }; 
};

const styles = StyleSheet.create({
    mainContainer:{
        flex : 1,
        backgroundColor : 'black'
    },
    headercontainer :{
        flex : 1
    },
     bodycontainer :{
        flex : 1
    },
     footcontainer :{
        flex : 1
    },
    closetext:{
      color : 'white',
      paddingTop : 25,
      paddingRight : 10,
      fontSize : 18,
      textAlign : 'right',
    }
})

export default connect(mapStateToProps)(GroupPhotoBrowserScreen)