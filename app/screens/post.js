"use strict";

import {
    Platform,
    AppRegistry,
    Text,
    ScrollView,
    StyleSheet,
    View,
    TextInput,
    Button,
    TouchableOpacity,
    ActivityIndicator,
    Image,
    Dimensions,
    TouchableHighlight,
    Switch
} from "react-native";
import React, { Component } from "react";
import * as rootNavAction from "../actions/rootNavActions";
import Icon from "react-native-vector-icons/FontAwesome";
import IconEntypo from "react-native-vector-icons/Entypo";
import Ionic from "react-native-vector-icons/Ionicons";
import MaterialIcons from "react-native-vector-icons/MaterialIcons";
import MaterialCommunityIcons from "react-native-vector-icons/MaterialCommunityIcons";
import { ImageCache, CustomCachedImage } from "react-native-img-cache";
import * as postapi from '../api/postapi'
import {
    commonStyle,
    RichTextToolBarActions,
    getRichTextToolBarActions
} from "../config/commonstyles";
import { connect } from "react-redux";
import ImagePicker from "react-native-image-crop-picker";
import * as postActions from "../actions/postActions";
import Picker from "react-native-picker";
import CustomBlurView from "../components/CustomBlurView";
//import tus from 'tus-js-client'
//import RNFetchBlob from 'react-native-fetch-blob'
//const Tus = require('uppy/lib/plugins/Tus')
//const Uppy = require('uppy/lib/core')
var EnvConfig = require("../config/environment");
var bugsnag = require("../config/bugsnag");
import { Navigation } from "react-native-navigation";
import Colors from "../constants/Colors";
//import {RichTextEditor, RichTextToolbar} from 'react-native-zss-rich-text-editor'
import { ifIphoneX } from "react-native-iphone-x-helper";
import Dialog, { DialogContent, SlideAnimation } from "react-native-popup-dialog";
import DateTimePicker from "react-native-modal-datetime-picker";
import CacheableImage from "../components/CacheableImage";
import ImageLoad from "react-native-image-placeholder";
import * as miscutils from '../utils/misc'
class PostScreen extends React.Component {

    _handleDatePicked = date => {
        this._hideDateTimePicker();
        //console.log(date)
        var newDate = new Date(date);

        var date =
            newDate.getDate() < 10
                ? "0" + newDate.getDate()
                : newDate.getDate();
        var month = newDate.getMonth() + 1;
        month = month < 10 ? "0" + month : month;
        this.setState({
            pinnedDate: month + "/" + date + "/" + newDate.getFullYear(),
            pinnedDateObj: newDate
        });
    };
    _handleDateSchedulePicked = date => {
        this._hideDateSchedulePicker();
        var newDate = new Date(date);

        var date =
            newDate.getDate() < 10
                ? "0" + newDate.getDate()
                : newDate.getDate();
        var month = newDate.getMonth() + 1;
        month = month < 10 ? "0" + month : month;
        this.setState({
            scheduleDate: month + "/" + date + "/" + newDate.getFullYear(),
            scheduleDateObj: newDate
        });
    };
    _handleTimeSchedulePicked = time => {
        this._hideTimeSchedulePicker();
        var date = new Date(time);
        var hours = date.getHours();
        var minutes = date.getMinutes();
        var ampm = hours >= 12 ? "PM" : "AM";
        hours = hours % 12;
        hours = hours ? hours : 12; // the hour '0' should be '12'
        minutes = minutes < 10 ? "0" + minutes : minutes;
        var strTime = hours + ":" + minutes + " " + ampm;

        var h = date.getHours();
        var m = date.getMinutes();
        var s = date.getSeconds();

        h = this.checkTime(h);
        m = this.checkTime(m);
        s = this.checkTime(s);
        console.log( h + ":" + m + ":" + s)
        this.setState({
            scheduleTime: "" + strTime ,
            scheduleTimeFormated: h + ":" + m + ":" + s,
            scheduleTimeObj: date
        });
    };

    componentWillMount() {
        if (this.props.inEditMode) {
            var groupname = this.props.selectedactivity.groupname == "" ? this.props.user.groups[this.props.selectedactivity.group_id].name
                : this.props.selectedactivity.groupname
            this.props.selectedactivity.groupname = groupname
            this.props.updatePostScreenwithData(this.props.selectedactivity)
            
           
            if (this.props.selectedactivity.isScheduled){
                var _scheduleTime = this.convertScheduleTime(this.props.selectedactivity.posttime)
                console.log(_scheduleTime)
                var _posttime = new Date(_scheduleTime)
                let date = _posttime.getDate() < 10
                    ? "0" + _posttime.getDate()
                    : _posttime.getDate();
                let _month = _posttime.getMonth()+1
                let month = _month < 10
                            ? "0" + _month
                            : _month;
                var dtPostTimeDateString = month + "/" + date + "/" + _posttime.getFullYear()
                
                var hours = _posttime.getHours();
                var minutes = _posttime.getMinutes();
                var ampm = hours >= 12 ? "PM" : "AM";
                hours = hours % 12;
                hours = hours ? hours : 12; // the hour '0' should be '12'
                minutes = minutes < 10 ? "0" + minutes : minutes;
                var dtPostTimeTimeString = hours + ":" + minutes + " " + ampm;

                
            }
            console.log(this.props.selectedactivity.posttime.replace(/"/g, ''))
            console.log(new Date(this.props.selectedactivity.posttime.replace(/"/g, '')))

            if  (this.props.selectedactivity.isScheduled){
                var _tmpdt = new Date(this.props.selectedactivity.posttime.replace(/"/g, ''))
                var h = _tmpdt.getHours();
                var m = _tmpdt.getMinutes();
                var s = _tmpdt.getSeconds();
        
                h = this.checkTime(h);
                m = this.checkTime(m);
                s = this.checkTime(s);
                var _scheduleTimeFormatted = h + ":" + m + ":" + s
            }else{
                var _scheduleTimeFormatted = '17:00:00'
            }

            
            this.setState({
                selectedGroup: groupname,
                selectedGroupId: this.props.selectedactivity.group_id,
                pinnedTitle: this.props.selectedactivity.pinnedtopic,
                isPinned: this.props.selectedactivity.pinnedflag,
                pinnedDate: this.props.selectedactivity.pinneduntil,
                pinnedDateObj: this.props.selectedactivity.pinneduntil === "None" ? new Date() :
                    new Date(this.props.selectedactivity.pinneduntil + ""),
                scheduleDate: this.props.selectedactivity.isScheduled ? dtPostTimeDateString : "",
                scheduleDateObj: this.props.selectedactivity.isScheduled ? new Date(this.props.selectedactivity.posttime.replace(/"/g, '')) : new Date(),
                scheduleTimeObj: this.props.selectedactivity.isScheduled ? new Date(this.props.selectedactivity.posttime.replace(/"/g, '')) : new Date(),
                scheduleTime: this.props.selectedactivity.isScheduled ? dtPostTimeTimeString : "",
                isScheduled : this.props.selectedactivity.isScheduled,
                selectedReview : this.props.selectedactivity.post_reviews,
                scheduleTimeFormated : _scheduleTimeFormatted,
            })

            //console.log(this.state)
        }
        this._updateGroupList(this.props.user.groups);
    }

    constructor(props) {
        super(props);
        //console.log(this.props);

        Navigation.events().bindComponent(this);
        this.getHTML = this.getHTML.bind(this);
        var _selectedSharingType = "Everyone"
        if (this.props.inEditMode) {
            if ((this.props.selectedactivity.children !== null) && (this.props.selectedactivity.children.length > 0)) {
                _selectedSharingType = "Students"
            }
            if (this.props.selectedactivity.draftflag) {
                _selectedSharingType = "Draft"
            }
            if (this.props.selectedactivity.children !== null){
                var _selectedkids = this.props.selectedactivity.children.map(function (item) { return item.id })
            }else{
                var _selectedkids = []
            }
        }
        
        this.state = {
            hashgroups: {},
            selectedGroup: this.props.inEditMode ? this.props.selectedactivity.groupname : this.props.postscreen.selectedgroupid,
            selectedGroupId: this.props.inEditMode ? this.props.selectedactivity.group_id : this.props.postscreen.selectedgroupname,
            livePostEdit: (!this.props.isScheduledPost && this.props.inEditMode && !this.props.inBriefCase && this.props.isNewPost !== true),
            showPostScreen: false,
            pickerData: null,
            pickerGroupData: [],
            postText: this.props.inEditMode ? this.props.selectedactivity.posttext : "",
            selectedSharingType: _selectedSharingType,
            visible: false,
            pinnedDialogVisible: false,
            scheduleDialogVisible: false,
            isDateSchedulePickerVisible: false,
            isTimeSchedulePickerVisible: false,
            isDateTimePickerVisible: false,
            isGroupDialogVisible: false,
            isStudentDialogVisible: false,
            studentList: [],
            studentGroups: {},
            isPinned: false,
            pinnedTitle: "",
            pinnedDate: "",
            pinnedDateObj: new Date(),
            isScheduled: false,
            scheduleDate: "",
            scheduleDateObj: new Date(),
            scheduleTimeObj: new Date(),
            scheduleTime: "",
            reviewDialogVisible: false,
            review_mode: this.props.inEditMode ? this.props.selectedactivity.reviewflag : false,
            errorReviewLoad: false,
            reviewerList: null,
            reviewerListGroup: null,
            selectedReview: [],
            posttopiccount: 50,
            editselectedkids: this.props.inEditMode ?  _selectedkids  : []

        };
        this.hideNewPost = this.hideNewPost.bind(this);
        this.onCancelPost = this.onCancelPost.bind(this);
        this.onPost = this.onPost.bind(this);
        console.log(this.state);
        bugsnag.leaveBreadcrumb("In Post Screen");
    }

    navigationButtonPressed({ buttonId }) {
        if (buttonId === "closebutton") {
            Navigation.dismissAllModals();
        }
    }


    getSeletedGroupID() {

        var groupid = this.state.hashgroups[this.state.selectedGroup.replace(/_/g, " ")];
        return groupid
    }

    onClickCamera() {
        bugsnag.leaveBreadcrumb("On Click Camera/Photo");
        if (this.props.inEditMode) return

        ImagePicker.openPicker({
            cropping: true,
            multiple: true,
            useFrontCamera: true,
            maxFiles: 1000
        })
            .then(images => {
                this.props.selectPhotos(images);
                bugsnag.leaveBreadcrumb("SelectedImg=" + images.length);
            })
            .catch(err => {
                bugsnag.leaveBreadcrumb(err.message.slice(0, 30));
            });
    }

    onClickVideo() {
        bugsnag.leaveBreadcrumb("On Click Video");
        if (this.props.inEditMode) return

        ImagePicker.openPicker({
            mediaType: "video",
            multiple: true
        })
            .then(videos => {
                this.props.selectVideos(videos);
                // var client = new Vimeo(this.props.user.config.vimeo.ClientIdentifier,
                //     this.props.user.config.vimeo.ClientSecret,
                //    this.props.user.config.vimeo.token);

                ////console.log(client)
                bugsnag.leaveBreadcrumb("SelectedVids=" + videos.length);
            })
            .catch(err => {
                //console.log(err.message);
                bugsnag.leaveBreadcrumb(err.message.slice(0, 30));
            });
    }

    onRemoveSelectedImage(key) {
        this.props.removeSelectedImage(key);
    }

    // onGroupPicker(event) {
    //   //console.log("Cliked");
    //   //console.log(this.state);
    //   Picker.init({
    //     pickerData: this.state.pickerData,
    //     pickerTitleText: "Select a Group",
    //     pickerCancelBtnText: "Cancel",
    //     pickerConfirmBtnText: "Select",
    //     selectedValue: [this.state.selectedGroup],
    //     onPickerConfirm: data => {
    //       this.setState({ selectedGroup: data[0] });
    //       this.props.setGroupid(this.getSeletedGroupID(), data[0]);
    //     }
    //   });
    //   Picker.show();
    // }

    onRemoveSelectedVideo(key) {
        this.props.removeSelectedVideo(key);
    }

    RenderSelectedImages() {
        var PhotoList = Object.keys(this.props.postscreen.selectedphotos).map(
            function (key) {
                return (
                    <View style={styles.imageItemView} key={key}>
                        <TouchableOpacity
                            onPress={this.onRemoveSelectedImage.bind(this, key)}
                        >
                            <Image
                                style={styles.selectedImage}
                                source={{
                                    uri: this.props.inEditMode ? this.props.postscreen.selectedphotos[key].lowres
                                        : this.props.postscreen.selectedphotos[key].path
                                }}
                            />
                        </TouchableOpacity>
                    </View>
                );
            },
            this
        );
        if (Object.keys(this.props.postscreen.selectedphotos).length > 0) {
            return (
                <View style={{ flex: 1, flexDirection: "column" }}>
                    <Text style={{ color: 'darkgray' }}>Tap a Photo to Remove it from the Post</Text>
                    <View style={{ flexDirection: "row", flexWrap: "wrap" }}>
                        {PhotoList}
                    </View>
                </View>
            );
        } else {
            return PhotoList;
        }
    }

    RenderSelectedVideos() {
        var VideoList = Object.keys(this.props.postscreen.selectedvideos).map(
            function (key) {
                return (
                    <View style={styles.imageItemView} key={key}>
                        <TouchableOpacity
                            onPress={this.onRemoveSelectedVideo.bind(this, key)}
                            style={{ paddingRight: 20, paddingTop: 10 }}
                        >
                            <Image
                                style={styles.selectedImage}
                                source={EnvConfig.VID_IMAGE}
                            />
                            <Text style={styles.videotext}>
                                {this.props.postscreen.selectedvideos[key].filename}
                            </Text>
                        </TouchableOpacity>
                    </View>
                );
            },
            this
        );
        if (Object.keys(this.props.postscreen.selectedvideos).length > 0) {
            return (
                <View style={{ flex: 1, flexDirection: "column" }}>
                    <Text>Tap a Video to Remove it from the Post</Text>
                    <View style={{ flexDirection: "row", flexWrap: "wrap" }}>
                        {VideoList}
                    </View>
                </View>
            );
        } else {
            return VideoList;
        }
    }

    onChangePostText(str) {
        //this.props.setPostText(str)
        this.setState({
            postText: str
        });
    }

    onPost() {
        // this.getHTML().then((val) => {
        //   //console.log(val)
        //   this.props.setPostText(val)
        //   this.props.sendPost(this.props.postscreen,this.props.user,this.props.user.token)
        //   //this.props.navigator.dismissModal()
        // }, this).done()
        bugsnag.leaveBreadcrumb("Sending Post");
        console.log(new Date(this.state.scheduleDate))
        console.log(new Date (this.state.pinnedDate))
        console.log(this.state.scheduleTimeFormated)
        if (this.state.isScheduled && this.state.isPinned) {
            if (new Date(this.state.scheduleDate) > new Date(this.state.pinnedDate)) {
                alert("Please Select Pinned date after schedule date.")
                return;
            }
        }

        var childids = [];
        var review_mode = 0;
        var pinned_post = 0;
        var schedule_post = 0;
        var scheduleDateTime = "";
        var pinned_until = "";
        var pinned_topic = "";
        var draft_mode = this.state.selectedSharingType === "Draft" ? true : false;
        if (this.state.selectedReview.length > 0) {
            review_mode = 1;
        }
        // If sent post is hit when in Review Mode then it should goto post
        if (this.props.isReviewPost && !this.state.livePostEdit){
            if (this.props.selectedactivity.user !== this.props.user.username)
                review_mode = 0;
        }
        
        if (this.state.selectedSharingType == "Students") {
            for (var i = 0; i < this.state.studentList.length; i++) {
                    if (this.state.studentList[i].isSelected) {
                        childids.push(this.state.studentList[i].id);
                    }

                }
            
        }
        console.log(childids)
        if (this.state.isPinned) {
            pinned_post = 1;
            if (this.state.pinnedDate.includes('/')) {
                var dateFormat = this.state.pinnedDate;
                var datePart = dateFormat.split("/");
                pinned_until = datePart[2] + "-" + datePart[0] + "-" + datePart[1];
            }else
            {
                pinned_until = this.state.pinnedDate
            }
            pinned_topic = this.state.pinnedTitle
        }
        if (this.state.isScheduled) {
            schedule_post = 1;
            var dateFormat = this.state.scheduleDate;
            
            var datePart = dateFormat.split("/");
            var currentTime = new Date();
            var timezone_standard = this.getTimeZone(currentTime);
            let _scheduleDateTime =
                datePart[2] +
                "-" +
                datePart[0] +
                "-" +
                datePart[1] + "T" + 
            this.state.scheduleTimeFormated + timezone_standard;
            console.log(_scheduleDateTime)
            var dt = new Date(_scheduleDateTime)
            console.log(dt)
            //console.log(dt.toISOString())
            scheduleDateTime = dt.toISOString()
            console.log(scheduleDateTime)
            
            
        }
        var postid = ((this.props.selectedactivity !== undefined) &&
            (this.props.selectedactivity.postid !== undefined) &&
            (this.props.selectedactivity.postid > 0)) ? this.props.selectedactivity.postid : null

        this.props.sendPost(
            postid,
            this.props.postscreen,
            childids,
            draft_mode,
            review_mode,
            this.state.selectedReview,
            pinned_post,
            pinned_topic,
            pinned_until,
            scheduleDateTime,
            this.props.user,
            this.state.postText,
            this.props.user.token
        );
        // const backAction = NavigationActions.back({
        //
        //     })
        // this.props.navigation.dispatch(backAction)
        this.props.removeAllSelectedMedia();
        this.hideNewPost();
    }

    getTimeZone(currentTime) {
        var timezone_offset_min = currentTime.getTimezoneOffset();
        var offset_hrs = parseInt(Math.abs(timezone_offset_min / 60));
        var offset_min = Math.abs(timezone_offset_min % 60);
        var timezone_standard = "";
        if (offset_hrs < 10)
            offset_hrs = '0' + offset_hrs;
        if (offset_min < 10)
            offset_min = '0' + offset_min;
        if (timezone_offset_min < 0)
            timezone_standard = '+' + offset_hrs + ':' + offset_min;
        else if (timezone_offset_min > 0)
            timezone_standard = '-' + offset_hrs + ':' + offset_min;
        else if (timezone_offset_min == 0)
            timezone_standard = 'Z';
        console.log(timezone_standard);
        return timezone_standard;
    }

    onCancelPost() {
        Navigation.dismissAllModals();
        this.props.removeAllSelectedMedia();
    }

    async getHTML() {
        const contentHtml = await this.richtext.getContentHtml();
        return contentHtml;
    }

    _updateGroupList(groupList) {

        if (groupList === null) return false;
        var first_item = true;
        let data = [];
        var pickerGroupData = [];
        var selectedGroupid = null
        for (var key in groupList) {
            if (
                !groupList[key].desc.includes("Public") ||
                this.props.user.roles === "admin"
            ) {
                var groupid = groupList[key].id;
                var groupname = groupList[key].name.replace(/_/g, " ");
                this.state.hashgroups[groupname] = groupid;
                data.push(groupname.replace(/_/g, " "));
                pickerGroupData.push(groupList[key]);
                if (
                    this.props.postscreen.lastgrouppostid !== null &&
                    this.props.postscreen.lastgrouppostid[0] === groupid
                ) {
                    selectedGroupid = groupid
                    this.state.selectedGroupId = groupid;
                    this.state.selectedGroup = groupname;
                    this.props.setGroupid([groupid], groupname);// hardcoding groupid as list
                } else {
                    if (first_item) {
                        this.state.selectedGroup = groupname;
                        selectedGroupid = groupid
                        this.state.selectedGroupId = groupid;
                        this.props.setGroupid([groupid], groupname);// hardcoding groupid as list
                    }
                }
                first_item = false;
            }
        }
        var studentGroups = this.props.attendance.groups;
        console.log(pickerGroupData)
        console.log(selectedGroupid)
        if (pickerGroupData.length > 0) {
            try {
                var groupid = selectedGroupid == null ? pickerGroupData[0].id : selectedGroupid;
                
                if (groupid in studentGroups) {
                    var stdsList = studentGroups[groupid];
                    var studentKeys = Object.keys(stdsList);
                    var studentsList = [];
                    for (var i = 0; i < studentKeys.length; i++) {
                        studentsList.push(stdsList[studentKeys[i]]);
                    }

                    for (var i = 0; i < studentsList.length; i++) {
                        studentsList[i].isSelected = false;
                        for (var j = 0; j < this.state.studentList.length; j++) {
                            if (this.state.studentList[j].id == studentsList[i].id) {
                                studentsList[i].isSelected = this.state.studentList[
                                    j
                                ].isSelected;
                            }
                        }
                    }
                    this.setState({
                        studentList: studentsList
                    });
                }

            } catch (err) {
                //console.log(err)
            }
        }
        
        this.setState({
            pickerGroupData: pickerGroupData,
            pickerData: data,
            studentGroups: studentGroups
        });

        return true;
    }

    shouldComponentUpdate(nextProps, nextState) {
        if (this.props.user.groups === null && nextProps.user.groups !== null) {
            {
                this._updateGroupList(nextProps.user.groups);
                return true;
            }
        }
        return true;
    }

    componentDidMount() {
        //console.log(this.props)
        if (this.props.inEditMode) {
            var group = {
                id: this.props.selectedactivity.group_id,
                name: this.props.selectedactivity.groupname
            }
            this.onGroupSelect(group)
        }
    }

    hideNewPost() {
        Navigation.dismissAllModals();
        Navigation.popToRoot(this.props.componentId)
    }

    _showDateTimePicker() {
        this.setState({ isDateTimePickerVisible: true });
    }

    _hideDateTimePicker() {
        this.setState({ isDateTimePickerVisible: false });
    }

    _showDateSchedulePicker() {
        this.setState({ isDateSchedulePickerVisible: true });
    }

    _hideDateSchedulePicker() {
        this.setState({ isDateSchedulePickerVisible: false });
    }

    _showTimeSchedulePicker() {
        this.setState({ isTimeSchedulePickerVisible: true });
    }

    _hideTimeSchedulePicker() {
        this.setState({ isTimeSchedulePickerVisible: false });
    }

    checkTime(i) {
        if (i < 10) {
            i = "0" + i;
        }
        return i;
    }

    

    onreviewSelected(user) {
        var userlist = this.state.reviewerList;
        var selectedReview = this.state.selectedReview
        try {
            var email = user.email;
            //console.log(email)
            //console.log(userlist)
            //console.log(selectedReview)
            var isSelected = false
            if (selectedReview.includes(email)) {
                selectedReview.splice(email, 1)
                isSelected = false
            } else {
                selectedReview.push(email);
                isSelected = true
            }
            //console.log(selectedReview)
            for (var i = 0; i < userlist.length; i++) {
                if (email === userlist[i].email) {
                    userlist[i].isSelected = isSelected
                }
            }
            this.setState({
                selectedReview: selectedReview,
                reviewerList: userlist
            })
            //console.log(userlist)

        } catch (err) {
            //console.log(err)

        }
    }

    onGroupSelect(group) {
        this.props.setGroupid([group.id], group.name);// hardcoding list
        console.log('Initializing Group')
        console.log(this.state)
        var studentgroups = this.state.studentGroups;
        try {
            var groupId = group.id;
            if (groupId in studentgroups) {
                var stdsList = studentgroups[groupId];
                var studentKeys = Object.keys(stdsList);
                var studentsList = [];
                for (var i = 0; i < studentKeys.length; i++) {
                    studentsList.push(stdsList[studentKeys[i]]);
                }
                console.log(studentsList)
                console.log(this.props.inEditMode)
                if (this.props.inEditMode) {
                    for (var i = 0; i < studentsList.length; i++) {
                        console.log(this.state.editselectedkids)
                        console.log(studentsList[i].id)
                        console.log(this.state.editselectedkids.indexOf(studentsList[i].id))
                        if (this.state.editselectedkids.indexOf(studentsList[i].id) >= 0){
                            console.log('Selected ' + studentsList[i].id )
                            studentsList[i].isSelected = true;
                        }else{
                            studentsList[i].isSelected = false;
                        }
                        // for (var j = 0; j < this.state.studentList.length; j++) {
                        //     if (this.state.editselectedkids.includes(studentsList[i].id)) {
                        //         studentsList[i].isSelected = true;
                        //     }
                        // }
                    }
                }
                console.log(studentsList)
                
                // *** Disable code to carry over selections to other groups
                // for (var i = 0; i < studentsList.length; i++) {
                //     studentsList[i].isSelected = false;
                //     for (var j = 0; j < this.state.studentList.length; j++) {
                //         if (this.state.studentList[j].id == studentsList[i].id) {
                //             studentsList[i].isSelected = this.state.studentList[j].isSelected;
                //         }
                //     }
                // }

                this.setState({
                    selectedGroup: group.name,
                    selectedGroupId: group.id,
                    isGroupDialogVisible: false,
                    studentList: studentsList
                });
            } else {
                this.setState({
                    selectedGroup: group.name,
                    selectedGroupId: group.id,
                    isGroupDialogVisible: false,
                    studentList: [],
                    selectedReview: [],
                    reviewerList: []
                });
            }
        } catch (err) {
            this.setState({
                selectedGroup: group.name,
                selectedGroupId: group.id,
                isGroupDialogVisible: false,
                studentList: [],
                selectedReview: [],
                reviewerList: null
            });
        }
    }

    onPinnedDone() {
        if (this.state.isScheduled) {
            if (this.state.scheduleDate > this.state.pinnedDate) {
                alert("Please Select Pinned date after schedule date.")
                return;
            }
        }
        if (this.state.pinnedTitle.trim().length > 0) {
            if (this.state.pinnedDate != "") {
                this.setState({ isPinned: true, pinnedDialogVisible: false });
            } else {
                alert("Please select a date");
            }
        } else {
            alert("Please enter a topic");
        }
    }

    onScheduleDone() {
        if (this.state.scheduleDate != "") {
            if (this.state.scheduleTime != "") {
                this.setState({ isScheduled: true, scheduleDialogVisible: false });
            } else {
                alert("Please select time");
            }
        } else {
            alert("Please select a date");
        }
    }


    renderReviewDialog() {
        return (
            <Dialog
                containerStyle={{ padding: 0 }}
                visible={this.state.reviewDialogVisible}
                dialogStyle={{top : -120}}
                rounded={true} dialogAnimation={new SlideAnimation({
                    slideFrom: 'bottom',
                  })}
                onTouchOutside={() => {
                    this.setState({ reviewDialogVisible: false });
                }}
            >
                <DialogContent
                    style={{
                        width: Dimensions.get("window").width * 0.9,
                        height: 350,
                        padding: 0,
                        margin: 0
                    }}
                >
                    <View
                        style={[
                            styles.dialogItem,
                            { justifyContent: "space-between", flexDirection: "row" }
                        ]}
                    >
                        <Text style={{ color: "black", fontSize: 16 }}>
                            Select Reviewer(s)
                        </Text>

                        <TouchableOpacity
                            onPress={() => {
                                this.setState({ reviewDialogVisible: false });
                            }}
                        >
                            <Text
                                style={{
                                    borderRadius: 5,
                                    backgroundColor: "#1B98E0",
                                    color: "#fff",
                                    paddingHorizontal: 10,
                                    paddingVertical: 5
                                }}
                            >
                                Done
                            </Text>
                        </TouchableOpacity>
                    </View>
                    <ScrollView style={{ flex: 1 }}>
                        {this.state.errorReviewLoad ? (
                            <View style={{ justifyContent: 'center' }}>
                                <Text style={{
                                    textAlign: 'center',
                                    fontSize: 20,
                                    color: '#FC440F',
                                    marginTop: 30

                                }}>Error Loading Reviewer List !</Text>
                            </View>
                        ) :
                            <View style={{ flexDirection: "row", flexWrap: "wrap" }}>
                                {this.state.reviewerList === null ?
                                    <View style={{ justifyContent: 'center' }}>
                                        <Text style={{
                                            textAlign: 'center',
                                            fontSize: 20,
                                            color: 'gray',
                                            marginTop: 30

                                        }}>Loading ...</Text>
                                    </View>
                                    : (this.state.reviewerList.map((item, index) => (
                                        <TouchableOpacity
                                            style={{
                                                width: (Dimensions.get("window").width * 0.9) / 4,
                                                margin: Dimensions.get("window").width * 0.02,
                                                borderRadius: 4,
                                                backgroundColor: item.isSelected ? "#1B98E0" : "#fff"
                                            }}
                                            onPress={() => {
                                                this.onreviewSelected(item);
                                            }}
                                        >
                                            <View
                                                style={{
                                                    padding: 5,
                                                    alignItems: "center"
                                                }}
                                            >
                                                {item.pictureurl == "" || item.pictureurl == null || item.pictureurl == "<DEFAULT>" ? (
                                                    <Image
                                                        style={styles.memberImage}
                                                        imageStyle={styles.roundImgStyle}
                                                        source={require("../img/groups.png")}
                                                    />
                                                ) : (
                                                        <ImageLoad
                                                            borderRadius={(Dimensions.get("window").width * 0.9) / 8}
                                                            style={styles.roundImgStyle}
                                                            placeholderStyle={styles.roundImgStyle}
                                                            loadingStyle={{ size: "small", color: "grey" }}
                                                            source={{
                                                                uri: item.pictureurl
                                                            }}
                                                        />
                                                    )}
                                                <Text numberOfLines={2} style={styles.groupName}>
                                                    {item.firstname + ' ' + item.lastname}
                                                </Text>
                                            </View>
                                        </TouchableOpacity>
                                    )))}
                            </View>
                        }
                    </ScrollView>
                </DialogContent>
            </Dialog>
        );
    }

    renderPinndedDialog() {
        return (
            <Dialog
                visible={this.state.pinnedDialogVisible}
                rounded={true}
                dialogStyle={{top : -100}}
                dialogAnimation={new SlideAnimation({
                    slideFrom: 'bottom',
                  })}
            // onTouchOutside={() => {
            //   this.setState({ visible: false });
            // }}
            >
                <DialogContent style={{ borderRadius: 0, width: "95%", height: 300 }}>
                    <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                        <Text style={styles.pinnedDialogHeader}>Post Topic</Text>
                        <Text style={styles.pinnedDialogHeaderCount}>{this.state.posttopiccount}</Text>
                    </View>
                    <TextInput
                        style={styles.topicTI}
                        placeholder="Topic Name"
                        value={this.state.pinnedTitle}
                        placeholderTextColor="grey"
                        multiline={true}
                        maxLength={50}
                        onChangeText={pinnedTitle => {
                            this.setState({
                                pinnedTitle,
                                posttopiccount: 50 - pinnedTitle.length
                            });

                        }}
                    />

                    <Text style={styles.pinnedDialogHeader}>
                        {" "}
                        Select Duration of Pinned Post :
                    </Text>
                    <TouchableOpacity onPress={() => this._showDateTimePicker()}>
                        <View style={{ flexDirection: "row", alignItems: "center" }}>
                            <Image
                                source={require("./../img/calendar_selected.png")}
                                style={{ height: 40, width: 40 }}
                            />
                            <TextInput
                                style={styles.tillDate}
                                value={this.state.pinnedDate}
                                placeholder="Till date"
                                placeholderTextColor="grey"
                                editable={true}
                                onFocus={() => this._showDateTimePicker()}
                            />
                        </View>
                    </TouchableOpacity>
                    <View
                        style={{
                            flexDirection: "row",
                            marginTop: 20,
                            justifyContent: "center"
                        }}
                    >
                        <TouchableOpacity
                            style={styles.pinnedDialogButton}
                            onPress={() =>
                                this.setState({ isPinned: false, pinnedDialogVisible: false })
                            }
                        >
                            <Text style={{ color: "white", fontSize: 16 }}>Unpin</Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                            style={[
                                styles.pinnedDialogButton,
                                { backgroundColor: EnvConfig.MAIN_HOME_COLOR }
                            ]}
                            onPress={() => this.onPinnedDone()}
                        >
                            <Text style={{ color: "white", fontSize: 16 }}>Done</Text>
                        </TouchableOpacity>
                    </View>
                </DialogContent>
            </Dialog>
        );
    }

    renderScheduledDialog() {
        return (
            <Dialog  rounded={true} dialogAnimation={new SlideAnimation({
                slideFrom: 'bottom',
              })} visible={this.state.scheduleDialogVisible}>
                <DialogContent
                    style={{ borderRadius: 0, width: "95%", height: 200, padding: 15 }}
                >
                    <TouchableOpacity onPress={() => this._showDateSchedulePicker()}>
                        <View style={{ flexDirection: "row", alignItems: "center" }}>
                            <Image
                                tintColor="black"
                                source={require("./../img/calendar_selected.png")}
                                style={{ height: 40, width: 40 }}
                            />
                            <TextInput
                                style={styles.tillDate} s
                                value={this.state.scheduleDate}
                                placeholder="Select Date"
                                placeholderTextColor="grey"
                                editable={false}
                                onFocus={() => this._showDateSchedulePicker()}
                            />
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => this._showTimeSchedulePicker()}>
                        <View style={{ flexDirection: "row", alignItems: "center" }}>
                            <Image
                                tintColor="black"
                                source={require("./../img/ic_clock.png")}
                                style={{ height: 38, width: 38 }}
                            />
                            <TextInput
                                style={styles.tillDate}
                                value={this.state.scheduleTime}
                                placeholder="Select Time"
                                placeholderTextColor="grey"
                                editable={true}
                                onFocus={() => this._showTimeSchedulePicker()}
                            />
                        </View>
                    </TouchableOpacity>
                    <View
                        style={{
                            flexDirection: "row",
                            marginTop: 20,
                            justifyContent: "center"
                        }}
                    >
                        <TouchableOpacity
                            style={styles.pinnedDialogButton}
                            onPress={() =>
                                this.setState({ isScheduled: false, scheduleDialogVisible: false })
                            }
                        >
                            <Text style={{ color: "white", fontSize: 16 }}>Clear</Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                            style={[
                                styles.pinnedDialogButton,
                                { backgroundColor: EnvConfig.MAIN_HOME_COLOR, padding: 5 }
                            ]}
                            onPress={() => this.onScheduleDone()}
                        >
                            <Text style={{ color: "white", fontSize: 16 }}>Schedule</Text>
                        </TouchableOpacity>
                    </View>
                </DialogContent>
            </Dialog>
        );
    }

    renderShareWithDialog() {
        return (
            <Dialog
                visible={this.state.visible}
                dialogStyle={{top : -100}}
                rounded={true} dialogAnimation={new SlideAnimation({
                    slideFrom: 'bottom',
                  })}
                onTouchOutside={() => {
                    this.setState({ visible: false });
                }}
            >
                <DialogContent style={{ width: 220, height: 300 }}>
                    <View style={[styles.dialogItem, { justifyContent: "center" }]}>
                        <Text>Share Post With</Text>
                    </View>
                    <TouchableOpacity
                        style={styles.dialogItem}
                        onPress={() =>
                            this.setState({
                                visible: false,
                                selectedSharingType: "Everyone"
                            })
                        }
                    >
                        <Image
                            style={{ height: 20, width: 20, marginHorizontal: 10 }}
                            source={require("./../img/groups.png")}
                        />
                        <Text>Everyone</Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                        style={styles.dialogItem}
                        onPress={() =>
                            this.setState({
                                visible: false,
                                selectedSharingType: "Draft"
                            })
                        }
                    >
                        <Image
                            style={{ height: 20, width: 20, marginHorizontal: 10 }}
                            source={require("./../img/lock.png")}
                        />
                        <Text>Draft</Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                        style={styles.dialogItem}
                        onPress={() =>
                            this.setState({
                                visible: false,
                                selectedSharingType: "Students",
                                isStudentDialogVisible: true
                            })
                        }
                    >
                        <Image
                            style={{ height: 20, width: 20, marginHorizontal: 10 }}
                            source={require("./../img/child.png")}
                        />
                        <Text>Students ({this.state.studentList.length})</Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                        style={styles.dialogItem}
                        onPress={() =>
                            this.setState({
                                visible: false
                            })
                        }
                    >
                        <MaterialIcons
                            name="cancel"
                            size={22}
                            style={{ color: "red", marginHorizontal: 10 }}
                        />
                        <Text>Cancel</Text>
                    </TouchableOpacity>
                </DialogContent>
            </Dialog>
        );
    }

    renderGroupSelectionDialog() {
        return (
            <Dialog
                containerStyle={{ padding: 0 }}
                dialogStyle={{top : -120}}
                rounded={true} dialogAnimation={new SlideAnimation({
                    slideFrom: 'bottom',
                  })} visible={this.state.isGroupDialogVisible}
                onTouchOutside={() => {
                    this.setState({ isGroupDialogVisible: false });
                }}
            >
                <DialogContent
                    style={{
                        width: Dimensions.get("window").width * 0.9,
                        height: 350,
                        padding: 0,
                        margin: 0
                    }}
                >
                    <View style={[styles.dialogItem, { justifyContent: "center" }]}>
                        <Text>Select Group To Post To:</Text>
                    </View>
                    <ScrollView style={{ flex: 1 }}>
                        <View style={{ flexDirection: "row", flexWrap: "wrap" }}>
                            {this.state.pickerGroupData.map((item, index) => (
                                <TouchableOpacity key={index}
                                    style={{
                                        width: (Dimensions.get("window").width * 0.9) / 4,
                                        margin: Dimensions.get("window").width * 0.02,
                                        borderRadius: 4,
                                    }}
                                    onPress={() => {
                                        this.onGroupSelect(item);
                                    }}
                                >
                                    <View
                                        style={{
                                            padding: 5,
                                            alignItems: "center"
                                        }}
                                    >
                                        {item.profile_photo == "" || item.profile_photo == null ? (
                                            <Image
                                                style={styles.memberImage}
                                                imageStyle={styles.roundImgStyle}
                                                source={require("../img/groups.png")}
                                            />
                                        ) : (
                                                <ImageLoad
                                                    borderRadius={(Dimensions.get("window").width * 0.9) / 8}
                                                    style={styles.roundImgStyle}
                                                    placeholderStyle={styles.roundImgStyle}
                                                    loadingStyle={{ size: "small", color: "grey" }}
                                                    source={{
                                                        uri: item.profile_photo
                                                    }}
                                                />
                                            )}
                                        <Text numberOfLines={2} style={styles.groupName}>
                                            {item.name.replace(/_/g, " ")}
                                        </Text>
                                    </View>
                                </TouchableOpacity>
                            ))}
                        </View>
                    </ScrollView>
                </DialogContent>
            </Dialog>
        );
    }

    renderStudentSelectionDialog() {
        return (
            <Dialog
                containerStyle={{ padding: 0 }}
                dialogStyle={{top : -120}}
                rounded={true} dialogAnimation={new SlideAnimation({
                    slideFrom: 'bottom',
                  })}
                visible={this.state.isStudentDialogVisible}
            >
                <DialogContent
                    style={{
                        width: Dimensions.get("window").width * 0.9,
                        height: 350,
                        padding: 0,
                        margin: 0
                    }}
                >
                    <View
                        style={[
                            styles.dialogItem,
                            { justifyContent: "space-between", flexDirection: "row" }
                        ]}
                    >
                        <Text style={{ color: "black", fontSize: 16 }}>
                            Select Students :
                        </Text>

                        <TouchableOpacity
                            onPress={() => {
                                this.setState({ isStudentDialogVisible: false });
                            }}
                        >
                            <Text
                                style={{
                                    borderRadius: 5,
                                    backgroundColor: "#1B98E0",
                                    color: "#fff",
                                    paddingHorizontal: 10,
                                    paddingVertical: 5
                                }}
                            >
                                Done
                            </Text>
                        </TouchableOpacity>
                    </View>
                    <ScrollView>
                        <View style={{ flexDirection: "row", flexWrap: "wrap" }}>
                            {this.state.studentList.map((item, index) => (
                                <TouchableOpacity key={index}
                                    style={{
                                        width: (Dimensions.get("window").width * 0.9) / 4,
                                        margin: Dimensions.get("window").width * 0.02,
                                        borderRadius: 4,
                                        backgroundColor: item.isSelected ? "#1B98E0" : "#fff"
                                    }}
                                    onPress={() => {
                                        var items = this.state.studentList;
                                        items[index].isSelected = !item.isSelected;
                                        this.setState({
                                            studentList: items
                                        });
                                    }}
                                >
                                    <View
                                        style={{
                                            padding: 5,
                                            alignItems: "center"
                                        }}
                                    >
                                        {item.profile_photo == "<DEFAULT>" ||
                                            item.profile_photo == "" ||
                                            item.profile_photo == null ? (
                                                <Image
                                                    style={styles.roundImgStyle}
                                                    imageStyle={styles.roundImgStyle}
                                                    source={require("../img/child.png")}
                                                />
                                            ) : (
                                                <ImageLoad
                                                    borderRadius={(Dimensions.get("window").width * 0.9) / 8}
                                                    style={styles.roundImgStyle}
                                                    placeholderStyle={styles.roundImgStyle}
                                                    loadingStyle={{ size: "small", color: "grey" }}
                                                    source={{
                                                        uri: item.profile_photo
                                                    }}
                                                />
                                            )}
                                        <Text numberOfLines={2} style={styles.groupName}>
                                            {item.firstname} {item.lastname}
                                        </Text>
                                    </View>
                                </TouchableOpacity>
                            ))}
                        </View>
                    </ScrollView>
                </DialogContent>
            </Dialog>
        );
    }

    convertScheduleTime(scheduledTime) {
        if (Platform.OS === 'ios') {
            var _tmp = new Date(scheduledTime.replace(/['"]+/g, ''));
            console.log('-------');
            console.log(scheduledTime.replace(/['"]+/g, ''));
            console.log(_tmp);
        }
        else {
            var _tmp = new Date(scheduledTime.replace(/['"]+/g, '') + "+00:00");
            console.log('-------');
            console.log(scheduledTime.replace(/['"]+/g, ''));
            console.log(_tmp);
        }
        if (_tmp.getHours() >= 12) {
            var hour = _tmp.getHours() == 12 || _tmp.getHours() == 0 ? 12 : _tmp.getHours() - 12;
            var ampm = 'PM';
        }
        else {
            var hour = _tmp.getHours() == 0 ? 12 : _tmp.getHours();
            var ampm = 'AM';
        }
        let minutes = _tmp.getMinutes() < 10 ? '0' + _tmp.getMinutes() : _tmp.getMinutes();
        var scheduletime = miscutils.getShortMonthName(_tmp.getMonth() + 1) + ' ' + _tmp.getDate() + ' ' + _tmp.getFullYear() + ' ' + hour + ':' + minutes + ' ' + ampm;
        return scheduletime;
    }

    _getSelectedStudentCount() {
        console.log(this.state.studentList)

        var selected = 0
        for (var i = 0; i < this.state.studentList.length; i++) {
            if (this.state.studentList[i].isSelected) {
                selected = selected + 1;
            }
        }
        if (selected === 0) {
            return "No Students Selected"
        }
        if (selected === 1) {
            return "1 Students Selected"
        }
        else {
            return selected + " Students Selected"
        }
    }


    async getReviewList() {
        var groupid = this.state.selectedGroupId
        if (groupid === undefined) {
            this.setState({
                errorReviewLoad: true,
                reviewerList: null
            })
        }
        var response = await postapi.getReviewList(groupid, this.props.user.token)
        if (response.status === 200) {
            //console.log(response.data.data)
            //console.log(this.state.selectedReview)
            this.setState({
                errorReviewLoad: false,
                reviewerList: response.data.data,
                reviewerListGroup: groupid
            })
            

        } else {
            this.setState({
                errorReviewLoad: true,
                reviewerList: null,
                reviewerListGroup: null
            })
        }

        //console.log(response)
    }

    render() {
        console.log(this.state)
        console.log(this.props)
        if (
            this.props.user.groups !== null ||
            this._updateGroupList(this.props.user.groups)
        ) {
            var ImageWidget = () => {

                if (this.props.profile_photo === '' || this.props.profile_photo === undefined ||
                    this.props.profile_photo === null || this.props.profile_photo.includes('gravatar.com')) {
                    var profilePhotoURI = require('../img/blankprofile.png')
                    //console.log(profilePhotoURI)
                    return (<Image defaultSource={require('../img/blankprofile.png')} source={profilePhotoURI}
                        style={styles.profileImage} imageStyle={{
                            width: 36,
                            height: 36,
                            borderRadius: 18,
                            borderWidth: 0.2,
                            borderColor: 'yellow'
                        }} />)

                } else {
                    var profilePhotoUri = { uri: this.props.profile_photo }
                    return (<CustomCachedImage
                        component={Image}
                        defaultSource={require('../img/blankprofile.png')}
                        imageStyle={{
                            width: 36,
                            height: 36,
                            borderRadius: 18,
                            borderWidth: 0.2,
                        }}
                        source={profilePhotoUri}
                        style={styles.profileImage} />)
                }

            }

            var renderPostShareWith = () => {
                if (this.props.user.roles === 'parent') {
                    return null
                }else{
                    return (
                        <TouchableOpacity
                        style={[styles.selectGroupStyle]}
                        onPress={() => {
                            this.setState({ visible: true });
                        }}>
                        <Text style={styles.buttonText}>
                            {this.state.selectedSharingType == "Students" ? this._getSelectedStudentCount()
                                : this.state.selectedSharingType}
                        </Text>
                        <Ionic style={{ marginLeft: 2 }} name="md-arrow-dropdown"></Ionic>
                    </TouchableOpacity>
                    )
                }
            }

            var postDropDowns = () => {
                if (this.state.livePostEdit) {
                    return (
                        <Text style={styles.buttonText}>
                            Posted to {this.state.selectedSharingType == "Students" ? this.state.editselectedkids.length + " Students"
                                        : this.state.selectedSharingType} in {this.state.selectedGroup.replace(/_/g, " ")}
                        </Text>
                    )
                } else {
                    return (
                        <View style={{ flex: 1, flexDirection: 'row', paddingLeft: 5 }}>
                            <TouchableOpacity
                                style={styles.selectGroupStyle}
                                onPress={() => {
                                    this.setState({ isGroupDialogVisible: true });
                                }}>
                                <Text style={styles.buttonText}>
                                    {this.state.selectedGroup.replace(/_/g, " ")}
                                </Text>
                                <Ionic style={{ marginLeft: 2 }} name="md-arrow-dropdown">

                                </Ionic>

                            </TouchableOpacity>
                            {renderPostShareWith()}
                        </View>
                       
                        )
                }
            }
            var ButtonName = "Post"
            
            if (this.state.isScheduled){
                ButtonName = "Schedule Post"
            }
            if (this.state.selectedReview.length > 0){
                ButtonName = "Send for Review"
            }
            if (this.state.selectedSharingType === "Draft")
            {
                ButtonName = "Save to Briefcase"
            }
            return (
                <View style={styles.container}>
                    <View style={styles.headerView}>
                        <TouchableOpacity onPress={this.onCancelPost}>
                            <Text style={styles.actionBarText}>Cancel</Text>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={this.onPost}>
                            <Text
                                style={styles.actionBarText}>{ButtonName}</Text>
                        </TouchableOpacity>
                    </View>
                    <View style={{ marginHorizontal: 5, flexDirection: 'row' }}>
                        {ImageWidget()}
                        <View style={{ flex: 1, flexDirection: 'column' }}>
                            <Text style={styles.profilename}>{this.props.user.display_name} </Text>
                            {postDropDowns()}
                        </View>
                    </View>
                    <View>


                        <TextInput
                            onChangeText={this.onChangePostText.bind(this)}
                            style={styles.postText}
                            placeholder="Share a moment ..."
                            multiline={true}
                            numberOfLines={4}
                            autoFocus={true}
                            value={this.state.postText}
                        />
                    </View>
                    {this.renderActionBar()}
                    <View style={styles.bottomview}>
                        
                        <View style={styles.scrollimageview}>
                            <ScrollView>
                                {this.RenderSelectedImages()}
                                {this.RenderSelectedVideos()}
                            </ScrollView>
                        </View>                        
                    </View>

                    {this.renderPinndedDialog()}
                    {this.renderReviewDialog()}
                    {this.renderScheduledDialog()}
                    {this.renderShareWithDialog()}

                    {this.renderGroupSelectionDialog()}
                    {this.renderStudentSelectionDialog()}

                    <DateTimePicker
                        isVisible={this.state.isDateTimePickerVisible}
                        onConfirm={this._handleDatePicked}
                        onCancel={() => this._hideDateTimePicker()}
                        mode="date"
                        date={this.state.pinnedDateObj}
                    />
                    <DateTimePicker
                        mode="date"
                        isVisible={this.state.isDateSchedulePickerVisible}
                        onConfirm={this._handleDateSchedulePicked}
                        onCancel={() => this._hideDateSchedulePicker()}
                        date={this.state.scheduleDateObj}
                    />
                    <DateTimePicker
                        mode="time"
                        isVisible={this.state.isTimeSchedulePickerVisible}
                        onConfirm={this._handleTimeSchedulePicked}
                        onCancel={() => this._hideTimeSchedulePicker()}
                        date={this.state.scheduleTimeObj}
                    />
                </View>
            );
        }
    }

    onEditorInitialized() {
        this.setFocusHandlers();
    }

    setFocusHandlers() {
        this.richtext.setContentFocusHandler(() => {
            //alert('content focus');
        });
    }

    renderActionBar() {

        var renderReviewAction = () => {
            if ((this.state.livePostEdit) || (this.props.isReviewPost)|| (this.props.user.roles === 'parent')) {
                return null
            }else{ 
                return (   <TouchableOpacity
                    style={styles.bottomWidgetView}
                    onPress={() => {
                        if (this.state.reviewerListGroup === this.state.selectedGroupId) {
                            this.setState({
                                reviewDialogVisible: true
                            })
                        } else {
                            this.setState({
                                reviewDialogVisible: true,
                                reviewerList: null,
                                selectedReview: [],
                                errorReviewLoad: false
                            })
                            this.getReviewList()

                        }

                    }
                    }
                >
                    <MaterialCommunityIcons size={32} name="account-check"
                        style={{ color: this.state.selectedReview.length > 0 ? "#E15554" : "#1B98E0" }} />
                    {this.state.selectedReview.length > 0 ? (<View style={{ marginBottom: 10 }}>
                        <View style={styles.countselectedstyle}><Text
                            style={{ color: 'white', fontSize: 10 }}>{this.state.selectedReview.length}</Text></View>

                    </View>) : null}
                </TouchableOpacity>)
            }
        }
        var renderPinnedAction = () => {
            if (this.props.user.roles !== 'parent'){
                return (
                    <TouchableOpacity
                            style={styles.bottomWidgetView}
                            onPress={() =>
                                this.setState({
                                    pinnedDialogVisible: true
                                })
                            }
                        >
                            <IconEntypo size={24} name="pin"
                                style={{ color: this.state.isPinned ? "#E15554" : "#1B98E0", marginTop: 3 }} />
                            {this.state.isPinned ? (<View style={{ marginBottom: 10 }}>
                                <Text style={styles.smallstatusText}>Pinned till</Text>
                                <Text style={styles.smallstatusText}>{this.state.pinnedDate}</Text>
                            </View>) : null}
                        </TouchableOpacity>
                )
            }else{
                return null
            }
            
        }

        var renderScheduleAction = () => {
            if (this.props.user.roles === 'parent'){
                return null
            }
            else{
                var scheduledPost = this.props.inEditMode ? this.state.scheduleDate + ' ' + this.state.scheduleTime: this.state.scheduleDate + ' ' + this.state.scheduleTime
            return (
                    <TouchableOpacity
                        style={[styles.bottomWidgetView]}
                        onPress={() => {
                            this.setState({
                                scheduleDialogVisible: true
                            });
                        }}
                    >
                        <Icon size={30} name="clock-o"
                            style={{ color: this.state.isScheduled ? "#E15554" : "#1B98E0" }} />
                        {this.state.isScheduled ? (<View style={{ marginBottom: 10 }}>
                            <Text style={styles.smallstatusText}>Scheduled for </Text>
                            <Text
                                style={styles.smallstatusText}>{scheduledPost}</Text>
                        </View>
                        ) : null}

                    </TouchableOpacity>
                    )
                }
        }


        if (this.props.inEditMode) {
            return (
                <View style={styles.tabView}>
                    {renderReviewAction()}
                    {renderPinnedAction()}
                     {renderScheduleAction()}
                </View>)
        } else {
            return (
                <View style={styles.tabView}>
                    {renderReviewAction()}
                    {renderPinnedAction()}
                    {renderScheduleAction()}
                    <TouchableOpacity
                        style={styles.bottomWidgetView}
                        onPress={this.onClickCamera.bind(this)}
                    >
                        <Icon size={24} name="camera" style={{
                            color: this.props.inEditMode ? "lightgray" : Object.keys(this.props.postscreen.selectedphotos).length > 0 ? "#E15554" : "#1B98E0",
                            marginTop: 6
                        }} />
                    </TouchableOpacity>

                    <TouchableOpacity
                        style={[styles.bottomWidgetView]}
                        onPress={this.onClickVideo.bind(this)}
                    >
                        <Ionic
                            name="ios-videocam"
                            size={36}
                            style={{ color: this.props.inEditMode ? "lightgray" : Object.keys(this.props.postscreen.selectedvideos).length > 0 ? "#E15554" : "#1B98E0" }}
                        />
                    </TouchableOpacity>
                </View>
            )
        }
    }
}

var styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: "column",
        justifyContent: "center",
        position: "absolute",
        top: 0,
        left: 0,
        bottom: 0,
        right: 0
    },
    smallstatusText: {
        fontSize: 10,
        textAlign: 'center',
        color: '#002e63'
    },
    headerView: {
        ...ifIphoneX(
            {
                paddingTop: 40,
                height: 90
            },
            {
                paddingTop: 20,
                height: 70
            }
        ),
        flexDirection: "row",
        alignItems: "center",
        justifyContent: "space-between",
        backgroundColor: EnvConfig.MAIN_HOME_COLOR
    },
    closeButton: {
        paddingTop: Platform.OS === "ios" ? 10 : 0,
        color: "black"
    },
    postButton: {
        backgroundColor: "#3fb0ac",
        width: 250,
        height: 30,
        alignItems: "center",
        justifyContent: "center",
        borderRadius: 3
    },
    buttonview: {
        flex: 1,
        justifyContent: "space-between",
        flexDirection: "column"
    },
    buttonuploadview: {
        flex: 1,
        flexDirection: "row",
        justifyContent: "space-around",
        alignItems: "center",
        marginTop: 3
    },
    postTextView: {
        flex: 1,
        borderColor: "gray",
        borderWidth: 1,
        marginTop: 10,

    },
    buttonClose: {
        flexDirection: "row-reverse",
        paddingTop: 10,
        paddingRight: 5
    },
    postText: {
        height: 150,
        borderColor: "darkgray",
        borderWidth: 1,
        marginTop: 10,
        padding: 5,
        fontSize: 14,
        textAlignVertical: "top",
        marginLeft: 3,
        marginRight: 3,
        backgroundColor: 'white',
        borderRadius: 5
    },
    buttons: {
        color: "#841584"
    },
    imageUpload: {
        flex: 1
    },
    videotext: {
        fontSize: 12
    },
    centering: {
        justifyContent: "center"
    },
    selectedImage: {
        width: Dimensions.get("window").width / 3 - 10,
        height: Dimensions.get("window").width / 3 - 10,
        alignSelf: "center",
        zIndex: 1,
        margin: 5
    },
    bottomview: {
        flex: 1
    },
    tabView: {
        height: 80,
        flexDirection: "row",
        justifyContent: "center",
        

    },
    bottomWidgetView: {
        alignItems: "center",
        justifyContent: 'space-between',
        marginTop: 10,
        width: 72,
        height : 70,
        
    },
    scrollimageview: {
        flex: 7,
        marginTop: 20
    },
    minuscircleicon: {
        backgroundColor: "transparent",
        color: "red",
        paddingLeft: 95,
        transform: [{ translateY: -120 }],
        zIndex: 10
    },
    imageItemView: {
        height: Dimensions.get("window").width / 3,
        width: Dimensions.get("window").width / 3
    },
    grouppicker: {
        height: 60,
        width: 100
    },
    headerText: {
        flexDirection: "row",
        paddingTop: 17
    },
    selectGroupStyle: {
        backgroundColor: "white",
        borderColor: '#363636',
        borderWidth: 1,
        zIndex: 100,
        borderRadius: 5,
        height: 20,
        paddingTop: 2,
        paddingRight: 5,
        paddingLeft : 5,
        flexDirection: 'row',
        marginLeft: 3
    },
    profilename: {
        padding: 5,
        fontWeight: 'bold',
        fontSize: 14

    },
    actionBarText: {
        color: "white",
        fontSize: 16,
        fontWeight: "bold",
        paddingHorizontal: 10,
        paddingVertical: 5
    },
    dialogItem: {
        height: 50,
        flexDirection: "row",
        alignItems: "center",
        borderBottomWidth: 1,
        borderBottomColor: "#d3d3d3"
    },
    pinnedDialogButton: {
        paddingVertical: 4,
        paddingHorizontal: 5,
        borderColor: "#d3d3d3",
        backgroundColor: "grey",
        width: 80,
        height: 40,
        borderWidth: 1,
        marginHorizontal: 15,
        borderRadius: 20,
        justifyContent: "center",
        alignItems: "center"
    },
    tillDate: {
        borderRadius: 3,
        height: 40,
        borderColor: "black",
        borderWidth: 1,
        flex: 1,
        margin: 5,
        padding: 4
    },
    topicTI: {
        borderRadius: 3,
        height: 80,
        borderColor: "black",
        borderWidth: 1,
        margin: 5,
        padding: 4
    },
    pinnedDialogHeader: {
        color: "black",
        fontSize: 16,
        marginTop: 15,
        marginBottom: 5
    },
    pinnedDialogHeaderCount: {
        fontSize: 16,
        marginTop: 15,
        marginBottom: 5,
        color: 'gray'
    },

    memberImage: {
        width: 70,
        height: 70,
        padding: 5,
        margin: 5
    },
    roundImgStyle: {
        width: (Dimensions.get("window").width * 0.9) / 4.1,
        height: (Dimensions.get("window").width * 0.9) / 4.1,
        marginHorizontal: 5,
        padding: 1,
        borderRadius: (Dimensions.get("window").width * 0.9) / 8.2,
        borderColor: "#d3d3d3",
        borderWidth: 1
    },
    groupName: {
        fontSize: 15,
        color: "black"
    },
    profileImage: {
        alignItems: 'center',
        justifyContent: 'center',
        flexDirection: 'column',
        width: 40,
        height: 40,
        borderColor: 'yellow',
        marginTop: 5,
        borderRadius: 20,
        borderWidth: 0.2,
        padding: 5
    },
    buttonText: {
        color: '#363636',
        fontSize: 12,
        marginLeft: 2
    },
    countselectedstyle: {
        backgroundColor: '#1b98e0',
        color: 'white',
        width: 14,
        height: 14,
        borderRadius: 7,
        alignItems: 'center',
        top: -10,
        right: 4
    }
});

var mapStateToProps = function (store) {
    var attendance = {};
    if (store.attendance != null) {
        attendance = store.attendance;
    }

    return {
        attendance: attendance,
        user: store.user,
        postscreen: store.postscreen,
        login: store.login,
        profile_photo: store.user.profile_photo
    };
};

var mapDispatchToProps = function (dispatch) {
    return {
        selectPhotos: function (photos) {
            dispatch(postActions.selectPhotos(photos));
        },
        selectVideos: function (videos) {
            dispatch(postActions.selectVideos(videos));
        },
        updatePostScreenwithData: function (data) {
            dispatch(postActions.updatePostScreenwithData(data))
        },
        removeSelectedVideo: function (key) {
            dispatch(postActions.removeSelectedVideo(key));
        },
        removeSelectedImage: function (key) {
            dispatch(postActions.removeSelectedImage(key));
        },
        setGroupid: function (groupid, groupname) {
            dispatch(postActions.setGroupid(groupid, groupname));
        },
        loadReviewerList: function (groupid) {
            dispatch(postActions.loadReviewerList(groupid))
        },
        // setPostText: function (postText) {
        //     dispatch(postActions.setPostText(postText))
        // },
        sendPost: function (postid, postProps,
            childids,
            draft_mode,
            review_mode,
            reviwer_list,
            pinned_post,
            pinned_topic,
            pinned_until,
            scheduleDateTime,
            user,
            postText,
            token) {
            dispatch(
                postActions.sendPost(
                    postid, postProps,
                    childids,
                    draft_mode,
                    review_mode,
                    reviwer_list,
                    pinned_post,
                    pinned_topic,
                    pinned_until,
                    scheduleDateTime,
                    user,
                    postText,
                    token
                )
            );
        },
        // hideNewPost: function () {
        //     dispatch(rootNavAction.hideNewPost())
        // }
        removeAllSelectedMedia: function () {
            dispatch(postActions.removeAllSelectedMedia());
        }
    };
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(PostScreen);
