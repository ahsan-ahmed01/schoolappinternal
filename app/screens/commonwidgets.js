import {
  Platform,
  AppRegistry,
  Text
} from 'react-native'

var RightButton = () => {
  return [{
    icon: require('../img/navicon_menu.png'),
    id: 'rightmenu'
  }]
}

var navigatorChatButtonsWidget = () => {
  let widget = {
    leftButtons: [{
      icon: require('../img/startchat.png'),
      id: 'startchat'
    }],
    rightButtons: [{
      icon: require('../img/navicon_menu.png'),
      id: 'rightmenu'
    }
    ]
  }
  return widget
}

var navigatorEventButtonsWidget = () => {
  let widget = {
    // leftButtons: [{
    //   icon: require('../../img/navicon_menu.png'),
    //   id: 'leftmenu'
    // }],
    leftButtons: [{
      icon: require('../img/icon_add_event.png'),
      id: 'addevent'
    }],
    rightButtons: [{
      icon: require('../img/navicon_menu.png'),
      id: 'rightmenu'
    }

    ]
  }
  return widget
}

exports.navigatorChatButtonsWidget = navigatorChatButtonsWidget
exports.navigatorEventButtonsWidget = navigatorEventButtonsWidget
exports.RightButton = RightButton
