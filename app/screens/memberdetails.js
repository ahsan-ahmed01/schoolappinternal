'use strict'
import {
    Platform,
    AppRegistry,
    Text,
    StyleSheet,
    View,
    Image, ImageBackground, TouchableOpacity, ActivityIndicator
} from 'react-native'
import React, {Component} from 'react'
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view'
import { Avatar,Card, Button, ListItem, Divider} from "react-native-elements";
import {connect} from 'react-redux'
import OneSignal from 'react-native-onesignal'
import {
    Form,
    Separator, InputField, LinkField,
    SwitchField, PickerField, DatePickerField, TimePickerField
} from 'react-native-form-generator';
import {Navigation} from 'react-native-navigation';
import Icon from 'react-native-vector-icons/FontAwesome';
import ImagePicker from 'react-native-image-crop-picker';
import * as memberActions from '../actions/memberprofileActions'
import {commonStyle} from '../config/commonstyles'
import * as firebaseapi from '../api/firebaseapi'
import * as miscutils from '../utils/misc'
//import NavigatorService from '../navigation/navigatorsvc'
import {ImageCache} from "react-native-img-cache";
import VersionNumber from 'react-native-version-number';
import codePush from "react-native-code-push";
import ChildDetailScreen from './attendance/childdetails'
import * as Layout from '../constants/Layout'
import Spinner from 'react-native-loading-spinner-overlay'
var closeIcon = null;
class MemberDetailsScreen extends Component {

    constructor(props) {
        super(props);
        console.log(this.props)
        Navigation.events().bindComponent(this);

        this.state = {
            editingOwnProfile: false, profile: false, isLoading: false, label: '',
            version: '',
            description: ''
        }

        if (this.props.selfView === true) {
            //Assign user for Self Profile
            console.log('Viewing Owns Profile')
            this.state.editingOwnProfile = true
            this.state.profile = this.props.user
        }
        else {
            //Assign Other user for  Profile
            if (this.props.userprofile !== null) {
                this.state.profile = this.props.userprofile
            }
            if (this.props.user.username === this.props.userprofile.username) {
                this.state.editingOwnProfile = true
            }
        }


        if (this.state.profile.linked === null) {
            this.state.profile.linked = {}
            this.state.profile.linked.LinkedDisplayName = ""
        }
        if (this.state.profile.linked === undefined) {
            this.state.profile['linked'] = {}
            this.state.profile.linked.LinkedDisplayName = ""
        }

        this.state.showChild = false
        this.state.childdetails = null
        console.log(this.props)
        console.log(this.state)

    }

    handleFormChange(formData) {

        this.setState({formData: formData})
        this.props.onFormChange && this.props.onFormChange(formData);
        console.log('FOrm changed')
    }

    handleFormFocus(e, component) {
        //console.log(e, component);
        this.refs.scroll.scrollToFocusedInput(component)
    }

    openTermsAndConditionsURL() {

    }

    navigationButtonPressed({ buttonId }) {
        console.log(buttonId)
        if (buttonId === 'closeprofilebutton'){
            Navigation.dismissAllModals()
        }
    }

    componentWillReceiveProps(nextProps) {
        console.log(this.props)
        console.log(this.state)
        console.log(nextProps)
        console.log(Layout.default.window.width)
        if (this.state.profile.id < 0 && nextProps.user.id > 0 && this.state.isLoading == false) {
            console.log('Calling View Profile to Update ')
            this.props.viewMyProfile(this.props.user)
            this.setState({isLoading: true})
            if (this.props.user.userid == nextProps.memberprofile.userid) {
                this.setState({editingOwnProfile: true})
            }

            this.UpdateFormWithProfile(nextProps)
        }
        if ((this.state.editingOwnProfile) && (nextProps.user.profile_photo !== false) && (this.state.profile !== false)
            && (nextProps.user.profile_photo !== this.state.profile.profile_photo)) {
            this.setState({
                profile: nextProps.user
            })
            console.log('Updated Pic !')
        }
        //this.state.profile.linked.LinkedDisplayName = nextProps.memberprofile.linked.LinkedDisplayName
        console.log('here ....')
        console.log(this.state.profile)
        console.log(nextProps.user)
        return true

    }

    UpdateFormWithProfile(nextProps) {
        console.log('Updating FOrm !!!')
        this.refs.registrationForm.refs.display_name.setValue(nextProps.memberprofile.display_name)
        if (nextProps.memberprofile.linked !== null) {
            this.refs.registrationForm.refs.linked.setValue(nextProps.memberprofile.linked.LinkedDisplayName)
        } else {
            this.refs.registrationForm.refs.linked.setValue('')
        }

        if (this.state.editingOwnProfile) {
            // this.refs.registrationForm2.refs.Email.setValue(nextProps.memberprofile.user_email)
            // this.refs.registrationForm2.refs.tel_home.setValue(nextProps.memberprofile.tel_home)
            // this.refs.registrationForm2.refs.tel_mobile.setValue(nextProps.memberprofile.tel_mobile)
        }

        console.log(this.props)
    }

    componentDidMount() {
        this.props.viewMyProfile(this.props.user)
        if (this.state.editingOwnProfile) {
            codePush.getUpdateMetadata().then((metadata) => {
                console.log(metadata)
                if ((metadata !== null) && (metadata !== undefined)) {
                    console.log(metadata)
                    this.setState({
                        label: metadata.label,
                        version: metadata.appVersion,
                        description: metadata.description
                    });
                }
            });
        }
    }

    async onstartChat(selectedUsers) {
        console.log('Selected ....')
        console.log(selectedUsers)
        // selectedUsers[this.props.user.id] = {
        //     id: this.props.user.id,
        //     display_name: this.props.user.display_name,
        //     profile_photo: this.props.user.profile_photo
        // }
        var users = miscutils.ObjectArraytoIndexArray(selectedUsers)
        if (users.length > 1) {
            console.log(users)
            var roomKey = await firebaseapi.getChatRoomKey(this.props.user.userid, users, "")
            if (roomKey === null) {
                roomKey = await  firebaseapi.setupNewChatRoom('', users, this.props.user.userid);
            }

            console.log(roomKey)
            Navigation.mergeOptions('idBottomTabs', {
                bottomTabs: {
                    currentTabIndex: 2
                }
            });
            this.EnterChatRoom(roomKey)

        } else {
            return null
        }
    }

    EnterChatRoom(roomKey) {
        var title = this.state.profile.display_name

        console.log('Entering Room with ChatID')
        Navigation.push('idChatTab', {
            component: {
                name: 'nav.Three60Memos.ChatScreen',
                passProps: {
                    roomKey, title
                },
                options: {
                    topBar: {
                        title: {
                            text:title,
                            color: 'white'
                        },
                        background: {
                            color: '#C95165'
                        },
                        backButton: {
                            visible: true,
                            color: 'white',
                            showTitle: false
                        },
                    }
                }
            }
        });

    }

    RenderPrivateProfile() {
        if (this.state.editingOwnProfile) {
            return (
                <View style={{flex : 1, height : 500}}>
                <Text style={styles.profilenametext}>{this.state.profile.display_name}</Text>
                        
                <Card title="Private Details">
                    <Text style={styles.textRow}>Email : {this.state.profile.user_email}</Text>
                    <Divider style={{marginLeft : -15,marginRight : -15, marginTop : 10, marginBottom : 10,backgroundColor: 'lightgray' }} />
                    {this.RenderChildren()}
                </Card>
                
                {this.RenderLogout()}
               
                </View>
            )
        }
    }

    onclickChild = (child) => ()=>{
        console.log(child)
        Navigation.push(this.props.componentId, {
            component: {
                name: 'nav.Three60Memos.ChildDetailScreen',
                passProps: {
                    childdetails : child
                },
                options: {
                    topBar: {
                        title: {
                            text: child.firstname + ' ' + child.lastname
                        }
                    }
                }
            }
        });
    }

    RenderChildren() {
        console.log('Rendering Children')
        if (this.props.user.children.length > 0) {
            var children = this.props.user.children.map((child) => {
                console.log(child)
                var childname = child.firstname + ' ' + child.lastname
                if (child.pictureurl === "" ||child.pictureurl === "<DEFAULT>") {
                    var leftAvatarprop = {rounded: true, title : child.firstname[0] + child.lastname[0]}
                }else{
                    var leftAvatarprop = {rounded: true, source: {uri: child.pictureurl}}
                }
                return (
                    <ListItem
                        key={child.id}
                        containerStyle={styles.containerStyle}
                        title={childname}
                        leftAvatar={leftAvatarprop}
                        onPress={this.onclickChild(child)}
                        chevronColor="gray"
                        chevron
                        />)
            }, this)
            return (
                <View style={{paddingTop:5}}>
                    {children}
                </View>
            )
        } else {
            return null
        }


    }

    RenderLogout() {
        if (this.state.editingOwnProfile === true && this.props.fromTopMenu === true) {
            var versionString = VersionNumber.appVersion + " " + VersionNumber.buildVersion + " (" + this.state.label + ') '

            return (
                <View>
                <TouchableOpacity activeOpacity={0.5} onPress={this.logoutUser.bind(this)} style={{paddingTop: 50}}>
                    <View style={[commonStyle.buttonAcross, {borderRadius: 4}]}>
                        <Text style={commonStyle.buttonText}>LogOut</Text>
                    </View>
                    <Text style={styles.poweredby}>Powered by 360 Memos</Text>
                    <Text style={styles.version}>{versionString}</Text>
                </TouchableOpacity>
                 <TouchableOpacity style={{alignSelf : 'center', top : 20, borderWidth : 1, borderColor : 'gray', borderRadius : 5, width : 200, height : 30}} onPress={this.clearCache.bind(this)}>
                 <Text style={{textAlign : 'center', color : 'gray', top : 4}}>Clear Image Cache</Text>
             </TouchableOpacity>
             </View>
             )
        } else {
            return null
        }
    }

    onClickCamera() {
        console.log('Camera Clicked !')
        ImagePicker.openPicker({
            width: 300,
            height: 400,
            cropping: true,
            smartAlbums : [ 'UserLibrary','PhotoStream',  'RecentlyAdded', 'SelfPortraits', 'Screenshots','Generic']
        }).then(image => {
            console.log(image)
            console.log(this.props)
            this.props.uploadMyProfilePhoto(image, this.props.user, this.props.user.profile_photo, this.props.user.token);
            //#TODO : Update all avataars for firebase chats
        });
    }

    renderCameraIcon() {
        if (this.state.editingOwnProfile) {
            return (
               <Icon onPress={this.onClickCamera.bind(this)} name="camera" style={styles.cameraicon} size={30}/>
               

            )
        }
    }

    clearCache(){

        ImageCache.get().clear();
        console.log('Image Cache Cleared')
        OneSignal.getTags((receivedTags) => {
            console.log(receivedTags);
            for (var key in receivedTags){
                if (key.startsWith('group')){
                    console.log('Deleting Tags ' + key)
                    OneSignal.deleteTag(key)
                }
 
            }
        });

    }

    logoutUser() {
        ImageCache.get().clear();
        this.props.logoutUser()
        firebaseapi.DeInit()
    }

    onmessageMember() {

        console.log(this.state.profile)
        Navigation.dismissAllModals()
        var selectedusers = {}
        selectedusers[this.props.user.userid] = {}
        selectedusers[this.props.user.userid]['username'] = this.props.user.username
        selectedusers[this.props.user.userid]['userid'] = this.props.user.userid
        selectedusers[this.props.user.userid]['profile_photo'] = this.props.user.profile_photo
        selectedusers[this.props.user.userid]['display_name'] = this.props.user.display_name

        selectedusers[this.state.profile.userid] = {}
        selectedusers[this.state.profile.userid]['username'] = this.state.profile.username
        selectedusers[this.state.profile.userid]['userid'] = this.state.profile.userid
        selectedusers[this.state.profile.userid]['profile_photo'] = this.state.profile.profile_photo
        selectedusers[this.state.profile.userid]['display_name'] = this.state.profile.display_name

        var key = new Promise.resolve(this.onstartChat(selectedusers, this.props.user))

    }


    renderMsgButton() {
        if ((this.state.profile['invalid_user'] !== true) && (!this.state.editingOwnProfile)) {
            return (
                <View style={{alignItems : 'center', paddingTop : 5, paddingBottom : 5}}>
                <TouchableOpacity onPress={this.onmessageMember.bind(this)}>
                    <View style={commonStyle.button}>
                        <Text style={commonStyle.buttonText}>Message</Text>
                    </View>
                </TouchableOpacity>
                </View>)
        }
        else
            return null
    }


    render() {
        console.log(this.props)
        console.log(this.state)
        console.log('RENDER->MEMBERDETAIL')
        if (this.state.profile) {

            console.log(this.state)
            // var backlink = ()=> {
            //     if (this.state.showChild){
            //         return(
            //         <TouchableOpacity style={styles.buttonBack}
            //                           onPress={()=>{
            //                               this.setState({showChild : false})
            //                           }}>
            //             <Icon name={'angle-left'} size={30}/>
            //         </TouchableOpacity>)
            //     }else{
            //         return(
            //         <TouchableOpacity style={styles.buttonBack}
            //                           >
            //             <Icon name={'angle-left'} size={30} style={{color : 'transparent'}}/>
            //         </TouchableOpacity>)
            //     }
            // }
            return (
                <View style={{flex: 1}}>
                        {this.renderMainBody()}

                </View>
            );

        }
        else {
            return (<View/>)
        }
    }


    renderChild(childdetails) {

        return (
            <ChildDetailScreen childdetails={this.state.childdetails}/>
        )
    }

    renderMainBody() {
        if (this.state.showChild) {
            console.log('Show Child')
            return this.renderChild()
        } else {
            var renderSeparator = (labelstr) => {
                if (this.state.editingOwnProfile) {
                    return <Separator label={labelstr}/>
                } else
                    return null
            }
            var initial =  this.state.profile.display_name.split(' ').map((n)=>n[0]).join(".");
            if (this.state.profile.profile_photo === "" || this.state.profile.profile_photo === null || this.state.profile.profile_photo === "<DEFAULT>") {
                var profilePhotoUri = () => { return (<TouchableOpacity  onPress={this.onClickCamera.bind(this)}>
                    <Avatar
                    size="xlarge"
                        rounded
                                        title={initial} />
                        </TouchableOpacity>)}
            }
            else {
                var profilePhotoUri = () => { return(<TouchableOpacity  onPress={this.onClickCamera.bind(this)}><Avatar
                                            rounded
                                            size="xlarge"
                                            source={{
                                                uri:
                                                    this.state.profile.profile_photo.replace('http:', 'https:')
                                            }}
                                        /></TouchableOpacity>)}

            }

            return (
                <View style={styles.container}>
                    <KeyboardAwareScrollView ref='scroll' style={{flex : 1}}>
                        <View style={styles.header}>
                            {profilePhotoUri()}
                            {this.renderCameraIcon()}
                            {this.updatePhotoLink()}
                            {this.renderMsgButton()}
                        </View>
                        <Spinner visible={this.props.memberprofile.isuploadingPhoto } textContent={'Saving Photo ...'}
                                 textStyle={{color: '#FFF'}}/>
                        {this.RenderPrivateProfile()}
                    </KeyboardAwareScrollView>
                </View>
            )
        }
    }

    updatePhotoLink(){
        if (Platform.OS === "ios"){
            return null
        }else{
            if (this.state.editingOwnProfile === true){
                return(
                    <TouchableOpacity style={{alignItems: 'center'}} onPress={this.onClickCamera.bind(this)}>
                        <Text style={{fontSize : 14, fontWeight : 'bold', textDecorationLine : 'underline'}}>Update Photo</Text>                
                    </TouchableOpacity>)
            }else return null
            
        }
    }

}



const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    head: {
        flex: 1,
        flexDirection: 'column'
    },
    body: {
        flex: 6,
        backgroundColor: 'transparent',
        flexDirection: 'column'
    }, MenuIcon: {
        paddingRight: 10
    },
    buttonClose: {
        paddingLeft: Layout.default.window.width - 55,
        paddingTop: 4,
        paddingBottom: 2

    },
    buttonBack : {
        flexDirection : 'row',
        paddingLeft: 5,
        paddingTop: 2,
        paddingBottom: 2

    },
    closebuttonContainer: {
        flexDirection : 'row',

    },
    containerStyle : {
        
    },
    headerimage: {
        flexGrow: 1,
        height : 200,
        justifyContent: 'center',
        alignItems: 'center'
    },
    profileImage: {
        flexDirection: 'column',
        width: 106,
        height: 106,
        borderColor: 'white',
        marginTop: 5,
        borderRadius: 10,
        borderWidth: 2,
        alignItems: 'center',
        shadowOffset: {height: 3, width: 6},
        shadowColor: 'black',
        shadowOpacity: 0.3,
        shadowRadius: 3
    },
    profilenametext: {
        fontSize: 24,
        textAlign: 'center'
    },
    
    form: {
        ...Platform.select({
            'ios': {
                backgroundColor: 'lightblue'
            }
        })

    },

    header : {
      flex : 1,
      alignItems : 'center',
        paddingTop : 10
    },
    cameraicon: {
        color: 'gray',
        opacity: 1,
        top : -90,
        zIndex : 999

    },
    centering: {
        alignItems: 'center',
        justifyContent: 'center',
        padding: 8
    },
    textRow : {
        padding : 5,
        fontSize : 18
    },
    poweredby: {
        textAlign: 'center',
        paddingTop: 5,
        color: 'navy',
        textDecorationStyle: 'solid'
    },
    version: {
        alignSelf: 'center',
        color: 'black',
        paddingBottom: 30,
        fontSize: 10
    },

})

var mapStateToProps = function (store) {
    return {user: store.user, memberprofile: store.memberprofile, rootNavprops: store.rootNavprops}
}

var mapDispatchToProps = function (dispatch) {
    return {
        uploadMyProfilePhoto: function (image, user, oldurl, token) {
            dispatch(memberActions.uploadMyProfilePhoto(image, user, oldurl, token))
        },
        viewMyProfile: function (userObj) {
            dispatch(memberActions.viewMyProfile(userObj))
        },
        logoutUser: function () {
            dispatch(memberActions.logoutUser())
        },

    }
}
export default connect(mapStateToProps, mapDispatchToProps)(MemberDetailsScreen)