import React, {Component} from 'react'
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Dimensions, Image, ScrollView
} from 'react-native'
import GroupSelector from './groupselector'
import * as memberlistactions from '../../actions/memberlistActions'
import Icon from 'react-native-vector-icons/FontAwesome';
import {connect} from 'react-redux'
import CacheableImage from '../../components/CacheableImage'

class MemberListScreen extends Component {

  constructor (props) {
    super(props)
    this.props.navigator.setOnNavigatorEvent(this.onNavigatorEvent.bind(this))
    
  }

  componentDidMount()
  {
    this.props.clearselections()
    console.log(this.props)
    if (this.props.initialSelectedList !== undefined){
      if (Object.keys(this.props.initialSelectedList).length !== 0){
      this.props.setSelectedUsers(this.props.initialSelectedList)
      }
    }
  }

  onSelectGroup(groupid){
      console.log('Got Group ID ' + groupid)
      this.props.getMemberList(groupid, this.props.user.token)
  }

  onNavigatorEvent(event)
    {
        if (event.id == 'startMemberList') {
          this.props.onSelectedList(this.props.memberlist.selectedlist)
          this.onClose(event)
        }
        if (event.id == 'cancelMemberList'){
          this.onClose(event)
        }
    }
  RenderSelectedList()
  {
    if (this.props.memberlist.selectedlist !== undefined){
        var userids = Object.keys(this.props.memberlist.selectedlist)
        var selectedMemberList = userids.map(function (userKey, i) {
            var user = this.props.memberlist.selectedlist[userKey]
            return (
              <Text key={user.id} style={styles.selectedlisttext}>{user.display_name}</Text>
            )
        }, this)

        return selectedMemberList
    }
    else{
      return null
    }
  }
  
  render () {
    return (
      <View style={styles.container}>
        <View  style={styles.addcontainer}>
            {this.RenderSelectedList()}
        </View>
        <View  style={styles.groupContainer}>
          <GroupSelector onSelectGroup={(groupid)=> this.onSelectGroup(groupid)}/>
        </View>
        <View style={styles.memberContainer}>
        <ScrollView style={{backgroundColor : 'white'}}>
        {this.RenderMemberList()}
         </ScrollView>
        </View>
      </View>
    )
  }

  RenderMemberList()
  {
    if ((this.props.memberlist.users === null) || (this.props.memberlist.users.length === 0)){
      return (<Text style={styles.noUsers}>Please select a group above to continue</Text>)
    }
    else{
      console.log(this.props.memberlist.users)
      var memberList = this.props.memberlist.users.map(function (user, i) {
        if (user.id !== this.props.user.id) {

        
        if (user.profile_photo == '') {
          var profilePhotoURI = require('../../../img/blankprofile.png')
            var ImageComp = <Image style={styles.memberImage} source={profilePhotoURI}></Image>
        } else {
          var profilePhotoURI = {uri: user.profile_photo.replace('http:', 'https:')}
          var ImageComp = <CacheableImage style={styles.memberImage} source={profilePhotoURI}></CacheableImage>
        }
        if ((user.is_admin === 1) || (user.is_mod === 1)) {
          var adminStar = this.renderAdminStar()
        } else {
          var adminStar = null
        }
        
        
        if (this.isAlreadySelected(user.id)){
          var memberButtonProps = {
            style: styles.memberlistSelected
          }
        }else{
           var memberButtonProps = {
            style: styles.memberlist
          }
        }

        return(
              <TouchableOpacity key={user.id} {...memberButtonProps} onPress={this.selectUser.bind(this, user)}>
                  {ImageComp}
                  <View>
                  <Text style={styles.memberText}>{user.display_name}</Text>
                    <Text style={[styles.memberText, styles.memberTextParentof]}>{user.parent_of}</Text>
                  </View>
                  {adminStar}
              </TouchableOpacity>)
      }
      }, this)
      return memberList
    }

  }

selectUser(user){
  if (this.isAlreadySelected(user.id))
  {
      this.props.unselectuser(user)
  }
  else{
    this.props.selectuser(user)
  }
    
}

  renderAdminStar () {
    console.log('_____ ADMIN ______')
    return (<View style={styles.adminstarcontainer}>
                    <Icon name="star" style={styles.adminstar} size={20} color="#900" />
                  </View>)
  }
  onClose (event) {
    this.props.navigator.dismissModal()
  }

  isAlreadySelected(userid)
  {
    console.log(this.props.memberlist.selectedlist)
    console.log(userid)
    var selectedids = Object.keys(this.props.memberlist.selectedlist)
    return (selectedids.indexOf(userid)>=0)
  }

}


var styles = StyleSheet.create({
  container: {
    flex: 1,
     flexDirection: 'column',
        justifyContent: 'center',
        padding: 10,
        backgroundColor : 'white'
  },
  addcontainer: {
    flex : 1,
    flexDirection : 'row',
    flexWrap:'wrap',
        backgroundColor : 'white'
  },
  groupContainer: {
    flex : 1,
    alignItems : 'stretch'
  },
  memberContainer: {
    flex : 4,
    flexDirection : 'column',
    alignItems: 'stretch',
      backgroundColor : 'white'
  },
  selectedlisttext:  {
    backgroundColor : 'blue',
    color : 'white',
    padding : 5,
    height : 25, 
    margin : 3,
    textAlign: 'center',
    borderRadius : 10
  },
  noUsers: {
    fontSize : 20,
    alignItems : 'flex-start',
    color : 'gray',
    padding : 20,
    textAlign :'center',
  },
  adminstarcontainer: {
    flex: 1,
    alignItems: 'flex-end',
    justifyContent: 'center',
    paddingRight: 10
  },
   memberImage: {
    width: 45,
    height: 45,
    borderRadius: 10,
    borderWidth: 1,
    borderColor: 'gray',
    flexDirection: 'row',
    marginLeft: 5
  },
  memberText: {
    color: 'black',
    textAlignVertical: 'center',
    fontSize: 18,
    paddingLeft: 5,
    paddingTop: 3
  },
  memberlist: {
    flexDirection: 'row',
    paddingTop: 5,
    paddingBottom: 5,
    margin: 5,
    backgroundColor: '#f1f1f1',
    height: 60,
    alignItems: 'stretch',
    elevation: 5,
    shadowOffset: {height: 2, width: 4},
    shadowColor: 'black',
    shadowOpacity: 1,
    shadowRadius: 3,
  },
    memberlistSelected: {
    flexDirection: 'row',
    paddingTop: 5,
    paddingBottom: 5,
    margin: 5,
    backgroundColor: '#90C3D4',
    height: 60,
    alignItems: 'stretch',
    elevation: 5
  },
  memberTextParentof: {
    fontSize: 10
  },
})


var mapStateToProps = function (store) {
  return {login: store.login, user: store.user, memberlist: store.memberlist}
}

var mapDispatchToProps = function (dispatch) {
  return {
     getMemberList: function (groupid, token) { dispatch(memberlistactions.getMemberList(groupid, token)) },
     selectuser: function (userid) { dispatch(memberlistactions.selectuser(userid)) },
     unselectuser: function(userid){ dispatch(memberlistactions.unselectuser(userid)) },
     clearselections: function(){ dispatch(memberlistactions.clearselections()) },
     setSelectedUsers: function(selectedUsers){dispatch(memberlistactions.setSelectedUsers(selectedUsers))}
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(MemberListScreen)