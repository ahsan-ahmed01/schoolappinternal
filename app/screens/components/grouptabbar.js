

import React, {Component} from 'react'
import {StyleSheet, View, ImageBackground, Image, Text, TouchableOpacity} from 'react-native'
import { ScrollableTabBar, } from 'react-native-scrollable-tab-view';
import Icon from 'react-native-vector-icons/FontAwesome';
import ImagePicker from 'react-native-image-crop-picker';
import * as groupdetailsAction from '../../actions/groupdetailsActions'
import {connect} from 'react-redux'
import Spinner from 'react-native-loading-spinner-overlay'
class GroupTabBar extends React.Component{

    shouldComponentUpdate(nextProps, nextState) {

        return true
    }


    render(){
        console.log(this.props)
        if (this.props.groupphotouri == null
            ||this.props.groupphotouri == ""
            || this.props.groupphotouri === undefined)
            var group_photo_uri = require('../../img/coverphoto.png')
        else
            var group_photo_uri = {uri: this.props.groupphotouri.replace('http:', 'https:')};

        console.log(group_photo_uri)
        if (this.props.profilephotouri == null
            || this.props.profilephotouri == "")
            var profile_photo_uri = require('../../img/blankprofile.png')
        else
            var profile_photo_uri = {uri: this.props.profilephotouri.replace('http:', 'https:')}
        return (
        <View style={styles.header}>
            <Spinner visible={this.props.groupphotouploading} textContent={'Updating Photo...'} textStyle={{color: '#FFF'}} />
            <ImageBackground source={group_photo_uri}  style={styles.coverimage} imageStyle={styles.imageStyle}>
                <Image source={profile_photo_uri} style={styles.profilephoto}/>
                {this.renderCameraProfilePhoto()}
                <Text style={styles.grouptext}>{this.props.group_name.replace(/_/g, ' ')}</Text>
                <Text
                    style={styles.groupdesc}>{this.props.group_desc}</Text>

            </ImageBackground>
            {this.renderCameraCoverPhoto()}
            <ScrollableTabBar tabs={this.props.tabs}
                              goToPage={this.props.goToPage}
                              activeTab={this.props.activeTab}
                              ref={this.props.ref}
                              scrollValue={this.props.scrollValue}/>

        </View>)
    }

    renderCameraCoverPhoto(){
        if (this.props.is_admin){
            return (
                <View style={{flex : 1}}>
            <TouchableOpacity onPress={this.onClickCoverCamera.bind(this)} style={{backgroundColor : 'transparent'}}>
                <Icon name="camera" style={styles.cameraicon} size={18}/>
            </TouchableOpacity>
                </View>
            )
        }else{
            return null
        }
    }
    renderCameraProfilePhoto(){
        if (this.props.is_admin){
            return (
                <View style={{flex : 1}}>
                    <TouchableOpacity onPress={this.onClickProfileCamera.bind(this)} style={{backgroundColor : 'transparent'}}>
                        <Icon name="camera" style={styles.cameraicon} size={18}/>
                    </TouchableOpacity>
                </View>
            )
        }else{
            return null
        }
    }

    onClickCoverCamera(){
        ImagePicker.openPicker({
            width: 800,
            height: 600,
            cropping: true,
            smartAlbums : [ 'UserLibrary','PhotoStream',  'RecentlyAdded', 'SelfPortraits', 'Screenshots','Generic']
        }).then(image => {
            console.log(image)
            console.log(this.props)
            this.props.uploadCoverPhoto(image,this.props.group_id, this.props.user_token);

        });
    }

    onClickProfileCamera(){
        ImagePicker.openPicker({
            width: 120,
            height: 120,
            cropping: true,
            smartAlbums : [ 'UserLibrary','PhotoStream',  'RecentlyAdded', 'SelfPortraits', 'Screenshots','Generic']
        }).then(image => {
            console.log(image)
            console.log(this.props)
            this.props.uploadGroupProfilePhoto(image,this.props.group_id, this.props.user_token);

        });
    }

}



const styles =  StyleSheet.create({
    header: {
        flexDirection: 'column',
        height: 190,
        marginBottom: 10
    },
    tabBarContainer: {
        height : 100
    },
    cameraicon: {
        color : 'gray',
        alignItems: 'flex-start',
        opacity : 0.8,
        marginTop: -23,
        paddingLeft: 5
    },
    coverimage: {
        height: 150,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor : '#71C7EC'
    },
    imageStyle : {
      height : 150
    },
    groupdesc: {
        backgroundColor: 'transparent',
        color: 'white',
        fontSize: 10
    },
    grouptext: {
        backgroundColor: 'transparent',
        color: 'white',
        fontWeight: 'bold'
    },
    profilephoto: {
        width: 80,
        height: 80,
        borderRadius: 37,
        borderColor: 'white',
        borderWidth: 2,
        marginTop: 20
    },

})


var mapStateToProps = function (store, ownProps) {
    console.log(ownProps)
    return {
        groupphotouploading : store.user.groupphotouploading,
        groupphotouri : store.user.groups[ownProps.group_id].cover_photo,
        profilephotouri : store.user.groups[ownProps.group_id].profile_photo,

    }
}

var mapDispatchToProps = function (dispatch) {
    return {
        uploadCoverPhoto: function (image, groupid, token) {
            dispatch(groupdetailsAction.uploadCoverPhoto(image, groupid, token ))
        },
        uploadGroupProfilePhoto: function (image, groupid, token) {
            dispatch(groupdetailsAction.uploadGroupProfilePhoto(image, groupid, token))
        },

    }
}
export default connect(mapStateToProps, mapDispatchToProps)(GroupTabBar)