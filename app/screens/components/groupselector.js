'use strict'
import {
  Platform,
  AppRegistry,
  Text,
  StyleSheet,
  ScrollView,
  Image, Dimensions,
  View, TouchableOpacity
} from 'react-native'
import React, {Component} from 'react'
import {connect} from 'react-redux'
import * as memberActions from '../../actions/memberprofileActions'

class GroupSelector extends Component {

  renderGroupList () {
    var groupList = Object.keys(this.props.user.groups).map(function (key, i) {
       var group =this.props.user.groups[key]
        console.log(group)
        if (group.profile_photo === '')  {
        var profilePhotoURI = require('../../img/groups.png')
      } else {
        var profilePhotoURI = {uri: group.profile_photo.replace('http:', 'https:')}
      }
      if (this.props.memberlist.selectedgroup === group.id) {
        var inputProps = {
          style: styles.selectedgroupImage
        }
        var grpText = {
            style : styles.selectedgroupText
        }
      } else {
        var inputProps = {
          style: styles.groupImage
        }
          var grpText = {
              style : styles.groupText
          }
      }
      if (group.desc.toUpperCase().includes('PUBLIC')){
        return null
      }else{
      return (
          <TouchableOpacity key={group.id} onPress={this.props.onSelectGroup.bind(this, group.id)}>
          <View style={{paddingRight: 20}}>
            <Image {...inputProps} source={profilePhotoURI}></Image>
            <Text {...grpText}>{group.name.replace(/_/g, ' ')}</Text>
        </View>
          </TouchableOpacity>
      )}
    }, this)
    return groupList
  }

  render () {
    console.log(this.props)
    return (
            <View style={styles.container}>
                <View style={{flexDirection : 'column', paddingTop: 2}}>
                  <Text>Select a Class</Text>
                  <ScrollView contentContainerStyle={styles.scrollContainer}
                                alwaysBounceVertical={false} showsVerticalScrollIndicator={false}
                                pagingEnabled={true}
                                horizontal={true}>

                    {this.renderGroupList()}
                  </ScrollView>
                </View>
            </View>
    )
  }

}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'row',
      backgroundColor : 'white'

  },
  scrollContainer: {
    flexDirection: 'row',
      padding : 5,
      paddingRight: 10
  },
  buttonObjectView: {
    width: 60,

  },
  selectedGroupView: {
    padding: 5,
    borderRadius: 10,

  },
  notselectedGroupView: {
    padding: 5,
    marginLeft: 5,

  },
  groupImage: {
    width: 50,
    height: 50,
    borderRadius: 25,
    borderWidth: 1,
    borderColor: 'gray',
      alignSelf: 'center'
  },
    selectedgroupImage: {
        width: 50,
        height: 50,
        borderRadius: 25,
        borderWidth: 3,
        borderColor: 'blue',
        alignSelf: 'center'
    },
  groupText: {
    textAlign: 'center',
    textAlignVertical: 'center',
    fontSize: 12,
      color : 'black',
      backgroundColor:'transparent',


  },
    selectedgroupText: {
        textAlign: 'center',
        textAlignVertical: 'center',
        fontSize: 12,
        color : 'blue',
        backgroundColor:'transparent',


    }
})

var mapStateToProps = function (store) {
  return {login: store.login, newsfeed: store.newsfeed, user: store.user, memberlist: store.memberlist}
}

var mapDispatchToProps = function (dispatch) {
  return {
    viewMyProfile: function (userObj) { dispatch(memberActions.viewMyProfile(userObj)) }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(GroupSelector)
