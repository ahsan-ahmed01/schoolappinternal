"use strict";
import {
  Text,
  StyleSheet,
  RefreshControl,
  ListView,
  View,
  TouchableOpacity,
  Dimensions,
  Alert,
  Switch,
  TouchableHighlight,
} from "react-native";
import React, { Component } from "react";
import { connect } from "react-redux";
import { commonStyle } from "../../config/commonstyles";
import * as miscutils from "../../utils/misc";
import * as eventactions from "../../actions/eventActions";
import { CustomCachedImage } from "react-native-img-cache";
import Entypo from "react-native-vector-icons/Entypo";
import Image from "react-native-image-progress";
import { Navigation } from "react-native-navigation";
import Ionic from "react-native-vector-icons/Ionicons";
import { ActionSheetCustom as ActionSheet } from "react-native-actionsheet";
import * as eventActions from "../../actions/eventActions";
import FontAwesome from "react-native-vector-icons/FontAwesome";
import * as Animatable from "react-native-animatable";
import { SwipeRow } from "react-native-swipe-list-view";
import Icon from "react-native-vector-icons/FontAwesome";
import AntDesign from "react-native-vector-icons/AntDesign";
import Feather from "react-native-vector-icons/Feather";
import * as eventsapi from "../../api/eventsapi";
var EnvConfig = require("../../config/environment");
var closeIcon = null;

class EventItem extends Component {
  constructor(props) {
    super(props);
    Ionic.getImageSource("md-close", 30).then((source) => {
      closeIcon = source;
    });
    this._rowRefs = [];
    this.showActionMenu = this.showActionMenu.bind(this);
    this.actionExecute = this.actionExecute.bind(this);
  }

  ///////getUserResponsesforEvent2///////
  componentDidMount() {
    // this.getUserResponsesforEvent2()
  }
  async getUserResponsesforEvent2() {
    var response = await eventsapi.getUserResponses(this.props.user.token);
    if (response.data.statusCode === 200) {
      console.log("--------getUserResponses---------");
      console.log(
        response.data,
        response.data.result.userResponse,
        "--getUserResponses-"
      );
      this.setState({
        // event4data: response.data.result.userResponse
      });
    } else {
      Alert.alert(
        "Error",
        "There was an error loading data for this event. Please try again"
      );
    }
  }
  actionExecute(index) {
    if (index === 1) {
      console.log("clone--------------->");
      if (this.props.event.eventtype === 1) {
        console.log("here", this.props.event.eventtype);
        this.onViewEvent(
          null,
          false,
          "Add Event",
          "AddEventScreen",
          this.props.event.eventtype,
          this.props.event.event_id
        );
      } else if (this.props.event.eventtype === 2) {
        console.log("here", this.props.event.eventtype);
        this.onViewEvent(
          null,
          false,
          "Add Rsvp Event",
          "AddRsvpEventScreen",
          this.props.event.eventtype,
          this.props.event.event_id
        );
      } else if (this.props.event.eventtype === 3) {
        console.log("here", this.props.event.eventtype);
        this.onViewEvent(
          null,
          false,
          "Add Parent-Teacher Event",
          "AddPTEventScreen",
          this.props.event.eventtype,
          this.props.event.event_id
        );
      } else if (this.props.event.eventtype === 4) {
        console.log("here", this.props.event.eventtype);
        this.onViewEvent(
          null,
          false,
          "Add Volunteer Event",
          "AddVolunteerEventScreen",
          this.props.event.eventtype,
          this.props.event.event_id
        );
      }
    } else if (index === 2) {
      console.log("edit--------------->");
      if (this.props.event.eventtype === 1) {
        console.log("here", this.props.event.eventtype);
        this.onViewEvent(
          this.props.event.event_id,
          false,
          "Edit Event",
          "AddEventScreen",
          this.props.event.eventtype
        );
      } else if (this.props.event.eventtype === 2) {
        console.log("here", this.props.event.eventtype);
        this.onViewEvent(
          this.props.event.event_id,
          false,
          "Edit Rsvp Event",
          "AddRsvpEventScreen",
          this.props.event.eventtype
        );
      } else if (this.props.event.eventtype === 3) {
        console.log("here", this.props.event.eventtype);
        this.onViewEvent(
          this.props.event.event_id,
          false,
          "Edit Parent-Teacher Event",
          "AddPTEventScreen",
          this.props.event.eventtype
        );
      } else if (this.props.event.eventtype === 4) {
        console.log("here", this.props.event.eventtype);
        this.onViewEvent(
          this.props.event.event_id,
          false,
          "Edit Volunteer Event",
          "AddVolunteerEventScreen",
          this.props.event.eventtype
        );
      }
    } else if (index === 3) {
      console.log("delete--------------->");
      this.props.deleteEventAction(
        this.props.event.event_id,
        this.props.user.token,
        this.props.user.groups
      );
    }
  }

  showActionMenu() {
    this.ActionSheet.show();
  }

  showAnimatedIcon() {
    console.log(this.props.event, "showAnimatedIcon");
    if (this.props.event.pendingflag == 1) {
      return (
        <Animatable.View animation="flash" iterationCount={10} useNativeDriver>
          <FontAwesome
            name="bell-o"
            size={20}
            style={styles.notificionIcon}
          ></FontAwesome>
        </Animatable.View>
      );
    } else {
      if (this.props.event.pendingflag == 2) {
        return null;
        // var attend = this.props.event.attend;
        // if (attend == undefined) return null;
        // return attend === "Yes" ? (
        //   <Entypo
        //     name="emoji-happy"
        //     size={20}
        //     style={[styles.notificionIcon, { color: "#7FB069" }]}
        //   ></Entypo>
        // ) : attend === "Maybe" ? (
        //   <Entypo
        //     name="emoji-neutral"
        //     size={20}
        //     style={[styles.notificionIcon, { color: "#FFBA49" }]}
        //   ></Entypo>
        // ) : attend === "No" ? (
        //   <Entypo
        //     name="emoji-sad"
        //     size={20}
        //     style={[styles.notificionIcon, { color: "#FF4545" }]}
        //   ></Entypo>
        // ) : null;
      } else {
        return null;
      }
    }
  }

  showTodayLabel() {
    if (this.props.isToday) {
      return (
        <View style={styles.showLabelRoot}>
          <Text style={styles.showLabelText}>Today</Text>
        </View>
      );
    } else {
      return null;
    }
  }
  showEventNameLabel() {
    const { event: { pendingflag, eventtype, attend, userresponse } } = this.props;
    if (eventtype === 1) {
      return <Text style={styles.eventNameLabelText}>Gene</Text>
    } else if (eventtype === 2) { 
      return <Text style={styles.eventNameLabelText}>Rsvp</Text>
    }
    else if (eventtype === 3) {
      return <Text style={styles.eventNameLabelText}>PT</Text>
     }
    else if (eventtype === 4) { 
      return <Text style={styles.eventNameLabelText}>Volunteer</Text>
    }
    else {
      return null
    }
  };
  RenderGroupIcon(event) {
    console.log(event);
    if (this.props.user.groupphotohash === null) {
      return null;
    }
    if (event.group_id !== "") {
      if (
        this.props.user.groupphotohash[event.grpid] === undefined ||
        this.props.user.groupphotohash[event.grpid] === ""
      ) {
        var photourl = require("../../img/groups.png");
        return (
          <Image
            style={styles.groupicon}
            imageStyle={{
              width: 40,
              height: 40,
              borderRadius: 20,
              backgroundColor: "white",
              paddingBottom: 20,
            }}
            source={photourl}
          ></Image>
        );
      } else {
        var photourl = {
          uri: this.props.user.groupphotohash[event.grpid].replace(
            "http:",
            "https:"
          ),
        };
        console.log("-->" + photourl);
        return (
          <CustomCachedImage
            component={Image}
            defaultSource={require("../../img/groups.png")}
            imageStyle={{
              width: 40,
              height: 40,
              borderRadius: 20,
              backgroundColor: "white",
            }}
            style={styles.groupicon}
            source={photourl}
          ></CustomCachedImage>
        );
      }
    } else {
      return null;
    }
  }

  onViewEvent(
    eventid,
    isReadonly = true,
    Event,
    navigate,
    eventtype,
    eventCloneId
  ) {
    console.log(eventid, this.props.event, eventtype, "onViewEvent");
    var eventdata = {};
    eventdata["eventname"] = this.props.event.event_name;
    eventdata["eventdesc"] = this.props.event.post_content;
    eventdata["startdate"] = this.props.event.event_start_date;
    eventdata["enddate"] = this.props.event.event_end_date;
    eventdata["starttime"] = this.props.event.event_start_time;
    eventdata["endtime"] = this.props.event.event_end_time;
    eventdata["location"] = this.props.event.location;
    eventdata["groupid"] = this.props.event.grpid;
    eventdata["users"] = this.props.event.users;
    eventdata["reminderdatetime"] = this.props.event.remindertime;
    eventdata["eventimgurl"] = this.props.event.eventimgurl;
    eventdata["remindsameday"] = this.props.event.remindsameday;
    eventdata["remindoneday"] = this.props.event.remindoneday;
    eventdata["remindtwoday"] = this.props.event.remindtwoday;
    eventdata["notifyusers"] = this.props.event.notifyusers;
    eventdata["alldayevent"] = this.props.event.alldayevent;

    console.log(eventdata, "eventdata");
    Navigation.showModal({
      stack: {
        children: [
          {
            component: {
              name: `nav.Three60Memos.${navigate}`,
              passProps: {
                eventdata,
                eventid,
                eventtype,
                isReadonly,
                eventCloneId,
              },
              options: {
                topBar: {
                  visible: true,
                  title: {
                    text: Event,
                    color: "#fff",
                  },
                  background: {
                    color: EnvConfig.MAIN_CHAT_COLOR,
                  },
                  rightButtons: [
                    {
                      id: "closebutton",
                      icon: closeIcon,
                      color: "#fff",
                    },
                  ],
                },
              },
            },
          },
        ],
      },
    });
  }

  shouldComponentUpdate(nextProps, nextState) {
    console.log("Should Update -->" + this.props.eventid);
    var curevent = this.props.event;
    var newevent = nextProps.event;
    if (
      curevent.attend === newevent.attend &&
      curevent.event_name === newevent.event_name &&
      curevent.alldayevent === newevent.alldayevent &&
      curevent.event_start_time === newevent.event_start_time &&
      curevent.event_end_time === newevent.event_end_time
    ) {
      return false;
    }
    console.log(curevent);
    console.log(newevent);
    return true;
  }

  collectRowRefs = (ref) => {
    this._rowRefs.push(ref);
  };

  onButtonClicked = (data) => {
    // do the button normal action
    this._rowRefs.forEach((ref) => {
      ref.closeRow();
    });
  };
  // #118744
  renderUserResponseOnEvent = () => {
    const { event: { pendingflag, eventtype, attend, userresponse } } = this.props;
    if (eventtype === 2) {
      if (pendingflag == 2) {
        if (attend == undefined) return null;
        return attend === "Yes" ? (
          <Text style={styles.userresponseViewText}>
            <Text>Your response: </Text>
            <Text style={{ fontWeight: "bold",color:"#000" }}>
              {`${attend} (${userresponse?.user_response?.adult} adults, ${userresponse?.user_response?.children} kids)`}
            </Text>
          </Text>
        ) : attend === "Maybe" ? (
            <Text style={styles.userresponseViewText}>
              <Text>Your response: </Text>
              <Text style={{fontWeight: "bold",color:"#000"}}>{attend}</Text>
           </Text>
        ) : attend === "No" ? (
            <Text style={styles.userresponseViewText}>
              <Text>Your response: </Text>
              <Text style={{fontWeight: "bold",color:"#000"}}>{attend}</Text>
            </Text>
        ) : null;
      } else {
        return null;
      }
    } else if (eventtype === 3) {
      return <Text style={[styles.userresponseViewText,{fontWeight: "bold",color:"#000"}]}>
        {`${userresponse?.user_response ? userresponse?.user_response?.length : 0} out of ${userresponse?.total_slots} filled`}
      </Text>
    } else if (eventtype === 4) {
      return <Text style={[styles.userresponseViewText,{fontWeight: "bold",color:"#000"}]}>
        {`${userresponse?.user_response ? userresponse?.user_response?.length : 0} out of ${userresponse?.total_slots} filled`}
      </Text>
    }
    else {
      return null
    }
  };

  render() {
    console.log("In Render ---> " + this.props.eventid);
    var day = this.props.eventday.getDate();
    var month = miscutils.getShortMonthName(this.props.eventday.getMonth() + 1);
    this.props.eventday.getDay();
    var dayofweek = miscutils.getShortDayofWeek(this.props.eventday.getDay());
    var event = this.props.event;
    console.log(event, "event");

    var starttime = miscutils.formatTimeinAMPM(event.event_start_time);
    var endtime = miscutils.formatTimeinAMPM(event.event_end_time);

    var renderActionMenu = () => {
      if (
        this.props.user.roles !== "parent" &&
        event.eventtype == 1 &&
        miscutils.isEntitledtoEvent(this.props.event, this.props.user)
      ) {
        return (
          <TouchableOpacity
            style={styles.morecontainer}
            onPress={this.showActionMenu}
          >
            <Ionic name="ios-more" size={22} color="darkgray" />
            <ActionSheet
              ref={(o) => (this.ActionSheet = o)}
              title={
                <Text style={{ color: "#000", fontSize: 18 }}>
                  Would you like to ...
                </Text>
              }
              options={[
                "Cancel",
                <View style={styles.actionbuttonstyle}>
                  <Entypo name={"edit"} size={20} />
                  <Text style={{ fontSize: 20, paddingLeft: 20 }}>
                    Edit Event
                  </Text>
                </View>,
                <View style={styles.actionbuttonstyle}>
                  <Entypo name={"trash"} style={{ color: "black" }} size={20} />
                  <Text
                    style={{ fontSize: 20, paddingLeft: 20, color: "black" }}
                  >
                    Delete Event
                  </Text>
                </View>,
              ]}
              cancelButtonIndex={0}
              destructiveButtonIndex={2}
              onPress={this.actionExecute}
            />
          </TouchableOpacity>
        );
      } else {
        return null;
      }
    };
    if (event.eventtype > 2) {
      var timeText = null;
    } else {
      if (
        starttime === endtime ||
        event.event_start_date === event.event_end_date ||
        event.alldayevent === 1
      ) {
        var timeText = "All Day";
      } else {
        var timeText = starttime + " - " + endtime;
      }
    }
    var renderDayDetails = () => {
      const todayDateViewBGColor = this.props.isToday
        ? { backgroundColor: "#3C3989" }
        : {};
      const todayDateViewColor = this.props.isToday ? { color: "#fff" } : {};
      return (
        <View style={[styles.leftDateContainer, todayDateViewBGColor]}>
          <Text style={[styles.monthtext2, todayDateViewColor]}>{month}</Text>
          <Text style={[styles.monthtext, todayDateViewColor]}>{day}</Text>
          <Text style={[styles.datetext, todayDateViewColor]}>{dayofweek}</Text>
        </View>
      );
    };
    var eventBGColor = this.getBGColorEventID(this.props.eventid);
    return (
      <View style={styles.container}>
        {this.props.user.roles !== "parent" &&
          (event.eventtype == 1 ||
            event.eventtype == 2 ||
            event.eventtype == 3 ||
            event.eventtype == 4) &&
          miscutils.isEntitledtoEvent(this.props.event, this.props.user) ? (
            <View style={styles.standalone}>
              <SwipeRow
                leftOpenValue={0}
                rightOpenValue={-120}
                closeOnRowPress
                ref={this.collectRowRefs}
                onRowOpen={(_) => {
                  console.log("onRowOpen");
                }}
                onRowPress={this.onButtonClicked}
                onRowClose={() => {
                  console.log("onRowClose");
                }}
              >
                <View style={styles.standaloneRowBack}>
                  <View style={styles.backTextWhite}></View>
                  <View style={styles.backTextWhite}>
                    <TouchableOpacity
                      onPress={() => {
                        this.actionExecute(1);
                        this.onButtonClicked();
                      }}
                    >
                      <Feather
                        active
                        name="copy"
                        color="#000"
                        size={24}
                        style={{ marginRight: 14 }}
                      />
                    </TouchableOpacity>
                    <TouchableOpacity
                      onPress={() => {
                        this.actionExecute(2);
                        this.onButtonClicked();
                      }}
                    >
                      <AntDesign
                        active
                        name="edit"
                        color="#000"
                        size={24}
                        style={{ marginRight: 14 }}
                      />
                    </TouchableOpacity>
                    <TouchableOpacity
                      onPress={() => {
                        this.actionExecute(3);
                        this.onButtonClicked();
                      }}
                    >
                      <Icon
                        active
                        name="trash-o"
                        size={24}
                        style={{ color: "red", marginRight: 14 }}
                      />
                    </TouchableOpacity>
                  </View>
                </View>
                <TouchableHighlight
                  key={event.event_id}
                  onPress={this.onViewEvent.bind(
                    this,
                    event.event_id,
                    true,
                    "View Event",
                    "AddEventScreen"
                  )}
                  delayLongPress={3800}
                >
                  <View key={event.event_id} style={styles.standaloneRowFront}>
                    {renderDayDetails()}
                    <View style={styles.rowFrontDisplay}>
                      <View style={styles.eventDetailContainer}>
                        <View style={styles.eventDetailImageContainer}>
                          {this.RenderGroupIcon(event)}
                        </View>
                        <View style={styles.eventDetailTextContainer}>
                          <Text style={styles.eventdetailtext}>
                            {event.event_name}
                          </Text>
                          {timeText && (
                            <Text style={styles.eventdetailtimetext}>
                              {timeText}
                            </Text>
                          )}
                          {this.renderUserResponseOnEvent()}
                        </View>
                      </View>
                      <View style={styles.eventUserResonseDtl}>
                        {/* {this.showTodayLabel()} */}
                        {/* {this.showEventNameLabel()} */}
                        {this.showAnimatedIcon()}
                      </View>
                    </View>
                  </View>
                </TouchableHighlight>
              </SwipeRow>
            </View>
          ) : (
            <View style={styles.standalone}>
              <TouchableHighlight
                key={event.event_id}
                onPress={this.onViewEvent.bind(
                  this,
                  event.event_id,
                  true,
                  "View Event",
                  "AddEventScreen"
                )}
              >
                <View key={event.event_id} style={styles.standaloneRowFront}>
                  {renderDayDetails()}
                  <View style={styles.rowFrontDisplay}>
                    <View style={styles.eventDetailContainer}>
                      <View style={styles.eventDetailImageContainer}>
                        {this.RenderGroupIcon(event)}
                      </View>
                      <View style={styles.eventDetailTextContainer}>
                        <Text style={styles.eventdetailtext}>
                          {event.event_name}
                        </Text>
                        {timeText && (
                          <Text style={styles.eventdetailtimetext}>
                            {timeText}
                          </Text>
                        )}
                        {this.renderUserResponseOnEvent()}
                      </View>
                    </View>
                    <View style={styles.eventUserResonseDtl}>
                      {/* {this.showTodayLabel()} */}
                      {/* {this.showEventNameLabel()} */}
                      {this.showAnimatedIcon()}
                    </View>
                  </View>
                </View>
              </TouchableHighlight>
            </View>
          )}
      </View>
    );
  };

  getBGColorEventID(eventid) {
    var BGColors = [
      { backgroundColor: "#d6cbd4" },
      { backgroundColor: "#e1ecef" },
      { backgroundColor: "#efe8e1" },
      { backgroundColor: "#e8efe1" },
      { backgroundColor: "#e1efeb" },
      { backgroundColor: "#e1e2ef" },
    ];
    //var BGColors = [{backgroundColor : '#d6cbd4'}]
    // var BGColors = [{backgroundColor : '#e1ecef'}]
    // var BGColors = [{backgroundColor: '#cbd8d6'}]
    var numColors = BGColors.length;
    var index = eventid % numColors;
    return BGColors[index];
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: "#fff",
    flex: 1,
    marginBottom: 10,
  },
  standalone: {},
  standaloneRowFront: {
    alignItems: "center",
    backgroundColor:"#fff",
    flexDirection: "row",
    justifyContent: "center",
    borderRadius: 6,
  },
  rowFrontDisplay: {
    flex: 1,
    flexDirection: "row",
    backgroundColor: "#fff",
    justifyContent: "space-between",
    alignItems: "center",
  },
  standaloneRowBack: {
    alignItems: "center",
    backgroundColor: "#fff",
    flex: 1,
    flexDirection: "row",
    justifyContent: "space-between",
    borderRadius: 6,
  },
  backTextWhite: {
    flexDirection: "row",
  },
  eventDetailTextContainer: {
    color: "#000",
    flexDirection: "column",
    flex: 1,
  },
  eventDetailImageContainer: {
    marginRight: 10,
    marginLeft: 10,
  },
  leftDateContainer: {
    width: 62,
    height: 80,
    textAlign: "center",
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "white",
    borderRadius: 6,
    elevation: 5,
    borderWidth: 1,
    borderColor: "#ededed",
  },
  eventDetailContainer: {
    flex: 1,
    color: "#000",
    alignItems: "center",
    justifyContent: "center",
    flexDirection: "row",
    backgroundColor: "#fff",
    height: 80,
    borderRadius: 5,
  },
  showLabelRoot: {
    position: "absolute",
    top: 2,
    right: 8,
  },
  showLabelText: { fontSize: 10, color: "#000", opacity: 0.5 },
  monthtext: {
    fontSize: 20,
    fontWeight: "bold",
    color: "#000",
  },
  monthtext2: {
    color: "#000",
  },
  datetext: {
    fontWeight: "bold",
    color: "#000",
  },
  morecontainer: {
    flexDirection: "row-reverse",
    marginTop: -5,
    marginLeft: -5,
    paddingLeft: 5,
  },
  eventdetailtext: {
    fontSize: 16,
    color: "black",
    fontWeight: "bold",
  },
  eventdetailtimetext: {
    color: "#897986",
  },
  groupicon: {
    width: 40,
    height: 40,
    borderRadius: 20,
    backgroundColor: "white",
  },
  actionbuttonstyle: {
    flex: 1,
    flexDirection: "row",
    justifyContent: "flex-start",
    paddingLeft: 10,
    width: Dimensions.get("window").width - 20,
    alignItems: "center",
  },
  notificionIcon: {
    color: "red",
    marginRight: 5,
  },
  eventUserResonseDtl: {
    position: "relative",
    width: "20%",
    height: 80,
    alignItems: "center",
    backgroundColor:"#fff",
    justifyContent: "center",
  },
  userresponseViewText: {
    color: "grey",
    fontSize:12
  },
  eventNameLabelText: {
    fontSize: 12, color: "#000", opacity: 0.5,paddingTop:4,paddingBottom:6
  }
});

var mapStateToProps = function (store) {
  return { user: store.user };
};

var mapDispatchToProps = function (dispatch) {
  return {
    deleteEventAction: function (eventid, token, groups) {
      dispatch(eventActions.deleteEventAction(eventid, token, groups));
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(EventItem);
