'use strict'

import {Platform, AppRegistry, Text,ActivityIndicator,View,StyleSheet,Image,TouchableOpacity,FlatList,ListView} from 'react-native'
import React, {Component} from 'react'
import {connect} from 'react-redux'
import ModalPhotoBrowser from '../modalphotobrowser'
import Modal from 'react-native-modal'
import CacheableImage from '../../components/CacheableImage'
class GroupPhotos extends Component
{
    constructor(props)
    {
        super(props)
        this.state = {
            showPhotoBrowser : false,
            photoParamObj : null,
            updateFlatList : 1
        }
        console.log(this.props)
    }
   onClickPhoto = (photoObj)=>
    {
        console.log('On Click Photo')
        console.log(photoObj)

        var photos = []
        photos.push(photoObj)
        var paramObj= {
            photos : photos,
            imagesindex : 0
        }
        console.log(this.props)
        this.setState({photoParamObj : paramObj})
        this.setState({showPhotoBrowser : true});


    }


    PhotoModal(){
        if (this.state.showPhotoBrowser) {
            return (
                <Modal onRequestClose={() => {
                    console.log('Modal has been closed.');
                }}
                       style={styles.modal} isVisible={this.state.showPhotoBrowser}>
                    <View style={{ backgroundColor : 'red', flex: 1 }}>
                        <ModalPhotoBrowser photoParamObj={this.state.photoParamObj} modaldismissfunc={this.dismissPhotoModal.bind(this)}/>
                    </View>
                </Modal>
            )
        }
        else{
            return null
        }
    }

    dismissPhotoModal(){
        this.setState({showPhotoBrowser : false})
    }

    componentWillReceiveProps(nextProps){
        this.setState({updateFlatList: this.state.updateFlatList + 1})
        console.log('flatlist ->' + this.state.updateFlatList)
    }

    render()
    {
       if (this.props.selectedgroup.isPhotoLoaded)
       {
        var PhotoList =  this.props.selectedgroup.photos.map(function(photoObj, i){ 
                var photo_uri = {uri : photoObj.photo.replace('http:','https:')}
                                    
                return (
                    <TouchableOpacity key={i} style={styles.touchcontainer} onPress={()=>this.onClickPhoto(photoObj, i)}>
                        <CacheableImage style={styles.memberImage} source={photo_uri}/>
                    </TouchableOpacity>
                )
        },this)
        return (

            <FlatList
                      horizontal={false}
                      extraData={this.state.updateFlatList}
                      numColumns={2}
                      removeClippedSubviews={true}
                      ListHeaderComponent= {this.PhotoModal()}
                      initialNumToRender={10}
                      contentContainerStyle={styles.container}
                      data={this.props.selectedgroup.photos}
                      renderItem={({item,i}) => {
                          console.log(item)
                          var photo_uri = {uri : item.photo.replace('http:','https:')}
                          return (
                          <TouchableOpacity key={i} style={styles.touchcontainer} onPress={()=>this.onClickPhoto(item)}>
                              <CacheableImage style={styles.memberImage} source={photo_uri}/>
                          </TouchableOpacity>)}}
                      keyExtractor={item => item.photo}
                          >

              </FlatList>

       )
       }
       else{
            return (
                <ActivityIndicator
            style={styles.centering}
            animating={!this.props.selectedgroup.isPhotoLoaded}
            color="#0000ff"
            size="large"/>
            )
       }

    }

    showb(){
        this.setState({showPhotoBrowser : true});

    }

}


var mapStateToProps = function(store){
    return {login: store.login, 
    user : store.user,
    selectedgroup : store.selectedgroup,
    startOnGrid : true,
    enableGrid : true
  } 
}
const styles = StyleSheet.create({
    scrollcontainer:{

    },
    container :{
        alignSelf : 'center',
            alignItems: 'flex-start',

    },
    modal: {
        backgroundColor: 'black',
        margin: 0, // This is the important style you need to set
        alignItems: undefined,
        justifyContent: undefined,
    },
    touchcontainer :{
         backgroundColor : 'white',
         margin : 3,
        // marginLeft : 10,
        shadowOffset : {height : 2, width : 2},
        shadowColor: "black",
        shadowOpacity: 0.3,
        shadowRadius: 2,
        // height : 110,
        // alignSelf : 'center'
    },
    memberImage :{
        width : 150,
        height : 150,
        padding : 5,
        margin : 5,
    }
})
export default connect(mapStateToProps)(GroupPhotos)