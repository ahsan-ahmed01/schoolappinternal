'use strict'
import {
  Text,
  StyleSheet,
  ScrollView,
  Image, RefreshControl, ListView,
  View, TouchableOpacity, ActivityIndicator
} from 'react-native'
import React, {Component} from 'react'
import {connect} from 'react-redux'
import {commonStyle} from '../../config/commonstyles'
import * as newsfeedaction from '../../actions/newsfeedActions'

class GroupActivity extends Component {

  componentDidMount () {
      var group = {}
      group[this.props.selectedgroup.id] = this.props.selectedgroup
      console.log(this.props)
    this.props.getNewsFeedByGroup(0, group, this.props.user.token)
  }

    onLoadNewsFeed()
    {
      console.log('in group !!!')
        var group = {}
        group[this.props.selectedgroup.id] = this.props.selectedgroup
        this.props.getNewsFeedByGroup(0, group, this.props.user.token)
    }

    onMoreNewsFeed () {
        var group = {}
        group[this.props.selectedgroup.id] = this.props.selectedgroup
        console.log('in group !!!')
        this.props.getNewsFeedByGroup(this.props.newsfeed.group_nextpg, group, this.props.user.token)
    }

  render () {
    return (
      <Text>NewsFeed</Text>
    )
  }

}


const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
})

var mapStateToProps = function (store) {
  return {login: store.login, newsfeed: store.newsfeed, user: store.user}
}
var mapDispatchToProps = function (dispatch) {
  return {

    getNewsFeedByGroup: function (page, groupid, token) { dispatch(newsfeedaction.getNewsFeedByGroup(page, groupid, token)) }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(GroupActivity)

