const React = require('react');
const { Component } = require('react');
const { AppState, View, Text, Platform, TouchableHighlight, TouchableOpacity } = require('react-native');
import { connect } from 'react-redux'
const { Navigation } = require('react-native-navigation');
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons'
import * as memberActions from '../../actions/memberprofileActions'
import * as attendanceactions from '../../actions/attendanceActions'
import * as userActions from '../../actions/userActions'
import * as loginActions from '../../actions/loginActions'
import {Auth} from 'aws-amplify'
class LogoutButton extends Component {
    constructor(props){
        super(props)
        this.refreshAttendance = this.refreshAttendance.bind(this)
    }

    logoutuser(){
        this.props.logoutUser(false)
    }


    componentDidMount() {
        AppState.addEventListener('change', this._handleAppStateChange.bind(this));
        console.log('Mounted Logout Button')
        if (this.props.login.AppReady === false){
            console.log('SOFT INIT APP !!!!!!')
            this.props.softinitApp(this.props.login.cred.userid, this.props.login.cred.password)
        }
    }

    _handleAppStateChange (currentAppState) {
        if (currentAppState === 'background') {
            // this.props.stopNotifications(this.props.user.id)
        }
        if (currentAppState === 'active') {
            console.log('----APP IS ACTIVE ---')
            console.log('Refreshing Token')
            Auth.signIn(this.props.login.cred.userid, this.props.login.cred.password)
                .then(user => {
                    console.log(user)
                    var updateNewsfeed = this.props.userrole !== 'Attendance'
                    console.log(this.props.userrole)
                    this.props.updateToken(user.signInUserSession.idToken.jwtToken, null, updateNewsfeed)
                })
                .catch(err => {
                        console.log(err)
                        return err
                    }
                )
        }
        // if (Platform.OS === 'ios') {
        //     PushNotificationIOS.setApplicationIconBadgeNumber(0);
        // }
    }

    refreshAttendance(){
        if (Object.keys(this.props._usergroups).length > 0) {
            let groups = []
            for (var key in this.props._usergroups) {
                groups.push(parseInt(key))
            }
            console.log(groups)
            this.props.refreshAttendance(groups,new Date().yyyymmdd(), this.props.token)
        }
    }

    render() {
        return (
                <View style={{flexDirection: 'row', padding : 2}}>
                    <TouchableOpacity onPress={this.refreshAttendance} style={{paddingRight : 5, paddingLeft : 3}}>
                        <MaterialCommunityIcons color={'lightgreen'} size={28} name={'refresh'}/>
                    </TouchableOpacity>
                <TouchableOpacity onPress={this.logoutuser.bind(this)}>
                    <MaterialCommunityIcons color={'white'}  size={24} name={'logout'}/>
                </TouchableOpacity>
        </View>
        );
    }
}

var mapDispatchToProps = function (dispatch) {
    return {
        logoutUser: function (logoutfirebase) {
            dispatch(memberActions.logoutUser(logoutfirebase))
        },
        refreshAttendance: function (groups, attendancedate, token) {
            dispatch(attendanceactions.refreshAttendance(groups, attendancedate, token))
        },
        updateToken: function (token, authobj, groups) {
            dispatch(userActions.updateToken(token, authobj, groups))
        },
        softinitApp : function (userid, password) {
            dispatch(loginActions.softinitApp(userid, password))
        },
    }
}

var mapStateToProps = function (store) {
    return {
        attendance: store.attendance,
        _usergroups: store.user.groups !== null ? Object.keys(store.user.groups).reduce(function (filtered, key) {
            var group = store.user.groups[key]
            if (group.attendancemode !== 0)
                filtered[key] = group
            return filtered
        }, {}) : [],
        attendancerefeshing :store.attendance.attendancerefeshing,
        token: store.user.token,
        login : store.login,
        userrole: store.user.roles
    }
};


export default connect(mapStateToProps, mapDispatchToProps, null, { withRef: true })(LogoutButton);


const styles = {
    root: {
        flexGrow: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#e8e8e8',
    },
    bar: {
        flex: 1,
        flexDirection: 'column',
        backgroundColor: '#e8e8e8',
        justifyContent: 'space-between'
    },
    h1: {
        fontSize: 24,
        textAlign: 'center',
        margin: 30
    },
    footer: {
        fontSize: 10,
        color: '#888',
        marginTop: 10
    }
};
