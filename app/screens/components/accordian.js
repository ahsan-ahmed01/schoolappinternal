import React, {Component} from 'react';
import { View, TouchableOpacity, Text, StyleSheet} from "react-native";
import * as Animatable from 'react-native-animatable';
import Icon from "react-native-vector-icons/MaterialIcons";

export default class Accordian extends Component{

    constructor(props) {
        super(props);
        this.state = { 
          data: props.data,
          expanded : false,
        }
    }
  
  render() {

    return (
       <View style={{flex : 1, borderBottomColor : 'lightgray',
       borderBottomWidth : 0.5}}>
            <TouchableOpacity style={styles.row} onPress={()=>this.toggleExpand()}>
                <View style={{flex : 1}}>
                    {this.props.title}
                </View>
                <Icon name={this.state.expanded ? 'keyboard-arrow-up' : 'keyboard-arrow-down'} size={30} color={'darkgray'} />
                
            </TouchableOpacity>
            <View style={styles.parentHr}/>
            {
                this.state.expanded &&
                <Animatable.View style={styles.child} animation={this.state.expanded ? "flipInX" : "flipOuty"} easing={this.state.expanded ? "ease-in" : "ease-out"}>   
                    {this.props.data}
                </Animatable.View>
            }
       </View>
    )
  }

  toggleExpand=()=>{
    this.setState({expanded : !this.state.expanded})
  }

}

const styles = StyleSheet.create({
    title:{
        fontSize: 14,
        fontWeight:'bold',
        color: 'darkgray',
    },
    row:{
        flex : 1,
        flexDirection: 'row',
        height:64,
        paddingLeft:0,
        paddingRight:0,
        alignItems:'center',
        backgroundColor: 'white',
        
    },
    parentHr:{
        height:1,
        color: 'white',
        width:'100%'
    },
    child:{
        backgroundColor: 'white',
        padding:16,
    }
    
});