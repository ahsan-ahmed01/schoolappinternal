import React, { Component } from "react";
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  TouchableHighlight,
  ImageBackground,
  ScrollView,
  ActivityIndicator,
  FlatList,
} from "react-native";
import GroupSelector from "./groupselector";
import * as memberlistactions from "../../actions/memberlistActions";
import _ from "lodash";
import Icon from "react-native-vector-icons/FontAwesome";

import { connect } from "react-redux";
import CustomBlurView from "../../components/CustomBlurView";
import { CustomCachedImage } from "react-native-img-cache";

import Image from "react-native-image-progress";
import * as Layout from "../../constants/Layout";
class ModalMemberListScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedGroup: null,
      selectedlist: {},
    };
  }

  componentDidMount() {
    //this.props.clearselections()
    console.log(this.props);
    if (this.props.initialSelectedList !== undefined) {
      if (Object.keys(this.props.initialSelectedList).length !== 0) {
        this.setState({ selectedlist: this.props.initialSelectedList });
      }
    }
  }

  onSelectGroup(groupid) {
    console.log("Got Group ID " + groupid);
    this.setState({ selectedGroup: groupid });
    //this.props.getMemberList(groupid, this.props.user.token)
  }

  onRemoveUser(user) {
    console.log("Delete ->" + user.username);
    this.setState({
      selectedlist: _.omit(this.state.selectedlist, user.username),
    });
  }

  RenderSelectedList() {
    if (this.state.selectedlist !== {}) {
      var userids = Object.keys(this.state.selectedlist);
      if (userids.length === 0) {
        var selectedMemberList = (
          <View
            style={{
              flexDirection: "column",
              flex: 1,
              justifyContent: "center",
              width: 350,
            }}
          >
            <Text style={styles.noneselectedText}>
              Please Select a Class and then{" "}
            </Text>
            <Text style={styles.noneselectedText}>
              a member from the list below
            </Text>
          </View>
        );
      } else {
        var selectedMemberList = userids.map(function (userKey, i) {
          var user = this.state.selectedlist[userKey];
          var profilePhotoURI = {
            uri: user.profile_photo.replace("http:", "https:"),
          };

          if (
            user.profile_photo === "" ||
            user.profile_photo === null ||
            profilePhotoURI["uri"].includes("gravatar.com")
          ) {
            var profilePhotoURI = require("../../img/blankprofile.png");
            var ImageComp = (
              <Image
                imageStyle={{
                  width: 45,
                  height: 45,
                  resizeMode: "stretch",
                  borderRadius: 22,
                }}
                style={styles.memberImage}
                source={profilePhotoURI}
              ></Image>
            );
          } else {
            var ImageComp = (
              <CustomCachedImage
                imageStyle={{
                  width: 45,
                  height: 45,
                  borderRadius: 22,
                  resizeMode: "stretch",
                }}
                component={Image}
                defaultSource={require("../../img/blankprofile.png")}
                style={styles.memberImage}
                source={profilePhotoURI}
              />
            );
          }
          console.log(user.display_name);
          return (
            <View key={userKey}>
              <Icon
                style={styles.closebutton}
                name={"minus-circle"}
                onPress={this.onRemoveUser.bind(this, user)}
                size={16}
              />

              {ImageComp}
              <Text style={styles.usernameText}>{user.display_name}</Text>
            </View>
          );
        }, this);
      }

      return selectedMemberList;
    } else {
      return null;
    }
  }

  _keyExtractor = (item, index) => item;

  render() {
    console.log("Rendering .....");
    return (
      <View style={{ flex: 1, marginTop: 20, flexGrow: 1 }}>
        <CustomBlurView blurAmount={1} style={styles.blurcontainer}>
          <View style={styles.container}>
            <View style={styles.closebuttonContainer}>
              <TouchableOpacity
                style={styles.buttonClose}
                onPress={this.onClose.bind(this)}
              >
                <View style={styles.buttonCloseView}>
                  <Text style={styles.DoneText}>Done</Text>
                </View>
              </TouchableOpacity>
            </View>
            <View style={styles.viewcontainer}>
              <View style={styles.selectorcontainer}>
                <ScrollView
                  style={{ flex: 1 }}
                  pagingEnabled={true}
                  horizontal={true}
                  contentContainerStyle={styles.addcontainer}
                >
                  {this.RenderSelectedList()}
                </ScrollView>
                <View style={styles.groupContainer}>
                  <GroupSelector
                    onSelectGroup={(groupid) => this.onSelectGroup(groupid)}
                  />
                </View>
              </View>
              <Text
                style={{
                  backgroundColor: "transparent",
                  padding: 3,
                  fontWeight: "bold",
                }}
              >
                Select a Member
              </Text>

              {this.RenderMemberList()}
            </View>
          </View>
        </CustomBlurView>
      </View>
    );
  }

  RenderMemberList() {
    if (this.state.selectedGroup === null) {
      return (
        <Text style={styles.noUsers}>
          Please select a group above to continue
        </Text>
      );
    } else {
      var members = this.props.login.groupcache[this.state.selectedGroup];
      console.log(members);
      var memberList = (
        <FlatList
          showsHorizontalScrollIndicator={true}
          overScrollMode={"always"}
          extraData={this.state}
          data={members}
          keyExtractor={this._keyExtractor}
          renderItem={(username) => {
            console.log(username);
            if (username.item !== this.props.user.username) {
              var item = this.props.login.usercache[username.item];
              if (item !== undefined) {
                console.log(item);

                if (
                  item === undefined ||
                  item.profile_photo === "" ||
                  item.profile_photo === null ||
                  item.profile_photo === "<DEFAULT>"
                ) {
                  var profilePhotoURI = require("../../img/blankprofile.png");
                  var ImageComp = (
                    <Image
                      imageStyle={{
                        width: 45,
                        height: 45,
                        resizeMode: "stretch",
                        borderRadius: 22,
                      }}
                      style={styles.memberImage}
                      source={profilePhotoURI}
                    ></Image>
                  );
                } else {
                  var profilePhotoURI = {
                    uri: item.profile_photo.replace("http:", "https:"),
                  };

                  var ImageComp = (
                    <CustomCachedImage
                      component={Image}
                      imageStyle={{
                        width: 45,
                        height: 45,
                        borderRadius: 23,
                        resizeMode: "stretch",
                      }}
                      defaultSource={require("../../img/blankprofile.png")}
                      style={styles.memberImage}
                      source={profilePhotoURI}
                    ></CustomCachedImage>
                  );
                }
                if (item.is_admin === 1 || item.is_mod === 1) {
                  var adminStar = this.renderAdminStar();
                } else {
                  var adminStar = null;
                }

                if (this.isAlreadySelected(item.username)) {
                  var memberButtonProps = {
                    style: styles.memberlistSelected,
                  };
                } else {
                  var memberButtonProps = {
                    style: styles.memberlist,
                  };
                }

                var parentof =
                  this.props.login.usercache[item.username].parent_of !==
                    undefined &&
                  this.props.login.usercache[item.username].parent_of[
                    this.state.selectedGroup
                  ] !== undefined
                    ? this.props.login.usercache[item.username].parent_of[
                        this.state.selectedGroup
                      ]
                    : "";
                return (
                  <TouchableOpacity
                    key={item.id}
                    {...memberButtonProps}
                    onPress={this.selectUser.bind(this, item)}
                  >
                    {ImageComp}
                    <View>
                      <Text style={styles.memberText}>{item.display_name}</Text>
                      <Text
                        style={[styles.memberText, styles.memberTextParentof]}
                      >
                        {parentof}
                      </Text>
                    </View>
                    {adminStar}
                  </TouchableOpacity>
                );
              }
            }
          }}
        />
      );

      return memberList;
    }
  }

  selectUser(user) {
    console.log(user);
    if (this.isAlreadySelected(user.usernameusername)) {
      this.setState({
        selectedlist: delete this.state.selectedlist[user.username],
      });
      console.log(user.username);
    } else {
      var obj = this.state.selectedlist;
      obj[user.username] = user;
      console.log(obj);
      console.log(this.state.selectedlist);
      this.setState({ selectedlist: obj });
    }
  }

  renderAdminStar() {
    console.log("_____ ADMIN ______");
    return (
      <View style={styles.adminstarcontainer}>
        <Icon name="star" style={styles.adminstar} size={20} color="#900" />
      </View>
    );
  }

  onClose(event) {
    //this.props.navigator.dismissModal()
    console.log(this.props);
    console.log(this.state.selectedlist);
    this.props.onSelectedList(this.state.selectedlist);
    this.props.modaldismissfunc();
  }

  isAlreadySelected(userid) {
    var selectedids = Object.keys(this.state.selectedlist);
    console.log(selectedids);
    console.log(userid);
    return selectedids.indexOf(userid) >= 0;
  }
}

var styles = StyleSheet.create({
  blurcontainer: {
    flex: 1,
    borderTopWidth: 1,
    borderRadius: 25,
    backgroundColor: "white",
    flexGrow: 1,
    paddingTop: 4,
  },
  container: {
    flex: 1,
    flexDirection: "column",
    justifyContent: "center",
    padding: 4,
    position: "absolute",
    backgroundColor: "white",
    top: 0,
    left: 0,
    bottom: 0,
    right: 0,
  },

  buttonClose: {
    flexDirection: "row-reverse",
    paddingLeft: 15,
    paddingTop: 10,
    paddingBottom: 2,
  },
  actionContainer: {
    alignItems: "flex-end",
    marginTop: 20,
    flexDirection: "row-reverse",
  },
  addcontainer: {
    flexDirection: "row",
    height: 90,
  },
  viewcontainer: {
    flex: 1,
    flexDirection: "column",
    flexGrow: 1,
  },
  usernameText: {
    color: "black",
    backgroundColor: "transparent",
    margin: 3,
    fontSize: 10,
    textAlign: "center",
    borderRadius: 10,
    width: 45,
  },
  memberImage: {
    width: 45,
    height: 45,
    borderRadius: 22,
    borderWidth: 1,
    borderColor: "gray",
    flexDirection: "row",
    marginRight: 2,
    marginLeft: 2,
  },
  groupContainer: {
    height: 100,
    borderTopWidth: 1,
    borderBottomWidth: 1,
    borderColor: "black",
  },
  memberContainer: {
    backgroundColor: "red",
    height: 10000,
  },
  selectedlisttouch: {
    backgroundColor: "#189ad3",
    padding: 5,
    height: 25,
    margin: 3,
    borderRadius: 4,
  },
  selectedlisttext: {
    color: "darkgray",
  },
  selectorcontainer: {
    height: 190,
  },
  noUsers: {
    fontSize: 20,
    alignItems: "flex-start",
    color: "gray",
    padding: 20,
    textAlign: "center",
    backgroundColor: "transparent",
  },
  adminstarcontainer: {
    flex: 1,
    alignItems: "flex-end",
    justifyContent: "center",
    paddingRight: 10,
  },

  memberText: {
    color: "black",
    textAlignVertical: "center",
    fontSize: 18,
    paddingLeft: 5,
    paddingTop: 3,
  },
  memberlist: {
    flexDirection: "row",
    paddingTop: 5,
    paddingBottom: 5,
    margin: 5,
    //backgroundColor: '#f1f1f1',
    height: 60,
    alignItems: "stretch",
    borderColor: "white",
    borderBottomWidth: 0.5,
    backgroundColor: "transparent",
    //elevation: 5,
    // shadowOffset: {height: 2, width: 4},
    // shadowColor: 'black',
    // shadowOpacity: 0.3,
    // shadowRadius: 3,
  },
  memberlistSelected: {
    flexDirection: "row",
    paddingTop: 5,
    paddingBottom: 5,
    margin: 5,
    backgroundColor: "#90C3D4",
    height: 60,
    alignItems: "stretch",
    elevation: 5,
    borderRadius: 5,
  },
  memberTextParentof: {
    fontSize: 10,
  },
  closebutton: {
    color: "red",
    zIndex: 10,
    marginLeft: 35,
    marginBottom: -15,
  },
  noneselectedText: {
    fontSize: 16,
    flexWrap: "wrap",
    backgroundColor: "transparent",
    color: "#000",
    opacity: 0.5,
    textAlign: "left",
  },
  DoneText: {
    textAlign: "center",
    color: "white",
  },
  buttonCloseView: {
    backgroundColor: "#1A9FFF",
    color: "white",
    borderRadius: 10,
    borderWidth: 0,
    height: 20,
    width: 50,
    textAlign: "center",
    marginLeft: 10,
    marginRight: 10,
  },
});

var mapStateToProps = function (store) {
  return { login: store.login, user: store.user, memberlist: store.memberlist };
};

var mapDispatchToProps = function (dispatch) {
  return {};
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ModalMemberListScreen);
