const React = require('react');
const { Component } = require('react');
const { View, Text, Platform, TouchableHighlight, FlatList } = require('react-native');
import { connect } from 'react-redux'
const { Navigation } = require('react-native-navigation');
import * as miscutils from '../../utils/misc'

import {Card} from 'react-native-elements'

class BriefCaseNewsList extends Component {
    constructor(props) {
        super(props)

    }
    _keyExtractor = (item, index) => item

    render() {
        console.log(this.props)
        if (this.props.feeddata === undefined){
            return(
                <Card title=" No Posts Found">
                    <Text style={{ marginBottom: 10 }}>
                        There are no Posts at the moment !
                    </Text>
                </Card>
            )
        }
        var feeds = miscutils.getNewListinOrder(this.props.feeddata)
        return (
           <Text>News</Text>
        );
    }
}

export default connect(null, null, null, { withRef: true })(BriefCaseNewsList);


const styles = {
    root: {
        flex: 1,
        backgroundColor: 'lightgray'
    },
    container: {
        flex: 1,
        padding: 10,
        backgroundColor: 'lightgray'
      },
};
