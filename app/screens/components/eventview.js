"use strict";
import {
  Text,
  StyleSheet,
  ScrollView,
  Image,
  RefreshControl,
  ListView,
  Platform,
  View,
  TouchableOpacity,
  ActivityIndicator,
  Switch,
} from "react-native";
import React, { Component } from "react";
import { connect } from "react-redux";
import { commonStyle } from "../../config/commonstyles";
import EventMonthView from "./eventmonthview";
import * as eventviewaction from "../../actions/eventActions";
import Spinner from "react-native-loading-spinner-overlay";
import FontAwesome from "react-native-vector-icons/FontAwesome";
import MaterialCommunityIcons from "react-native-vector-icons/MaterialCommunityIcons";
import * as Animatable from "react-native-animatable";
var EnvConfigs = require("../../config/environment");
import timer from "react-native-timer";
import EventItem from "./eventitem";
import { SwipeListView } from "react-native-swipe-list-view";
class EventView extends Component {
  constructor(props) {
    super(props);
    var currdate = new Date();
    this.state = {
      currentMonth: new Date(currdate.getFullYear(), currdate.getMonth(), 1),
      dataSource: new ListView.DataSource({
        rowHasChanged: (row1, row2) => row1 !== row2,
      }),
      renderHeader: false,
      showAttentionEvents: false,
    };
    this.showSpinner = this.showSpinner.bind(this);
  }
  addMonthtoView() {
    var newcount = this._data.length;
    this.setState({
      dataSource: this.getDataSource(newcount),
    });
  }
  shouldComponentUpdate(nextProps, nextState) {
    if (nextProps.events.isLoading || !nextProps.AppReady) {
      console.log("AppReady->" + nextProps.AppReady);
      console.log("IsLoading->" + nextProps.events.isLoading);
      return false;
    }
    if (this.state.showAttentionEvents != nextState.showAttentionEvents) {
      return true;
    }
    // Fix for White Screen in Events
    if (
      nextProps.rootNavprops.currentTab === "nav.Three60Memos.HomeScreen" &&
      this.props.rootNavprops.currentTab === "nav.Three60Memos.HomeScreen"
    ) {
      console.log("Force Render");

      return true;
    }

    if (
      nextProps.rootNavprops.currentTab !== "nav.Three60Memos.EventsScreen" &&
      nextProps.rootNavprops.currentTab !== "nav.Three60Memos.AddEventScreen"
    ) {
      console.log("No Need to Render Events");
      return false;
    }
    if (this.props.events.todayYlocation !== nextProps.events.todayYlocation) {
      this.scrollListviewto(nextProps.events.todayYlocation);
      console.log("scrolling");
      return true;
    }
    // if (
    //   this.props.events.todayYlocation > 0 &&
    //   this.props.events.todayYlocation === nextProps.events.todayYlocation
    // ) {
    //   this.scrollListviewto(nextProps.events.todayYlocation);
    //   console.log("scrolling");
    //   return true;
    // }
    console.log("Refeshing ....");
    return true;
  }

  addPrevMonthtoView() {
    var newcount = -1 * (this._prevdata.length + 1);
    console.log(newcount);
    this.setState({
      dataSource: this.getDataSource(newcount),
    });
  }

  componentDidMount() {
    this._data = [];
    var data = [0, 1, 2];
    this._prevdata = [];
    this.setState({
      dataSource: this.getDataSource(data),
      renderHeader: false,
    });
  }

  getDataSource(nums) {
    console.log(nums);

    if (nums < 0) {
      this._prevdata.unshift(nums);
    } else {
      this._data = this._data.concat(nums);
    }

    console.log(this._prevdata);
    var data = this._prevdata.concat(this._data);
    return this.state.dataSource.cloneWithRows(data);
  }

  loadPrevious(event) {
    console.log(event);
    this.addPrevMonthtoView();
    this.setState({
      renderHeader: false,
    });
  }

  RefreshEvents(clearEvents = false) {
    console.log("Refreshing Functions (clearEvents = " + clearEvents + ")");
    if (clearEvents) {
      this.props.clearEventsAction();
    }
    // var curmonth = new Date().getMonth()
    // var curYear = new Date().getFullYear()
    //   if ((curmonth + this._data.length) > 12) {
    //     var x = Math.floor((curmonth + this._data.length) / 12)
    //     var endYear = curYear + x
    //   }
    //   else{
    //     var  endYear = curYear
    //   }
    // console.log(this._data.length)
    // var endmonth =   curmonth + this._data.length > 12 ? curmonth + this._data.length -11 : curmonth + this._data.length
    console.log(
      this.props.usertoken,
      this.props.usergroups,
      "-----getEventsAction---"
    );
    this.props.getEventsAction(this.props.usertoken, this.props.usergroups);
    this.setState({
      renderHeader: false,
    });
  }

  renderNotificationsBar() {
    if (this.props.events.eventnotifications > 0) {
      return (
        <TouchableOpacity
          onPress={() => {
            this.setState({
              showAttentionEvents: !this.state.showAttentionEvents,
            });
          }}
        >
          <View
            style={{
              flexDirection: "row",
              backgroundColor: "#FF4545",
              height: 40,
              marginTop: -5,
              marginLeft: -5,
              marginRight: -5,
            }}
          >
            <Animatable.View
              animation="flash"
              iterationCount={10}
              useNativeDriver
            >
              <FontAwesome
                name="bell-o"
                size={20}
                style={styles.notificionIcon}
              ></FontAwesome>
            </Animatable.View>
            <Text style={styles.notificionIcon}>
              You have {this.props.events.eventnotifications} Events that
              require your attention
            </Text>
            <MaterialCommunityIcons
              name={
                this.state.showAttentionEvents
                  ? "toggle-switch"
                  : "toggle-switch-off"
              }
              size={32}
              style={[styles.notificionIcon, { marginTop: -6 }]}
            />
          </View>
        </TouchableOpacity>
      );
    } else {
      return null;
    }
  }

  renderHeader() {
    if (this.state.renderHeader) {
      return (
        <View style={styles.actioncontainer}>
          <TouchableOpacity onPress={this.RefreshEvents.bind(this)}>
            <Text style={styles.actionText}>Refresh</Text>
          </TouchableOpacity>

          <TouchableOpacity onPress={this.loadPrevious.bind(this)}>
            <Text style={styles.actionText}>Load Previous</Text>
          </TouchableOpacity>
        </View>
      );
    } else {
      return null;
    }
  }

  showSpinner() {
    if (Platform.OS == "android") {
      return false;
    } else {
      return this.props.events.isLoading;
    }
  }

  render() {
    console.log("RENDER->EVENTVIEW");
    console.log(this.props, "this.props");
    var renderMainBody = () => {
      if (
        this.state.showAttentionEvents &&
        this.props.events.eventnotifications > 0
      ) {
        console.log(this.props.events.events);
        var events = {};
        if (Object.keys(this.props.events).length) {
          Object.keys(this.props.events.events).forEach(function (eventid) {
            var event = this.props.events.events[eventid];
            if (event.pendingflag == 1) {
              events[event.event_id] = event;
            }
          }, this);
        }
        console.log(events);
        var renderEvents = () => {
          return Object.keys(events).map((eventid) => {
            var event = events[eventid];
            console.log(event);
            return (
              <View style={{ height: 80 }}>
                <EventItem
                  navigation={this.props.navigation}
                  isToday={false}
                  key={eventid}
                  eventday={new Date(event.event_start_date)}
                  event={event}
                  eventid={eventid}
                />
              </View>
            );
          });
        };
        return (
          <ScrollView style={{ flex: 1, backgroundColor: "white" }}>
            {renderEvents()}
          </ScrollView>
        );
      } else {
        return (
          <View style={styles.container}>
            {this.renderHeader()}
            <Spinner
              visible={this.showSpinner()}
              textContent={"Loading..."}
              textStyle={{ color: "#FFF" }}
            />
            <SwipeListView
              dataSource={this.state.dataSource}
              closeOnScroll
              closeOnRowPress
              closeOnRowOpen
              keyExtractor={(item, index) => {
                console.log(item, "item123");
              }}
              renderListView={() => (
                <ListView
                  keyExtractor={(item, index) => {
                    console.log(item, "item");
                  }}
                  style={styles.container}
                  ref={(view) => (this._listview = view)}
                  onScroll={this.handleScroll.bind(this)}
                  dataSource={this.state.dataSource}
                  renderRow={(rowData) => this.RenderNextMonth(rowData)}
                  removeClippedSubviews={false}
                  onEndReached={this.onEndReached.bind(this)}
                  pageSize={3}
                  onEndReachedThreshold={1}
                  onLayout={this.onListviewLayout.bind(this)}
                />
              )}
            />
          </View>
        );
      }
    };

    console.log(this.props);
    console.log("RENDER->EVENTVIEW");
    if (this.props.usertoken !== "") {
      return (
        <View style={{ flex: 1 }}>
          {this.renderNotificationsBar()}
          {renderMainBody()}
        </View>
      );
    } else {
      return null;
    }
  }

  onListviewLayout(event) {
    console.log("on layout");
    console.log(event);
  }

  handleScroll(event) {
    if (event.nativeEvent.contentOffset.y < -100) {
      this.setState({
        renderHeader: true,
      });
    }
  }

  componentDidUpdate(prevProps, prevState) {
    console.log(this.props.rootNavprops.currentTab);
    if (
      this.props.rootNavprops.currentTab === "nav.Three60Memos.EventsScreen"
    ) {
      // If tab is switched immediately refresh
      if (
        this.props.rootNavprops.currentTab !== prevProps.rootNavprops.currentTab
      ) {
        var curtime = new Date();
        if (curtime - this.props.events.lastrefreshtime >= 60000) {
          this.RefreshEvents(false);
        } else {
          console.log("SKIPPING EVENT REFRESH !!!");
        }
      }
      // if (!timer.intervalExists('EventRefresh')){
      //     timer.setInterval('EventRefresh', ()=> this.RefreshEvents(),
      //         EnvConfigs.EVENTS_INTERVAL);
      // }
    }
  }

  onEndReached() {
    this.addMonthtoView();
  }

  RenderNextMonth(i) {
    var newDate = new Date(this.state.currentMonth);
    newDate.setMonth(newDate.getMonth() + i);
    console.log(newDate);
    return (
      <EventMonthView
        navigation={this.props.navigation}
        key={newDate}
        month={newDate.getMonth() + 1}
        year={newDate.getFullYear()}
      />
    );
  }

  scrollListviewto(ylocation) {
    if (this._listview !== undefined) {
      console.log("Scccorrroolllingggg");
      this._listview.scrollTo({ x: 0, y: ylocation, animated: true });
    }
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "white",
  },
  actioncontainer: {
    flexDirection: "row",
    justifyContent: "space-between",
    paddingLeft: 20,
    paddingRight: 20,
  },
  actionText: {
    fontSize: 14,
    backgroundColor: "#3fb0ac",
    color: "white",
    padding: 5,
    width: 120,
    textAlign: "center",
    marginTop: 2,
    marginBottom: 2,
    borderRadius: 20,
  },
  swipeheadercontainer: {
    flex: 1,
  },
  swipewrapper: {
    height: 100,
    backgroundColor: "green",
  },
  eventContainer: {
    flex: 5,
  },
  monthText: {
    color: "white",
    alignSelf: "center",
    paddingTop: 70,
    fontSize: 24,
  },
  notificionIcon: {
    paddingTop: 10,
    paddingLeft: 10,
    color: "white",
  },
  notificionIconText: {
    paddingTop: 10,
    paddingLeft: 10,
    color: "white",
  },
});

var mapStateToProps = function (store) {
  return {
    usertoken: store.user.token,
    usergroups: store.user.groups,
    events: store.events,
    AppReady: store.login.AppReady,
    rootNavprops: store.rootNavprops,
  };
};

var mapDispatchToProps = function (dispatch) {
  return {
    showaheadMonths: function (numMonths) {
      dispatch(eventviewaction.showaheadMonths(numMonths));
    },
    clearEventsAction: function () {
      dispatch(eventviewaction.clearEventsAction());
    },
    getEventsAction: function (
      startMonth,
      endMonth,
      startYear,
      endYear,
      token,
      groups
    ) {
      dispatch(
        eventviewaction.getEventsAction(
          startMonth,
          endMonth,
          startYear,
          endYear,
          token,
          groups
        )
      );
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(EventView);
