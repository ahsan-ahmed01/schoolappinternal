"use strict";
import {
  Text,
  StyleSheet,
  Image,
  RefreshControl,
  ListView,
  View,
  TouchableOpacity,
  ActivityIndicator,
  ImageBackground,
} from "react-native";
import React, { Component } from "react";
import { connect } from "react-redux";
import { commonStyle } from "../../config/commonstyles";
import * as miscutils from "../../utils/misc";
import * as eventactions from "../../actions/eventActions";
// import shallowCompare from 'react-addons-shallow-compare'
import EventItem from "./eventitem";

const CalendarImages = {
  "0": require("../../img/calendar/0.jpg"),
  "1": require("../../img/calendar/1.jpg"),
  "2": require("../../img/calendar/2.jpg"),
  "3": require("../../img/calendar/3.jpg"),
  "4": require("../../img/calendar/4.jpg"),
  "5": require("../../img/calendar/5.jpg"),
  "6": require("../../img/calendar/6.jpg"),
  "7": require("../../img/calendar/7.jpg"),
  "8": require("../../img/calendar/8.jpg"),
  "9": require("../../img/calendar/9.jpg"),
  "10": require("../../img/calendar/10.jpg"),
  "11": require("../../img/calendar/11.jpg"),
};

class EventMonthView extends Component {
  componentDidMount() {
    console.log(
      "Getting Data for Month #" +
        this.props.month +
        " " +
        miscutils.getMonthName(this.props.month)
    );
    if (this.props.month + 1 >= 11) {
      var nextYear = this.props.year + 1;
    } else {
      var nextYear = this.props.year;
    }
    var endmonth =
      this.props.month + 1 > 12
        ? this.props.month + 1 - 11
        : this.props.month + 1;

    //this.props.getEventsAction(this.props.month,endmonth, this.props.year, nextYear, this.props.user.token, this.props.user.groups)

    setTimeout(this.getTodayLocation.bind(this), 1000);
  }

  getTodayLocation() {
    if (this.refs.currentday !== undefined) {
      this.refs.currentday.measure((fx, fy, width, height, px, py) => {
        console.log(fx, fy, width, height, px, py);
        this.props.setLocationforToday(py - 100);
      });
    }
  }

  RenderWeeks() {
    var weeks = this.getStartofWeek();
    var renderdate = weeks.map(function (startdate) {
      var enddate = new Date(startdate.getTime());
      enddate.setDate(enddate.getDate() + 6);
      var dateText =
        miscutils.getMonthName(startdate.getMonth() + 1) +
        " " +
        startdate.getDate() +
        " - " +
        miscutils.getMonthName(enddate.getMonth() + 1) +
        " " +
        enddate.getDate();
      return (
        <View key={startdate} style={styles.weekscontainer}>
          <Text style={styles.weekstext}>{dateText.toUpperCase()}</Text>
          {this.renderEventItems(startdate, enddate)}
        </View>
      );
    }, this);
    return renderdate;
  }

  componentDidUpdate(nextProps, nextState) {
    if (
      Object.keys(this.props.events.events).length !== 0 &&
      this.props.events.isLoading !== nextProps.events.isLoading
    ) {
      if (this.props.events.isLoading === false) {
        this.getTodayLocation();
      }
    }
    return true;
  }

  renderEventItems(startdate, enddate) {
    console.log("renderEventItems");
    console.log(startdate);
    var retval = [];
    for (var i = 0; i <= 6; i++) {
      var dt = new Date(startdate);
      dt.setDate(startdate.getDate() + i);
      var datekey = miscutils.convertDatetoYYYYMMDD(dt, "-");
      var currentdate = new Date();
      currentdate.setHours(0, 0, 0, 0);
      var isToday = false;
      if (currentdate.getTime() === dt.getTime()) {
        retval.push(
          <View key={currentdate.getTime()} ref="currentday">
            {/* <Text style={styles.todayText}>TODAY</Text> */}
            {/* <Image
              style={styles.memberImage}
              source={require("../../img/line.png")}
            ></Image> */}
          </View>
        );
        isToday = true;
      }
      if (this.props.events.hashmap[datekey] !== undefined) {
        for (var eventid in this.props.events.hashmap[datekey]) {
          retval.push(
            <EventItem
              navigation={this.props.navigation}
              isToday={isToday}
              key={eventid + datekey}
              eventday={dt}
              event={
                this.props.events.events[
                  this.props.events.hashmap[datekey][eventid]
                ]
              }
              eventid={this.props.events.hashmap[datekey][eventid]}
            />
          );
        }
      }
    }
    if (retval.length === 0) {
      retval.push(
        <Text key={datekey} style={styles.noeventtext}>
          No Events
        </Text>
      );
    }
    return retval;
  }

  renderMonthHeader() {
    // if ((this.props.events.calendarconfig !== null) && (this.props.events.calendarconfig[this.props.month - 1] !== '')) {
    //
    var photourl = CalendarImages[this.props.month - 1];
    //var photourl = {uri: this.props.events.calendarconfig[this.props.month - 1].replace('http:', 'https:')}
    return (
      <View style={styles.swipewrapperimage} height={130}>
        <ImageBackground
          style={styles.calendarHeaderImage}
          source={photourl}
          resizeMethod={"auto"}
          resizeMode={"cover"}
        >
          {/*<Text style={styles.monthText}>{miscutils.getMonthName(this.props.month)} {this.props.year}</Text>*/}
        </ImageBackground>
      </View>
    );
    // } else {
    //   return (<View style={styles.swipewrapper} height={130}>
    //               <View style={styles.swipeheadercontainer}>
    //                 <Text style={styles.monthText}>{miscutils.getMonthName(this.props.month)} {this.props.year}</Text>
    //             </View>
    //         </View>)
    // }
  }

  render() {
    console.log("RENDER->EVENTMONTHVIEW");
    return (
      <View style={styles.container}>
        {this.renderMonthHeader()}
        <View style={styles.eventContainer}>{this.RenderWeeks()}</View>
      </View>
    );
  }

  getStartofWeek() {
    var d = new Date(this.props.year, this.props.month - 1, 1);
    var month = d.getMonth();
    var sundays = [];

    // Get the first Monday in the month
    while (d.getDay() !== 0) {
      d.setDate(d.getDate() + 1);
    }
    // Get all the other Mondays in the month
    while (d.getMonth() === month) {
      sundays.push(new Date(d.getTime()));
      d.setDate(d.getDate() + 7);
    }
    return sundays;
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "white",
    flexDirection: "column",
    justifyContent: "space-between",
  },
  swipeheadercontainer: {
    flex: 1,
  },
  swipewrapper: {
    height: 100,
    backgroundColor: "black",
  },
  swipewrapperimage: {
    backgroundColor: "transparent",
  },
  eventContainer: {
    flex: 1,
  },
  monthText: {
    color: "white",
    alignSelf: "center",
    paddingTop: 70,
    fontSize: 24,
  },
  weekstext: {
    // paddingLeft: 50,
    color: "#000",
    opacity: 0.5,
  },
  weekscontainer: {
    paddingBottom: 5,
  },
  noeventtext: {
    paddingTop: 10,
    paddingBottom: 10,
    paddingLeft: 20,
    fontStyle: "italic",
    color: "gray",
  },
  todayText: {
    fontSize: 12,
    fontStyle: "italic",
    textAlign: "right",
    color: "blue",
  },
  calendarHeaderImage: {
    height: 130,
  },
});

var mapStateToProps = function (store) {
  return { user: store.user, events: store.events };
};

var mapDispatchToProps = function (dispatch) {
  return {
    getEventsAction: function (token, groups) {
      dispatch(eventactions.getEventsAction(token, groups));
    },
    setLocationforToday: function (ylocation) {
      dispatch(eventactions.setLocationforToday(ylocation));
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(EventMonthView);
