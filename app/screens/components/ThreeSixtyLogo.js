import React, {Component} from 'react'
import {
    Image, StyleSheet, Dimensions, TouchableOpacity, TouchableHighlight
} from 'react-native'
import {Navigation} from 'react-native-navigation'
import Ionic from 'react-native-vector-icons/Ionicons';
import { Hub } from 'aws-amplify';
var closeIcon = null;
var emailIcon = null
export default class ThreeSixtyLogo extends Component {
    constructor (props) {
        super(props)
        this.clicklogo = this.clicklogo.bind(this)
        this.clickcount = 0
        Hub.listen('stats', this, 'MyListener');
        Ionic.getImageSource('md-close', 30).then((source) => {
            closeIcon = source
        });
        Ionic.getImageSource('md-mail', 30).then((source) => {
            emailIcon = source
        });
    }
    render () {
        return (
            <TouchableHighlight onPress={this.clicklogo}>
            <Image resizeMode="contain"
                   style={{width: 40, height: 14}}
                   source={require('../../img/360.png')}></Image>
            </TouchableHighlight>
        )
    }

    onHubCapsule(capsule) {
        const { channel, payload } = capsule;
        if (channel === 'stats') {
            console.log(payload)
            this.statdata.push(payload.data)
        }
    }

    componentDidMount(){
        this.clickcount = 0
        this.statdata = []
    }

    clicklogo(){
        this.clickcount = this.clickcount + 1
        console.log(this.clickcount)
        if (this.clickcount === 5) {
            this.clickcount = 0
            console.log('Click Logo')
            Navigation.showModal({
                stack: {
                    children: [{
                        component: {
                            name: 'nav.Three60Memos.StatScreen',
                            passProps: {
                                statdata : this.statdata
                            },
                            options : {
                                topBar: {
                                    visible : true,
                                    title: {
                                        text: "Stats [Debug]",
                                        color : "Black"
                                    },
                                    rightButtons:
                                    (Platform.OS == 'android') ? {} :[
                                        {
                                            id : 'closebutton',
                                            icon : closeIcon,
                                            color : 'black'
                                        }
                                    ]
                                    ,
                                    leftButtons:[
                                        {
                                            id : 'emailbutton',
                                            icon : emailIcon,
                                            color : 'black'
                                        }
                                    ]

                                }
                            }
                        }
                    }]
                }
            });
        }
    }

}

var styles = StyleSheet.create({
    container: {
        flex: 1,
        width: Dimensions.get('window').width,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#d6e7ad'
    },
    welcome: {
        fontSize: 20,
        textAlign: 'center',
        margin: 10,
        paddingTop: 20
    },
    button: {
        textAlign: 'center',
        fontSize: 18,
        marginBottom: 10,
        marginTop: 10,
        color: '#4692ad'
    }
})
