'use strict'
import {
    Text,
    StyleSheet,
    RefreshControl, ListView,
    View, TouchableOpacity, Dimensions, Alert
} from 'react-native'
import React, { PureComponent } from 'react'
import { ButtonGroup, Button, } from 'react-native-elements'

class NumberSpinner extends React.PureComponent {

    constructor(props) {
        super(props)
        this.state = {
            value: props.defaultValue
        }
        this.clickButton = this.clickButton.bind(this)
    }

    clickButton(index) {
        console.log(index)
        console.log(this.props)
        var val = this.state.value
        var func = null
        if (index == 0) {
            val = val + 1
            func = this.props.onIncrease
            console.log()
        } else {
            val = val - 1
            func = this.props.onDecrease
        }
        if ((val >= this.props.minimumValue) && (val <= this.props.maximumValue)) {
            this.setState({
                value: val
            })
            func()
            console.log('new Val :' + val)
        }
    }

    render() {
        console.log(this.props)
        if (this.props.align == 'horizontal') {
            return (
                <View style={[styles.container, { marginLeft: 10 }]}>
                    <View style={{ flexDirection: 'row', justifyContent: 'center', paddingTop: 12 }}>
                        <Text style={styles.textStyle}> {this.props.label}</Text>
                    </View>
                    <View style={{ flexDirection: "row" }}>
                        <Button
                            buttonStyle={{
                                width: 100,
                                backgroundColor: "#F5F5F5",
                                borderRadius: 10
                            }}
                            title="+"
                            onPress={this.clickButton}
                        />
                        <Text style={[styles.textStyle, { color: '#002e63' }]}>{this.state.value}</Text>
                        <Button
                            buttonStyle={{
                                width: 50,
                                backgroundColor: "#F5F5F5",
                                borderRadius: 10
                            }}
                            title="-"
                            onPress={this.clickButton}
                        />
                    </View>
                </View>
            )
        }
        // <ButtonGroup
        //     containerStyle={{ height: 35, width: 75 }}
        //     onPress={this.clickButton}
        //     buttons={['+', '-']}>
        // </ButtonGroup>
        else {
            return (
                <View style={[styles.container, { marginLeft: 10 }]}>
                    <View style={{ flexDirection: 'row', justifyContent: 'center' }}>
                        <Text style={styles.textStyle}> {this.props.label}</Text>
                    </View>
                    <View style={{
                        flexDirection: "row", alignItems: "center", justifyContent: "center",
                        backgroundColor: "white",
                        borderRadius: 10,
                        elevation: 5,
                        borderWidth: 1,
                        borderColor: "#ededed",
                        marginBottom: 20
                    }}>
                        <Button
                            buttonStyle={{
                                width: 50,
                                backgroundColor: "#F5F5F5",
                                borderRadius: 10
                            }}
                            titleStyle={{ color: "grey" }}
                            title="+"
                            onPress={()=>this.clickButton(0)}
                        />
                        <Text style={[styles.textStyle, { color: '#002e63', width: 50 }]}>{this.state.value}</Text>
                        <Button
                            buttonStyle={{
                                width: 50,
                                backgroundColor: "#F5F5F5",
                                borderRadius: 10
                            }}
                            titleStyle={{ color: "grey" }}
                            title="-"
                            onPress={()=>this.clickButton(1)}
                        />
                    </View>
                </View>
            )
        }
    }
    // <ButtonGroup
    //     containerStyle={{ height: 45, width: 100 }}
    //     onPress={this.clickButton}
    //     buttons={['+', '-']}>

    // </ButtonGroup>

}

var styles = StyleSheet.create({
    container: {
        justifyContent: 'center',
        // paddingTop: 5
    },
    textStyle: {
        textAlign: 'center',
        fontSize: 16,
        color: "#000"
    }
})

export default NumberSpinner