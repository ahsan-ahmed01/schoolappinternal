import React, { Component } from "react";
import { TouchableOpacity, View, Text } from "react-native";
import Icon from "react-native-vector-icons/Entypo";

import Colors from "../constants/Colors";
import { connect } from "react-redux";

export class ActionScreen extends Component {
  static navigationOptions = ({ navigation }) => ({
    title: ``
  });

  constructor(props) {
    super(props);
  }

  shouldComponentUpdate(nextProps, nextState) {
    if (
      this.props.user.totalnotifications !== nextProps.user.totalnotifications
    ) {
      return true;
    } else {
      return false;
    }
  }

  render() {
    return (
      <View>
        <Text> Please select an action to Perform</Text>
      </View>
    );
  }
}

var mapStateToProps = function(store) {
  return { user: store.user };
};

export default connect(mapStateToProps)(ActionScreen);
