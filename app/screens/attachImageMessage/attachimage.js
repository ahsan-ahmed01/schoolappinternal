'use strict'
import {
  View,
  ScrollView,
  StyleSheet,
  CameraRoll,
  Image,
  TouchableOpacity
} from 'react-native'
import React, {Component} from 'react'
import {connect} from 'react-redux'

/* Actions */
import { finishPickPhoto } from '../../actions/chatActions'

/* Utils */
import CameraRollView from './CameraRollView'
import AssetScaledImageExampleView from './AssetScaledImageExample'

class AttachImageToMessageScreen extends Component {

  constructor (props) {
    super(props)
    this.state = {
      groupTypes: 'SavedPhotos',
      sliderValue: 1,
      bigImages: true
    }
  };

  render () {
    return (
      <View style={ styles.container }>
        <CameraRollView
          batchSize={20}
          groupTypes={this.state.groupTypes}
          renderImage={this.renderImage.bind(this)}
        />
      </View>
    )
  };

  renderImage (asset) {
    const location = asset.node.location ? JSON.stringify(asset.node.location) : 'Unknown location'
    return (
      <TouchableOpacity key={asset} onPress={ () => this.loadAsset(asset) }>
        <View style={styles.row}>
          <Image
            source={asset.node.image}
            style={styles.image}
          />
        </View>
      </TouchableOpacity>
    )
  };

  loadAsset (asset) {
    if (asset.node.image.uri !== undefined){
        this.props.finishPickPhoto(asset.node.image.uri.toString(), this.props.roomkey)
        this.props.navigator.dismissModal()
    }

  };
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    backgroundColor: 'black'
  },
  row: {
    flexDirection: 'row',
    flex: 1
  },
  url: {
    fontSize: 9,
    marginBottom: 14
  },
  image: {
    width: 250,
    height: 250,
    margin: 4
  },
  info: {
    flex: 1
  }
})

var mapStateToProps = function (store) {
  return {
  }
}

var mapDispatchToProps = function (dispatch) {
  return {
    finishPickPhoto: function (imagePath, roomkey) { dispatch(finishPickPhoto(imagePath, roomkey)) }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(AttachImageToMessageScreen)
