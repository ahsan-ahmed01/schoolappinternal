'use strict'
import {
  Text,
  StyleSheet,
  ScrollView,
  Image, RefreshControl, ListView,
  View, TouchableOpacity, ActivityIndicator
} from 'react-native'
import React, {Component} from 'react'
import {connect} from 'react-redux'
import * as memberActions from '../actions/memberprofileActions'
import * as rootNavActions from '../actions/rootNavActions'
import Ionic from 'react-native-vector-icons/Ionicons';
import {Navigation} from 'react-native-navigation'
import * as miscutils from '../utils/misc'
var closeIcon = null;

class LikedUsersScreen extends Component {
    navigationButtonPressed({ buttonId }) {
        console.log(buttonId)
        if (buttonId === 'closecommentbutton'){
            Navigation.dismissAllModals()
        }
    }
    constructor(props){
      super(props)
        Navigation.events().bindComponent(this);
        Ionic.getImageSource('md-close', 30).then((source) => {
            closeIcon = source
        });
    }

    render () {
    console.log(this.props)
    console.log(this.props.selectedactivity.likes.users)

      var MemberList = this.props.selectedactivity.likes.users.map(function (key) {
      var likedUser = miscutils.getUserfromCache(this.props.login.usercache, key)
      if (likedUser.profile_photo == '' || likedUser.profile_photo == '<DEFAULT>') {
        var profilePhotoURI = require('../img/blankprofile.png')
      } else {
        var profilePhotoURI = {uri: likedUser.profile_photo.replace('http:', 'https:')}
      }
      var group_id = this.props.selectedactivity.group_id
      console.log(this.props.selectedactivity.group_id)
      console.log(likedUser)
      var parent_of = group_id in likedUser.parent_of ?
          likedUser.parent_of[this.props.selectedactivity.group_id] : ""

        console.log(likedUser.parent_of)
        console.log(this.props.selectedactivity)
      return (
              <TouchableOpacity key={key} style={styles.memberlist} onPress={() => this.onClickMember(key)}>
                  <Image style={styles.memberImage} source={profilePhotoURI}></Image>
                  <View>
                  <Text style={styles.memberText}>{likedUser.display_name}</Text>
                    <Text style={[styles.memberText, styles.memberTextParentof]}>{parent_of}</Text>
                  </View>
              </TouchableOpacity>
      )
    }, this)
    return (
        <ScrollView style={styles.container}>
            {MemberList}
        </ScrollView>
    )
  }
  onClickMember (userid) {
      //Navigation.dismissAllModals()
      var user = miscutils.getUserfromCache(this.props.login.usercache, userid)
      // console.log(user)
      // this.props.showProfileModalforUser(user, false)
      Navigation.showModal({
          stack: {
              children: [{
                  component: {
                      name: 'nav.Three60Memos.MemberDetailsScreen',
                      passProps: {
                          selfView : false,
                          fromTopMenu : false,
                          userprofile : user
                      },
                      options : {
                          topBar: {
                              visible : true,
                              title: {
                                  text: "Profile",
                                  color : "Black"
                              },
                              rightButtons:[
                                  {
                                      id : 'closeprofilebutton',
                                      icon : closeIcon,
                                      color : 'black'
                                  }
                              ]

                          }
                      }
                  }
              }]
          }
      });
  }

}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
    padding: 10
  },
  textHeader: {
    fontSize: 18,
    alignSelf: 'center'
  },
  memberImage: {
    width: 45,
    height: 45,
    borderRadius: 10,
    borderWidth: 0.5,
    borderColor: 'gray',
    flexDirection: 'row',
    marginLeft: 5
  },
  memberText: {
    color: 'black',
    textAlignVertical: 'center',
    fontSize: 18,
    paddingLeft: 5,
    paddingTop: 3
  },
  memberlist: {
    flexDirection: 'row',
    paddingTop: 5,
    paddingBottom: 5,
    margin: 5,
    backgroundColor: '#f1f1f1',
    height: 60,
    alignItems: 'stretch',
    elevation: 5,
    shadowOffset: {height: 2, width: 4},
    shadowColor: 'black',
    shadowOpacity: 0.3,
    shadowRadius: 3
  },
  memberTextParentof: {
    fontSize: 10
  }
})

var mapDispatchToProps = function (dispatch) {
  return {
      viewMyProfile: function (userObj) { dispatch(memberActions.viewMyProfile(userObj)) },
      showProfileModalforUser: function (user,fromTopMenu) { dispatch(rootNavActions.showProfileModalforUser(user,fromTopMenu))}
  }
}
var mapStateToProps = function (store) {
  return {login: store.login, newsfeed: store.newsfeed, user: store.user}
}
export default connect(mapStateToProps, mapDispatchToProps)(LikedUsersScreen)
