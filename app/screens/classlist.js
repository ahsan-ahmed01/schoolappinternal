import React from 'react';
import { View, StyleSheet, Text, TouchableOpacity , FlatList} from 'react-native';
import {connect} from 'react-redux';
import * as groupdetailactions from '../actions/groupdetailsActions'
import Icon from  'react-native-vector-icons/FontAwesome'
import {CustomCachedImage} from "react-native-img-cache";
import ProfileButton from '../components/ProfileButton'
import Image from 'react-native-image-progress';
import {Navigation} from 'react-native-navigation'

class ClassListScreen extends React.Component {



    constructor(props) {
        super(props)
        this.state = {
            parentchooservisible : false,
            updateFlatList : 1
        }

    }

    _keyExtractor = (item, index) => item;

    componentWillReceiveProps(nextProps){
        this.setState({updateFlatList: this.state.updateFlatList + 1})
        console.log('flatlist ->' + this.state.updateFlatList)
    }

    render() {
        console.log(this.props.usergroups)
        console.log('RENDER->CLASSLIST')
        if ((this.props.usergroups !== undefined) && (this.props.usergroups !== null)) {
            // const ds = new FlatList.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
            // var dataSource = ds.cloneWithRows(this.props.user.groups);
            // console.log(dataSource)
            return (
                <FlatList
                    data={Object.keys(this.props.usergroups)}
                    extraData={this.state.updateFlatList}
                    keyExtractor={this._keyExtractor}
                    renderItem={(key) => {
                        console.log(this.props)
                        console.log(key)
                        var item = this.props.usergroups[key.item]
                        console.log(item)
                        if (item.profile_photo == '<DEFAULT>' || item.profile_photo == '' || item.profile_photo == null) {
                            var groupprofilePhotoURI = require('../img/groups.png')
                            var imagecomp =  <Image imageStyle={{
                                width: 46,
                                height: 46,
                                borderRadius: 23

                            }}
                                                    style={styles.groupImage} source={groupprofilePhotoURI}></Image>
                        } else {
                            groupprofilePhotoURI = {uri: item.profile_photo.replace('http:', 'https:')}
                            var imagecomp =  <CustomCachedImage
                                component={Image}
                                defaultSource={require('../img/groups.png')}
                                imageStyle={{
                                    width: 46,
                                    height: 46,
                                    borderRadius: 23,

                                }}
                                style={styles.groupImage} source={groupprofilePhotoURI}></CustomCachedImage>
                        }
                        return (

                            <TouchableOpacity key={item.id} style={styles.groupMenuItem}
                                              onPress={this.onClickGroup.bind(this, item)}>
                                {imagecomp}
                                <View style={styles.groupTextContainer}>
                                    <Text style={styles.groupText}>{item.name.replace(/_/g,' ')}</Text>
                                    <Text style={styles.groupTextDesc}>{item.description}</Text>
                                </View>
                                <Icon name="angle-right" size={32} style={styles.FontAwesomeStyle}></Icon>
                            </TouchableOpacity>
                        )}
                    }

                />
            );
        }
        else{
            return null
        }
    }

    onClickGroup(rowData){
        console.log(rowData);
        //this.props.viewGroupDetails(rowData, this.props.token)
        Navigation.push(this.props.componentId, {
            component: {
                name: 'nav.Three60Memos.GroupDetailScreen',
                passProps: {
                    selectedgroup : rowData,
                    className: rowData.name,
                    componentId : this.props.componentId
            },
                options: {
                    topBar: {
                        title: {
                            text:rowData.name.replace(/_/g, ' '),
                            color: 'white'
                        },
                        background: {
                            color: '#1987D1'
                        },
                        backButton: {
                            visible: true,
                            color: 'white',
                            showTitle: false
                        },
                    }
                }
            }
        });
        //this.props.viewGroupDetails(rowData, this.props.token)
        //console.log(rowData.id)
        //this.props.getGroupPhotos(rowData, this.props.user.token)
        //this.props.navigation.dispatch(navigateAction)
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingTop: 15,
        backgroundColor: '#fff',
    },        MenuIcon: {
        paddingRight : 10
    },
    groupTextContainer : {
      flex : 1,
      flexDirection: 'column'
    },
    listcontainer : {
        padding : 20,
        flex :1,
        backgroundColor: 'white'
    },
    groupMenuItem: {
        flexDirection: 'row',
        borderBottomWidth: 0.5,
        borderBottomColor: 'lightgray',

        padding: 10
    },
    groupImage: {
        width: 46,
        height: 46,
        borderRadius: 23,
        flexDirection: 'row',
        borderColor: 'gray'
    },
    groupText: {
        textAlign: 'left',
        color: 'black',
        textAlignVertical: 'center',
        fontSize: 16,
        paddingLeft: 10,
        paddingTop: 3
    },
    groupTextDesc : {
        textAlign: 'left',
        color: 'black',
        textAlignVertical: 'center',
        fontSize: 12,
        fontStyle : 'italic',
        paddingLeft: 10,
        paddingTop: 3
    },
    FontAwesomeStyle : {
        justifyContent: 'center',
        alignItems: 'center',
        color: 'darkgray',
    }
});

var mapStateToProps = function(store){
    return {usergroups : store.user.groups , token: store.user.token};
};

var mapDispatchToProps = function(dispatch){
    return {
        viewGroupDetails: function (groupid, token) { dispatch(groupdetailactions.viewGroupDetails(groupid, token)) },
        getGroupPhotos: function (groupid, token) { dispatch(groupdetailactions.getGroupPhotos(groupid, token)) },

    }
};

export default connect(mapStateToProps,mapDispatchToProps)(ClassListScreen)
