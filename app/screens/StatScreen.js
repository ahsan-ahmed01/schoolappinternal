const React = require('react');
const { Component } = require('react');
const { View, Text, Platform, TouchableHighlight,FlatList,Linking } = require('react-native');
import { connect } from 'react-redux'
const { Navigation } = require('react-native-navigation');

class StatScreen extends Component {

    constructor(props){
        super(props)
        Navigation.events().bindComponent(this);
    }


    sendLogs(){
        console.log('here')
        var url = 'mailto:admin@360memos.com?subject=DebugLog&body=' + this.props.statdata.join('\\n')
        Linking.canOpenURL(url).then(supported => {
                if (!supported) {
                    console.log('Can\'t handle url: ' + url);
                } else {
                    Linking.openURL(url)
                }
            }
        )
        //Linking.openURL()// + this.props.statdata.join('\n') )
    }

    navigationButtonPressed({ buttonId }) {
        console.log(buttonId)
        if (buttonId === 'closebutton'){
            Navigation.dismissAllModals()
        }
        if (buttonId === 'emailbutton'){
            this.sendLogs()
        }
    }

    render() {
        return (
            <View style={styles.root}>
                <FlatList
                    data={this.props.statdata}
                    renderItem={({item}) => <Text style={styles.item}>{item}</Text>}
                />
            </View>
        );
    }
}

export default connect(null, null, null, { withRef: true })(StatScreen);


const styles = {
    root: {
        flex: 1,
        justifyContent: 'flex-start',
        padding : 5,
        backgroundColor: '#e8e8e8',
    },
    message : {
      textAlign: 'center',
        fontSize : 32
    },
    message2 : {
        textAlign: 'center',
        fontSize : 20,
        color : 'blue'
    },
    bar: {
        flex: 1,
        marginTop : 200,
        flexDirection: 'column',
        backgroundColor: '#e8e8e8',
        height : 100,


    },
    h1: {
        fontSize: 24,
        textAlign: 'center',
        margin: 30
    },
    footer: {
        fontSize: 10,
        color: '#888',
        marginTop: 10
    }
};
