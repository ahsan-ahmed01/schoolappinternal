'use strict'
import {
  Text,
  StyleSheet,
  ScrollView,
  Image, TextInput,
  View, TouchableOpacity, ActivityIndicator
} from 'react-native'
import React, {Component} from 'react'
import {connect} from 'react-redux'
import * as postactions from '../actions/postActions'
import {commonStyle} from '../config/commonstyles'
import {Navigation} from 'react-native-navigation';
class CommentsScreen extends Component {



  constructor(props) {
    super(props)
      Navigation.events().bindComponent(this);
    if (this.props.commentid === undefined){
      var buttonText = 'Add Comment'  
    }
    else{
      var buttonText = 'Save Comment'
    }
    this.state = {
      commentText : this.props.commentText,
      errorText : "",
      buttonText : buttonText
    }
    
  }
    navigationButtonPressed({ buttonId }) {
        console.log(buttonId)
        if (buttonId === 'closecommentbutton'){
            Navigation.dismissAllModals()
        }
    }


  componentDidMount(){
    if (this.props.commentid !== undefined){
      this.setState({
        commentText : this.props.commentText
      })
    }
  }


  onSaveComment(){
    console.log(this.props)
    if (this.state.commentText === ""){
      this.setState({
        errorText : 'Please Enter a Comment Before Saving !'
      })
    }
    else {
      if (this.props.inGroupActivity){
          var groupid = this.props.groupid
      }else{
        var groupid = this.props.groupid
      }

      if (this.props.commentid === undefined) {
        this.props.addComment(this.props.activityid, this.state.commentText, this.props.usertoken, groupid)
      }else{
        this.props.updateComment(this.props.activityid,this.props.commentid, this.state.commentText, this.props.usertoken, groupid)
      }
      if (this.state.buttonText === 'Add Comment'){
          Navigation.dismissAllModals()
          //Navigation.popToRoot(this.props.componentId)
      }else{
         // This has to be a pop as actionsheet conflicts with ShowModal
          Navigation.pop(this.props.componentId)
      }

    }


  }

  render () {
    console.log(this.props)
    return (
      <View style={styles.container}>
        <Text style={styles.errorText}>{this.state.errorText}</Text>
        <TextInput
          multiline={true}
          placeholder={'Enter Your Comments Here'}
          onChangeText={this.onChangePostText.bind(this)}
          numberOfLines={10}
          value={this.state.commentText}
          style={styles.postText}
          autoFocus={true}>

        </TextInput>

        <TouchableOpacity style={styles.buttonContainer} onPress={this.onSaveComment.bind(this)}>
          <View style={commonStyle.buttonAcross}>
          <Text style={commonStyle.buttonTextAcross}>{this.state.buttonText}</Text>
          </View>
        </TouchableOpacity>
      </View>
    )
  }

  onChangePostText(str){
    this.setState({
      commentText : str
    })
  }

}


const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    padding: 10,
    backgroundColor : "white"
  },
  postText: {
    height: 200,
    borderColor: 'lightgray',
    borderWidth: 1,
    marginTop: 10,
    padding: 5
  },
  errorText:{
    color : 'red'
  },
  buttonContainer: {
    marginTop : 10
  }
})

var mapDispatchToProps = function (dispatch) {
  return {
    addComment: function (activityid, comment, token, groupid) { dispatch(postactions.addComment(activityid, comment, token, groupid)) },
    updateComment: function (activityid,commentid, comments, token, groupid) { dispatch(postactions.updateComment (activityid,commentid, comments, token, groupid)) },
  }
}
var mapStateToProps = function (store) {
  return {
    user : store.user
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(CommentsScreen)
