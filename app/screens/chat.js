'use strict'
import {Platform, ActivityIndicator, Text, View, ScrollView, StyleSheet, TouchableHighlight, Alert} from 'react-native'
import React, {Component} from 'react'
import {CustomCachedImage} from "react-native-img-cache";

import Image from 'react-native-image-progress';
import _ from 'lodash'

import {GiftedChat, Actions, Bubble, GiftedAvatar} from 'react-native-gifted-chat'
import * as chatActions from '../actions/chatActions'
import * as userActions from '../actions/userActions'

import {connect} from 'react-redux'
import * as firebaseapi from '../api/firebaseapi'
import ImagePicker from 'react-native-image-crop-picker'
import * as uploadmediaapi from '../api/uploadmedia'
import Modal from 'react-native-modal'
import ModalMemberListScreen from './components/modalmemberlist'
import {Navigation} from 'react-native-navigation'
class ChatScreen extends Component {

    constructor(props) {
        super(props)
        this.roomKey = this.props.roomKey;
        this.lastmsgTimestamp = null
        this.state = {
            parentchooservisible: false
        }
    }


    shouldComponentUpdate(nextProps, nextState) {
        if ((nextProps.user.notifications !== null && this.props.user.notifications !== null &&
                (nextProps.user.notifications.Rooms[this.roomKey]) > (this.props.user.notifications.Rooms[this.roomKey]))) {
            console.log('Clearing Unread Chat Messages')
            firebaseapi.clearChatUnRead(this.props.user.userid, this.roomKey)
        }
        if (nextProps.chatrooms.rooms[this.roomKey] === undefined){
            return true
        }
        if (this.props.chatrooms.rooms[this.roomKey] === undefined){
            return true
        }
        var newusers = Object.keys(nextProps.chatrooms.rooms[this.roomKey].Details.users)
        var users = Object.keys(this.props.chatrooms.rooms[this.roomKey].Details.users)
        var newusers = Object.keys(nextProps.chatrooms.rooms[this.roomKey].Details.users)
        var users = Object.keys(this.props.chatrooms.rooms[this.roomKey].Details.users)
        var lastmsgidnew = nextProps.chatrooms.rooms[this.roomKey].Messages === undefined ? null : nextProps.chatrooms.rooms[this.roomKey].Messages[0]._id
        var lastmsgidold = this.props.chatrooms.rooms[this.roomKey].Messages === undefined ? null : this.props.chatrooms.rooms[this.roomKey].Messages[0]._id

        if ((lastmsgidnew === lastmsgidold) && (_.isEqual(newusers.sort(), users.sort()))) {
            console.log('Skipping Render')
            return false
        }
        return true
    }


    getLastMsgTimestampofRoom(roomKey) {
        if (this.props.chatrooms.rooms[roomKey] !== undefined) {
            var msgs = this.props.chatrooms.rooms[roomKey].Messages
            if (msgs === undefined) {
                return 0
            } else {
                var lastts = msgs[0].orderBy
                return lastts
            }
        }
        else return 0
    }

    componentWillUnmount() {
        console.log('Detected Unmount !!!!')
        this.props.stopListener(this.roomKey, this.lastmsgTimestamp)
    }

    ParentChooserModal() {
        if (this.state.parentchooservisible) {
            console.log('SHowing Modal')
            return (
                <Modal onRequestClose={() => {
                    console.log('Modal has been closed.');
                }} isVisible={this.state.parentchooservisible} style={styles.modal}>
                    <ModalMemberListScreen modaldismissfunc={this.hideParentChooser.bind(this)}
                                           onSelectedList={(e) => this.onUsersInvited(e)}
                    />

                </Modal>
            )
        }
        else {
            return null
        }
    }

    componentWillMount() {

        console.log(this.props)
        var lastmsgtimestamp = this.getLastMsgTimestampofRoom(this.roomKey)
        this.lastmsgTimestamp = lastmsgtimestamp
        this.props.startListener(this.roomKey, lastmsgtimestamp)
        console.log(lastmsgtimestamp)
        if (this.props.user.notifications !== null) {
            if (this.props.user.notifications.Rooms[this.roomKey] > 0) {
                firebaseapi.clearChatUnRead(this.props.user.userid, this.roomKey)
            }
        }
    }

    onLeaveChat() {

        Alert.alert(
            'Warning',
            'Are you sure you want to leave this conversation?',
            [
                {text: 'Cancel', onPress: () => {}},
                {
                    text: 'OK',
                    onPress: () => {
                        this.props.leaveChat(this.props.user.userid, this.roomKey)
                        Navigation.pop(this.props.componentId)
                    }
                }
            ],
            {cancelable: false}
        )

    }

    showParentChooser() {
        console.log('show parent chooser')
        this.setState({parentchooservisible: true})
    }

    hideParentChooser() {
        this.setState({parentchooservisible: false})
    }

    onAttachImageActionSelected() {
        console.log('clicked on Image')
        // this.props.navigator.showModal({
        //   screen: 'Three60Memos.AttachImageToMessageScreen',
        //   title: 'Choose a picture',
        //   passProps: { roomkey: this.roomKey },
        //   navigatorStyle: {},
        //   animationType: 'slide-up'
        // })
        ImagePicker.openPicker({
            width: 600,
            height: 800,
            cropping: true,
            multiple: false,
            useFrontCamera: true,
            maxFiles: 1,
            compressImageQuality: 0.9,
            smartAlbums : [ 'UserLibrary','PhotoStream',  'RecentlyAdded', 'SelfPortraits', 'Screenshots','Generic']
        }).then(images => {
            console.log(images.path)
            if (images.path.length > 1) {
                console.log('in here')

                //var response = uploadmediaapi.UploadPhoto(images, this.props.user.id, 0)
                //console.log(response.body.postResponse.location)
                // this.props.finishPickPhotoviaWP(images[0].imagePath, this.roomKey)
                console.log(images.path)
                //this.props.finishPickPhotoviaAWS(images, this.roomKey, this.props.user.id,)
                this.props.finishPickPhotoviaAWS(images, this.roomKey, this.props.user.token)
            }
        })
    }

    onInviteUserActionSelected() {
        console.log('invite users')
        this.showParentChooser()


    }

    onUsersInvited(selectedUsers) {
        this.props.inviteUsersToChat(this.roomKey, selectedUsers, this.props.chatrooms.rooms[this.roomKey].Details.users)
    }


    onSend(messages = []) {
        console.log(messages)
        console.log(this.props.chatrooms.rooms[this.roomKey].Details)
        console.log(this.roomKey)
        this.props.writeChatMessage(this.roomKey,
            messages,
            this.props.chatrooms.rooms[this.roomKey].Details.users,
            this.props.user.userid,
            this.props.user.display_name,
            true
        )
    }

    renderchatHeader() {
        var chatroom = this.props.chatrooms.rooms[this.roomKey]
        var users = (chatroom) ? chatroom.Details.users : null
        if (Object.keys(users).length === 2) {
            return null
        }
        else {

            if (users && Object.keys(users).length > 1) {
                var userlist = Object.keys(users).map(function (userkey) {
                    console.log(userkey)

                    var user = this.props.chatrooms.rooms[this.roomKey].Details.users[userkey]
                    console.log(user)
                    if (user.profile_photo === '' || user.profile_photo === null || user.profile_photo.includes('gravatar.com')) {
                        var profilePhotoURI = require('../img/blankprofile.png')
                        return (
                            <View key={userkey}>
                                <Image style={styles.memberImage} imageStyle={{
                                    width: 40,
                                    height: 40,
                                    borderRadius: 20,

                                }}
                                       source={profilePhotoURI}></Image>
                                <Text style={styles.usernameText}>{user.display_name}</Text>
                            </View>
                        )
                    } else {
                        var profilePhotoURI = {uri: user.profile_photo.replace('http:', 'https:')}
                        return (
                            <View key={userkey}>
                                <CustomCachedImage
                                    component={Image}
                                    defaultSource={require('../img/blankprofile.png')}
                                    imageStyle={{
                                        width: 40,
                                        height: 40,
                                        borderRadius: 20,

                                    }}

                                    style={styles.memberImage} source={profilePhotoURI}></CustomCachedImage>
                                <Text style={styles.usernameText}>{user.display_name}</Text>
                            </View>
                        )
                    }
                    console.log(user.display_name)

                }, this)
                console.log(userlist)
                return (
                    <View style={styles.chatheadercontainer}>
                        <ScrollView contentContainerStyle={styles.scrollViewStyle}
                                    pagingEnabled={true}
                                    horizontal={true}>
                            {userlist}
                        </ScrollView>
                    </View>
                )
            } else {
                return null
            }
        }
    }

    renderBubble(props) {
        var {currentMessage: {user}} = props

        if (user._id === 'system') {
            return <Bubble {...props} containerStyle={{left: {alignItems: 'center'}}}/>
        }

        return <Bubble {...props}/>
    }

    renderCustomActions(props) {
        var chatroom = this.props.chatrooms.rooms[this.roomKey]
        var users = (chatroom) ? chatroom.Details.users : null
        if (users === null) {
            return null
        }
        console.log(Object.keys(users).length)
        var options = null

        if (Object.keys(users).length > 2) {
            options = {
                'Invite user': (props) => {
                    console.log(props)
                    this.onInviteUserActionSelected()
                },
                'Send image': (props) => {
                    console.log(props)
                    this.onAttachImageActionSelected()
                },
                'Leave chat': (props) => {
                    console.log(props)
                    this.onLeaveChat()
                },
                'Cancel': () => {
                }
            }

        }
        else {
            options = {
                'Invite user': (props) => {
                    console.log(props)
                    this.onInviteUserActionSelected()
                },
                'Send image': (props) => {
                    console.log(props)
                    this.onAttachImageActionSelected()
                },
                'Cancel': () => {
                }
            }

        }

        return (
            <Actions
                {...props}
                options={options}
            />
        )
    }

    render() {
        console.log(this.props)
        if (!this.props.chatrooms.rooms[this.roomKey]) return null
        console.log('RENDER->CHAT WINDOW')
        var messages = this._filterChatRoomMessages(this.props.chatrooms.rooms[this.roomKey].Messages, this.props.chatrooms.rooms[this.roomKey].Details.users[this.props.user.userid])

        return (
            <View style={{flex: 1, backgroundColor: 'white'}}>
                {this.ParentChooserModal()}
                {this.renderchatHeader()}
                <GiftedChat
                    loadEarlier={true}
                    isAnimated={true}
                    isLoadingEarlier={this.props.chatrooms.isloadingMessages}
                    onLoadEarlier={this.loadEarlierMessages.bind(this)}
                    messages={messages}
                    onSend={this.onSend.bind(this)}
                    user={{
                        _id: this.props.user.userid,
                        name: this.props.user.display_name,
                        avatar: this.props.user.profile_photo
                    }}
                    renderBubble={(props) => this.renderBubble(props)}
                    renderAvatar={(props) => this.renderAvatar(props)}
                    renderActions={this.renderCustomActions.bind(this)}
                    renderLoading={this.renderLoading.bind(this)}
                />
            </View>
        )
    }

    renderLoading() {
        return (
            <ActivityIndicator animating={true}>

            </ActivityIndicator>
        )
    }

    renderAvatar(props) {
        console.log('---')
        console.log(props)
        return (
            <View>
                <GiftedAvatar
                    user={props.currentMessage.user}
                />
                <Text style={{fontSize: 10}}>{props.currentMessage.user.name}</Text>
            </View>
        )
    }

    loadEarlierMessages() {
        console.log('called Earlied !!!')
        var Messages = this.props.chatrooms.rooms[this.roomKey].Messages
        if (Messages !== undefined) {
            if (Messages.length > 1) {
                var lasttimestamp = Messages[Messages.length - 1].orderBy
                this.props.getMoreMessages(this.roomKey, lasttimestamp)
            } else {
                this.props.getMoreMessages(this.roomKey, null)
            }
        }
    }

    _filterChatRoomMessages(allMessages, userDetail) {
        if (!allMessages || !userDetail) return []

        var filteredMessages = allMessages.filter(message => {
            return (new Date(message.orderBy) >= new Date(userDetail.joinedTs))
        })

        return (!userDetail.joinedTs) ? allMessages : filteredMessages
    }

}

const styles = StyleSheet.create({
    chatheadercontainer: {
        flex: 0,
        height: 75,
        borderBottomWidth: 1,
        borderBottomColor: 'gray',
        paddingLeft: 5,
        marginBottom: 2,
        paddingBottom: 2,
        paddingTop: 2
    },
    scrollViewStyle: {
        flexDirection: 'row'
    },
    usernameText: {
        color: 'gray',
        margin: 3,
        fontSize: 10,
        textAlign: 'center',
        borderRadius: 10,
        width: 45
    },
    leaveGroupText: {
        marginTop: 5,
        textAlign: 'center'
    },
    modal: {
        backgroundColor: 'transparent',
        margin: 0, // This is the important style you need to set
        alignItems: undefined,
        justifyContent: undefined,
    },
    memberImage: {
        width: 40,
        height: 40,
        borderRadius: 20,
        borderWidth: 1,
        borderColor: 'gray',
        flexDirection: 'row',
        marginRight: 2,
        marginLeft: 2,
        marginTop: 2

    },
})

var mapStateToProps = function (store) {
    return {
        user: store.user,
        chatrooms: store.chatrooms
    }
}
var mapDispatchToProps = function (dispatch) {
    return {
        writeChatMessage: function (roomkey, message, users, myuserid, mydisplayname, sendnotification) {
            dispatch(chatActions.writeChatMessage(roomkey, message, users, myuserid, mydisplayname, sendnotification))
        },
        startListener: function (roomkey, lastmsgtimestamp) {
            dispatch(chatActions.startListener(roomkey, lastmsgtimestamp))
        },
        stopListener: function (roomkey, lastmsgtimestamp) {
            dispatch(chatActions.stopListener(roomkey, lastmsgtimestamp))
        },
        setNavigationTab: function (tabName) {
            dispatch(userActions.setNavigationTab(tabName))
        },
        getMoreMessages: function (roomKey, lasttimestamp) {
            dispatch(chatActions.getMoreMessages(roomKey, lasttimestamp))
        },
        leaveChat: function (userid, roomkey) {
            dispatch(chatActions.leaveChat(userid, roomkey))
        },
        finishPickPhotoviaAWS: function (imagePath, roomkey, userid) {
            dispatch(chatActions.finishPickPhotoviaAWS(imagePath, roomkey, userid))
        },
        finishPickPhotoviaWP: function (imagePath, roomkey, token) {
            dispatch(chatActions.finishPickPhotoviaWP(imagePath, roomkey, token))
        },
        inviteUsersToChat: function (roomkey, selectedUsers, oldusers) {
            dispatch(chatActions.inviteUsersToChat(roomkey, selectedUsers, oldusers))
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ChatScreen)
