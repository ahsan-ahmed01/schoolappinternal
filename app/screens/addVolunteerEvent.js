/* eslint-disable no-unused-vars */
"use strict";
import {
  Platform,
  ScrollView,
  StyleSheet,
  View,
  Alert,
  ImageBackground,
  TouchableOpacity,
  Switch,
  Text,
  Linking,
  Dimensions,
  FlatList,
  TextInput,
} from "react-native";
import {
  Item,
  Input,
  Container,
  Tab,
  Tabs,
  TabHeading,
  CheckBox,
} from "native-base";
import moment from "moment"; // 2.20.1
import Entypo from "react-native-vector-icons/Entypo";
import * as Animatable from "react-native-animatable";
import Image from "react-native-image-progress";
import Dialog, {
  DialogFooter,
  DialogButton,
  DialogContent,
} from "react-native-popup-dialog";
import AutoHeightWebView from "react-native-autoheight-webview";
import { CustomCachedImage } from "react-native-img-cache";
import { Storage } from "aws-amplify";
import ImagePicker from "react-native-image-crop-picker";
import { ProgressCircle } from "react-native-svg-charts";
import Spinner from "react-native-loading-spinner-overlay";
import React, { Component } from "react";
import Accordian from "./components/accordian";
import { connect } from "react-redux";
import Icon from "react-native-vector-icons/FontAwesome";
import MaterialIcons from "react-native-vector-icons/MaterialIcons";
import * as postActions from "../actions/postActions";
import { commonStyle } from "../config/commonstyles";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import * as eventActions from "../actions/eventActions";
import * as miscutils from "../utils/misc";
import Modal from "react-native-modal";
import { WebView } from "react-native-webview";
import ModalMemberListScreen from "./components/modalmemberlist";
import shallowCompare from "react-addons-shallow-compare";
import { UploadPhoto } from "../api/uploadmedia";
import {
  Card,
  Tile,
  ThemeProvider,
  ButtonGroup,
  Button,
  Divider,
} from "react-native-elements";
import SectionedMultiSelect from "react-native-sectioned-multi-select";
import Picker from "react-native-picker";
import RNFetchBlob from "react-native-fetch-blob";
import { Buffer } from "buffer";
import DateTimePicker from "react-native-modal-datetime-picker";
import { RNS3 } from "react-native-aws3";
import { Navigation } from "react-native-navigation";
import NumberSpinner from "./components/numberspinner";
import * as eventsapi from "../api/eventsapi";
import { Calendar } from "react-native-calendars";
import _isEmpty from "lodash/isEmpty";
var EnvConfig = require("../config/environment");
const rsvpButtons = ["Yes", "Maybe", "No"];
const _format = "YYYY-MM-DD";
const _today = moment().format(_format);
// const _maxDate = moment().add(15, 'days').format(_format)

const dafaultSpotSelection = {
  person: "1",
  type: "Bring",
  description: "",
  comment: "",
  dates: [],
  errorDescription: "",
};
class AddVolunteerEventScreen extends Component {
  constructor(props) {
    super(props);
    console.log(props);
    this.state = {
      _showParents: false,
      selectedUsers: {},
      formData: {},
      errorText: "",
      _alldayevent: undefined,
      isEditable: true,
      parentchooservisible: false,
      eventimgurl: null,
      coverimage: null,
      updatingImage: false,
      _eventname: "",
      _eventdesc: "",
      _eventlocation: "",
      _eventstartDate: "",
      _eventendDate: "",
      _eventstartTime: "",
      _eventendTime: "",
      _eventnotify: false,
      _eventstartDateObj: Date(),
      _eventendDateObj: null,
      _remindsameday: false,
      _remindsOneday: false,
      _remindsTwoday: false,
      groupshash: {},
      descHeight: 100,
      _grpSelect: "",
      _grpSelectId: 0,
      rsvpIndex: 4,
      adultcount: 1,
      kidscount: 0,
      childDialogModal: false,
      childDialogModalData: [],
      childDialogModalselection: null,
      showRefreshing: false,
      type3signupSlot: null,
      type4signupSlot: null,
      event4data: null,
      _eventStep: 0,
      selectNoticationDayAgo: null,
      addAttachment: null,
      addAttachmentUrl: null,
      _markedCalendarDate: {},
      start: {},
      end: {},
      showMarkCalendarDateModal: false,
      _selectGroupParent: "Group",
      _disableNextStepBtn: true,
      volunteertype: "1",
      daterangetype: "1",
      volunteerDetail: {
        volunteertype: "1",
        daterangetype: "1",
        daterange: {},
        selectedDays: null,
        volunteer: [],
      },
      dateMarkingType: "simple",
      slotReminderInputModalShow: false,
      spot: [{ ...dafaultSpotSelection }],

      personsChooserVisible: false,
      spotChooserVisible: false,
      selectRecurring: { "Repeat every week on the days": 0 },
      recurringText: "Repeat every week on the days",
      selectedDays: [
        { name: "1", checked: false, label: "MO" },
        { name: "2", checked: false, label: "TU" },
        { name: "3", checked: false, label: "WE" },
        { name: "4", checked: false, label: "TH" },
        { name: "5", checked: false, label: "FR" },
        { name: "6", checked: false, label: "SA" },
        { name: "7", checked: false, label: "SU" },
      ],
      daterange: {},
      dates: {},
      daterangeDate: {},
      errorDateTimeSelectModal: { isvisible: false, message: "" },
      isAtleastOneSpotSelect: true,
      _renderMarkedCalendarDate: {},
      slotTimeIntervalPickerVisible: false,
      slotTimeIntervalTime:"",
      _selectedTimeInterval: { identifier: "", slot: null, index:null },
    };

    this.updateRSVPIndex = this.updateRSVPIndex.bind(this);
    this.confirmRSVP = this.confirmRSVP.bind(this);
    this.showSpinner = this.showSpinner.bind(this);
    this.onNavigationURLChange = this.onNavigationURLChange.bind(this);
    Navigation.events().bindComponent(this);
    if (this.props.eventid !== null) {
      if (this.props.eventData === undefined) return;
      this.eventData = this.props.eventdata;
      //this.state.eventimgurl = this.props.eventdata.eventimgurl
      if (
        this.eventData.starttime !== null ||
        this.eventData.endtime !== null
      ) {
        this.state._alldayevent = false;
      }
    } else {
      this.eventData = null;
    }
  }

  showSpinner() {
    // if (Platform.OS == 'android') {
    //   return  false

    // }else{
    return this.props.events.isLoading;
    //}
  }

  componentWillMount() {
    console.log("AddEvent->CWM");
    console.log(this.props.eventid);
    if (this.props.eventid != null) {
      // this.getCoverImage();
    } else {
      this.setState({
        eventimgurl: require("../img/bg.png"),
      });
    }
  }

  componentWillUnmount() {
    console.log("componentWillUnmount");
    this.setState({ showMarkCalendarDateModal: false });
  }
  navigationButtonPressed({ buttonId }) {
    if (buttonId === "closebutton") {
      this.setState({
        childDialogModal: false,
        childDialogModalselection: null,
        showMarkCalendarDateModal: false,
      });
      Navigation.dismissAllModals();
    }
  }

  showParentChooser() {
    this.setState({ parentchooservisible: true });
  }

  hideParentChooser() {
    this.setState({ parentchooservisible: false });
  }

  onNavigatorEvent(event) {
    if (event.id === "closePost") {
      this.props.navigator.dismissModal();
    }
  }

  componentDidUpdate(prevProps, prevState) {
    const {
      _eventname,
      _eventlocation,
      _grpSelect,
      _selectGroupParent,
      selectedUsers,
    } = this.state;
    if (
      this.state.childDialogModal === false &&
      prevState.childDialogModal === true &&
      this.state.childDialogModalselection != null &&
      prevState.childDialogModalselection === null
    ) {
      console.log("Sign up CHild !!!!!");
      console.log(
        (this.state.type3signupSlot, this.state.childDialogModalselection.id)
      );
      if (this.props.events.events[this.props.eventid].eventtype === 3) {
        this.postsignupSlotEvent3(
          this.state.type3signupSlot,
          this.state.childDialogModalselection.id
        );
      } else {
        this.postsignupSpotEvent4(
          this.state.type4signupSlot,
          this.state.childDialogModalselection.id
        );
      }
    }

    if (
      prevState._eventname !== _eventname ||
      prevState._grpSelect !== _grpSelect ||
      prevState._selectGroupParent !== _selectGroupParent ||
      prevState.selectedUsers !== selectedUsers
    ) {
      if (
        _eventname &&
        _selectGroupParent &&
        (_grpSelect || Object.keys(selectedUsers).length !== 0)
      ) {
        this.setState({ _disableNextStepBtn: false });
      } else {
        this.setState({ _disableNextStepBtn: true });
      }
    }
  }

  shouldComponentUpdate(nextProps, nextState) {
    //return shallowCompare(this, nextProps, nextState);

    return true;
  }

  renderImageHeader(isEditable) {
    if (this.state.formData !== null) {
      var headerText = (
        <Text style={styles.headerTextStyle}>
          {this.state.formData.eventname}
        </Text>
      );
    } else {
      var headerText = <Text style={styles.headerTextStyle}></Text>;
    }
    console.log(this.state.eventimgurl);
    if (this.state.eventimgurl === "null" || this.state.eventimgurl === null) {
      var coverurl = require("../img/bg.png");
    } else {
      if (
        typeof this.state.eventimgurl === "string" &&
        this.state.eventimgurl.includes("http")
      ) {
        var coverurl = { uri: this.state.eventimgurl };
      } else {
        var coverurl = this.state.eventimgurl;
      }
    }
    console.log(coverurl);
    return (
      <View style={{ position: "relative" }}>
        <ImageBackground
          style={styles.headerimage}
          resizeMethod={"auto"}
          resizeMode={"cover"}
          source={coverurl}
        >
          {headerText}
          {this.state._eventStep <= 0 && this.renderCameraCoverPhoto()}
        </ImageBackground>
      </View>
    );
  }

  getCoverImage() {
    var coverurl = require("../img/bg.png");
    if (
      this.props.eventdata.eventimgurl === null ||
      this.props.eventdata.eventimgurl === "" ||
      this.props.eventdata.eventimgurl === "0" ||
      this.props.eventdata.eventimgurl === "null"
    ) {
      this.setState({ eventimgurl: null });
    } else {
      if (
        this.props.eventdata.eventimgurl.includes("https:") ||
        this.props.eventdata.eventimgurl.includes("http:")
      ) {
        coverurl = { uri: this.props.eventdata.eventimgurl };
        console.log(coverurl);
        this.setState({ eventimgurl: coverurl });
      } else {
        var url = Storage.get(this.props.eventdata.eventimgurl).then((url) => {
          this.setState({ eventimgurl: url });
          return url;
        });
      }
    }
  }

  renderCameraCoverPhoto() {
    if (this.state.isEditable) {
      return (
        <View
          style={{
            alignItems: "center",
            justifyContent: "flex-end",
            flex: 1,
            marginBottom: 10,
          }}
        >
          <TouchableOpacity style={{ backgroundColor: "transparent" }}>
            <Button
              title="Change Picture"
              type="outline"
              buttonStyle={{
                backgroundColor: "transparent",
                width: 200,
                borderRadius: 20,
              }}
              onPress={this.onClickCoverCamera.bind(this)}
              titleStyle={{ color: "white" }}
            />
          </TouchableOpacity>
        </View>
      );
    } else {
      return null;
    }
  }

  onClickCoverCamera() {
    ImagePicker.openPicker({
      width: 800,
      height: 600,
      cropping: true,
      smartAlbums: [
        "UserLibrary",
        "PhotoStream",
        "RecentlyAdded",
        "SelfPortraits",
        "Screenshots",
        "Generic",
      ],
    }).then((image) => {
      this.setState({ coverimage: image });
      this.setState({ eventimgurl: { uri: image.path } });
    });
  }
  onClickAddAttachement() {
    ImagePicker.openPicker({
      width: 800,
      height: 600,
      cropping: true,
      smartAlbums: [
        "UserLibrary",
        "PhotoStream",
        "RecentlyAdded",
        "SelfPortraits",
        "Screenshots",
        "Generic",
      ],
    }).then((image) => {
      console.log(image, "image");
      this.setState({ addAttachment: image });
      this.setState({ addAttachmentUrl: { uri: image.path } });
    });
  }
  ParentChooserModal() {
    if (this.state.parentchooservisible) {
      return (
        <Modal
          onRequestClose={() => {
            console.log("Modal has been closed.");
          }}
          transparent={true}
          isVisible={this.state.parentchooservisible}
          style={styles.modal}
        >
          <ModalMemberListScreen
            modaldismissfunc={this.hideParentChooser.bind(this)}
            onSelectedList={(e) => this.onSelectedList(e)}
            initialSelectedList={this.state.selectedUsers}
          />
        </Modal>
      );
    } else {
      return null;
    }
  }
  _eventStepper() {
    const { _eventStep } = this.state;
    return (
      <>
        <View style={styles.stepperContainer}>
          <View style={styles.stepperElement}>
            <View
              style={
                _eventStep === 0
                  ? styles.hightlightRoundElement
                  : styles.roundElement
              }
            >
              <Text
                style={
                  _eventStep === 0
                    ? styles.eventStepTextColorLight
                    : styles.eventStepTextColor
                }
              >
                1
              </Text>
            </View>
            <Text
              style={
                _eventStep === 0 ? styles.highlightedStepText : styles.stepText
              }
            >
              General Details
            </Text>
          </View>
          <View style={styles.stepperElement}>
            <View
              style={
                _eventStep === 1
                  ? styles.hightlightRoundElement
                  : styles.roundElement
              }
            >
              <Text
                style={
                  _eventStep === 1
                    ? styles.eventStepTextColorLight
                    : styles.eventStepTextColor
                }
              >
                2
              </Text>
            </View>
            <Text
              style={
                _eventStep === 1 ? styles.highlightedStepText : styles.stepText
              }
            >
              Dates/Time
            </Text>
          </View>
          <View style={styles.stepperElement}>
            <View
              style={
                _eventStep === 2
                  ? styles.hightlightRoundElement
                  : styles.roundElement
              }
            >
              <Text
                style={
                  _eventStep === 2
                    ? styles.eventStepTextColorLight
                    : styles.eventStepTextColor
                }
              >
                3
              </Text>
            </View>
            <Text
              style={
                _eventStep === 2 ? styles.highlightedStepText : styles.stepText
              }
            >
              Slots
            </Text>
          </View>
          <View style={styles.stepperElement}>
            <View
              style={
                _eventStep === 3
                  ? styles.hightlightRoundElement
                  : styles.roundElement
              }
            >
              <Text
                style={
                  _eventStep === 3
                    ? styles.eventStepTextColorLight
                    : styles.eventStepTextColor
                }
              >
                4
              </Text>
            </View>
            <Text
              style={
                _eventStep === 3 ? styles.highlightedStepText : styles.stepText
              }
            >
              Preview
            </Text>
          </View>
        </View>
        <Divider style={{ backgroundColor: "blue" }} />
      </>
    );
  }
  _footerTab() {
    let isSpotSelect = false;
    if (this.state._eventStep === 2) {
      isSpotSelect = this.state.isAtleastOneSpotSelect;
    }
    return (
      <View
        style={{
          flexDirection: "row",
          padding: 12,
          borderTopColor: "#ccc",
          borderTopWidth: 1,
          alignItems: "center",
          justifyContent: "center",
        }}
      >
        <Button
          type="clear"
          disabled={this.state._eventStep <= 0}
          buttonStyle={
            !this.state._eventStep <= 0
              ? styles.footerBackBtn
              : styles.disableFooterBackBtn
          }
          icon={
            <Entypo
              name="chevron-left"
              style={styles.actionButtonIcon}
              size={24}
            />
          }
          onPress={this._handleNavigateBackInRsvp.bind(this)}
        />
        <View style={{ flex: 1 }}>
          <Button
            title="Cancel"
            titleStyle={{ color: "#000" }}
            type="clear"
            onPress={() => {
              this.navigationButtonPressed({ buttonId: "closebutton" });
            }}
          />
        </View>
        <Button
          disabled={
            this.state._disableNextStepBtn ||
            this.state._eventStep >= 3 ||
            isSpotSelect
          }
          type="clear"
          buttonStyle={
            !this.state._disableNextStepBtn &&
            !(this.state._eventStep >= 3) &&
            !isSpotSelect
              ? styles.footerBackBtn
              : styles.disableFooterBackBtn
          }
          icon={
            <Entypo
              name="chevron-right"
              style={styles.actionButtonIcon}
              size={24}
            />
          }
          onPress={this._handleNavigateNextInRsvp.bind(this)}
        />
      </View>
    );
  }

  _handleNavigateBackInRsvp() {
    this.setState({ _eventStep: this.state._eventStep - 1 });
  }

  _handleNavigateNextInRsvp = () => {
    if (this.state._eventStep <= 3 && this.state._eventStep >= 0) {
      if (
        this.state._eventStep === 2 &&
        Object.keys(this.state._markedCalendarDate).length === 0
      ) {
        let error =
          Object.keys(this.state._markedCalendarDate).length === 0
            ? "At least one Date must be entered"
            : "error";
        // "At least one Time must be entered"
        console.log("not possible to move forward");
        this.setState({
          errorDateTimeSelectModal: {
            ...this.state.errorDateTimeSelectModal,
            isvisible: true,
            message: error,
          },
        });
      } else {
        this.setState({ _eventStep: this.state._eventStep + 1 }, () => {
          if (this.state._eventStep === 2) {
            this.handleAddSpotReminderIntoModal();
            if (this.state.volunteertype === "3") {
              this.handleTransformDaterangeData();
            }
          }
        });
      }
    }
  };

  validateElement(elementValue, element, errtext, setFocus = false) {
    if (
      elementValue === undefined ||
      elementValue === null ||
      elementValue.length === 0
    ) {
      if (setFocus) {
        element.focus();
      }
      this.setState({ errorText: "Error, " + errtext + "!" });
      return false;
    }
    return true;
  }

  componentDidMount() {
    if (this.props.isReadonly) {
      this.setState({ isEditable: false });
    } else {
      if (
        this.props.user.roles === "parent" ||
        this.props.user.roles === "attendance"
      ) {
        this.setState({ isEditable: false });
      } else {
        this.setState({ isEditable: true });
      }
    }
    var groups = this.getGroupData();
    var hash = {};
    var firstkey = null;
    Object.keys(groups).forEach(function (key) {
      hash[groups[key]] = key;
      if (firstkey == null) firstkey = key;
    });

    console.log(this.state);
    if (
      this.props.eventid !== null ||
      (this.props.eventCloneId && this.props.eventCloneId !== null)
    ) {
      var eventprops = this.props.events.events[
        this.props.eventid || this.props.eventCloneId
      ];
      var users = {};
      for (var i in this.props.eventdata.users) {
        var username = this.props.eventdata.users[i];
        if (username !== this.props.user.username)
          users[username] = miscutils.getUserfromCache(
            this.props.login.usercache,
            username
          );
      }
      console.log(this.props.eventdata);
      if (
        this.props.eventdata &&
        this.props.eventdata.endtime === this.props.eventdata.starttime &&
        this.props.eventdata.startdate === this.props.eventdata.enddate
      ) {
        var _alldayevent = true;
      } else {
        _alldayevent = false;
      }
      console.log(_alldayevent);
      // RSVP Settings for EventType2
      var _rsvpindex = 4;
      var _adultcount = 1;
      var _kidscount = 0;
      var _selectGroupParent = null;
      var _markedCalendarDate = {};
      var volunteertype = undefined;
      var daterangetype = "1";
      var spot = [];

      if (eventprops.eventtype === 2) {
        if (
          eventprops.userresponse !== undefined &&
          eventprops.userresponse.user_response !== null &&
          typeof eventprops.userresponse.user_response == "object" &&
          eventprops.userresponse.user_response.user ===
            this.props.user.username
        ) {
          _rsvpindex = rsvpButtons.indexOf(
            eventprops.userresponse.user_response.attend
          );
          _adultcount = eventprops.userresponse.user_response.adult;
          _kidscount = eventprops.userresponse.user_response.children;
          console.log(_adultcount);
          console.log(_kidscount);
        }
      }

      if (eventprops.eventtype === 4) {
        if (
          eventprops?.volunteerDetail.volunteertype === "1" &&
          eventprops?.volunteerDetail.volunteer !== null
        ) {
          console.log("cloneAndEditCodeRun----->");
          volunteertype = eventprops.volunteerDetail.volunteertype;
          eventprops.volunteerDetail.volunteer.forEach((item) => {
            let timeIntervals = [];

            item.slotTime.forEach((slot) => {
              timeIntervals = [...timeIntervals, { ...slot, spot: slot.spots }];
            });
            _markedCalendarDate = {
              ..._markedCalendarDate,
              [moment(item.event_date).format("YYYY-MM-DD")]: {
                disableTouchEvent: true,
                marked: true,
                selected: true,
                selectedDotColor: "orange",
                breakBtwConference: 15,
                day: {
                  dateString: moment(item.event_date).format("YYYY-MM-DD"),
                },
                timeIntervals,
              },
            };
          });
        } else if (eventprops?.volunteerDetail.volunteertype === "2") {
        } else if (eventprops?.volunteerDetail.volunteertype === "3") {
        } else if (eventprops?.volunteerDetail.volunteertype === "4") {
        }
      }
      if (
        eventprops.users.length > 0 &&
        (eventprops.group_id === null || eventprops.grpid === null)
      ) {
        _selectGroupParent = "Parents";
      } else {
        _selectGroupParent = "Group";
      }
      this.setState({
        groupshash: hash,
        _eventname: this.props.eventdata.eventname,
        _eventdesc: this.props.eventdata.eventdesc,
        _eventstartDate: this.GetFormattedDate(this.props.eventdata.startdate),
        _eventendDate: this.GetFormattedDate(this.props.eventdata.enddate),
        _eventstartTime: miscutils.formatTimeinAMPM(
          this.props.eventdata.starttime
        ),
        _eventendTime: miscutils.formatTimeinAMPM(this.props.eventdata.endtime),
        _eventlocation: this.props.eventdata.location,
        _eventnotify: this.props.eventdata.notifyusers === 0 ? false : true,
        _eventstartDateObj: new Date(this.props.eventdata.startdate),
        _eventendDateObj: new Date(this.props.eventdata.enddate),
        eventimgurl: this.props.eventdata.eventimgurl,
        _remindsameday: this.props.eventdata.remindsameday === 0 ? false : true,
        _remindsOneday: this.props.eventdata.remindoneday === 0 ? false : true,
        _remindsTwoday: this.props.eventdata.remindtwoday === 0 ? false : true,
        _grpSelectId: this.props.eventdata.groupid,
        _grpSelect: groups[this.props.eventdata.groupid],
        _alldayevent: _alldayevent,
        _showParents: this.props.eventdata.users.length === 0 ? false : true,
        selectedUsers: Object.assign(users),
        rsvpIndex: _rsvpindex,
        adultcount: _adultcount,
        kidscount: _kidscount,
        _selectGroupParent: _selectGroupParent,
        _markedCalendarDate: _markedCalendarDate,
      });
      //RSVP Changes
    } else {
      this.setState({
        groupshash: hash,
        _grpSelectId: firstkey,
        _grpSelect: groups[firstkey],
      });
    }
  }

  async uploadEventPhoto(image) {
    const options = {
      bucket: EnvConfig.AWS_CONFIG.S3_PROFILE_BUCKET,
      region: EnvConfig.AWS_CONFIG.S3_BUCKET_REGION,
      accessKey: EnvConfig.AWS_CONFIG.S3_PROFILE_ACCESS_KEY,
      secretKey: EnvConfig.AWS_CONFIG.S3_PROFILE_SECRET_KEY,
      successActionStatus: 201,
    };
    var filename = "Event/EventPhoto_" + Date.now() + ".jpg";
    const file = {
      uri: image.path,
      type: "image/jpeg",
      name: filename,
    };
    var response = await RNS3.put(file, options)
      .then((response) => {
        if (response.status !== 201) {
          throw new Error("Failed to upload image to S3");
        }
        var url = EnvConfig.AWS_CONFIG.S3_PROFILE_URL + filename;
        return url;
      })
      .catch((error) => {
        dispatch({ type: "UPLOAD_PROFILE_PHOTO_REJECTED", payload: err });
        return error;
      });
    return response;
  }

  getGroupData() {
    var groups = {};
    for (var key in this.props.user.groups) {
      var group = this.props.user.groups[key];
      if (
        group.is_admin === 1 ||
        group.is_mod === 1 ||
        group.is_admin === "1" ||
        group.is_mod === "1"
      ) {
        groups[group.id] = group.name.replace(/_/g, " ");
      } else {
        if (!this.state.isEditable) {
          groups[group.id] = group.name.replace(/_/g, " ");
        }
      }
    }
    return groups;
  }

  handleFormFocus(event, reactNode) {
    this.refs.scroll.scrollToFocusedInput(reactNode);
  }

  RenderErrorText() {
    if (this.state.errorText !== "") {
      return <Text style={styles.errorText}>{this.state.errorText}</Text>;
    } else {
      return null;
    }
  }

  getCalendarIcons() {
    if (Platform.OS === "ios") {
      return [
        <Icon
          key="0"
          style={{ alignSelf: "center", marginLeft: 10 }}
          name="calendar"
          size={24}
        />,
        <Icon
          key="1"
          style={{ alignSelf: "center", marginLeft: 10, color: "purple" }}
          name="calendar"
          size={24}
        />,
      ];
    } else {
      return [
        <Icon
          key="1"
          style={{
            alignSelf: "center",
            paddingBottom: 20,
            marginLeft: 10,
            color: "purple",
          }}
          name="calendar"
          size={24}
        />,
      ];
    }
  }
  getDateString(timestamp) {
    const date = new Date(timestamp);
    const year = date.getFullYear();
    const month = date.getMonth() + 1;
    const day = date.getDate();

    let dateString = `${year}-`;
    if (month < 10) {
      dateString += `0${month}-`;
    } else {
      dateString += `${month}-`;
    }
    if (day < 10) {
      dateString += `0${day}`;
    } else {
      dateString += day;
    }

    return dateString;
  }

  getPeriod(startTimestamp, endTimestamp, dayObj) {
    const period = {};
    let currentTimestamp = startTimestamp;
    while (currentTimestamp < endTimestamp) {
      const dateString = this.getDateString(currentTimestamp);
      period[dateString] = {
        color: "green",
        startingDay: currentTimestamp === startTimestamp,
        day: { dateString },
        timeIntervals: [
          {
            startTime: this._addMinutes(new Date(), 0),
            endTime: this._addMinutes(new Date(), 30),
          },
        ],
        breakBtwConference: 15,
      };
      currentTimestamp += 24 * 60 * 60 * 1000;
    }
    const dateString = this.getDateString(endTimestamp);
    period[dateString] = {
      color: "green",
      endingDay: true,
      day: { dateString },
      timeIntervals: [
        {
          startTime: this._addMinutes(new Date(), 0),
          endTime: this._addMinutes(new Date(), 30),
        },
      ],
      breakBtwConference: 15,
    };
    return period;
  }

  setDay(dayObj) {
    const { start, end } = this.state;
    const { dateString, day, month, year } = dayObj;
    // timestamp returned by dayObj is in 12:00AM UTC 0, want local 12:00AM
    const timestamp = new Date(year, month - 1, day).getTime();
    const newDayObj = { ...dayObj, timestamp };
    // if there is no start day, add start. or if there is already a end and start date, restart
    const startIsEmpty = _isEmpty(start);
    if (startIsEmpty || (!startIsEmpty && !_isEmpty(end))) {
      const period = {
        [dateString]: {
          color: "green",
          endingDay: true,
          startingDay: true,
          day: { dateString },
          timeIntervals: [
            {
              startTime: this._addMinutes(new Date(), 0),
              endTime: this._addMinutes(new Date(), 30),
            },
          ],
          breakBtwConference: 15,
        },
      };
      this.setState({ start: newDayObj, end: {} });
      return period;
    } else {
      // if end date is older than start date switch
      const { timestamp: savedTimestamp } = start;
      if (savedTimestamp > timestamp) {
        const period = this.getPeriod(timestamp, savedTimestamp);
        this.setState({ start: newDayObj, end: start });
        return period;
      } else {
        const period = this.getPeriod(savedTimestamp, timestamp);
        this.setState({ end: newDayObj, start });
        return period;
      }
    }
  }

  onDaySelect = (day) => {
    const {
      volunteerDetail: { volunteertype },
    } = this.state;
    let updatedMarkedDates = {};
    let daterange = {};
    let dates = {};
    console.log("volunteerDetail--------", day, volunteertype);

    const _selectedDay = moment(day.dateString).format(_format);
    let marked = true;
    if (this.state._markedCalendarDate[_selectedDay]) {
      marked = !this.state._markedCalendarDate[_selectedDay].marked;
    }
    // Create a new object using object property spread since it should be immutable
    // Reading: https://davidwalsh.name/merge-objects
    if (volunteertype === "1") {
      updatedMarkedDates = {
        ...{
          [_selectedDay]: {
            day,
            timeIntervals: [
              {
                startTime: this._addMinutes(new Date(), 0),
                endTime: this._addMinutes(new Date(), 30),
              },
            ],
            breakBtwConference: 15,
            marked,
            selected: true,
            disableTouchEvent: true,
            selectedDotColor: "orange",
          },
        },
      };
    } else if (volunteertype === "2") {
      updatedMarkedDates = {
        ...this.state._markedCalendarDate,
        ...{
          [_selectedDay]: {
            day,
            timeIntervals: [
              {
                startTime: this._addMinutes(new Date(), 0),
                endTime: this._addMinutes(new Date(), 30),
              },
            ],
            breakBtwConference: 15,
            marked,
            selected: true,
            disableTouchEvent: true,
            selectedDotColor: "orange",
          },
        },
      };
    } else if (volunteertype === "3") {
      updatedMarkedDates = {
        // ...this.state._markedCalendarDate,
        ...this.setDay(day),
      };
      let calendarDates = Object.values(updatedMarkedDates);
      // updatedMarkedDates = {
      //   ...updatedMarkedDates,
      //   from: moment(calendarDates[0].day.dateString).format(),
      //   to: moment(calendarDates.slice(-1)[0].day.dateString).format(),
      //   timeIntervals: [
      //     {
      //       startTime: this._addMinutes(new Date(), 0),
      //       endTime: this._addMinutes(new Date(), 30),
      //     },
      //   ],
      //   breakBtwConference: 15,
      // };
      if (calendarDates.length === 1) {
      } else if (calendarDates.length > 1) {
        daterange = {
          from: moment(calendarDates[0].day.dateString).format(),
          to: moment(calendarDates.slice(-1)[0].day.dateString).format(),
          timeIntervals: [
            {
              startTime: this._addMinutes(new Date(), 0),
              endTime: this._addMinutes(new Date(), 30),
            },
          ],
          breakBtwConference: 15,
        };
        let tempDay = [];
        calendarDates.forEach((date) => {
          console.log(date, "date", moment(date.day.dateString).day());

          if (moment(date.day.dateString).day() === 0) {
            if (dates[moment(date.day.dateString).day() + 7]) {
              tempDay = [...dates[moment(date.day.dateString).day() + 7]];
            }
            dates = {
              ...dates,
              [moment(date.day.dateString).day() + 7]: [
                ...tempDay,
                moment(date.day.dateString).format(),
              ],
            };
          } else if (moment(date.day.dateString).day() === 1) {
            if (dates[moment(date.day.dateString).day()]) {
              tempDay = [...dates[moment(date.day.dateString).day()]];
            }
            dates = {
              ...dates,
              [moment(date.day.dateString).day()]: [
                ...tempDay,
                moment(date.day.dateString).format(),
              ],
            };
          } else if (moment(date.day.dateString).day() === 2) {
            if (dates[moment(date.day.dateString).day()]) {
              tempDay = [...dates[moment(date.day.dateString).day()]];
            }
            dates = {
              ...dates,
              [moment(date.day.dateString).day()]: [
                ...tempDay,
                moment(date.day.dateString).format(),
              ],
            };
          } else if (moment(date.day.dateString).day() === 3) {
            if (dates[moment(date.day.dateString).day()]) {
              tempDay = [...dates[moment(date.day.dateString).day()]];
            }
            dates = {
              ...dates,
              [moment(date.day.dateString).day()]: [
                ...tempDay,
                moment(date.day.dateString).format(),
              ],
            };
          } else if (moment(date.day.dateString).day() === 4) {
            if (dates[moment(date.day.dateString).day()]) {
              tempDay = [...dates[moment(date.day.dateString).day()]];
            }
            dates = {
              ...dates,
              [moment(date.day.dateString).day()]: [
                ...tempDay,
                moment(date.day.dateString).format(),
              ],
            };
          } else if (moment(date.day.dateString).day() === 5) {
            if (dates[moment(date.day.dateString).day()]) {
              tempDay = [...dates[moment(date.day.dateString).day()]];
            }
            dates = {
              ...dates,
              [moment(date.day.dateString).day()]: [
                ...tempDay,
                moment(date.day.dateString).format(),
              ],
            };
          } else if (moment(date.day.dateString).day() === 6) {
            if (dates[moment(date.day.dateString).day()]) {
              tempDay = [...dates[moment(date.day.dateString).day()]];
            }
            dates = {
              ...dates,
              [moment(date.day.dateString).day()]: [
                ...tempDay,
                moment(date.day.dateString).format(),
              ],
            };
          }
        });
      }
      console.log(updatedMarkedDates, daterange, dates, "updatedMarkedDates");
    } else if (volunteertype === "4") {
      let date = new Date().toLocaleDateString();
      let time = "07:00:00";
      updatedMarkedDates = {
        ...{
          [_selectedDay]: {
            day,
            timeIntervals: [
              {
                startTime: moment(date + " " + time).format(),
                endTime: moment(date + " " + time).format(),
              },
            ],
            breakBtwConference: 15,
            marked,
            selected: true,
            disableTouchEvent: true,
            selectedDotColor: "orange",
          },
        },
      };
    }

    // Triggers component to render again, picking up the new state
    this.setState(
      { _markedCalendarDate: updatedMarkedDates, daterange, dates },
      console.log("selected dates:", this.state._markedCalendarDate)
    );
  };
  setOkCalendarDays = () => {
    let calendarDays = {};
    if (Object.keys(this.state._markedCalendarDate).length !== 0) {
      calendarDays = {...calendarDays, ...this.state._markedCalendarDate };
    }
    console.log("setOkCalendarDays",calendarDays,this.state._renderMarkedCalendarDate, this.state._markedCalendarDate);

    this.setState({
      _renderMarkedCalendarDate: calendarDays,
      _markedCalendarDate: calendarDays,
      showMarkCalendarDateModal: false,
    });
  }
  setCancelCalendarDays = () => { 
    let calendarDays = {};
    if (Object.keys(this.state._renderMarkedCalendarDate).length !== 0 &&
      Object.keys(this.state._markedCalendarDate).length !== 0) {
      calendarDays = {...calendarDays, ...this.state._renderMarkedCalendarDate };
     }
    console.log("setCancelCalendarDays",calendarDays,this.state._renderMarkedCalendarDate, this.state._markedCalendarDate);
    this.setState({
      _markedCalendarDate: calendarDays,
      _renderMarkedCalendarDate:calendarDays,
      showMarkCalendarDateModal: false,
    });
  }
  renderMultipleDateSelectCalendar(heading) {
    return (
      <Dialog
        visible={this.state.showMarkCalendarDateModal}
        onTouchOutside={() => {
          this.setState({
            showMarkCalendarDateModal: false,
          });
        }}
        footer={
          <DialogFooter>
            <DialogButton
              text="Cancel"
              onPress={this.setCancelCalendarDays}
            />
             <DialogButton
              text="OK"
              onPress={this.setOkCalendarDays}
            />
          </DialogFooter>
        }
      >
        <DialogContent style={{ width: 300 }}>
          <View style={[styles.dialogItem, { justifyContent: "center" }]}>
            <Text>{heading}</Text>
          </View>

          <Calendar
            hideExtraDays
            minDate={_today}
            // maxDate={_maxDate}
            onDayPress={this.onDaySelect}
            markedDates={this.state._markedCalendarDate}
            markingType={this.state.dateMarkingType}
          />
        </DialogContent>
      </Dialog>
    );
  }
  renderMainWidget() {
    console.log(this.state, this.props, "renderMainWidgetPT");
    return (
      <View style={{ flex: 1 }}>
        <Spinner
          visible={this.showSpinner()}
          textContent={"Loading..."}
          textStyle={{ color: "#FFF" }}
        />
        {this.ParentChooserModal()}
        <View style={styles.container}>
          {this.RenderErrorText()}
          <ThemeProvider theme={theme}>
            <KeyboardAwareScrollView ref="scroll">
              <View style={{ marginBottom: 50 }}>
                {this._eventStepper()}
                {this.renderImageHeader(this.props.isEditable)}
                {/* //1st Section */}
                <View style={{ marginTop: 5 }}>
                  <Text style={styles.sectionTextHeading}>General Details</Text>
                  <View style={{ padding: 10 }}>
                    <Text style={styles.inputlabel}>Event name</Text>
                    <Item rounded style={styles.inputFieldCover}>
                      <Input
                        errorStyle={{ color: "red" }}
                        ref={(component) => (this._inputeventName = component)}
                        value={this.state._eventname}
                        style={styles.inputFieldStyles}
                        onChangeText={(text) =>
                          this.setState({ _eventname: text })
                        }
                      />
                    </Item>
                  </View>
                  <View style={{ padding: 10 }}>
                    <Text style={styles.inputlabel}>Description</Text>
                    <Item rounded style={styles.inputFieldCover}>
                      <Input
                        errorStyle={{ color: "red" }}
                        multiline={true}
                        value={this.state._eventdesc}
                        onChangeText={(text) =>
                          this.setState({ _eventdesc: text })
                        }
                        inputStyle={{
                          height: this.state.descHeight,
                        }}
                        style={styles.inputFieldStyles}
                        // numberOfLines={3}
                        onContentSizeChange={(e) =>
                          this.updateDescSize(e.nativeEvent.contentSize.height)
                        }
                      />
                    </Item>
                  </View>
                  <View style={{ padding: 10 }}>
                    <Text style={styles.inputlabel}>Location</Text>
                    <Item rounded style={styles.inputFieldCover}>
                      <Input
                        ref={(component) =>
                          (this._inputeventLocation = component)
                        }
                        errorStyle={{ color: "red" }}
                        value={this.state._eventlocation}
                        style={styles.inputFieldStyles}
                        onChangeText={(text) =>
                          this.setState({ _eventlocation: text })
                        }
                      />
                    </Item>
                  </View>
                  <View
                    style={{
                      justifyContent: "space-between",
                      flexDirection: "row",
                      padding: 10,
                    }}
                  >
                    <Text style={styles.generalTextView}>
                      {this.state.addAttachmentUrl && "1 file selected"}
                    </Text>
                    <Button
                      buttonStyle={{
                        width: 180,
                        backgroundColor: "#05B9A4",
                        borderRadius: 20,
                      }}
                      icon={
                        <Entypo name="attachment" size={15} color="white" />
                      }
                      title="Add Attachments"
                      onPress={this.onClickAddAttachement.bind(this)}
                    />
                  </View>
                </View>

                {/* //3rd Section */}
                <View style={{ marginTop: 5 }}>
                  <Text style={styles.sectionTextHeading}>
                    Add Group/Parents
                  </Text>
                  <TouchableOpacity
                    onPress={this.onSelectGroupsParents.bind(this)}
                  >
                    <View style={{ padding: 10 }} pointerEvents="none">
                      <Item rounded style={styles.inputFieldCover}>
                        <Input
                          placeholder="Select Group"
                          editable={false}
                          style={styles.inputFieldStyles}
                          value={this.state._selectGroupParent}
                        />
                        <Entypo
                          active
                          name="chevron-down"
                          size={24}
                          style={{ marginRight: 10, color: "black" }}
                        />
                      </Item>
                    </View>
                  </TouchableOpacity>
                  {this.renderInviteSection()}
                </View>
                {/* //4rth Section */}
                <View style={{ marginTop: 5 }}>
                  <Text style={styles.sectionTextHeading}>Notifications</Text>
                  {this.RenderSetNotifyParent()}
                  {/* {this.RenderSetNotificationDayAgo()} */}
                </View>
                {/* //5th Section */}
                <View style={{ marginTop: 5 }}>
                  <Text style={styles.sectionTextHeading}>Set Reminders</Text>
                  {this.RenderSetReminderSameDay()}
                  {this.RenderSetReminderOneDayBefore()}
                  {this.RenderSetReminderTwoDay()}
                </View>
              </View>
            </KeyboardAwareScrollView>
          </ThemeProvider>
          {this._footerTab()}
        </View>
      </View>
    );
  }
  renderMainPreviewWidget() {
    let element =
      this.state.volunteertype === "3"
        ? this.state.daterangeDate
        : this.state._markedCalendarDate;
    console.log(this.state, this.props, "renderMainPreviewWidgetPT");
    return (
      <View style={{ flex: 1 }}>
        <Spinner
          visible={this.showSpinner()}
          textContent={"Loading..."}
          textStyle={{ color: "#FFF" }}
        />
        {/* {this.ParentChooserModal()} */}
        <View style={styles.container}>
          {this.RenderErrorText()}
          <ThemeProvider theme={theme}>
            <KeyboardAwareScrollView ref="scroll">
              <View style={{ marginBottom: 20 }}>
                {this._eventStepper()}
                {this.renderImageHeader(this.props.isEditable)}

                <View style={{ paddingLeft: 10, paddingBottom: 50 }}>
                  <Text style={styles.newSectionHeading}>
                    {this.state._eventname}
                  </Text>
                  <Text style={styles.newGeneralTextView}>
                    {this.state._eventdesc}
                  </Text>
                  <View style={{ flexDirection: "row", margin: 10 }}>
                    <Entypo
                      active
                      name="location-pin"
                      color="#05B9A4"
                      size={24}
                      style={{ width: 40 }}
                    />
                    <Text style={styles.previewTextShow}>
                      {this.state._eventlocation}
                    </Text>
                  </View>
                  {this.state._grpSelect !== "" && (
                    <View style={{ flexDirection: "row", margin: 10 }}>
                      <Icon
                        active
                        name="group"
                        color="#05B9A4"
                        size={24}
                        style={{ width: 40 }}
                      />
                      <Text style={styles.previewTextShow}>
                        {this.state._grpSelect}
                      </Text>
                    </View>
                  )}
                  {Object.keys(this.state.selectedUsers).length !== 0 && (
                    <View style={{ flexDirection: "row", margin: 10 }}>
                      <MaterialIcons
                        active
                        name="group"
                        color="#05B9A4"
                        size={24}
                        style={{ width: 40 }}
                      />
                      <Text style={styles.previewTextShow}>
                        {Object.keys(this.state.selectedUsers).length} Parents
                        Selected
                      </Text>
                    </View>
                  )}
                  <View style={{ flexDirection: "row", margin: 10 }}>
                    <Icon
                      active
                      name="bell"
                      color="#FF0000"
                      size={24}
                      style={{ width: 40 }}
                    />
                    <Text style={styles.previewTextShow}>
                      {this.state._reminderCount} Reminders
                    </Text>
                  </View>
                </View>
                <View>
                  {Object.keys(element).length !== 0 ? (
                    <FlatList
                      data={Object.values(element)}
                      renderItem={this._renderSlotReminderList.bind(this)}
                      keyExtractor={(item) => item.day.dateString}
                    />
                  ) : null}
                </View>
                <View
                  style={{
                    padding: 10,
                    justifyContent: "center",
                    flexDirection: "row",
                  }}
                >
                  <Button
                    buttonStyle={{
                      width: 180,
                      backgroundColor: "#3C3989",
                      borderRadius: 20,
                    }}
                    title="Save"
                    onPress={this.addSaveEvent.bind(this)}
                  />
                </View>
              </View>
            </KeyboardAwareScrollView>
          </ThemeProvider>
          {this._footerTab()}
        </View>
      </View>
    );
  }
  _addMinutes(date, minutes) {
    console.log(date, minutes, "-----date, minutes-------");
    return moment(date).add(minutes, "minutes").format();
  }
  _formattedTime(time) {
    // Hours part from the timestamp
    let hours = new Date(time).getHours();
    // Minutes part from the timestamp
    let minutes = "0" + new Date(time).getMinutes();
    // Seconds part from the timestamp
    // let seconds = "0" + new Date(time).getSeconds();
    // Will display time in 10:30:23 format
    let newFormatTime = hours + ":" + minutes.substr(-2);
    return newFormatTime;
  }

  _handleDeleteItemFromCalendar(item, index) {
    console.log("deleting:..", index);

    let _filterMarkedCalendarDate = Object.entries(
      this.state._markedCalendarDate
    ).filter((markItem) => {
      return markItem[1].day.dateString !== item.day.dateString;
    });
    let transformMarkedCalendarDate = {};
    _filterMarkedCalendarDate.forEach((item) => {
      transformMarkedCalendarDate = {
        ...transformMarkedCalendarDate,
        ...{
          [item[0]]: {
            ...item[1],
          },
        },
      };
    });
    this.setState({
      _markedCalendarDate: transformMarkedCalendarDate,
      _renderMarkedCalendarDate:transformMarkedCalendarDate,
    });
  }

  _handleUpdateBreakBtwConference(item, time) {
    let updateBreakBtwConference = {};

    Object.entries(this.state._markedCalendarDate).forEach((element) => {
      if (element[1].day.dateString === item.day.dateString) {
        updateBreakBtwConference = {
          ...updateBreakBtwConference,
          ...{
            [element[0]]: {
              ...element[1],
              breakBtwConference: time.replace(/[^0-9]/g, ""),
            },
          },
        };
      } else {
        updateBreakBtwConference = {
          ...updateBreakBtwConference,
          ...{
            [element[0]]: {
              ...element[1],
            },
          },
        };
      }
    });
    this.setState({
      _markedCalendarDate: updateBreakBtwConference,
    });
  }

  _renderItems({ item, index }) {
    console.log("renderItems:", item, index);
    return (
      <View style={styles.elevationOnDiv}>
        <View
          style={{
            flexDirection: "row",
            padding: 5,
          }}
        >
          <View style={{ flexDirection: "row" }}>
            <Text
              style={{ fontSize: 24, fontWeight: "bold", color: "#05B9A4" }}
            >
              {this.getMonth(item.day.dateString) + " "}
            </Text>
            <Text
              style={{ fontSize: 24, fontWeight: "bold", color: "#05B9A4" }}
            >
              {new Date(item.day.dateString).getDate() + " "}
            </Text>
            <Text style={{ fontSize: 24, color: "#05B9A4" }}>
              {new Date(item.day.dateString).getFullYear() + " "}
            </Text>
          </View>

          <View style={{ justifyContent: "center" }}>
            <Icon
              active
              name="trash-o"
              size={24}
              style={{ marginLeft: 10, color: "black" }}
              onPress={this._handleDeleteItemFromCalendar.bind(
                this,
                item,
                index
              )}
            />
          </View>
        </View>
        {this.state.volunteertype !== "4" && (
          <View>
            {item.timeIntervals &&
              item.timeIntervals.map((date, indexO) => {
                console.log(date, "slot---->");
                return (
                  <View
                    style={{
                      flexDirection: "row",
                      justifyContent: "space-between",
                      padding: 5,
                    }}
                    key={"slot" + indexO}
                  >
                    <View
                      style={{
                        flexDirection: "row",
                        justifyContent: "space-between",
                        flex: 1,
                      }}
                    >
                      <Text style={{ color: "black", fontWeight: "bold" }}
                        onPress={() => {
                          this.setState({
                            slotTimeIntervalPickerVisible: true,
                            _selectedTimeInterval: { identifier: "startTime", slot: item, index:indexO }
                          })
                        }}>
                        {miscutils.formatTimeinAMPM(date.startTime) + " "}
                      </Text>

                      <View
                        style={{
                          justifyContent: "center",
                          paddingLeft: 4,
                          paddingRight: 4,
                        }}
                      >
                        <Divider
                          style={{
                            backgroundColor: "#000",
                            opacity: 0.3,
                            width: 50,
                            height: 2,
                          }}
                        />
                      </View>
                      <Text style={{ color: "black", fontWeight: "bold" }}
                       onPress={() => {
                        this.setState({
                          slotTimeIntervalPickerVisible: true,
                          _selectedTimeInterval: { identifier: "endTime", slot: item, index:indexO }
                        })}}>
                        {miscutils.formatTimeinAMPM(date.endTime) + " "}
                      </Text>
                    </View>

                    <View
                      style={{
                        flexDirection: "row",
                        justifyContent: "flex-end",
                        width: 60,
                      }}
                    >
                      <Icon
                        active
                        name="trash-o"
                        size={24}
                        style={{ marginRight: 6, color: "black" }}
                        onPress={this._handleDeleteBreakbtwConference.bind(
                          this,
                          item,
                          date,
                          indexO
                        )}
                      />
                    </View>
                  </View>
                );
              })}

            <View style={{ justifyContent: "center", padding: 10 }}>
              <Text style={{ color: "black", textAlign: "center" }}>
                Breaks Between Conferences
              </Text>
            </View>
            <View
              style={{
                justifyContent: "center",
                flexDirection: "row",
                marginBottom: 5,
              }}
            >
              <TextInput
                value={item.breakBtwConference.toString()}
                keyboardType="numeric"
                style={{
                  color: "black",
                  borderRadius: 10,
                  height: 40,
                  borderWidth: 1,
                  padding: 10,
                  borderColor: "#ededed",
                }}
                maxLength={4} //setting limit of input
                onChangeText={(time) =>
                  this._handleUpdateBreakBtwConference(item, time)
                }
              />
              <Text
                style={{
                  color: "black",
                  textAlign: "center",
                  alignSelf: "center",
                }}
              >
                Mins
              </Text>
            </View>
            <View
              style={{
                justifyContent: "center",
                flexDirection: "row",
              }}
            >
              <Button
                buttonStyle={{
                  width: 140,
                  backgroundColor: "#3C3989",
                  borderRadius: 20,
                }}
                title="Add Time"
                onPress={this._addTimeByDefinedInterval.bind(this, item, index)}
              />
            </View>
          </View>
        )}
      </View>
    );
  }
  _handleChangeSlotTimeInterval=()=> {
    const {identifier, slot, index:childIndex}=this.state._selectedTimeInterval
    let addConferenceInDay = {};
     console.log(childIndex,this.state.slotTimeIntervalTime, "_handleChangeSlotTimeInterval");
    Object.entries(this.state._markedCalendarDate).forEach((element) => {
      if (element[0] === slot.day.dateString) {
        let tempTimeInterval = [];
        if (element[1].timeIntervals.length !== 0) {
          element[1].timeIntervals.forEach((timeInt, index) => {
            if (index === childIndex) {
              tempTimeInterval = [...tempTimeInterval, {
                ...timeInt,
                [identifier]: moment(`${element[0]} ${this.state.slotTimeIntervalTime}`).format()
              }]
            }else{ tempTimeInterval=[...tempTimeInterval,timeInt]}
          })
        }
        addConferenceInDay = {
          ...addConferenceInDay,
          ...{
            [element[0]]: {
              ...element[1],
              timeIntervals: tempTimeInterval,
            },
          },
        };
      }else {
        addConferenceInDay = {
          ...addConferenceInDay,
          ...{
            [element[0]]: {
              ...element[1],
            },
          },
        };
      }
    });
     console.log(addConferenceInDay, "addConferenceInDay");
    this.setState({
      _markedCalendarDate: addConferenceInDay,
    });
  }
  _renderDateRangeItems({ item, index }) {
    console.log("renderItems:", item, index);
    return (
      <View style={styles.elevationOnDiv}>
        <View
          style={{
            flexDirection: "row",
            padding: 5,
          }}
        >
          <View style={{ flexDirection: "row" }}>
            <Text
              style={{ fontSize: 24, fontWeight: "bold", color: "#05B9A4" }}
            >
              {this.getMonth(item.from) + " "}
            </Text>
            <Text
              style={{ fontSize: 24, fontWeight: "bold", color: "#05B9A4" }}
            >
              {new Date(item.from).getDate() + " "}
            </Text>
            <Text style={{ fontSize: 24, color: "#05B9A4" }}>
              {new Date(item.from).getFullYear() + " "}
            </Text>
          </View>
          <Text>-</Text>
          <View style={{ flexDirection: "row" }}>
            <Text
              style={{ fontSize: 24, fontWeight: "bold", color: "#05B9A4" }}
            >
              {this.getMonth(item.to) + " "}
            </Text>
            <Text
              style={{ fontSize: 24, fontWeight: "bold", color: "#05B9A4" }}
            >
              {new Date(item.to).getDate() + " "}
            </Text>
            <Text style={{ fontSize: 24, color: "#05B9A4" }}>
              {new Date(item.to).getFullYear() + " "}
            </Text>
          </View>
        </View>

        {item.timeIntervals &&
          item.timeIntervals.map((date, indexO) => {
            console.log(date, "slot---->");
            return (
              <View
                style={{
                  flexDirection: "row",
                  justifyContent: "space-between",
                  padding: 5,
                }}
                key={"slot" + indexO}
              >
                <View
                  style={{
                    flexDirection: "row",
                    justifyContent: "space-between",
                    flex: 1,
                  }}
                >
                  <Text style={{ color: "black", fontWeight: "bold" }}>
                    {miscutils.formatTimeinAMPM(date.startTime) + " "}
                  </Text>

                  <View
                    style={{
                      justifyContent: "center",
                      paddingLeft: 4,
                      paddingRight: 4,
                    }}
                  >
                    <Divider
                      style={{
                        backgroundColor: "#000",
                        opacity: 0.3,
                        width: 50,
                        height: 2,
                      }}
                    />
                  </View>
                  <Text style={{ color: "black", fontWeight: "bold" }}>
                    {miscutils.formatTimeinAMPM(date.endTime) + " "}
                  </Text>
                </View>

                <View
                  style={{
                    flexDirection: "row",
                    justifyContent: "flex-end",
                    width: 60,
                  }}
                >
                  <Icon
                    active
                    name="trash-o"
                    size={24}
                    style={{ marginRight: 6, color: "black" }}
                    onPress={this._handleDltBreakbtwConferenceDateRange.bind(
                      this,
                      item,
                      date,
                      indexO
                    )}
                  />
                </View>
              </View>
            );
          })}

        <View style={{ justifyContent: "center", padding: 10 }}>
          <Text style={{ color: "black", textAlign: "center" }}>
            Breaks Between Conferences
          </Text>
        </View>
        <View
          style={{
            justifyContent: "center",
            flexDirection: "row",
            marginBottom: 5,
          }}
        >
          <TextInput
            value={item.breakBtwConference.toString()}
            keyboardType="numeric"
            style={{
              color: "black",
              borderRadius: 10,
              height: 40,
              borderWidth: 1,
              padding: 10,
              borderColor: "#ededed",
            }}
            maxLength={4} //setting limit of input
            onChangeText={(time) =>
              this._handleUpdBreakBtwConferenceDateRange(item, time)
            }
          />
          <Text
            style={{ color: "black", textAlign: "center", alignSelf: "center" }}
          >
            Mins
          </Text>
        </View>
        <View
          style={{
            justifyContent: "center",
            flexDirection: "row",
          }}
        >
          <Button
            buttonStyle={{
              width: 140,
              backgroundColor: "#3C3989",
              borderRadius: 20,
            }}
            title="Add Time"
            onPress={this._addTimeByDefinedIntervalDateRange.bind(
              this,
              item,
              index
            )}
          />
        </View>
      </View>
    );
  }

  _handleDltBreakbtwConferenceDateRange(item, date, index) {
    console.log("deleting:..", index);
    let modifyMarkedCalendarDate = {};
    let filterDeleteBreakbtwConference = item.timeIntervals.filter(
      (filtertime) => {
        return filtertime.startTime !== date.startTime;
      }
    );
    modifyMarkedCalendarDate = {
      ...modifyMarkedCalendarDate,
      ...{
        ...this.state.daterange,
        timeIntervals: filterDeleteBreakbtwConference,
      },
    };
    this.setState({
      daterange: modifyMarkedCalendarDate,
    });
  }
  _handleUpdBreakBtwConferenceDateRange(item, time) {
    let updateBreakBtwConference = {};
    updateBreakBtwConference = {
      ...updateBreakBtwConference,
      ...{
        ...this.state.daterange,
        breakBtwConference: time.replace(/[^0-9]/g, ""),
      },
    };
    this.setState({
      daterange: updateBreakBtwConference,
    });
  }
  _addTimeByDefinedIntervalDateRange(item, index) {
    let addConferenceInDay = {};
    console.log(item, index);
    addConferenceInDay = {
      ...addConferenceInDay,
      ...{
        ...this.state.daterange,
        timeIntervals: [
          ...item.timeIntervals,
          {
            startTime: item.timeIntervals.slice(-1)[0]
              ? this._addMinutes(
                  new Date(item.timeIntervals.slice(-1)[0].endTime),
                  Number(item.breakBtwConference)
                )
              : new Date(),
            endTime: item.timeIntervals.slice(-1)[0]
              ? this._addMinutes(
                  new Date(item.timeIntervals.slice(-1)[0].endTime),
                  Number(item.breakBtwConference) + 30
                )
              : this._addMinutes(new Date(), Number(30)),
          },
        ],
      },
    };
    this.setState({
      daterange: addConferenceInDay,
    });
  }
  _addTimeByDefinedInterval(item, index) {
    let addConferenceInDay = {};
    console.log(item, index);

    Object.entries(this.state._markedCalendarDate).forEach((element) => {
      if (element[1].day.dateString === item.day.dateString) {
        addConferenceInDay = {
          ...addConferenceInDay,
          ...{
            [element[0]]: {
              ...element[1],
              timeIntervals: [
                ...item.timeIntervals,
                {
                  startTime: item.timeIntervals.slice(-1)[0]
                    ? this._addMinutes(
                        new Date(item.timeIntervals.slice(-1)[0].endTime),
                        Number(item.breakBtwConference)
                      )
                    : new Date(),
                  endTime: item.timeIntervals.slice(-1)[0]
                    ? this._addMinutes(
                        new Date(item.timeIntervals.slice(-1)[0].endTime),
                        Number(item.breakBtwConference) + 30
                      )
                    : this._addMinutes(new Date(), Number(30)),
                },
              ],
            },
          },
        };
      } else {
        addConferenceInDay = {
          ...addConferenceInDay,
          ...{
            [element[0]]: {
              ...element[1],
            },
          },
        };
      }
    });
    this.setState({
      _markedCalendarDate: addConferenceInDay,
    });
  }

  _handleDeleteBreakbtwConference(item, date, index) {
    console.log("deleting:..", index);
    let modifyMarkedCalendarDate = {};

    Object.entries(this.state._markedCalendarDate).forEach((dateItem) => {
      if (dateItem[1].day.dateString === item.day.dateString) {
        let filterDeleteBreakbtwConference = dateItem[1].timeIntervals.filter(
          (filtertime) => {
            return filtertime.startTime !== date.startTime;
          }
        );
        modifyMarkedCalendarDate = {
          ...modifyMarkedCalendarDate,
          ...{
            [dateItem[0]]: {
              ...dateItem[1],
              timeIntervals: filterDeleteBreakbtwConference,
            },
          },
        };
      } else {
        modifyMarkedCalendarDate = {
          ...modifyMarkedCalendarDate,
          ...{
            [dateItem[0]]: {
              ...dateItem[1],
            },
          },
        };
      }
    });

    this.setState({
      _markedCalendarDate: modifyMarkedCalendarDate,
    });
  }

  renderMainDateTimeWidget() {
    return (
      <View style={{ flex: 1 }}>
        <Spinner
          visible={this.showSpinner()}
          textContent={"Loading..."}
          textStyle={{ color: "#FFF" }}
        />
        <DateTimePicker
          mode="time"
          display="calendar" // 'default', 'spinner', 'calendar', 'clock' // Android Only
          minuteInterval={5}
          isVisible={this.state.slotTimeIntervalPickerVisible}
          onConfirm={(dt) => {
            this.handleConfirmTime(dt, "slotTimeIntervalTime");
          }}
          onCancel={() =>
            this.setState({ slotTimeIntervalPickerVisible: false })
          }
          titleIOS={"Pick a Time"}
        />
        <View style={styles.container}>
          {this.RenderErrorText()}
          {this._eventStepper()}
          {this.renderDateTimeTabs()}
          {this._footerTab()}
        </View>
      </View>
    );
  }

  renderDateTimeTabs() {
    return (
      <Container>
        <Tabs
          initialPage={Number(this.state.volunteertype) - 1}
          tabBarUnderlineStyle={{ backgroundColor: "#5078F2" }}
          onChangeTab={(val) => {
            const { i: volunteertype } = val;
            let dateMarkingType = "simple";
            if (String(volunteertype) === "2") {
              dateMarkingType = "period";
            }

            this.setState(
              {
                volunteerDetail: {
                  ...this.state.volunteerDetail,
                  volunteertype: String(Number(volunteertype) + 1),
                },
                volunteertype: String(Number(volunteertype) + 1),
                dateMarkingType,
                _markedCalendarDate: {},
                _renderMarkedCalendarDate:{},
              },
              () => {
                if (
                  Object.keys(this.state.daterangeDate).length !== 0 &&
                  Object.keys(this.state._markedCalendarDate).length === 0 &&
                  this.state.volunteertype === "3"
                ) {
                  this.setState({
                    _markedCalendarDate: { ...this.state.daterangeDate },
                  });
                }
              }
            );
          }}
        >
          <Tab
            tabStyle={{ backgroundColor: "#fff" }}
            textStyle={{ color: "#000", fontSize: 13 }}
            activeTabStyle={{ backgroundColor: "#fff" }}
            activeTextStyle={{
              color: "#5078F2",
              borderBottomColor: "#5078F2",
              fontSize: 13,
            }}
            heading={"Single Date"}
          >
            {this.renderSelectSingleDateTime()}
          </Tab>
          <Tab
            tabStyle={{ backgroundColor: "#fff" }}
            textStyle={{ color: "#000", fontSize: 13 }}
            activeTabStyle={{ backgroundColor: "#fff" }}
            activeTextStyle={{
              color: "#5078F2",
              borderBottomColor: "#5078F2",
              fontSize: 13,
            }}
            heading={"Multiple Dates"}
          >
            {this.renderSelectMultipleDateTime()}
          </Tab>
          <Tab
            tabStyle={{ backgroundColor: "#fff" }}
            textStyle={{ color: "#000", fontSize: 13 }}
            activeTabStyle={{ backgroundColor: "#fff" }}
            activeTextStyle={{
              color: "#5078F2",
              borderBottomColor: "#5078F2",
              fontSize: 13,
            }}
            heading={"Date Range"}
          >
            {this.renderSelectDateTimeRange()}
          </Tab>
          <Tab
            tabStyle={{ backgroundColor: "#fff" }}
            textStyle={{ color: "#000", fontSize: 13 }}
            activeTabStyle={{ backgroundColor: "#fff" }}
            activeTextStyle={{
              color: "#5078F2",
              borderBottomColor: "#5078F2",
              fontSize: 13,
            }}
            heading={"No Date"}
          >
            {this.renderSelectNoDateTime()}
          </Tab>
        </Tabs>
      </Container>
    );
  }
  renderSelectSingleDateTime() {
    return (
      <ThemeProvider theme={theme}>
        <KeyboardAwareScrollView ref="scroll">
          <View
            style={{
              marginTop: 10,
              paddingBottom: 20,
            }}
          >
            <View style={{ alignItems: "center" }}>
              <Text style={styles.sectionTextHeading}>
                Select Single Date {"&"} Time
              </Text>
            </View>
            <View
              style={{
                padding: 10,
                justifyContent: "center",
                flexDirection: "row",
              }}
            >
              <Button
                buttonStyle={{
                  width: 180,
                  backgroundColor: "#3C3989",
                  borderRadius: 20,
                }}
                title="+ Add Date/Time"
                onPress={() =>
                  this.setState({ showMarkCalendarDateModal: true })
                }
              />
            </View>

            <View style={{ padding: 5 }}>
              {Object.values(this.state._markedCalendarDate).length !== 0 ? (
                <FlatList
                  data={Object.values(this.state._markedCalendarDate)}
                  renderItem={this._renderItems.bind(this)}
                  keyExtractor={(item) => item.day.dateString}
                />
              ) : null}
            </View>
            {this.renderMultipleDateSelectCalendar("Select Single Date")}
          </View>
        </KeyboardAwareScrollView>
      </ThemeProvider>
    );
  }
  renderSelectMultipleDateTime() {
    return (
      <ThemeProvider theme={theme}>
        <KeyboardAwareScrollView ref="scroll">
          <View
            style={{
              marginTop: 10,
              paddingBottom: 20,
            }}
          >
            <View style={{ alignItems: "center" }}>
              <Text style={styles.sectionTextHeading}>
                Select Multiple Date {"&"} Time
              </Text>
            </View>
            <View
              style={{
                padding: 10,
                justifyContent: "center",
                flexDirection: "row",
              }}
            >
              <Button
                buttonStyle={{
                  width: 180,
                  backgroundColor: "#3C3989",
                  borderRadius: 20,
                }}
                title="+ Add Date/Time"
                onPress={() =>
                  this.setState({ showMarkCalendarDateModal: true })
                }
              />
            </View>

            <View style={{ padding: 5 }}>
              {Object.keys(this.state._markedCalendarDate).length !== 0 ? (
                <FlatList
                  data={Object.values(this.state._markedCalendarDate)}
                  renderItem={this._renderItems.bind(this)}
                  keyExtractor={(item) => item.day.dateString}
                />
              ) : null}
            </View>
            {this.renderMultipleDateSelectCalendar("Select Multiple Dates")}
          </View>
        </KeyboardAwareScrollView>
      </ThemeProvider>
    );
  }

  renderSelectDateTimeRange() {
    return (
      <ThemeProvider theme={theme}>
        <KeyboardAwareScrollView ref="scroll">
          <View
            style={{
              marginTop: 10,
              paddingBottom: 20,
            }}
          >
            <View style={{ alignItems: "center" }}>
              <Text style={styles.sectionTextHeading}>
                Select Recuring days, Range Date {"&"} Time
              </Text>
            </View>
            <TouchableOpacity
              onPress={this._handleChangeRecurringValue.bind(this)}
            >
              <View style={{ padding: 10 }} pointerEvents="none">
                <Item rounded style={styles.inputFieldCover}>
                  <Input
                    placeholder="Select Recuring days"
                    editable={false}
                    style={styles.inputFieldStyles}
                    value={this.state.recurringText}
                  />
                  <Entypo
                    active
                    name="chevron-down"
                    size={24}
                    style={{ marginRight: 10, color: "black" }}
                  />
                </Item>
              </View>
            </TouchableOpacity>
            {this.state.selectRecurring[this.state.recurringText] === 0 && (
              <View
                style={{
                  flexDirection: "row",
                  justifyContent: "center",
                  flexWrap: "wrap",
                  padding: 10,
                }}
              >
                {this.state.selectedDays.map((day, idx) => (
                  <View
                    style={{ flexDirection: "row", marginTop: 5 }}
                    key={day.name + idx}
                  >
                    <CheckBox
                      checked={day.checked}
                      name={day.name}
                      onPress={(e) => this._handleChangeSelectedDays(e, day)}
                    />
                    <Text
                      style={{ color: "#000", paddingLeft: 15 }}
                      onPress={(e) => this._handleChangeSelectedDays(e, day)}
                    >
                      {day.label}
                    </Text>
                  </View>
                ))}
              </View>
            )}

            <View
              style={{
                padding: 10,
                justifyContent: "center",
                flexDirection: "row",
              }}
            >
              <Button
                buttonStyle={{
                  width: 180,
                  backgroundColor: "#3C3989",
                  borderRadius: 20,
                }}
                title="+ Add Date/Time"
                onPress={() =>
                  this.setState({ showMarkCalendarDateModal: true })
                }
              />
            </View>

            <View style={{ padding: 5 }}>
              {Object.keys(this.state.daterange).length !== 0
                ? this._renderDateRangeItems({
                    item: this.state.daterange,
                    index: 0,
                  })
                : null}
              {Object.keys(this.state.selectedDays).length !== 0
                ? this.renderRecurringDayOfDaterange()
                : null}
            </View>
            {this.renderMultipleDateSelectCalendar("Select Date Range")}
          </View>
        </KeyboardAwareScrollView>
      </ThemeProvider>
    );
  }

  renderSelectNoDateTime() {
    return (
      <ThemeProvider theme={theme}>
        <KeyboardAwareScrollView ref="scroll">
          <View
            style={{
              marginTop: 10,
              paddingBottom: 20,
            }}
          >
            <View style={{ alignItems: "center" }}>
              <Text style={styles.sectionTextHeading}>Select No Date</Text>
            </View>
            <View
              style={{
                padding: 10,
                justifyContent: "center",
                flexDirection: "row",
              }}
            >
              <Button
                buttonStyle={{
                  width: 180,
                  backgroundColor: "#3C3989",
                  borderRadius: 20,
                }}
                title="+ Add Date/Time"
                onPress={() =>
                  this.setState({ showMarkCalendarDateModal: true })
                }
              />
            </View>

            <View style={{ padding: 5 }}>
              {Object.keys(this.state._markedCalendarDate).length !== 0 ? (
                <FlatList
                  data={Object.values(this.state._markedCalendarDate)}
                  renderItem={this._renderItems.bind(this)}
                  keyExtractor={(item) => item.day.dateString}
                />
              ) : null}
            </View>
            {this.renderMultipleDateSelectCalendar("Select No Date")}
          </View>
        </KeyboardAwareScrollView>
      </ThemeProvider>
    );
  }
  renderRecurringDayOfDaterange() {
    return (
      <>
        {this.state.selectedDays.map((recurring, idx1) => {
          if (recurring.checked) {
            return (
              <View key={recurring.name + idx1}>
                {Object.keys(this.state.dates).length !== 0 &&
                  this.state.dates[recurring.name] &&
                  this.state.dates[recurring.name].map((item) => (
                    <View style={styles.elevationOnDiv} key={item}>
                      <View style={{ flexDirection: "row" }}>
                        <Text style={{ color: "#000", flex: 1 }}>
                          {moment(item).format("MMMM DD, YYYY")}
                        </Text>
                        <Text style={{ color: "#000", flex: 1 }}>
                          {moment(item).format("dddd")}
                        </Text>
                        <View style={{ justifyContent: "center", flex: 1 }}>
                          <Icon
                            active
                            name="trash-o"
                            size={24}
                            style={{ marginLeft: 10, color: "black" }}
                            onPress={this.handleDeleteRecurringDayOfDaterange.bind(
                              this,
                              recurring
                            )}
                          />
                        </View>
                      </View>
                    </View>
                  ))}
              </View>
            );
          }
        })}
      </>
    );
  }

  renderMainSlotReminderWidget() {
    return (
      <View style={{ flex: 1 }}>
        <Spinner
          visible={this.showSpinner()}
          textContent={"Loading..."}
          textStyle={{ color: "#FFF" }}
        />
        {this.renderErrorModalOfDaytimeSelect()}
        {this.slotReminderInputModal()}
        <View style={styles.container}>
          {this.RenderErrorText()}
          <ThemeProvider theme={theme}>
            <KeyboardAwareScrollView ref="scroll">
              {this._eventStepper()}
              {this.renderSlotReminderBody()}
            </KeyboardAwareScrollView>
          </ThemeProvider>
          {this._footerTab()}
        </View>
      </View>
    );
  }

  renderSlotReminderBody() {
    console.log(
      this.state._markedCalendarDate,
      this.state.daterangeDate,
      "renderSlotReminderBody"
    );
    let element =
      this.state.volunteertype === "3"
        ? this.state.daterangeDate
        : this.state._markedCalendarDate;
    return (
      <View style={{ padding: 5 }}>
        <TouchableOpacity
          style={{ alignItems: "flex-end", margin: 5 }}
          onPress={this.handleClickAddEditSpot.bind(this)}
        >
          <Text style={{ color: "#3C3989" }}>+Add/Edit Spots</Text>
        </TouchableOpacity>
        <View>
          {Object.keys(element).length !== 0 ? (
            <FlatList
              data={Object.values(element)}
              renderItem={this._renderSlotReminderList.bind(this)}
              keyExtractor={(item) => item.day.dateString}
            />
          ) : null}
        </View>
      </View>
    );
  }

  _renderSlotReminderList({ item, index }) {
    console.log(item, index, "_renderSlotReminderList");

    return (
      <View style={styles.slotReminderContainer}>
        <View
          style={{
            flexDirection: "row",
            padding: 5,
          }}
        >
          <Text
            style={{
              fontSize: 24,
              fontWeight: "bold",
              color: "#05B9A4",
            }}
          >
            {this.getMonth(item.day.dateString) + " "}
          </Text>
          <Text
            style={{
              fontSize: 24,
              fontWeight: "bold",
              color: "#05B9A4",
            }}
          >
            {new Date(item.day.dateString).getDate() + " "}
          </Text>
          <Text style={{ fontSize: 24, color: "#05B9A4" }}>
            {new Date(item.day.dateString).getFullYear() + " "}
          </Text>
        </View>
        <View>
          {item.timeIntervals.map((slot, idx) => {
            console.log(slot, "_renderSlotReminderList");
            return (
              <View key={"slot" + idx} style={{ flex: 1 }}>
                <View style={styles.slotItemColorGrey}>
                  <View style={styles.slotItemInnerView}>
                    <Text style={{ color: "#000000", fontWeight: "600" }}>
                      {miscutils.formatTimeinAMPM(slot.startTime) + " "}
                    </Text>

                    <View
                      style={{
                        justifyContent: "center",
                        paddingLeft: 4,
                        paddingRight: 4,
                      }}
                    >
                      <Divider style={styles.slotDivider} />
                    </View>
                    <Text style={{ color: "#000000", fontWeight: "600" }}>
                      {miscutils.formatTimeinAMPM(slot.endTime) + " "}
                    </Text>
                  </View>
                </View>
                {slot.spot &&
                  slot.spot.map((spot, index) => (
                    <View
                      style={{
                        flex: 1,
                        flexDirection: "row",
                        alignItems: "center",
                        padding: 5,
                      }}
                      key={"spot" + index}
                    >
                      <Text style={{ color: "#000" }}>{index + 1}.</Text>
                      <View
                        // index % 2 === 0 ? styles.slotItemColor : styles.slotItemColorGrey
                        style={{
                          flex: 1,
                          flexDirection: "row",
                          justifyContent: "space-between",
                          padding: 5,
                          paddingLeft: 10,
                          paddingRight: 10,
                        }}
                      >
                        <Text style={{ color: "#000000", fontWeight: "600" }}>
                          {spot.description}
                        </Text>

                        <Text style={{ color: "#909090" }}>
                          0 of {spot.person}
                        </Text>

                        <Text style={{ color: "#000000", fontWeight: "600" }}>
                          {spot.comment}
                        </Text>
                      </View>
                      {this.state._eventStep === 3 && (
                    <Button
                      disabled
                      disabledStyle={{
                        width: 80,
                        padding:6,
                        backgroundColor: "#3C3989",
                        borderRadius: 20,
                      }}
                      disabledTitleStyle={{
                        fontSize: 14,
                      }}
                      title="Sign Up"
                    />
                  )}
                    </View>
                  ))}
              </View>
            );
          })}
        </View>
      </View>
    );
  }

  onSelectPersonsForSpot(name, idx) {
    Picker.init({
      pickerData: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13],
      pickerTitleText: "",
      pickerBg: [255, 255, 255, 1],
      pickerCancelBtnText: "Cancel",
      pickerConfirmBtnText: "Select",
      selectedValue: [this.state.spot.persons],
      onPickerConfirm: (data) => {
        this.handleSpotReminderModify(name, data[0], idx);
      },
    });
    Picker.show();
  }

  onSelectSpotTypeForSpot(name, idx) {
    Picker.init({
      pickerData: ["Bring", "Todo"],
      pickerBg: [255, 255, 255, 1],
      pickerTitleText: "",
      pickerCancelBtnText: "Cancel",
      pickerConfirmBtnText: "Select",
      selectedValue: [this.state._spotType],
      onPickerConfirm: (data) => {
        this.handleSpotReminderModify(name, data[0], idx);
      },
    });
    Picker.show();
  }

  renderAddSpotListItem(spotItem, idx) {
    let items = [];
    if (this.state.volunteertype === "3") {
      Object.values(this.state.daterangeDate).forEach((day) => {
        day.timeIntervals.forEach((item) => {
          items = [...items, { id: item.id, name: item.name }];
        });
      });
    } else {
      Object.values(this.state._markedCalendarDate).forEach((day) => {
        day.timeIntervals.forEach((item) => {
          items = [...items, { id: item.id, name: item.name }];
        });
      });
    }

    console.log(items, "renderAddSpotListItem", spotItem, idx);
    return (
      <View style={styles.slotReminderModalBody} key={"spot" + idx}>
        <View style={styles.slotReminderSpotItem}>
          <TouchableOpacity
            style={{ flex: 1 }}
            onPress={this.onSelectPersonsForSpot.bind(this, "person", idx)}
          >
            <View pointerEvents="none" style={{ flex: 1 }}>
              <Item style={{ flex: 1 }}>
                <Input
                  placeholder="Persons"
                  editable={false}
                  value={spotItem.person}
                  style={[styles.inputFieldStyles]}
                />
              </Item>
            </View>
          </TouchableOpacity>
          <Text style={styles.spotItemDefaultText}>Person To</Text>
          <TouchableOpacity
            style={{ flex: 1 }}
            onPress={this.onSelectSpotTypeForSpot.bind(this, "type", idx)}
          >
            <View pointerEvents="none" style={{ flex: 1 }}>
              <Item style={{ flex: 1 }}>
                <Input
                  placeholder="Type"
                  editable={false}
                  value={spotItem.type}
                  style={[styles.inputFieldStyles]}
                />
              </Item>
            </View>
          </TouchableOpacity>
        </View>

        <View style={styles.slotReminderSpotItem}>
          <Item style={{ flex: 1 }}>
            <Input
              placeholder="What would you like"
              errorStyle={{ color: "red" }}
              ref={(component) => (this._inputeventName = component)}
              value={spotItem.description}
              style={[styles.inputFieldStyles]}
              onChangeText={(text) => {
                this.handleSpotReminderModify("description", text, idx);
              }}
            />
          </Item>
        </View>
        <Text style={{ color: "red" }}>{spotItem.errorDescription}</Text>
        <View style={styles.slotReminderSpotItem}>
          <Text style={styles.spotItemDefaultText}>on</Text>
          <View style={{ flex: 1 }}>
            <SectionedMultiSelect
              ref={(SectionedMultiSelect) =>
                (this.SectionedMultiSelect = SectionedMultiSelect)
              }
              items={items}
              uniqueKey="id"
              selectText={this.state.spot[idx].dates.length===items.length?"All dates":`${this.state.spot[idx].dates.length} dates/time`}
              alwaysShowSelectText
              showDropDowns
              showChips={false}
              hideSearch
              onSelectedItemsChange={(selectedItems) => {
                this.hanldeSelectDatesItemChange(selectedItems, idx);
              }}
              selectedItems={this.state.spot[idx].dates}
            />
          </View>
        </View>

        <View style={styles.slotReminderSpotItem}>
          <Item>
            <Input
              placeholder="Comments"
              multiline={true}
              value={spotItem.comment}
              onChangeText={(text) =>
                this.handleSpotReminderModify("comment", text, idx)
              }
              inputStyle={{
                height: this.state.descHeight,
              }}
              style={styles.inputFieldStyles}
              onContentSizeChange={(e) =>
                this.updateDescSize(e.nativeEvent.contentSize.height)
              }
            />
          </Item>
        </View>
        <View style={{ alignItems: "center", marginTop: 5 }}>
          <Icon
            active
            name="trash-o"
            size={24}
            style={{ color: "black" }}
            onPress={() => {
              this.handleDeleteSpotItemFromList(idx);
            }}
          />
        </View>

        <Divider
          style={{
            marginTop: 20,
            marginBottom: 20,
          }}
        />
      </View>
    );
  }
  renderErrorModalOfDaytimeSelect() {
    return (
      <Modal
        isVisible={this.state.errorDateTimeSelectModal.isvisible}
        onBackButtonPress={() => {
          this.setState({
            errorDateTimeSelectModal: {
              ...this.state.errorDateTimeSelectModal,
              isvisible: false,
            },
          });
        }}
        onBackdropPress={() => {
          this.setState({
            errorDateTimeSelectModal: {
              ...this.state.errorDateTimeSelectModal,
              isvisible: false,
            },
          });
        }}
      >
        <View style={{ backgroundColor: "#fff", padding: 20, borderRadius: 4 }}>
          <Text
            style={{
              color: "red",
              fontSize: 20,
              fontWeight: "bold",
              textAlign: "center",
              marginBottom: 10,
            }}
          >
            Error!
          </Text>
          <Text
            style={{
              color: "#000",
              fontSize: 16,
              textAlign: "center",
              marginBottom: 10,
            }}
          >
            {this.state.errorDateTimeSelectModal.message}
          </Text>
          <Button
            title="Hide modal"
            onPress={() => {
              this.setState({
                errorDateTimeSelectModal: {
                  ...this.state.errorDateTimeSelectModal,
                  isvisible: false,
                },
              });
            }}
          />
        </View>
      </Modal>
    );
  }
  slotReminderInputModal() {
    return (
      <Modal
        isVisible={this.state.slotReminderInputModalShow}
        onBackButtonPress={() => {
          this.setState({ slotReminderInputModalShow: false });
        }}
        style={styles.slotReminderModalView}
        // onBackdropPress={() => {
        //   this.setState({ slotReminderInputModalShow: false });
        // }}
        // onSwipeComplete={() =>
        //   this.setState({ slotReminderInputModalShow: false })
        // }
        // swipeDirection="left"
      >
        <ThemeProvider theme={theme}>
          <KeyboardAwareScrollView ref="scroll">
            <View>
              <View style={styles.slotReminderModalHeader}>
                <Text style={styles.slotReminderModalHeaderText}>
                  Add Spots
                </Text>
              </View>
              <View>
                {this.state.spot &&
                  this.state.spot.map((spotItem, idx) =>
                    this.renderAddSpotListItem(spotItem, idx)
                  )}
              </View>
              <View style={styles.slotReminderModalBtn}>
                <Button
                  buttonStyle={{
                    width: 35,
                    height: 35,
                    backgroundColor: "#05B9A4",
                    borderRadius: 50,
                  }}
                  title="+"
                  onPress={() => {
                    this.setState(
                      {
                        spot: [...this.state.spot, dafaultSpotSelection],
                      },
                      () => {
                        this.handleAddSpotReminderIntoModal();
                      }
                    );
                  }}
                />
              </View>
              <View style={styles.slotReminderModalBtn}>
                <Button
                  buttonStyle={{
                    width: 140,
                    backgroundColor: "#3C3989",
                    borderRadius: 20,
                  }}
                  title="Done"
                  onPress={this.hanldeAddSpotIntoVolunteerSlots.bind(this)}
                />
              </View>
            </View>
          </KeyboardAwareScrollView>
        </ThemeProvider>
        </Modal>
      );
  }
  handleAddSpotReminderIntoModal() {
    let element =
      this.state.volunteertype === "3"
        ? this.state.daterangeDate
        : this.state._markedCalendarDate;
    let _markedCalendarDate = {};
    let dates = [];
    let spot = [];
    let date = "0.0";
    console.log(
      this.state._markedCalendarDate,
      element,
      this.state,
      "handleClickAddEditSpot"
    );
    if (Object.keys(element).length) {
      Object.values(element).forEach((item, idx) => {
        let timeIntervals = [];
        item.timeIntervals.forEach((slot, index) => {
          dates = [...dates, date];
          timeIntervals = [
            ...timeIntervals,
            {
              ...slot,
              id: date,
              name: `${item.day.dateString} @ ${moment(slot.startTime).format(
                "h:mm a"
              )}-${moment(slot.endTime).format("h:mm a")}`,
            },
          ];
          console.log("handleClickAddEditSpot", timeIntervals);
          date = Number(Number(date) + 0.1).toFixed(1);
        });

        _markedCalendarDate = {
          ..._markedCalendarDate,
          [item.day.dateString]: { ...item, timeIntervals },
        };

        date = String((idx + 1).toFixed(1));
      });
      if (this.state.spot.length > 0) {
        this.state.spot.forEach((spotItem) => {
          spot = [...spot, { ...spotItem, dates }];
        });
      }
      if (this.state.volunteertype === "3") {
        this.setState({ daterangeDate: _markedCalendarDate, spot });
      } else {
        this.setState({ _markedCalendarDate, spot });
      }
    }
    console.log(this.state, "handleAddSpotReminderIntoModal");
  }

  handleTransformDaterangeData() {
    let tempDatesRecurring = {};
    let tempDaterangeDate = {};
    this.state.selectedDays.forEach((day) => {
      if (day.checked && this.state.dates[day.name]) {
        tempDatesRecurring = {
          ...tempDatesRecurring,
          [day.name]: this.state.dates[day.name],
        };
      }
    });

    Object.values(this.state._markedCalendarDate).forEach((item) => {
      Object.values(tempDatesRecurring).forEach((recurring) => {
        recurring.forEach((date) => {
          if (moment(item.day.dateString).format() === moment(date).format()) {
            tempDaterangeDate = {
              ...tempDaterangeDate,
              [item.day.dateString]: {
                ...item,
                ...this.state.daterange,
              },
            };
          }
        });
      });
    });
    this.setState({ daterangeDate: { ...tempDaterangeDate } }, () => {
      this.handleAddSpotReminderIntoModal();
    });
    console.log(
      tempDatesRecurring,
      this.state,
      tempDaterangeDate,
      "tempDatesRecurring"
    );
  }

  handleClickAddEditSpot() {
    this.setState({ slotReminderInputModalShow: true });
  }

  handleSpotReminderModify(name, data, idx) {
    console.log(this, name, data, idx, "handleSpotReminderModify--->");
    let spot = [];
    if (this.state.spot.length > 0) {
      this.state.spot.forEach((spotItem, origIdx) => {
        if (idx === origIdx) {
          spot = [...spot, { ...spotItem, [name]: data }];
        } else {
          spot = [...spot, { ...spotItem }];
        }
      });
      this.setState({ spot });
    }
  }

  handleDeleteSpotItemFromList(idx) {
    console.log(this, idx, "handleDeleteSpotItemFromList---->");
    if (this.state.spot.length > 0) {
      let spot = this.state.spot.filter((spotItem, OrigIdx) => idx !== OrigIdx);
      this.setState({ spot });
    }
  }
  hanldeSelectDatesItemChange(selectedItems, idx) {
    let spot = [];
    this.state.spot.forEach((item, OrigIdx) => {
      if (OrigIdx === idx) {
        spot = [...spot, { ...item, dates: selectedItems }];
      } else {
        spot = [...spot, { ...item }];
      }
    });
    this.setState({ spot });
    console.log(selectedItems, spot, "handleClickAddEditSpot");
  }

  hanldeAddSpotIntoVolunteerSlots() {
    let tempSpot = [];
    this.state.spot.forEach((spotItem) => {
      if (spotItem.description === "") {
        tempSpot = [...tempSpot, { ...spotItem, errorDescription: "required" }];
      } else {
        tempSpot = [...tempSpot, { ...spotItem, errorDescription: "" }];
      }
    });
    this.setState({ spot: tempSpot }, () => {
      let spot = this.state.spot.filter((item) => item.description === "");
      console.log(spot, "hanldeAddSpotIntoVolunteerSlots");
      if (!spot.length) {
        let element =
          this.state.volunteertype === "3"
            ? this.state.daterangeDate
            : this.state._markedCalendarDate;
        let _markedCalendarDate = {};
        if (Object.values(element).length) {
          Object.values(element).forEach((item) => {
            let timeIntervals = [];
            item.timeIntervals.forEach((slot) => {
              let spotList = [];
              this.state.spot.forEach((spot) => {
                if (spot.dates.includes(slot.id)) {
                  spotList = [...spotList, spot];
                } else {
                  spotList = [...spotList];
                }
              });
              timeIntervals = [...timeIntervals, { ...slot, spot: spotList }];
            });

            _markedCalendarDate = {
              ..._markedCalendarDate,
              [item.day.dateString]: { ...item, timeIntervals },
            };
          });
        }
        console.log(
          "hanldeAddSpotIntoVolunteerSlots",
          this.state.spot,
          _markedCalendarDate
        );
        if (this.state.volunteertype === "3") {
          this.setState({
            slotReminderInputModalShow: false,
            daterangeDate: _markedCalendarDate,
          });
        } else {
          this.setState({
            slotReminderInputModalShow: false,
            _markedCalendarDate,
          });
        }
      }
      console.log(this.state, "_footerTab");
      if (this.state._eventStep === 2) {
        Object.values(this.state._markedCalendarDate).forEach((date) => {
          date.timeIntervals.forEach((interval) => {
            this.setState({
              isAtleastOneSpotSelect: interval.hasOwnProperty("spot"),
            });
          });
        });
      }
    });
  }

  _handleChangeRecurringValue() {
    const values = {
      "Repeat every week on the days": 0,
      "Repeat every month with same numerics days": 1,
      "Repeat every month with same relative days": 2,
    };
    Picker.init({
      pickerData: Object.keys(values),
      pickerTitleText: "",
      pickerBg: [255, 255, 255, 1],
      pickerCancelBtnText: "Cancel",
      pickerConfirmBtnText: "Select",
      selectedValue: [this.state.recurringText],
      onPickerConfirm: (data) => {
        this.setState({
          selectRecurring: { [data[0]]: values[data[0]] },
          recurringText: data[0],
        });
      },
    });
    Picker.show();
  }

  _handleChangeSelectedDays(event, selectDay) {
    let selectDaysList = [];
    this.state.selectedDays.forEach((day) => {
      if (day.name === selectDay.name) {
        selectDaysList = [...selectDaysList, { ...day, checked: !day.checked }];
      } else {
        selectDaysList = [...selectDaysList, { ...day }];
      }
    });
    this.setState({ selectedDays: selectDaysList });
  }

  handleDeleteRecurringDayOfDaterange(recurringItem) {
    console.log(this, recurringItem, "delete--->");
    let tempSelectedDays = [];
    this.state.selectedDays.forEach((item) => {
      if (item.name === recurringItem.name && item.checked) {
        tempSelectedDays = [...tempSelectedDays, { ...item, checked: false }];
      } else {
        tempSelectedDays = [...tempSelectedDays, { ...item }];
      }
    });
    this.setState({ selectedDays: tempSelectedDays });
  }
  updateDescSize = (height) => {
    if (height < 100) return;
    this.setState({
      descHeight: height,
    });
  };

  handleConfirmTime(dt, type) {
    var time = dt.toLocaleTimeString([], {
      hour: "numeric",
      minute: "numeric",
    });
    if (type === "slotTimeIntervalTime") {
      this.setState({
        slotTimeIntervalPickerVisible: false,
        slotTimeIntervalTime: time,
      }, () => {
        this._handleChangeSlotTimeInterval()
      });
    }
  }

  handleConfirm(dt, type) {
    var newDate = new Date(dt);
    var date =
      newDate.getDate() < 10 ? "0" + newDate.getDate() : newDate.getDate();
    var month = newDate.getMonth() + 1;
    month = month < 10 ? "0" + month : month;
    var dtstr = month + "/" + date + "/" + newDate.getFullYear();
    if (type === "startDate") {
      this.setState({
        startDatePickerVisible: false,
        _eventstartDate: dtstr,
        _eventstartDateObj: newDate,
        _eventendDate: dtstr,
        _eventendDateObj: newDate,
      });
    }
    if (type === "endDate") {
      this.setState({
        endDatePickerVisible: false,
        _eventendDate: dtstr,
        _eventendDateObj: newDate,
      });
    }
  }

  getMonth(datestr) {
    const monthNames = [
      "Jan",
      "Feb",
      "Mar",
      "Apr",
      "May",
      "Jun",
      "Jul",
      "Aug",
      "Sep",
      "Oct",
      "Nov",
      "Dec",
    ];
    let dt = new Date(datestr);
    return monthNames[dt.getMonth()];
  }

  getFullDay(datestr) {
    const monthNames = [
      "Jan",
      "Feb",
      "Mar",
      "Apr",
      "May",
      "Jun",
      "Jul",
      "Aug",
      "Sep",
      "Oct",
      "Nov",
      "Dec",
    ];
    let dt = new Date(datestr);
    return (
      monthNames[dt.getMonth()] + " " + dt.getDate() + " " + dt.getFullYear()
    );
  }

  onNavigationURLChange(event) {
    if (
      !(
        event.url.startsWith("about:blank") ||
        event.url.startsWith("data:text/html") ||
        event.url.startsWith("file://")
      )
    ) {
      this.eventView.stopLoading();
      Linking.openURL(event.url).catch((err) => {});
    }
  }

  getChildIdtoSignUp() {
    return undefined;
  }

  async getUserResponsesforEvent4() {
    var response = await eventsapi.getEventResponses(
      this.props.eventid,
      this.props.user.token
    );
    if (response.data.statusCode === 200) {
      console.log("-----------------");
      console.log(response.data);
      this.setState({
        event4data: response.data.result.userResponse,
      });
    } else {
      Alert.alert(
        "Error",
        "There was an error loading data for this event. Please try again"
      );
    }
  }

  async postsignupSpotEvent4(spot, childid) {
    console.log(spot);
    console.log(childid);
    var event = this.props.events.events[this.props.eventid];
    var response = await eventsapi.volunteerforEvent(
      this.props.eventid,
      event.eventtype,
      childid,
      spot.id,
      this.props.user.token
    );
    if (response.data.statusCode === 200) {
      //Navigation.dismissAllModals()
      //this.props.getEventsAction(this.props.user.token, this.props.user.groups)
      this.getUserResponsesforEvent4();
    } else {
      Alert.alert(
        "Error",
        "There was an error saving your Signup Preference. Please try again"
      );
    }
  }

  async postsignupSlotEvent3(slot, childid) {
    console.log(slot);
    console.log(childid);
    var event = this.props.events.events[this.props.eventid];
    var response = await eventsapi.signupforChild(
      this.props.eventid,
      event.eventtype,
      childid,
      slot.slotId,
      this.props.user.token
    );
    if (response.data.statusCode === 200) {
      //Navigation.dismissAllModals()
      this.props.getEventsAction(this.props.user.token, this.props.user.groups);
    } else {
      Alert.alert(
        "Error",
        "There was an error saving your Signup Preference. Please try again"
      );
    }
  }

  signupSlotEvent3(slot) {
    console.log(slot);
    //var childid = this.getChildIdtosignupSlotEvent3()
    var children = {};
    console.log(this.props);
    console.log(this.props.children_groups);

    if (this.props.eventdata.groupid != null) {
      if (this.props.user.roles !== "parent") {
        if (this.props.eventdata.groupid in this.props.children_groups) {
          children = this.props.children_groups[this.props.eventdata.groupid];
        }
      } else {
        if (this.props.eventdata.groupid in this.props.children_groups) {
          var children_list = Object.keys(
            this.props.children_groups[this.props.eventdata.groupid]
          );
          console.log(children_list);
          this.props.user.children_array.forEach((childid) => {
            console.log(childid);
            console.log(children_list);
            if (children_list.indexOf(childid.toString()) >= 0) {
              console.log("here");
              console.log(childid);
              children[childid] = this.props.children_groups[
                this.props.eventdata.groupid
              ][childid];
            }
          });
          console.log(children);
          console.log(Object.keys(children).length);
          if (Object.keys(children).length === 1) {
            console.log("Signing up !!!!");
            this.postsignupSlotEvent3(slot, Object.keys(children)[0]);
            return;
          }
        }
      }
    } else {
      console.log("here2222");
      if (this.props.user.children_array.length === 1) {
        this.postsignupSlotEvent3(slot, this.props.user.children_array[0]);
        return;
      } else {
        this.props.user.children.forEach((child) => {
          child["id"] = child.childid; // Silly thing to do but userpref has diff key then cache
          children[child.childid] = child;
        });
      }
    }
    console.log(children);
    if (Object.keys(children).length === 0) {
      Alert.alert(
        "Error Selecting a Child",
        "Hmmm ... it seems you dont have a child linked to this group. Please contact the School Administrator"
      );
      return;
    }
    this.setState({
      childDialogModalData: children,
      childDialogModal: true,
      childDialogModalselection: null,
      type3signupSlot: slot,
    });

    console.log("done");
  }
  //TODO : Need to optomize both the signup functions so code can be reused
  signupSpotEvent4(spot) {
    console.log(spot);
    //var childid = this.getChildIdtosignupSlotEvent3()
    var children = {};
    console.log(this.props);
    console.log(this.props.children_groups);

    if (this.props.eventdata.groupid != null) {
      if (this.props.user.roles !== "parent") {
        if (this.props.eventdata.groupid in this.props.children_groups) {
          children = this.props.children_groups[this.props.eventdata.groupid];
        }
      } else {
        if (this.props.eventdata.groupid in this.props.children_groups) {
          var children_list = Object.keys(
            this.props.children_groups[this.props.eventdata.groupid]
          );
          console.log(children_list);
          this.props.user.children_array.forEach((childid) => {
            console.log(childid);
            console.log(children_list);
            if (children_list.indexOf(childid.toString()) >= 0) {
              console.log("here");
              console.log(childid);
              children[childid] = this.props.children_groups[
                this.props.eventdata.groupid
              ][childid];
            }
          });
          console.log(children);
          console.log(Object.keys(children).length);
          if (Object.keys(children).length === 1) {
            console.log("Signing up !!!!");
            this.postsignupSpotEvent4(spot, Object.keys(children)[0]);
            return;
          }
        }
      }
    } else {
      if (this.props.user.children_array.length === 1) {
        this.postsignupSpotEvent4(spot, this.props.user.children_array[0]);
        return;
      } else {
        this.props.user.children.forEach((child) => {
          child["id"] = child.childid; // Silly thing to do but userpref has diff key then cache
          children[child.childid] = child;
        });
      }
    }
    console.log(children);
    if (children.length === 0) {
      Alert.alert(
        "Error Selecting a Child",
        "Hmmm ... it seems you dont have a child linked to signup. Please contact the School Administrator"
      );
      return;
    }
    this.setState({
      childDialogModalData: children,
      childDialogModal: true,
      childDialogModalselection: null,
      type4signupSlot: spot,
    });

    console.log("done");
  }

  async removeChildfromEvent(slot) {
    console.log(slot);
    var response = await eventsapi.deleteSignupforChild(
      this.props.eventid,
      slot.childid,
      slot.slotId,
      this.props.user.token
    );
    if (response.data.statusCode === 200) {
      //Navigation.dismissAllModals()
      this.props.getEventsAction(this.props.user.token, this.props.user.groups);
    } else {
      Alert.alert(
        "Error",
        "There was an error saving your RSVP preference. Please try again"
      );
    }
    console.log("done");
  }

  async removeChildfromType4Event(spotid, childid) {
    var response = await eventsapi.deleteVolunteerSignupforChild(
      this.props.eventid,
      childid,
      spotid,
      this.props.user.token
    );
    if (response.data.statusCode === 200) {
      //Navigation.dismissAllModals()
      //this.props.getEventsAction(this.props.user.token, this.props.user.groups)
      this.getUserResponsesforEvent4();
    } else {
      Alert.alert(
        "Error",
        "There was an error saving your RSVP preference. Please try again"
      );
    }
    console.log("done");
  }

  updateRSVPIndex(rsvpIndex) {
    this.setState({
      rsvpIndex: rsvpIndex,
    });
  }
  async confirmRSVP() {
    var event = this.props.events.events[this.props.eventid];
    var attend = rsvpButtons[this.state.rsvpIndex];
    console.log(this.adultselector);
    var response = await eventsapi.sendUserResponse(
      event.event_id,
      event.eventtype,
      attend,
      this.state.rsvpIndex == 0
        ? this.adultselector.state.value.toString()
        : -1,
      this.state.rsvpIndex == 0 ? this.kidsselector.state.value.toString() : -1,
      this.props.user.token
    );

    console.log(response);
    if (response.data.statusCode === 200) {
      this.props.clearLastRefreshTime();
      Navigation.dismissAllModals();
    } else {
      Alert.alert(
        "Error",
        "There was an error saving your RSVP preference. Please try again"
      );
    }
  }

  RenderGroupIcon(grpid) {
    if (this.props.user.groupphotohash === null) {
      return null;
    }
    if (grpid !== "") {
      if (
        this.props.user.groupphotohash[grpid] === undefined ||
        this.props.user.groupphotohash[grpid] === ""
      ) {
        var photourl = require("../img/groups.png");
        return (
          <Image
            style={styles.groupicon}
            imageStyle={{
              width: 40,
              height: 40,
              borderRadius: 20,
              backgroundColor: "white",
              paddingBottom: 20,
            }}
            source={photourl}
          ></Image>
        );
      } else {
        var photourl = {
          uri: this.props.user.groupphotohash[grpid].replace("http:", "https:"),
        };
        return (
          <CustomCachedImage
            component={Image}
            defaultSource={require("../img/groups.png")}
            imageStyle={{
              width: 40,
              height: 40,
              borderRadius: 20,
              backgroundColor: "white",
            }}
            style={styles.groupicon}
            source={photourl}
          ></CustomCachedImage>
        );
      }
    } else {
      return null;
    }
  }

  render() {
    console.log("---- IN RENDER -----");
    if (this.state.isEditable || this.props.openReadOnly) {
      switch (this.state._eventStep) {
        case 0:
          return <View style={{ flex: 1 }}>{this.renderMainWidget()}</View>;
        case 1:
          return (
            <View style={{ flex: 1 }}>{this.renderMainDateTimeWidget()}</View>
          );
        case 2:
          return (
            <View style={{ flex: 1 }}>
              {this.renderMainSlotReminderWidget()}
            </View>
          );
        case 3:
          return (
            <View style={{ flex: 1 }}>{this.renderMainPreviewWidget()}</View>
          );
      }
    }
  }

  readFile(filePath) {
    console.log(filePath);
    RNFetchBlob.fs.exists(filePath).then((exist) => {
      console.log(`file ${exist ? "" : "not"} exists`);
    });
    return RNFetchBlob.fs
      .readFile(filePath, "base64")
      .then((data) => new Buffer(data, "base64"));
  }

  transformMarkCalendarToVolunteer() {
    let element =
      this.state.volunteertype === "3"
        ? this.state.daterangeDate
        : this.state._markedCalendarDate;
    let volunteer = [];
    if (Object.values(element).length > 0) {
      Object.values(element).forEach((item) => {
        let slotTime = [];
        item.timeIntervals.forEach((time) => {
          if (time.spot.length > 0) {
            slotTime = [
              ...slotTime,
              {
                start_time: moment(time.startTime).toISOString(),
                end_time: moment(time.endTime).toISOString(),
                spot: time.spot,
              },
            ];
          } else {
            slotTime = [
              ...slotTime,
              {
                start_time: moment(time.startTime).toISOString(),
                end_time: moment(time.endTime).toISOString(),
              },
            ];
          }
        });
        volunteer = [
          ...volunteer,
          { event_date: moment(item.day.dateString).toISOString(), slotTime },
        ];
      });
    }
    return volunteer;
  }
  transformDataToPayload() {
    let daterange = {};
    let selectedDays = [];
    let slotTime = [];
    let dates = {};
    if (Object.keys(this.state.daterange).length) {
      this.state.daterange.timeIntervals.forEach((slot) => {
        slotTime = [...slotTime, { ...slot, spot: [] }];
      });
      daterange = {
        ...daterange,
        from: this.state.daterange.from,
        to: this.state.daterange.to,
      };
    }
    this.state.selectedDays.forEach((selectDay) => {
      if (selectDay.checked) {
        selectedDays = [...selectedDays, selectDay.name];
      }
    });
    Object.values(this.state.dates).forEach((dateList) => {
      let tempDateList = [];
      dateList.forEach((date) => {
        Object.values(this.state.daterangeDate).forEach((dateRange) => {
          if (moment(dateRange.day.dateString).format() === date) {
            tempDateList = [...tempDateList, date];
            if (moment(date).day() === 0) {
              dates = { ...dates, [moment(date).day() + 7]: tempDateList };
            } else {
              dates = { ...dates, [moment(date).day()]: tempDateList };
            }
          }
        });
      });
    });
    return { daterange, selectedDays, slotTime, dates };
  }

  async addSaveEvent() {
    let volunteer = this.transformMarkCalendarToVolunteer();
    let volunteerType3Payload = this.transformDataToPayload();
    const { selectedDays, slotTime, daterange, dates } = volunteerType3Payload;
    console.log(
      volunteer,
      volunteerType3Payload,
      this.state,
      this.state._markedCalendarDate,
      this.state.spot,
      "volunteer------------"
    );
    console.log(
      this.state,
      retval,
      "--------------retValue-",
      this.props.eventid
    );
    let retval = this.formisValid();

    if (retval) {
      console.log("here");
      console.log(this.state.coverimage);
      this.setState({ updatingImage: true });
      this.setState({ errorText: "" });
      if (this.state.coverimage !== null) {
        var response = await this.readFile(this.state.coverimage.path).then(
          (buffer) => {
            Storage.put(Date.now(), buffer).then((response) => {
              var url = response["key"];
              if (url !== undefined) {
                if (this.props.eventid === null) {
                  this.props.addEventAction(
                    this.state,
                    this.state.selectedUsers,
                    this.props.user.display_name,
                    this.props.user.token,
                    url,
                    this.props.user.groups,
                    this.props.eventtype,
                    null,
                    null,
                    volunteer,
                    this.state.spot,
                    this.state.volunteertype,
                    this.state.daterangetype,
                    daterange,
                    selectedDays,
                    slotTime,
                    dates
                  );
                } else {
                  this.props.addEventAction(
                    this.state,
                    this.state.selectedUsers,
                    this.props.user.display_name,
                    this.props.user.token,
                    url,
                    this.props.user.groups,
                    this.props.eventtype,
                    this.props.eventid,
                    parentTeacherSlots
                  );
                }
                this.setState({ updatingImage: false });

                Navigation.dismissAllModals();
              }
            }, this);
          },
          this
        );
      } else {
        if (this.props.eventid === null) {
          this.props.addEventAction(
            this.state,
            this.state.selectedUsers,
            this.props.user.display_name,
            this.props.user.token,
            null,
            this.props.user.groups,
            this.props.eventtype,
            null,
            null,
            volunteer,
            this.state.spot,
            this.state.volunteertype,
            this.state.daterangetype,
            daterange,
            selectedDays,
            slotTime,
            dates
          );
        } else {
          this.props.addEventAction(
            this.state,
            this.state.selectedUsers,
            this.props.user.display_name,
            this.props.user.token,
            this.props.eventdata.eventimgurl,
            this.props.user.groups,
            this.props.eventtype,
            this.props.eventid,
            parentTeacherSlots
          );
        }
        this.setState({ updatingImage: false });
        Navigation.dismissAllModals();
      }
    }
  }

  RenderDeleteButton() {
    if (this.state.isEditable && this.props.eventid !== null) {
      return (
        <TouchableOpacity onPress={this.deleteEvent.bind(this)}>
          <View style={[commonStyle.buttonAcross, { backgroundColor: "red" }]}>
            <Text style={commonStyle.buttonTextAcross}>Delete Event</Text>
          </View>
        </TouchableOpacity>
      );
    } else {
      return null;
    }
  }

  formisValid() {
    //**** Event Name ****
    console.log(this.state);
    var retval = this.validateElement(
      this.state._eventname,
      this._inputeventName,
      "Please enter an Event Name",
      true
    );
    // retval =
    //   retval &&
    //   this.validateElement(
    //     this.state._eventstartDate,
    //     null,
    //     "Please enter an Event Start Date",
    //     false
    //   );
    // retval =
    //   retval &&
    //   this.validateElement(
    //     this.state._eventendDate,
    //     null,
    //     "Please enter an Event End Date",
    //     false
    //   );
    // if (!this.state._alldayevent) {
    //   retval =
    //     retval &&
    //     this.validateElement(
    //       this.state._eventstartTime,
    //       null,
    //       "Please enter an Event Start Time",
    //       false
    //     );
    //   retval =
    //     retval &&
    //     this.validateElement(
    //       this.state._eventendTime,
    //       null,
    //       "Please enter an Event End Time",
    //       false
    //     );
    // }
    // Ensure end date is after start date
    var sdate = new Date(this.state._eventstartDateObj).getTime();
    var edate = new Date(this.state._eventendDateObj).getTime();
    // if (edate - sdate <= -86400000) {
    //   retval = false;
    //   this.setState({
    //     errorText: "Please Ensure End Date of Event is after the Start Date"
    //   });
    // }

    if (retval && this.state._showParents) {
      if (Object.keys(this.state.selectedUsers).length === 0) {
        retval = false;
        this.setState({ errorText: "Please Select atleast 1 Parent" });
      }
    } else {
      retval =
        retval &&
        this.validateElement(
          this.state._grpSelectId,
          null,
          "Please enter a Group",
          false
        );
    }
    console.log(retval);
    return retval;
  }

  RenderSaveButton() {
    if (this.state.isEditable && this.props.eventid !== undefined) {
      return (
        <TouchableOpacity
          onPress={this.addSaveEvent.bind(this)}
          style={{ marginBottom: 5, marginTop: 5 }}
        >
          <View style={commonStyle.buttonAcross}>
            <Text style={commonStyle.buttonTextAcross}>Save Event</Text>
          </View>
        </TouchableOpacity>
      );
    } else {
      return null;
    }
  }

  deleteEvent(event) {
    this.props.deleteEventAction(
      this.props.eventid,
      this.props.user.token,
      this.props.user.groups
    );
    Navigation.dismissAllModals();
  }

  RenderInviteSwitch() {
    if (this.state.isEditable) {
      return (
        <View style={{ flexDirection: "row" }}>
          <Text style={styles.switchText}>Invite Parents Individually ...</Text>
          <Switch
            style={styles.switchStyle}
            editable={this.state.isEditable}
            value={this.state._showParents}
            onValueChange={(val) => {
              this.setState({ _showParents: val });
              if (!val) {
                var grp = Object.keys(this.state.groupshash)[0];
                this.setState({
                  selectedUsers: {},
                  _grpSelect: grp,
                  _grpSelectId: this.state.groupshash[grp],
                });
              }
            }}
          />
        </View>
      );
    } else {
      return null;
    }
  }

  RenderAllDaySwitch() {
    if (this.state.isEditable) {
      return (
        <View
          style={{
            padding: 10,
            flexDirection: "row",
            justifyContent: "space-between",
          }}
        >
          <Text style={styles.switchText}>All Day Event</Text>
          <Switch
            style={styles.switchStyle}
            trackColor={{ true: "#777CF7", false: "grey" }}
            thumbColor={this.state._alldayevent ? "#3C3989" : null}
            editable={this.state.isEditable}
            label="All Day Event"
            value={this.state._alldayevent}
            onValueChange={this.onAllDayValueChange.bind(this)}
          />
        </View>
      );
    } else {
      return null;
    }
  }

  RenderSetNotificationDayAgo() {
    if (this.state.isEditable) {
      return (
        <TouchableOpacity
          onPress={this.onSelectNoticationDayAgo.bind(this)}
          // style={{ flexDirection: "row" }}
        >
          <View style={{ padding: 10 }} pointerEvents="none">
            <Item rounded style={styles.inputFieldCover}>
              <Input
                placeholder="Select Notification"
                editable={false}
                style={styles.inputFieldStyles}
                value={this.state.selectNoticationDayAgo}
              />
              <Entypo
                active
                name="chevron-down"
                size={24}
                style={{ marginRight: 10, color: "black" }}
              />
            </Item>
          </View>
        </TouchableOpacity>
      );
    } else {
      return null;
    }
  }
  RenderSetNotifyParent() {
    if (this.state.isEditable) {
      return (
        <View
          style={{
            padding: 10,
            flexDirection: "row",
            justifyContent: "space-between",
          }}
        >
          <Text style={styles.switchText}>
            Notify Parents once the event is created
          </Text>
          <Switch
            style={styles.switchStyle}
            trackColor={{ true: "#777CF7", false: "grey" }}
            thumbColor={this.state._eventnotify ? "#3C3989" : null}
            editable={this.state.isEditable}
            value={this.state._eventnotify}
            onValueChange={(val) => {
              this.setState({ _eventnotify: val });
            }}
          />
        </View>
      );
    } else {
      return null;
    }
  }
  RenderSetReminderSameDay() {
    if (this.state.isEditable) {
      return (
        <View
          style={{
            padding: 10,
            flexDirection: "row",
            justifyContent: "space-between",
          }}
        >
          <Text style={styles.switchText}>Remind Same Day </Text>
          <Switch
            style={styles.switchStyle}
            trackColor={{ true: "#777CF7", false: "grey" }}
            thumbColor={this.state._remindsameday ? "#3C3989" : null}
            editable={this.state.isEditable}
            value={this.state._remindsameday}
            onValueChange={(val) => {
              if (val)
                this.setState({
                  _remindsameday: val,
                  _reminderCount: this.state._reminderCount + 1,
                });
              else {
                this.setState({
                  _remindsameday: val,
                  _reminderCount: this.state._reminderCount - 1,
                });
              }
            }}
          />
        </View>
      );
    } else {
      return null;
    }
  }

  RenderSetReminderOneDayBefore() {
    if (this.state.isEditable) {
      return (
        <View
          style={{
            padding: 10,
            flexDirection: "row",
            justifyContent: "space-between",
          }}
        >
          <Text style={styles.switchText}>Remind one day before</Text>
          <Switch
            style={styles.switchStyle}
            trackColor={{ true: "#777CF7", false: "grey" }}
            thumbColor={this.state._remindsOneday ? "#3C3989" : null}
            editable={this.state.isEditable}
            value={this.state._remindsOneday}
            onValueChange={(val) => {
              this.setState({ _remindsOneday: val });
            }}
          />
        </View>
      );
    } else {
      return null;
    }
  }

  RenderSetReminderTwoDay() {
    if (this.state.isEditable) {
      return (
        <View
          style={{
            padding: 10,
            flexDirection: "row",
            justifyContent: "space-between",
          }}
        >
          <Text style={styles.switchText}>Remind Two Days Before</Text>
          <Switch
            style={styles.switchStyle}
            trackColor={{ true: "#777CF7", false: "grey" }}
            thumbColor={this.state._remindsTwoday ? "#3C3989" : null}
            editable={this.state.isEditable}
            value={this.state._remindsTwoday}
            onValueChange={(val) => {
              this.setState({ _remindsTwoday: val });
            }}
          />
        </View>
      );
    } else {
      return null;
    }
  }

  renderStartTime() {
    if (!this.state._alldayevent) {
      return (
        <View style={{ padding: 10 }}>
          <Item
            rounded
            style={styles.inputFieldCover}
            onPress={() => this.setState({ startTimePickerVisible: true })}
          >
            <Input
              placeholder="Start Time"
              errorStyle={{ color: "red" }}
              value={this.state._eventstartTime}
              editable={false}
              style={styles.inputFieldStyles}
            />
            <Icon
              active
              name="clock-o"
              size={24}
              style={{ marginRight: 10, color: "black" }}
              onPress={() => this.setState({ startTimePickerVisible: true })}
            />
          </Item>
        </View>
      );
    } else {
      return null;
    }
  }
  renderEndTime() {
    if (!this.state._alldayevent) {
      return (
        <View style={{ padding: 10 }}>
          <Item
            rounded
            style={styles.inputFieldCover}
            onPress={() => this.setState({ endTimePickerVisible: true })}
          >
            <Input
              placeholder="End Time"
              errorStyle={{ color: "red" }}
              value={this.state._eventendTime}
              editable={false}
              style={styles.inputFieldStyles}
            />
            <Icon
              active
              name="clock-o"
              size={24}
              style={{ marginRight: 10, color: "black" }}
              onPress={() => this.setState({ endTimePickerVisible: true })}
            />
          </Item>
        </View>
      );
    } else {
      return null;
    }
  }

  DateTimeFormat(date, mode) {
    if (!date) return "";
    let value = date.toLocaleTimeString([], {
      day: "2-digit",
      month: "2-digit",
      year: "2-digit",
      hour: "2-digit",
      minute: "2-digit",
    });
    return value;
  }

  TimeFormat(date, mode) {
    if (!date) return "";
    let value = date.toLocaleTimeString([], {
      hour: "numeric",
      minute: "numeric",
    });
    return value;
  }

  onAllDayValueChange(value) {
    this.setState({ _alldayevent: value });
    if (value == true) {
      this.setState({ _eventstartTime: "", _eventendTime: "" });
    }
  }

  renderInviteSection() {
    var userids = Object.keys(this.state.selectedUsers);
    var selectedMemberList = userids.map(function (userKey, i) {
      var user = this.state.selectedUsers[userKey];
      return (
        <Text key={user.username} style={styles.selectedlisttext}>
          {user.display_name}
        </Text>
      );
    }, this);
    var grps = this.getGroupData();
    if (this.props.user.groups !== undefined) {
      if (this.state._selectGroupParent === "Group") {
        return (
          <TouchableOpacity
            onPress={this.onGroupPicker.bind(this)}
            // style={{ flexDirection: "row" }}
          >
            {/* <Text style={styles.groupSelectText}>Select Group</Text>
            <Text style={styles.groupSelectTextSelected}>
              {this.state._grpSelect}
            </Text> */}
            <View style={{ padding: 10 }} pointerEvents="none">
              <Item rounded style={styles.inputFieldCover}>
                <Input
                  placeholder="Select Group"
                  editable={false}
                  style={styles.inputFieldStyles}
                  value={this.state._grpSelect}
                />
                <Entypo
                  active
                  name="chevron-down"
                  size={24}
                  style={{ marginRight: 10, color: "black" }}
                />
              </Item>
            </View>
          </TouchableOpacity>
        );
      } else if (this.state._selectGroupParent === "Parents") {
        // console.log(selectedMemberList, "selectedMemberList");
        return (
          <View>
            <TouchableOpacity onPress={() => this.showParentChooser()}>
              <View style={{ padding: 10 }} pointerEvents="none">
                <Item rounded style={styles.inputFieldCover}>
                  <Input
                    placeholder=" Select Parents to Invite ..."
                    editable={false}
                    style={styles.inputFieldStyles}
                    value={`Select Parents (${selectedMemberList.length} Selected)`}
                  />
                  <Entypo
                    active
                    name="chevron-down"
                    size={24}
                    style={{ marginRight: 10, color: "black" }}
                  />
                </Item>
              </View>
            </TouchableOpacity>
            <View style={styles.addcontainer}>{selectedMemberList}</View>
          </View>
        );
      }
    }
  }

  onGroupPicker(event) {
    var groupsdict = this.state.groupshash;
    console.log(groupsdict, "groupsdict");
    Picker.init({
      pickerData: Object.keys(groupsdict),
      pickerTitleText: "Select a Group",
      pickerCancelBtnText: "Cancel",
      pickerConfirmBtnText: "Select",
      selectedValue: [this.state._grpSelect],
      onPickerConfirm: (data) => {
        this.setState({
          _grpSelect: data[0],
          _grpSelectId: this.state.groupshash[data[0]],
        });
        //this.props.setGroupid(this.getSeletedGroupID(), data[0])
      },
    });
    Picker.show();
  }

  onSelectGroupsParents(event) {
    Picker.init({
      pickerData: ["Group", "Parents"],
      pickerTitleText: "",
      pickerCancelBtnText: "Cancel",
      pickerConfirmBtnText: "Select",
      selectedValue: [this.state._selectGroupParent],
      onPickerConfirm: (data) => {
        if (data[0] === "Parents") {
          this.setState({ _showParents: true, _selectGroupParent: data[0] });
        } else {
          var grp = Object.keys(this.state.groupshash)[0];
          this.setState({
            _selectGroupParent: data[0],
            selectedUsers: {},
            _grpSelect: grp,
            _grpSelectId: this.state.groupshash[grp],
          });
        }
      },
    });
    Picker.show();
  }
  onSelectNoticationDayAgo(event) {
    Picker.init({
      pickerData: ["One day ago", "Two day ago", "Three day ago"],
      pickerTitleText: "",
      pickerCancelBtnText: "Cancel",
      pickerConfirmBtnText: "Select",
      selectedValue: [this.state.selectNoticationDayAgo],
      onPickerConfirm: (data) => {
        this.setState({ selectNoticationDayAgo: data[0] });
      },
    });
    Picker.show();
  }
  GetFormattedDate(dt) {
    var newDate = new Date(dt);
    var date =
      newDate.getUTCDate() < 10
        ? "0" + newDate.getUTCDate()
        : newDate.getUTCDate();
    var month = newDate.getUTCMonth() + 1;
    month = month < 10 ? "0" + month : month;
    var dtstr = month + "/" + date + "/" + newDate.getUTCFullYear();
    return dtstr;
  }

  onSelectedList(selectedUsers) {
    console.log(selectedUsers, "selectedUsers");
    this.setState({
      selectedUsers: selectedUsers,
    });
  }

  onValueChange(value) {
    this.setState({ _showParents: !value });
    if (value === false) {
      this.setState({ selectedUsers: {} });
    }
  }

  validateEndDate(value) {
    return new Date();
  }

  handleFormChange(formData) {
    this.setState({ formData: formData });
    this.props.onFormChange && this.props.onFormChange(formData);
  }
}

var styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: "column",
    justifyContent: "center",
    backgroundColor: "white",
  },
  eventDateView: {
    marginTop: 10,
  },
  errorText: {
    color: "red",
    backgroundColor: "white",
    height: 20,
    alignSelf: "center",
    paddingLeft: 5,
    paddingRight: 5,
    borderRadius: 10,
    marginTop: 2,
  },
  headerimage: {
    height: 300,
    paddingTop: 0,
    flex: 1,
    zIndex: 1,
    flexDirection: "column",
    flexWrap: "wrap",
  },
  headerTextStyle: {
    color: "white",
    textAlign: "center",
    backgroundColor: "transparent",
    justifyContent: "flex-start",
    fontSize: 20,
  },
  addcontainer: {
    flex: 1,
    flexDirection: "row",
    flexWrap: "wrap",
    padding: 10,
  },
  selectedlisttext: {
    backgroundColor: "blue",
    color: "white",
    padding: 5,
    paddingTop: 2,
    height: 25,
    margin: 3,
    textAlign: "center",
    borderRadius: 10,
  },
  cameraicon: {
    ...Platform.select({
      ios: {
        color: "white",
        alignItems: "flex-end",
        opacity: 0.8,
        paddingTop: 200,
        paddingLeft: 10,
        zIndex: 20,
      },
      android: {
        color: "white",
        alignItems: "flex-end",
        opacity: 0.8,
        paddingTop: 200,
        paddingLeft: 10,
        zIndex: 20,
      },
    }),
  },
  deleteButton: {
    paddingLeft: 10,
    paddingRight: 10,
    marginTop: 3,
    backgroundColor: "#FF4545",
    borderRadius: 2,
    flexDirection: "row",
    height: 18,
  },
  formContainer: {
    //   backgroundColor : 'purple',
    //   color : 'white'
  },
  modal: {
    margin: 0, // This is the important style you need to set
    alignItems: undefined,
    justifyContent: undefined,
  },
  titleStyle: {
    fontSize: 14,
    backgroundColor: "lightgray",
    padding: 3,
    top: 0,
  },
  dividerStyle: {
    width: 0,
  },
  actionButtonIcon: {
    // fontSize: 20,
    // height: 22,
    // width:22,
    color: "white",
  },
  switchText: {
    color: "#000",
    fontSize: 16,
    ...Platform.select({
      android: {},
      default: {},
    }),
    // paddingLeft: 10
  },
  switchStyle: {},
  groupSelectText: {
    color: "#86939e",
    fontSize: 16,
    ...Platform.select({
      android: {
        fontWeight: "bold",
      },
      default: {
        fontWeight: "bold",
      },
    }),
    paddingLeft: 10,
  },
  roundImgStyle: {
    width: (Dimensions.get("window").width * 0.9) / 4,
    height: (Dimensions.get("window").width * 0.9) / 4,
    marginHorizontal: 5,
    padding: 0,
    borderRadius: (Dimensions.get("window").width * 0.9) / 8.2,
    borderColor: "#d3d3d3",
    borderWidth: 0,
  },
  dialogItem: {
    height: 50,
    flexDirection: "row",
    alignItems: "center",
    borderBottomWidth: 1,
    borderBottomColor: "#d3d3d3",
  },

  groupSelectTextSelected: {
    marginLeft: 20,
    paddingTop: 5,
    paddingLeft: 10,
    paddingBottom: 5,
    paddingRight: 10,
    backgroundColor: "#3fb0ac",
    marginTop: -5,
    color: "white",
  },
  monthPublicText: {
    fontSize: 16,
    color: "#545E75",
    fontWeight: "bold",
    textAlign: "left",
    alignSelf: "center",
  },
  dateFromPublicText: {
    fontSize: 32,
    color: "darkgray",
    fontWeight: "bold",
    textAlign: "center",
  },
  dateToPublicText: {
    fontSize: 32,
    color: "darkgray",
    fontWeight: "bold",
    alignContent: "center",
    textAlign: "center",
  },
  timePublicFromText: {
    fontSize: 18,
    color: "blue",
    fontWeight: "bold",
    paddingLeft: 5,
  },
  timePublicToText: {
    fontSize: 18,
    color: "blue",
    fontWeight: "bold",
  },
  publicCard: { marginLeft: -5, marginRight: -5, flex: 1 },
  stepperContainer: {
    flexDirection: "row",
    justifyContent: "space-around",
    padding: 10,
  },
  stepperElement: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },

  roundElement: {
    width: 50,
    height: 50,
    borderRadius: 50,
    alignItems: "center",
    borderWidth: 2,
    borderColor: "#3C3989",
    justifyContent: "center",
  },
  hightlightRoundElement: {
    width: 50,
    height: 50,
    borderRadius: 50,
    backgroundColor: "#3C3989",
    alignItems: "center",
    borderWidth: 2,
    borderColor: "#3C3989",
    justifyContent: "center",
  },
  highlightedStepText: {
    fontSize: 12,
    paddingTop: 4,
    fontWeight: "bold",
    color: "#3C3989",
  },
  eventStepTextColorLight: { color: "white", fontSize: 16 },
  eventStepTextColor: { color: "#3C3989", fontSize: 16 },
  stepText: {
    fontSize: 12,
    paddingTop: 4,
    color: "#3C3989",
  },
  textContainerStyle: {
    width: 100,
    textAlign: "center",
  },
  inputFieldCover: {
    backgroundColor: "#F4F4F4",
    paddingLeft: 10,
    paddingRight: 10,
  },
  inputFieldStyles: {
    fontFamily: "Helvetica Neue",
    fontSize: 16,
  },
  sectionTextHeading: {
    padding: 10,
    fontWeight: "bold",
    fontSize: 18,
    fontFamily: "Helvetica Neue",
    color: "#000000",
  },
  generalTextView: {
    padding: 10,
    fontSize: 16,
    fontFamily: "Helvetica Neue",
    color: "#000000",
  },
  newGeneralTextView: {
    margin: 10,
    fontSize: 18,
    fontFamily: "Helvetica Neue",
    color: "#000000",
  },
  newSectionHeading: {
    margin: 10,
    fontWeight: "bold",
    fontSize: 22,
    fontFamily: "Helvetica Neue",
    color: "#000000",
  },
  previewTextShow: {
    fontSize: 18,
    fontFamily: "Helvetica Neue",
    color: "#000000",
  },
  footerBackBtn: {
    backgroundColor: "#3C3989",
    borderRadius: 50,
  },
  disableFooterBackBtn: {
    backgroundColor: "#cccccc",
    borderRadius: 50,
  },
  slotReminderContainer: {
    backgroundColor: "white",
    borderRadius: 10,
    elevation: 5,
    borderWidth: 1,
    // padding: 10,
    borderColor: "#ededed",
    marginBottom: 20,
  },
  slotItemColor: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    padding: 5,
  },
  slotItemColorGrey: {
    flex: 1,
    justifyContent: "space-between",
    alignItems: "center",
    backgroundColor: "#F5F5F5",
    flexDirection: "row",
    padding: 10,
    margin: 2,
  },
  slotItemInnerView: {
    flexDirection: "row",
    flex: 1,
    paddingRight: 20,
  },
  slotDivider: {
    backgroundColor: "#000",
    opacity: 0.5,
    width: 10,
    height: 2,
  },
  slotReminderModalView: {
    borderRadius: 4,
    margin:0,
    backgroundColor: "white",
    padding: 10,
  },
  slotReminderModalHeaderText: {
    fontWeight: "bold",
    color: "#000",
    fontSize: 16,
  },
  slotReminderModalBtn: {
    marginTop: 20,
    justifyContent: "center",
    flexDirection: "row",
  },
  slotReminderModalBody: {
    marginTop: 10,
  },
  slotReminderSpotItem: {
    flexDirection: "row",
    alignItems: "center",
    marginBottom: 4,
  },
  spotItemDefaultText: {
    paddingLeft: 10,
    paddingRight: 10,
    color: "#3C3989",
    fontSize: 16,
  },
  spotAllDateCheckBoxContainer: {
    borderRadius: 4,
    backgroundColor: "white",
    padding: 10,
  },
  spotAllDateCheckBox: { flexDirection: "row", flex: 1, alignItems: "center" },
  spotAllDateCheckBoxItem: {},
  elevationOnDiv: {
    backgroundColor: "white",
    borderRadius: 10,
    elevation: 5,
    borderWidth: 1,
    padding: 10,
    borderColor: "#ededed",
    marginBottom: 5,
  },
  inputlabel: {
    fontFamily: "Helvetica Neue",
    padding: 2,
    color:"#000",
  },
});

const theme = {
  Input: {
    inputStyle: {
      // color: "blue",
    },
  },
};

var mapStateToProps = function (store) {
  return {
    user: store.user,
    login: store.login,
    events: store.events,
    children_groups: store.attendance.groups,
  };
};

var mapDispatchToProps = function (dispatch) {
  return {
    addEventAction: function (
      formData,
      selectedUsers,
      displayName,
      token,
      eventimgurl,
      groups,
      eventtype,
      eventid,
      parentTeacherSlots,
      volunteer,
      spot,
      volunteertype,
      daterangetype,
      daterange,
      selectedDays,
      slotTime,
      dates
    ) {
      dispatch(
        eventActions.addEventAction(
          formData,
          selectedUsers,
          displayName,
          token,
          eventimgurl,
          groups,
          eventtype,
          eventid,
          parentTeacherSlots,
          volunteer,
          spot,
          volunteertype,
          daterangetype,
          daterange,
          selectedDays,
          slotTime,
          dates
        )
      );
    },
    deleteEventAction: function (eventid, token, groups) {
      dispatch(eventActions.deleteEventAction(eventid, token, groups));
    },
    deleteSignupforChild: function (event_id, child_id, slotId, token) {
      dispatch(
        eventActions.deleteSignupforChild(event_id, child_id, slotId, token)
      );
    },
    getEventsAction: function (token, groups) {
      dispatch(eventActions.getEventsAction(token, groups));
    },
    clearLastRefreshTime: function () {
      dispatch(eventActions.clearLastRefreshTime());
    },
  };
};
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AddVolunteerEventScreen);
