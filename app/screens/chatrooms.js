'use strict'

import {
    Platform,
    AppRegistry,
    Text,
    View,
    StyleSheet,
    ScrollView,
    TouchableOpacity,
    RefreshControl, FlatList, ActivityIndicator, Alert
} from 'react-native'
import React, {Component} from 'react'
var EnvConfig = require('../config/environment')
var navigatorChatButtonsWidget = require('./commonwidgets').navigatorChatButtonsWidget
import * as firebase from 'firebase'
import * as firebaseapi from '../api/firebaseapi'
import * as chatroomActions from '../actions/chatRoomActions'
import {connect} from 'react-redux'
import * as chatActions from '../actions/chatActions'
import * as chatRoomActions from '../actions/chatRoomActions'
import * as userActions from '../actions/userActions'
import * as miscutils from '../utils/misc'
import Icon from 'react-native-vector-icons/FontAwesome';
import Ionic from 'react-native-vector-icons/Ionicons';

import Modal from 'react-native-modal'
import ModalMemberListScreen from './components/modalmemberlist'
import ProfileButton from '../components/ProfileButton'
import {CustomCachedImage} from "react-native-img-cache";

import Image from 'react-native-image-progress';
//tempo
import ChatRoomItem from './chat/chatroomItem'
//import Perf from 'ReactPerf';
import {Navigation} from 'react-native-navigation'
let _this = null
import HMSActionButton from '../navigation/HMSActionButton'
import * as utils from '../utils/misc'
class ChatRoomsScreen extends Component {

    constructor(props) {
        super(props)
        this.state = {
            parentchooservisible: false
        }
        this.showParentChooser = this.showParentChooser.bind(this)
    }


    showParentChooser() {
        this.setState({parentchooservisible: true})
    }

    hideParentChooser() {
        this.setState({parentchooservisible: false})
    }

    ParentChooserModal() {
        if (this.state.parentchooservisible) {
            return (
                <Modal onRequestClose={() => {
                    console.log('Modal has been closed.');
                }} transparent={true} isVisible={this.state.parentchooservisible} style={styles.modal}>
                    <ModalMemberListScreen modaldismissfunc={this.hideParentChooser.bind(this)}
                                               onSelectedList={(e) => this.onstartChat(e)}
                        />

                </Modal>
            )
        }
        else {
            return null
        }
    }

    componentWillMount(){
        console.log('~!~!~!')
        // setTimeout(() => {
        //     Perf.start();
        //     setTimeout(() => {
        //         Perf.stop();
        //         const measurements = Perf.getLastMeasurements();
        //         console.log(measurements)
        //         Perf.printInclusive();
        //     }, 20000);
        // }, 1000);
        // console.log(this.props)

    }

    componentDidMount() {
        _this = this


    }

    getLastMsgTimestampofRoom(roomKey) {
        if (this.props.chatrooms.rooms[roomKey] !== undefined) {
            var msgs = this.props.chatrooms.rooms[roomKey].Messages
            if (msgs === undefined) {
                return 0
            } else {
                var lastts = msgs[0].orderBy
                return lastts
            }
        }
        else return 0
    }



    _onRefresh() {
        console.log('Refreshing Chat Room !')

        this.props.clearGroups()
        this.props.stopListener(this.props.userid)
        this.props.getChatGroups(this.props.userid)

    }

    startConversation() {

    }

    shouldComponentUpdate(nextProps, nextState) {
        //if user just went to a chatroom disable render
        if (this.props.chatrooms.rooms === undefined ){
            return false
        }
        return true
        if (((nextProps.chatrooms.liveRoom !== null) || (this.props.chatrooms.liveRoom === null)) ||
            ((nextProps.chatrooms.liveRoom === null) || (this.props.chatrooms.liveRoom !== null))){
            console.log(nextProps.chatrooms.rooms)
            if ((nextProps.chatrooms.rooms === undefined) || (nextProps.chatrooms.rooms === null)){
                return true
            }
            console.log(Object.keys(nextProps.chatrooms.rooms).length)
            console.log(Object.keys(this.props.chatrooms.rooms).length)

            // if (Object.keys(nextProps.chatrooms.rooms).length === Object.keys(this.props.chatrooms.rooms).length){
            //     if (this.props.user.totalnotifications === nextProps.user.totalnotifications) {
            //         if (this.state.parentchooservisible === nextState.parentchooservisible) {
            //             console.log('No Need to Render Screen')
            //             return false
            //         }
            //     }
            // }
        }

        return true
    }

    async onstartChat(selectedUsers) {
        console.log('Selected ....')
        console.log(selectedUsers)
        var origselectedusers = Object.assign(selectedUsers)
        console.log(origselectedusers)
        console.log(this.props)
        selectedUsers[this.props.userid] = {
            userid: this.props.userid,
            username : this.props.username,
            display_name: this.props.user_display_name,
            profile_photo: this.props.user_profile_photo
        }
        console.log(selectedUsers)
        var users = miscutils.ObjectArraytoIndexArray(selectedUsers)
        console.log(users)
        if (users.length > 1) {
            console.log(users)
            var roomKey = await firebaseapi.getChatRoomKey(this.props.userid, users, "")
            if (roomKey === null){
                roomKey = await  firebaseapi.setupNewChatRoom('', users, this.props.userid);
            }

            console.log(roomKey)
            console.log(origselectedusers)

            if (Object.keys(origselectedusers).length === 2){
                delete origselectedusers[this.props.userid]
                console.log('Entering Chat for Single User ' + origselectedusers[Object.keys(origselectedusers)[0]].display_name)
                this.onEnterChat(roomKey, origselectedusers[Object.keys(origselectedusers)[0]].display_name)
            }else{
                console.log('Entering Chat for Group')
                this.onEnterChat(roomKey, 'Group Chat')
            }

        }
    }
    _keyExtractor = (item, index) => {
        return item.key
    };

    onEnterChat(roomKey, username) {
        var title = username


        Navigation.push(this.props.componentId, {
            component: {
                name: 'nav.Three60Memos.ChatScreen',
                passProps: {
                    roomKey, title
                },
                options: {
                    topBar: {
                        title: {
                            text:title,
                            color: 'white'
                        },
                        background: {
                            color: EnvConfig.MAIN_CHAT_COLOR
                        },
                        backButton: {
                            visible: true,
                            color: 'white',
                            showTitle: false
                        },
                    }
                }
            }
        });

    }

    render(){
        console.log(this.props.chatrooms.rooms)
        var roomkeys = Object.values(this.props.chatrooms.rooms).filter(function (room) {
            return room.visibleRoom;
        })
        console.log(this.props.chatrooms.rooms)
        roomkeys.sort(function(akey, bkey) {
            return (bkey.Details.lastmsgtimestamp - akey.Details.lastmsgtimestamp)
        });
        console.log('RENDER->NEWCHATROOM->' + roomkeys.length)
        if (roomkeys.length > 0) {
            return (
                <View style={{flex: 1}}>

                    <ScrollView ref='activityScrollView'
                                style={{paddingBottom: 50}}
                                scrollEnabled={true}
                                automaticallyAdjustContentInsets={true}
                                refreshControl={
                                    <RefreshControl
                                        refreshing={this.props.chatrooms.isLoading}
                                        onRefresh={this._onRefresh.bind(this)}
                                        tintColor="#d3d3d3"
                                        title="Loading..."
                                        titleColor="black"
                                        colors={['#ff0000', '#00ff00', '#0000ff']}
                                        progressBackgroundColor="#ffff00"
                                    />}>
                        <FlatList
                            data={roomkeys}
                            keyExtractor={this._keyExtractor}
                            renderItem={({item}) => {
                                return (
                                    <ChatRoomItem componentId={this.props.componentId} roomdetails={item}/>

                                )
                            }
                            }
                        />

                    </ScrollView>
                    <HMSActionButton userroles={this.props.userroles}
                                     parent={"nav.Three60Memos.ChatRoomsScreen"}
                                     customClick={this.showParentChooser}/>
                    {this.ParentChooserModal()}
                </View>
            )
        }else{
            if (this.props.chatrooms.isLoading){
                return(
                <View>

                    <Text style={styles.noconversation}>Loading ...</Text>
                    <ActivityIndicator style={{paddingTop : 5}} size="small" color="#0000ff" />
                </View>
                )
            }else {
                return (
                    <View style={{flex : 1}}>
                        <Text style={styles.noconversation}>No Conversations Found</Text>
                        <HMSActionButton userroles={this.props.userroles}
                                         parent={"nav.Three60Memos.ChatRoomsScreen"}
                                         customClick={this.showParentChooser}/>
                        {this.ParentChooserModal()}
                        <TouchableOpacity onPress={this._onRefresh.bind(this)}
                                          style={{
                                              backgroundColor: '#1ebbd7',
                                              width: 100, height: 30, borderRadius: 3, marginTop : 5,alignSelf: 'center'
                                          }}>
                            <Text style={{textAlign: 'center', paddingTop : 5, color: 'white'}}>
                                Refresh
                            </Text>
                        </TouchableOpacity>
                    </View>
                )
            }
        }
    }

}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor : 'white'
    },
    composeMessage: {
        paddingLeft: 10
    },
    MenuIcon: {
        paddingRight: 10
    },
    messageRoomContainer: {
        flexDirection: 'row',
        borderColor: '#c3c3c3',
        borderBottomWidth: 1,
        padding: 5
    },
    textLayout: {
        flexDirection: 'column'
    },
    startConversationText: {
        color: 'blue',
        fontSize: 18,
        alignSelf: 'flex-end'
    },
    actioncontainer: {
        backgroundColor: 'lightgray',
        padding: 10
    },
    memberImage: {
        width: 46,
        height: 46,
        borderRadius: 23,
        borderWidth: 0.5,
        borderColor: 'gray',
        flexDirection: 'row',
        marginLeft: 5
    },
    messageRoomText: {
        paddingLeft: 7,
        fontWeight: 'normal'
    },
    messageRoomText2: {
        paddingLeft: 7,
        fontStyle: 'italic'
    },
    countertext: {
        fontWeight: 'bold',
        color: 'white',
        alignSelf: 'center'
    },
    noconversation: {
        alignSelf: 'center',
        fontSize: 20,
        paddingTop: 20,
        color: 'darkgray'
    },
    rightImage: {
        color: 'lightgray',
        paddingTop: 7
    },
    arrowcontainer: {
        flex: 1,
        flexDirection: 'row-reverse'
    },
    modal: {
        margin: 0, // This is the important style you need to sealignItems: undefined,
        justifyContent: undefined,
        backgroundColor : 'transparent'
    }

})
var mapStateToProps = function (store) {
    return {
            userid: store.user.userid,
            username : store.user.username,
            user_display_name : store.user.display_name,
            user_profile_photo : store.user.profile_photo,
            chatrooms: store.chatrooms};
};

var mapDispatchToProps = function (dispatch) {
    return {
        loadMessages: function (roomKey) {
            dispatch(chatActions.loadMessages(roomKey))
        },
        startListener: function (roomKey, lastmsgtimestamp) {
            dispatch(chatActions.startListener(roomKey, lastmsgtimestamp))
        },
        getChatGroups: function (userid) {
            dispatch(chatRoomActions.getChatGroups(userid))
        },
        setNavigationTab: function (tabName) {
            dispatch(userActions.setNavigationTab(tabName))
        },
        clearGroups: function () {
            dispatch(chatRoomActions.clearGroups())
        },
        stopListener: function (userid) {
            dispatch(chatRoomActions.stopListener(userid))
        }
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(ChatRoomsScreen)