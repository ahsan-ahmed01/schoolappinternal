import {
  Platform,
  AppRegistry,
  Text,
  StyleSheet,
  View,
  ScrollView,
  Image,
  findNodeHandle,
  TextInput,
  TouchableOpacity,
  ActivityIndicator
} from 'react-native'
import React, {Component} from 'react'
import {ifIphoneX} from "react-native-iphone-x-helper";
import Spinner from 'react-native-loading-spinner-overlay'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import CheckBox from 'react-native-checkbox';
// var AuthService = require('../lib/authservice');
import Icon from 'react-native-vector-icons/FontAwesome';
var bugsnag = require('../config/bugsnag')

const lockIcon = require('../img/login1_lock.png')
const personIcon = require('../img/login1_person.png')
var EnvConfig = require('../config/environment');
import { Avatar,Card, Button, ListItem} from "react-native-elements";
const logo = EnvConfig.LOGO_IMAGE
import { connect } from 'react-redux'
import * as loginActions from '../actions/loginActions'
import CodePushComponent from '../components/codepushcomponent'
class LoginScreen extends Component {

  constructor (props) {
    super(props)
    this.state = {
      emailaddress : '',
      emailisValid : false,
      resetCode : '',
      passwordvalidation : false,
      password1 : '',
      password2 : '',
      resetError : true,
        showAgreement : false,
        togglePasswordVisibility : true

    }
  }

  textInputFocused (refName) {
    console.log('Goooo')
    setTimeout(() => {
      let scrollResponder = this.refs.scrollView.getScrollResponder()
      scrollResponder.scrollResponderScrollNativeHandleToKeyboard(
        findNodeHandle(this.refs[refName]),
        250, // additionalOffset
        true
      )
    }, 50)
  }
  _scrollToInput (event, reactNode) {
    // Add a 'scroll' ref to your ScrollView
      console.log(reactNode)
    this.refs.scroll.scrollToFocusedInput(reactNode)
  }

  render () {
    console.log(this.props)
    if (this.props.login.error != '') {
      setTimeout(() => this.props.resetError(), 2000)
    }
    if ((this.props.login.isLoggedin) && (this.props.login.logincredvalidated === true)) {
      //this.props.navigator.dismissModal()
    }
    if (this.props.login.viewScreen === 'LoginScreen') {
      return this.renderLoginScreen()
    }
    else{
      if (this.props.login.viewScreen === 'LostPassword'){
        return  this.renderLostPassword()
      }
      else{
        if (this.props.login.viewScreen === 'ResetCode'){
          return this.renderresetCode()
        }
      }
    }
  }

  setResetCode(resetCode){
    this.setState({
      resetCode : resetCode,
      resetError : true
    })
  }

  setPassword1(password){
    this.setState({
      password1 : password,
      resetError : true
    })
    this.setState({
        passwordvalidation: this.validatePassword(password, this.state.password2)
      })
  }

  setPassword2(password){
    this.setState({
      password2 : password,
      resetError : true
    })
    this.setState({
      passwordvalidation: this.validatePassword(this.state.password1, password)
    })
  }

  validatePassword(password1, password2){
    if (password1 === ''){
      return false
    }
    if (password1.length < 8) {
      return false
    }
    return (password1 === password2)
  }


  renderresetCode(){
    return (
          <KeyboardAwareScrollView ref='scroll' extraHeight={200} style={styles.container} >
            <Spinner visible={this.props.login.resettingPassword} textContent={'Resetting Password...'} textStyle={{color: '#FFF'}} />
            <TouchableOpacity style={styles.leftarrow} onPress={this.backtoLogin.bind(this)}>
              <Icon  size={18} name="chevron-left" style={{color : '#3b479d'}}></Icon>
            </TouchableOpacity>
            <View  style={this.props.login.error != ''
              ? styles.containerError
              : styles.lostpasswordcontainer}>
              <Text>Login : {this.state.emailaddress}</Text>
              <View style={styles.inputWrap}>
                <View style={styles.iconWrap}>
                  <Image source={personIcon} style={styles.icon} resizeMode="contain"/>
                </View>
                <TextInput
                    style={styles.input}
                    autoCorrect={false}
                    autoCapitalize="none"
                    placeholderTextColor="darkgray"
                    placeholder={'Email Address'}
                    defaultValue={this.state.emailaddress}
                    onChangeText={this.emailchange.bind(this)} >
                </TextInput>
              </View>
              <Text style={{marginTop : 10}}>
                Please Enter Your Code
              </Text>
              <View style={styles.inputWrap}>
                <View style={styles.iconWrap}>
                  <Image source={lockIcon} style={styles.icon} resizeMode="contain"/>
                </View>
                <TextInput
                  style={styles.input}
                  keyboardType='numeric'
                  autoCorrect={false}
                  autoCapitalize="none"
                  placeholderTextColor="darkgray"
                  placeholder={'One Time Code'}
                  onChangeText={this.setResetCode.bind(this)} >

                </TextInput>
              </View>
              <Text style={{marginTop: 20}}>
                Please select a Password :
              </Text>

              <View style={[styles.inputWrap]}>
                <View style={styles.iconWrap}>
                  <Image source={lockIcon} style={styles.icon} resizeMode="contain"/>
                </View>
                <TextInput
                  style={styles.input}
                  autoCorrect={false}
                  autoCapitalize="none"
                  placeholderTextColor="darkgray"
                  placeholder={'New Password'}
                  onChangeText={this.setPassword1.bind(this)} >
                </TextInput>
              </View>
              <View style={styles.inputWrap}>
                <View style={styles.iconWrap}>
                  <Image source={lockIcon} style={styles.icon} resizeMode="contain"/>
                </View>
                <TextInput
                  style={styles.input}
                  autoCorrect={false}
                  autoCapitalize="none"
                  placeholderTextColor="darkgray"
                  placeholder={'Confirm New Password'}
                  onChangeText={this.setPassword2.bind(this)} >
                </TextInput>
              </View>
              {this.renderResetPasswordButton()}

            </View>
          </KeyboardAwareScrollView>
    )

  }
  renderErrorTextforChangePassword()
  {
    console.log(this.props.login)
    if ((this.props.login.changePasswordSuccess === "ERROR")  && (this.props.login.registererror === 'UsernameExistsException')){
      return (<Text style={styles.invalidEmail}>The User already exists. If you dont remember your password, please go back to the login screen and click on 'Forgot Password' </Text>)

    }
    if ((this.props.login.changePasswordSuccess === "ERROR") && (!this.state.resetError))
    {
    return (<Text style={styles.invalidEmail}>Error Resetting your Password. Please check the reset Code or Contact your Administrator</Text>)
    }

  }

  componentDidMount(){
      this.initAWS()
      console.log('Comp Did Mount')
      if ((this.props.login.storeloaded === true )
          && (this.props.login.cred.userid !== '') && (this.props.login.cred.password !== '')){
          console.log('Auto Login Enabled')
          this.props.authenticate(this.props.login.cred.userid, this.props.login.cred.password, this.props.user)
          return true
      }
  }

    componentDidUpdate(prevProps, prevState){
     console.log('Login Prop =>' + this.props.login.isLoggingin)
      console.log('Prev Login Prop =>' + prevProps.login.isLoggingin)
        console.log('showBusySpinner Prop =>' + this.props.login.showBusySpinner)
        console.log('AppReady =>' + this.props.login.AppReady)
      if ((this.props.login.isLoggingin === true) && (this.props.login.AppReady) &&
          ((prevProps.login.isLoggingin === undefined) || (prevProps.login.isLoggingin === false))){
          console.log('In Component Update after Login Render Refresh !!!')
          this.props.authenticate(this.props.login.cred.userid, this.props.login.cred.password, this.props.user)
        }
    }


  shouldComponentUpdate(nextProps, nextState) {
        console.log('in Should Comp Update')
    console.log(this.props.login.storeloaded + '-->' + nextProps.login.storeloaded )
      console.log(nextProps)
      if ((this.props.login.storeloaded === false ) && (nextProps.login.storeloaded === true)
        && (nextProps.login.cred.userid !== '') && (nextProps.login.cred.password !== '')){
      console.log(nextProps)
      console.log('Auto Login Enabled')
      this.props.authenticate(nextProps.login.cred.userid, nextProps.login.cred.password, this.props.user)
      return true
    }
    //**** Respond to Login Button ******


     return true
  }

  renderResetPasswordButton(){
    if (this.state.passwordvalidation){

      return (
        <View style={{marginTop : 20}}>
          {this.renderErrorTextforChangePassword()}
          <TouchableOpacity onPress={this.registerUser.bind(this)}>
            <View style={styles.button}>
              <Text style={styles.buttonText}>Set Password</Text>
            </View>
          </TouchableOpacity>
        </View>
      )

    }else{
      if (this.state.password1 === '' || this.state.password2 === ''){
        return  null
      }else {
        return (
            <View>
              <Text style={styles.invalidEmail}>Password does not Match. Password should be atleast 8 characters.</Text>
            </View>)
      }
    }
  }

  registerUser(){
    this.setState({
      resetError : false
    })
    this.props.registerUser(this.state.emailaddress, this.state.resetCode, this.state.password2)
    //this.props.changePassword(this.state.emailaddress, this.state.resetCode, this.state.password2)

  }

  changePassword(){
    this.setState({
      resetError : false
    })
    this.props.changePassword(this.state.emailaddress, this.state.resetCode, this.state.password2)

  }


  initAWS(){
      console.log('------ IN AWS INIT ------------')
      // Auth.signOut()
      // Auth.cleanCachedItems()
      // Auth.currentCredentials()
      //     .then(credentials => {
      //         const db= new DynamoDB.DocumentClient({
      //             region : 'us-east-1',
      //             credentials:  Auth.essentialCredentials(credentials)
      //         });
      //         console.log(credentials)
      //         console.log( Auth.essentialCredentials(credentials))
      //         console.log(db)
      //         var params = {
      //             TableName : 'dev_posts',
      //             Key: {
      //                 'groups': 'Orange',
      //                 'updatetime' : 1529783737
      //             }
      //         };
      //         db.get(params, function(err, data) {
      //             if (err) console.log(err);
      //             else console.log(data);
      //         });
      //         console.log(params)
      //     })
      //     .catch(err =>{
      //         console.log(err)
      //     })
      return null

  }

  renderLostPassword()
  {
    return(
      <KeyboardAwareScrollView ref='scroll' extraHeight={400} style={styles.container} >
        <Spinner visible={this.props.login.resettingPassword} textContent={'Sending Email ...'} textStyle={{color: '#FFF'}} />
        <TouchableOpacity style={styles.leftarrow} onPress={this.backtoLogin.bind(this)}>
          <Icon  size={18} name="chevron-left" style={{color : '#3b479d'}}></Icon>
        </TouchableOpacity>
        <View>
          <Card title={"FORGOT PASSWORD"}>
          <View style={styles.inputWrap}>
            <View style={styles.iconWrap}>
              <Image source={personIcon} style={styles.icon} resizeMode="contain"/>
            </View>
            <TextInput
            style={styles.input}
            autoCorrect={false}
            autoCapitalize="none"
            placeholderTextColor="darkgray"
            placeholder={'Email Address'}
            defaultValue={this.state.emailaddress}
            onChangeText={this.emailchange.bind(this)} >
          </TextInput>
          </View>
          <Text>
              {this.props.login.messageStatus}
          </Text>
            {this.RenderInvalidEmail()}
          {/*<Text style={styles.resetCodeText}>*/}
            {/*Already have a reset code ?*/}
          {/*</Text>*/}

          {/*<TouchableOpacity onPress={this.showresetcode.bind(this)}>*/}
            {/*<View style={styles.button}>*/}
              {/*<Text style={styles.buttonText}>Enter Reset Code</Text>*/}
            {/*</View>*/}
          {/*</TouchableOpacity>*/}
          </Card>
          <Card title={"I HAVE MY RESET CODE"}>
              <Text>Login : {this.state.emailaddress}</Text>
              <View style={styles.inputWrap}>
                <View style={styles.iconWrap}>
                  <Image source={personIcon} style={styles.icon} resizeMode="contain"/>
                </View>
                <TextInput
                    style={styles.input}
                    autoCorrect={false}
                    autoCapitalize="none"
                    placeholderTextColor="darkgray"
                    placeholder={'Email Address'}
                    defaultValue={this.state.emailaddress}
                    onChangeText={this.emailchange.bind(this)} >
                </TextInput>
              </View>
              <Text style={{marginTop : 10}}>
                Please Enter Your Reset Code
              </Text>
              <View style={styles.inputWrap}>
                <View style={styles.iconWrap}>
                  <Image source={lockIcon} style={styles.icon} resizeMode="contain"/>
                </View>
                <TextInput
                    style={styles.input}
                    keyboardType='numeric'
                    autoCorrect={false}
                    autoCapitalize="none"
                    placeholderTextColor="darkgray"
                    placeholder={'Password Reset Code'}
                    onChangeText={this.setResetCode.bind(this)} >

                </TextInput>
              </View>
              <Text style={{marginTop: 20}}>
                Please select a Password :
              </Text>

              <View style={[styles.inputWrap]}>
                <View style={styles.iconWrap}>
                  <Image source={lockIcon} style={styles.icon} resizeMode="contain"/>
                </View>
                <TextInput
                    style={styles.input}
                    autoCorrect={false}
                    autoCapitalize="none"
                    placeholderTextColor="darkgray"
                    placeholder={'New Password'}
                    onChangeText={this.setPassword1.bind(this)} >
                </TextInput>
              </View>
                <View style={styles.inputWrap}>
                  <View style={styles.iconWrap}>
                    <Image source={lockIcon} style={styles.icon} resizeMode="contain"/>
                  </View>
                  <TextInput
                      style={styles.input}
                      autoCorrect={false}
                      autoCapitalize="none"
                      placeholderTextColor="darkgray"
                      placeholder={'Confirm New Password'}
                      onChangeText={this.setPassword2.bind(this)} >
                  </TextInput>
                </View>
            <View style={{marginTop : 20}}>
              {this.renderErrorTextforChangePassword()}
              <TouchableOpacity onPress={this.changePassword.bind(this)}>
                <View style={styles.button}>
                  <Text style={styles.buttonText}>Reset Password</Text>
                </View>
              </TouchableOpacity>
            </View>

          </Card>


      </View>
      </KeyboardAwareScrollView>

    )
  }
  showresetcode(){
    this.props.changeScreen('ResetCode')
  }
  resetPassword(){
    this.props.resetPassword(this.state.emailaddress)
  }

  togglePasswordText(){
    console.log(this.state.togglePasswordVisibility)
    this.setState({
      togglePasswordVisibility : !this.state.togglePasswordVisibility
    })
    console.log(this.state.togglePasswordVisibility)
  }

  RenderInvalidEmail(){
    if (!this.state.emailisValid){
      return ( <Text style={styles.invalidEmail}>Invalid Email</Text>)
    }else {
      if (this.props.login.invalidEmailAddress) {
        return (<Text style={styles.invalidEmail}>Incorrect Email Address. Please verify your email address and try
          again</Text>)
      }
      else {
        if (this.props.login.resetCodeSuccess) {
          return (
              <View>
                <Text style={styles.successEmail}>An Email with a Reset Code is sent to the associated Email Address !</Text>
              </View>
          )
        }
        else {
          return (
            <TouchableOpacity onPress={this.resetPassword.bind(this)}>
              <View style={styles.button}>
                <Text style={styles.buttonText}>Reset my Password</Text>
              </View>
            </TouchableOpacity>
          )
        }
      }
    }

  }

  emailchange(strtext){
      if (strtext === undefined){
          return
      }

      this.setState({
          emailaddress : strtext.trim().toLowerCase(),
          emailisValid : true
      })
      this.props.setEmailAddress(strtext.trim().toLowerCase())
      return
    // Skip email validation
    if (strtext.length > 0){
      let status = this.validateEmail(strtext)
      this.setState({
        emailaddress : strtext,
        emailisValid : status
      })
      console.log(strtext)
      this.props.setEmailAddress(strtext)
    }
  }


    close(){
      this.setState({
          showAgreement : false
      })
    }

  renderLoginScreen () {
    if (this.state.showAgreement){
      return(
          <View style={styles.container}>
            <CodePushComponent/>

            <View style={{flexDirection: 'row', justifyContent:'space-between',paddingTop : 15}}>

                  <Text style={styles.headerText}>Terms & Conditions</Text>
                  <TouchableOpacity onPress={this.close.bind(this)} >
                      <Icon  size={20} style={{color: 'black',  paddingRight : 10}} name="times-circle"></Icon>
                  </TouchableOpacity>
              </View>
            <ScrollView contentContainer={{flex : 1}}>
            <View style={{flex : 1}}>
             <Text style={{color  :'black'}}>
               We are delighted to be a part of your school! Below are the rules and restrictions that govern your use of our website, Products, services and application (the 'Services'). When we say "Service", we mean not only 360Memos products and website, but also all the other websites, products, services and application made available by 360Memos ( for example, your schools' 360 app available by Android Marketplace, and your school's 360 iPhone and iPad apps available at the Apple app Store) . If you have any question, comments or concerns regarding the Terms and Services, please contact us at support@360Memos.com These terms are a legally binding contract between you and 360Memos LLC. By accessing or using our Service, you agree to be bound to "all" of the terms and these Terms will remain in effect while you use the Services. These terms include the provisions in this document. You must agree and accept all of the Terms, or you don't have the right to use the Services. Will the Terms every CHANGE?

               We are constantly working towards making the "services" better for you and this may at times require the terms to change along. We reserve the right, to changes these Terms at any time and when we do, we will do our best to tell you in advance by placing a notification on our services or sending you an email but, you are responsible for knowing what the Current Terms are and can do so by periodically reviewing them. If you use the service in any way after the change to the Terms is effective, please remember it means you agree to all of the Terms.

               What are the Rules and Regulations? 1) Eligibility a) You must be of legal age to form a binding contract (which is typically 18 years old many places). b) If not, then you must get your parents, teachers or guardian's permission to use the Services and get them to agree to these Terms on your behalf. c) If you're agreeing to these Terms on behalf of an organization or entity (for example, if you're an administrator agreeing to these Terms on behalf of your school), you represent and warrant that you are authorized to agree to these Terms on that organization or entity's behalf and bind them to these Terms.

               2) Accounts and Registration Policies: You are required to sign up for an account and select a password. If you are a school, you will be permitted to register a school sub domain within 360Memos. I. You promise to provide us with accurate, complete and updated registration information about yourself and the school. Your 360Memos User Id and initial access code will be provided to you by your school. In all cases (the school or yourself) cannot use a user id that you don't have the rights to use to impersonate that person; II. You are solely responsible for maintaining the confidentially of your account and password. You agree to accept responsibility for all activities that occur under your account. You may not transfer your account to anyone else without our and your schools written and signed permission; III. You will not attempt in any manner , to obtain the password, account or other security information from any other user; IV. You will not violate the security of any computer network, or crack any passwords or security encryption codes V. You will not share your access code, provided for registration with anyone else. VI. You will not decompile, reverse engineer or otherwise attempt to obtain the source code or underlying ideas or information of or relating to the Services. VII. If you have reason to believe that your account is no longer secure for any reason ( for example in the event of a loss, theft or unauthorized disclosure or use of your password) , then you agree to immediately notify your school and us at support@360memos.com. If you violate the terms of this Agreement, 360Memos reserves the right to terminate your access and account.

               3) Intellectual Property rights 1. 2. 3. 3.1. The Content: The materials displayed or performed or available on or through the Services ( including, but not limited to, text, graphics, articles, photos, images, illustrations, User Submissions (defined below), the "Content" , trademarks, service marks and logos on the services are protected by copyright and other intellectual property laws. i) You promise to abide by all copyright notices and trademark rules and restrictions and will not use to copy, reproduce, modify, translate, publish, broadcast, transmit, license, sell or otherwise exploit for any purpose any content without prior written consent of the owner of the content, or in any way that violates someone else's rights (including 360Memos rights). ii) You understand that "360Memos LLC" owns the Services. You will not modify, publish transmit, participate in the transfer or sale to reproduce creative derivative works based on, or otherwise exploit any of the Services. iii) The Services may place certain content such as (but not limited to) push notifications on your device or may allow you to download specific content such as (but not limited to) school or teacher specific artifacts. Please note in all cases the stated restrictions in these Terms continue to apply. 3.2. USER Submissions: 1 2 3 3.1 3.2 1.1.1 Granting Rights to 360Memos on your User Submissions Anything you post, upload, share, store, or otherwise provide through the Service is your "User Submission." Some User Submissions are viewable by other users. In order to display your User Submissions on the Services and to allow other users to enjoy them (where applicable) you grant 360Memos certain rights in those User Submissions.

               You will always retain copyright and proprietary rights to the User Submission's that you post to the Service. For all User Submissions, you hereby grant 360Memos a worldwide, non-exclusive, transferable, assignable, fully paid-up, royalty-free right and license to host, transfer, display, perform, reproduce, modify, distribute and re-distribute and otherwise act on all User submissions, in whole or in part, in any media formats and through any media channels to enable us to operate the Services (for example , but not limited to, making sure your content is viewable on your mobile devices as well as your computer). This is a license only ? your ownership in User Submissions is not affected. 1.1.2 Responsibilities and rights A. Ownership around what I see and do on the Services You are solely responsible for all of your User submissions and the consequences of posting or publishing User Submissions. By posting or publishing to the Services you accept, represent and warrant that: i. Your submissions do not and will not infringe, violate or misappropriate any third-party right, including any copyright, trademark, patent, trade secret, moral right, privacy right, right of publicity or any other proprietary right; ii. You will not slander, defame, libel or invade the right of privacy, publicity or any other property rights of any other person; iii. Your submissions will not contain any viruses, adware, spyware, worms or other malicious code;

               iv. You will not Violate any law or regulation of any local, state, national or international laws governing intellectual property , other proprietary rights , data protection , privacy and any applicable export control laws; v. Copy or Store any significant portion of the Content ( with the exception of education material requested by school staff); vi. Make unsolicited offers, advertisements and proposals or send junk mail or spam to other users on the service. This includes, but is not limited to , unsolicited advertising, promotional materials, or other solicitation material, bulk mailing of commercial advertising , charity requests, petitions for signatures. Please note 360Memos has no control over and assumes no responsibility for the content, accuracy, privacy, policies, practices, or opinions of any third party website. 360Memos will not and cannot monitor, verify, censor or edit the content of any third party site or service. When you access third party websites you accept that that there are risks in doing so and that 360Memos is not responsible for such risks. By using third party services you release and hold us harmless from any and all liability arising from the use of their website or service.

               360Memos is under no obligation to edit or control User Submissions that you and other users post or publish and will not be in any way be responsible or liable for User Submissions. You understand that when using the Service, you will be exposed to User submissions from a a variety of sources and acknowledge that User submissions may be inaccurate, offensive, indecent or objectionable. You agree to waive and hereby do waive any legal or equitable rights or remedies you have or may have against 360Memos LLC with respect thereto. B. User Submission Infringement: If notified by a user or content owner that User Submissions allegedly does not conform to the Terms, we may investigate the allegation and based on our sole discretion remove the content without giving any notice. This will particularly (but not limited to) apply to copyright infringing activities on the Service as 360Memos complies with the provisions of the Digital Millennium Copyright Act (DMCA). To learn more about DMCA, go to (http://www.copyright.gov/legislation/dmca.pdf)

               C. Limited Audiences User Submission (to a Private Group) A Limited audience would be a list of registered/specified members that are part of the access group created by those who have access to create the "private" group (can be a class group, school group, message group etc). If you share a User submission within the limited access group, i. You grant 360Memos the licenses above as well as the licenses to display, perform and distribute your "limited access user submission" for the sole purpose of displaying that "Limited access User submission" to other members with in the "Limited Access Group". ii. You also grant other members of the "limited access group" a license to access that "Limited Access User" Submission.

               D. Public Audience User Submission (to a Public Group) A Public audience would be a list of registered members that make the community on 360Memos. If you share a User submission to the public group, then i. You grant 360Memos the licenses above as well as the licenses to display, perform and distribute your "Public user submission" for the sole purpose of displaying that "Public User submission" to all members of 360Memos. ii. You also grant other members of the "Public access group" a license to access that "Public Access User" Submission.

               1.1.3 Parent CONSENT: Comply with FerPA To Use the Services, there may be instances where information is provided to 360Memos by a school official (such as administration, teacher, teacher assistant, school nurse etc). The information could be considered As an "Education record"( that is related directly to a student under the Family Education Rights and Privacy Act(FERPA) ) Or as a "Directory Information" (such as student name, grade level, birth date, parent name, parent email etc). A school is generally not permitted to disclose personally identifiable information of their student to a third party without consent of the parent or without meeting one of the exemptions set forth in the FERPA ( FERPA Exemption's ,Directory Information Exemption and School Official Exemption with a legitimate educational interest) As a school official or an Institution you represent, warrant and confirm to 360Memos LLC that your institution has 1) Obtained all necessary parental written consent to share the "Directory Information" and "Educational Records" with 360Memos; and

               2) Complied and explained "Directory Information and its Exemption (if any)" to the parents and have given them reasonable amount of time to request the school to not proceed further with disclosing their directory information; and/or 3) Complied with the "School Official Exemption", including, ( not limited to),by informing parents a. The Annual notification of the FERPA rights b. The institution defines "school official" to include service providers such as 360Memos c. The institution defines "legitimate education interest" to include services such as those provided by 360Memos 1.1.4 Updates on the Services We are always trying to improve the Services, so they may change over time. We may suspend, or discontinue any part of the Services or may introduce or impose restrictions on certain features or access to some parts or all of the Services. By using the services you understand that we reserve the right to remove any content/features from services at any time, for any reason, at our sole discretion and without notice (though we will try to give you notice upfront on any material change to services, but it may not be practical always)

               1.1.5 Termination of Use You are free to terminate your account at any time by contacting your school which in turn will contact 360Memos . 360Memos is also free to terminate (or suspend access to) your use of the Services or your account for any reason, including your breach of these Terms. 360Memos has the sole right to decide whether you are in violation of any of the restrictions set forth in these Terms. Account termination may result in destruction of any content associated with your account, so keep in mind before you decide to terminate your account. We will try to provide advance notice to you prior to our terminating your account so that you can retrieve any important documents you may have stored ( to the extent allowed by Law and these terms) , but we may not do so if we determine it would be impractical, illegal or would not be in the interest of someone's safety or security to do so. 1.1.6 APP Download 360Memo's App's may be made available to you via 3rd party distributors such as the Apple ITunes App Store. 360Memo grants you a limited, non-exclusive, non-transferable, non-sub licensable, revocable license to install and use one copy of the App in object code format , solely for your personal use , on one device that you own or control.

               These terms apply to your use of all the Services available via the Apple Itunes Store ( App Store, the application), but the following additional terms also apply to the application. (A) Both you and 360Memos acknowledge that the Terms are concluded between you and 360Memos only, and not with Apple, and that Apple is not responsible for the Application or its Content; (b) You acknowledge and agree that Apple has no obligation whatsoever to furnish any maintenance and support services with respect to the Application; (c) In the event of any failure of the Application to conform to any applicable warranty, including those implied by law, then you may notify Apple of such failure; upon notification, Apple's sole warranty obligation to you will be to refund to you the purchase price, if any, of the Application; (d) You acknowledge and agree that 360Memos, and not Apple, is responsible for addressing any claims you or any third party may have in relation to the Application; (e) You acknowledge and agree that, in the event of any third party claim that the Application or your possession and use of the Application infringes that third party's intellectual property rights, 360Memos, and not Apple, will be responsible for the investigation, defense, settlement and discharge of any such infringement claim; (f) Both you and 360Memos acknowledge and agree that, in your use of the Application, you will comply with any applicable third party terms of agreement which may affect or be affected by such use; and (g) Both you and 360Memos acknowledge and agree that Apple and Apple's subsidiaries are third party beneficiaries of these Terms, and that upon your acceptance of these Terms, Apple will have the right (and will be deemed to have accepted the right) to enforce these Terms against you as the third party beneficiary hereof.

               1.1.7 360Memos OwnershIP and rights 360Memos owns and operates the Services. The United States Copyright, trade dress and trademark laws, international conventions and all other relevant intellectual property and proprietary rights and applicable laws protect 360Memos "Materials". Materials are inclusive of visual interfaces, graphics, design, compilation, information, source code, products, software, services and all other elements provided by 360Memos. All trademarks, service marks and trade names are proprietary to 360Memos or its affiliates and/or third party licenses. You agree not to sell license, distribute, copy, modify, publicly perform or display, publish edit, adapt create derivative works from or otherwise make unauthorized use of the Materials 1.1.8 Warranty Disclaimer Neither 360Memos nor its licensors or suppliers makes any representations or guarantees concerning any content contained in or accessed through the Services, and we will not be responsible or liable for the accuracy, copyright compliance, legality, or decency of material contained in or accessed through the Services. �We (and our licensors and suppliers) make no representations or warranties regarding suggestions or recommendations of services or products offered or purchased through the Services. �THE SERVICES AND CONTENT ARE PROVIDED BY 360Memos (AND ITS LICENSORS AND SUPPLIERS) ON AN "AS-IS" BASIS, WITHOUT WARRANTIES OR ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING, WITHOUT LIMITATION, IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR �A PARTICULAR PURPOSE, NON-INFRINGEMENT, OR THAT USE OF THE SERVICES WILL BE UNINTERRUPTED OR ERROR-FREE. �SOME STATES DO NOT ALLOW LIMITATIONS ON HOW LONG AN IMPLIED WARRANTY LASTS, SO THE ABOVE LIMITATIONS MAY NOT APPLY TO YOU.

               1.1.9 limitations of liability TO THE MAXIMUM EXTENT PERMITTED BY APPLICABLE LAW, 360Memos SHALL NOT BE LIABLE FOR ANY INDIRECT, INCIDENTAL, SPECIAL, CONSEQUENTIAL OR PUNITIVE DAMAGES, OR ANY LOSS OF PROFITS OR REVENUES, WHETHER INCURRED DIRECTLY OR INDIRECTLY, OR ANY LOSS OF DATA, USE, GOOD-WILL, OR OTHER INTANGIBLE LOSSES, RESULTING FROM (i) YOUR ACCESS TO OR USE OF OR INABILITY TO ACCESS OR USE THE PRODUCTS; (ii) ANY CONDUCT OR CONTENT OF ANY THIRD PARTY ON,THROUGH, OR ASSOCIATED WITH THE PRODUCTS, INCLUDING WITHOUT LIMITATION, ANY DEFAMATORY, OFFENSIVE OR ILLEGAL CONDUCT OF OTHER USERS OR THIRD PARTIES; (iii) ANY CONTENT OBTAINED FROM THE PRODUCTS; OR (iv) UNAUTHORIZED ACCESS, USE OR ALTERATION OF YOUR TRANSMISSIONS OR CONTENT. IN NO EVENT SHALL THE AGGREGATE LIABILITY OF 360Memos EXCEED THE GREATER OF ONE HUNDRED U.S. DOLLARS (U.S. $100.00) OR THE AMOUNT YOU PAID 360Memos, IF ANY, IN THE PAST SIX MONTHS FOR THE PRODUCTS GIVING RISE TO THE CLAIM. THE LIMITATIONS OF THIS SUBSECTION SHALL APPLY TO ANY THEORY OF LIABILITY, WHETHER BASED ON WARRANTY, CONTRACT, STATUTE, TORT (INCLUDING NEGLIGENCE) OR OTHERWISE, AND WHETHER OR NOT 360Memos INFORMED OF THE POSSIBILITY OF ANY SUCH DAMAGE, AND EVEN IF A REMEDY SET FORTH HEREIN IS FOUND TO HAVE FAILED OF ITS ESSENTIAL PURPOSE. 1.1.1 indemnity You agree to indemnify and hold 360Memos, its affiliates, officers, agents, employees and partners harmless from and against any and all claims, liabilities, damages (actual and consequential), losses and expenses (including attorneys' fees) arising from or in any way related to any third party claims relating to ( a) your use of the Services( including any actions taken by a third party using your account) and (b) your violation of these Terms. In the event of such a claim, suit, or action ("Claim") we will attempt to provide notice of the Claim to the contact information we have for your account (provided that the failure to deliver such notice shall not eliminate or reduce your indemnification obligations here under) 1.1.10 assignment You may not assign, delegate or transfer these Terms or your rights or obligations here under or your services account, in any way ( by operation of law or otherwise) without your school and 360Memos prior written consent. We may transfer, assign or delegate these Terms and our rights and obligations without consent.

               1.1.11 miscellaneous You will be responsible for paying, withholding, filing, and reporting all taxes, duties, and other governmental assessments associated with your activity in connection with the Services, provided that 360Memos may, in its sole discretion, do any of the foregoing on your behalf or for itself as it sees fit. �The failure of either you or us to exercise, in any way, any right here in shall not be deemed a waiver of any further rights hereunder. �If any provision of these Terms is found to be unenforceable or invalid, that provision will be limited or eliminated, to the minimum extent necessary, so that these Terms shall otherwise remain in full force and effect and enforceable. �You and 360Memos agree that these Terms are the complete and exclusive statement of the mutual understanding between you and 360Memos, and that it supersedes and cancels all previous written and oral agreements, communications and other understandings relating to the subject matter of these Terms. �You hereby acknowledge and agree that you are not an employee, agent, partner, or joint venture of 360Memos, and you do not have any authority of any kind to bind 360Memos in any respect whatsoever. Except as expressly set forth in the section above regarding the Apple Application, you and 360Memos agree there are no third-party beneficiaries intended under these Terms.
             </Text>
            </View>
            </ScrollView>
          </View>
      )

    }else {
        console.log('----------------')
        console.log(this.props.login.showBusySpinner)
        console.log('----------------')
        if (this.props.login.showBusySpinner || this.props.isLoggingin){
          var ShowLoggingin = <ActivityIndicator
              style={styles.centering}
              animating={true}
              color="#0000ff"
              size="large"/>
        }else{
          var ShowLoggingin = null
        }
        return (
            <KeyboardAwareScrollView ref='scrollView' extraHeight={200} style={{flex : 1}}>
              <View
                  style={this.props.login.error != ''
                      ? styles.containerError
                      : styles.Logincontainer}>
                <ScrollView ref='scrollView' keyboardDismissMode='interactive' style={styles.scrollView}>
                  <View style={styles.header}>
                    <Image source={logo} style={styles.logo}/>
                      {ShowLoggingin}

                  </View>
                  <View style={styles.footer}>
                    <Text
                        style={styles.errorMessage}>{this.props.login.error === '' ? '' : this.props.login.error}</Text>
                    <View style={styles.inputWrap}>
                      <View style={styles.iconWrap}>
                        <Image source={personIcon} style={styles.icon} resizeMode="contain"/>
                      </View>
                      <TextInput ref="usernameInput"
                                 // onFocus={(event) => {
                                 //     // `bind` the function if you're using ES6 classes
                                 //     this._scrollToInput.bind(this, findNodeHandle(event.target))
                                 // }}
                           onFocus={this.textInputFocused.bind(this, 'usernameInput')}
                                 placeholderTextColor="darkgray"
                                 placeholder={'Email Address'}
                                 style={styles.input}
                                 autoCorrect={false}
                                 autoCapitalize="none"
                                 defaultValue={this.props.login.cred.userid}
                                 onChangeText={(text) => this.props.setusername(text)}/>
                    </View>
                    <View style={styles.inputWrap}>
                      <View style={styles.iconWrap}>
                        <Image source={lockIcon} style={styles.icon} resizeMode="contain"/>
                      </View>
                      <TextInput ref="passwordInput"
                          // onFocus={this.textInputFocused.bind(this, 'passwordInput')}
                                 placeholderTextColor="darkgray"
                                 autoCorrect={false}
                                 autoCapitalize="none"
                                 placeholder="Password"
                                 style={styles.input}
                                 defaultValue={this.props.login.cred.password}
                                 onChangeText={(text) => this.props.setpassword(text)}
                                 secureTextEntry={this.state.togglePasswordVisibility}/>
                                 <View style={styles.iconWrap}>
                                   <TouchableOpacity style={{paddingRight : 10, paddingLeft : 10}} onPress={this.togglePasswordText.bind(this)}>
                                     <Text style={{color : 'gray', fontSize : 12}}>{this.state.togglePasswordVisibility ? "SHOW" : "HIDE"}</Text>
                        </TouchableOpacity>
                      </View>
                    </View>
                    <TouchableOpacity
                        activeOpacity={0.5}
                        onPress={() => {
                          this.props.isLoggingIn()
                          //this.props.registerUser('admin3@scolalabs.com', '12345', 'testtest')

                        }}
                    >
                      <View style={styles.button}>
                        <Text style={styles.buttonText}>Sign In</Text>
                      </View>
                    </TouchableOpacity>
                    <TouchableOpacity activeOpacity={0.5} onPress={this.showAgreement.bind(this)}>
                      <View>
                        <Text style={styles.licenseagreementtext}>By Signing In, I Agree to Terms & Conditions</Text>
                      </View>
                    </TouchableOpacity>
                    <View style={{flexDirection : 'row', justifyContent : 'space-between', paddingRight : 5, paddingLeft : 5}}>
                    <TouchableOpacity activeOpacity={0.5} onPress={this.lostPassword.bind(this)}>
                      <View>
                        <Text style={styles.forgotPasswordText}>Forgot Password ?</Text>
                      </View>
                    </TouchableOpacity>

                      <TouchableOpacity activeOpacity={0.5} onPress={this.showresetcode.bind(this)}>
                      <View>
                        <Text style={styles.forgotPasswordText}>Register</Text>
                      </View>
                    </TouchableOpacity>

                    </View>

                  </View>
                </ScrollView>
              </View>
            </KeyboardAwareScrollView>
        )
    }
  }

    showAgreement(){
      this.setState({
          showAgreement : true
      })
    }

  validateEmail = (email) => {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email)
  }


  lostPassword(){
    this.props.changeScreen('LostPassword')
    this.emailchange( this.props.login.cred.userid)
  }

  backtoLogin(){
    this.props.changeScreen('LoginScreen')

  }
}

const styles = StyleSheet.create({
  lostpasswordcontainer: {
    flex : 1,
    padding : 10,
    height : 800,
  },
  resetCodeText: {
    paddingTop : 50
  },
  container: {
    flex: 1,
    padding: 10,
      marginTop : 15,
    backgroundColor: 'white'
  },
    Logincontainer:{
        flex: 1,
        padding: 10,
        backgroundColor: 'white'
    },
  containerError: {
    flex: 1,
    padding: 10,
    //backgroundColor: '#F24949'
  },
  invalidEmail: {
    color : 'red',
    textAlign : 'center'
  },
  successEmail: {
    color : 'blue',
    textAlign : 'center'
  },
  backtoLogin: {
    paddingTop : 50
  },
  validEmail: {
    color : 'red',
    textAlign : 'center'
  },
  background: {
    resizeMode: 'cover'
  },
  logo: {
    width: 300,
      ...Platform.select({
          ios: {
              height: 325,
              resizeMode: 'contain'
          },
          android: {
              height: 185,
              resizeMode: 'contain'
          },
      }),
  },
  header: {
    flex: 2,
    alignItems: 'center'
  },
  footer: {
    flex: 1
  },
  setpasswordContainer: {
    paddingTop : 20
  },
  inputWrap: {
    flexDirection: 'row',
    marginVertical: 10,
    height: (Platform.OS === 'ios') ? 35 : 45,
    borderBottomWidth: (Platform.OS === 'ios')
      ? 1
      : 0,
    borderBottomColor: '#CCC'
  },
  iconWrap: {
    paddingHorizontal: 7,
    alignItems: 'center',
    justifyContent: 'center'
  },
  icon: {
    height: 20,
    width: 20
  },
    headerText: {
      fontSize : 16,
      fontWeight: 'bold'
    },
  input: {
    flex: 1,
    paddingHorizontal: 10,
      fontSize : 20
  },
  button: {
    backgroundColor: '#3b479d',
    paddingVertical: 12,
    alignItems: 'center',
    justifyContent: 'center',
      borderRadius : 5,
  },

  buttonText: {
    color: '#FFF',
    fontSize: 18
  },
  forgotPasswordText: {
    color: '#173e43',
    backgroundColor: 'transparent',
    textAlign: 'center',
    paddingVertical: 5
  },
  registerText : {
    color: '#173e43',
    backgroundColor: 'transparent',
    textAlign: 'center',
    paddingVertical: 5,

  },
    licenseagreementtext: {
        color: '#173e43',
        backgroundColor: 'transparent',
        textAlign: 'center',
        fontSize : 12,
        paddingVertical: 5,
        textDecorationLine: 'underline'
    },
  centering: {
    alignItems: 'center',
    justifyContent: 'center',
    padding: 8
  },
  errorMessage: {
    textAlign: 'center',
    color: 'red',
    fontSize: 14,
      paddingTop : 10
  },
  scrollView: {
    flex: 1
  }
})

var mapStateToProps = function (store) {
  return {login: store.login,
    user: store.user}
}

var mapDispatchToProps = function (dispatch) {
  return {
    setusername: function (username) { dispatch(loginActions.setUserName(username)) },
    setpassword: function (password) { dispatch(loginActions.setPassword(password)) },
    authenticate: function (username, password, userobj) { dispatch(loginActions.authenticateUser(username, password, userobj)) },
    resetError: function () { dispatch(loginActions.resetError()) },
    resetPassword: function (email) { dispatch(loginActions.resetPassword(email))},
    setEmailAddress: function (emailstr) { dispatch(loginActions.setEmailAddress(emailstr))},
    changePassword: function (emailstr, resetcode, password) { dispatch(loginActions.changePassword(emailstr,resetcode, password))},
    registerUser: function (emailstr, resetcode, password) { dispatch(loginActions.registerUser(emailstr,resetcode, password))},
    changeScreen: function (screenName) { dispatch(loginActions.changeScreen(screenName))},
      isLoggingIn: function () {
          dispatch(loginActions.isLoggingIn())
      }
    }
  }

export default connect(mapStateToProps, mapDispatchToProps, null, {withRef: true})(LoginScreen)
