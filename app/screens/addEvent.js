/* eslint-disable no-unused-vars */
"use strict";

import {
  Platform,
  AppRegistry,
  ScrollView,
  StyleSheet,
  View,
  Alert,
  ImageBackground,
  TouchableOpacity,
  Switch,
  Text,
  Linking,
  Dimensions,
} from "react-native";
import * as Animatable from "react-native-animatable";
import Entypo from "react-native-vector-icons/Entypo";
import Image from "react-native-image-progress";
import Dialog, { DialogContent } from "react-native-popup-dialog";
import AutoHeightWebView from "react-native-autoheight-webview";
import { CustomCachedImage } from "react-native-img-cache";
import { Storage } from "aws-amplify";
import ImagePicker from "react-native-image-crop-picker";
import { ProgressCircle } from "react-native-svg-charts";
import Spinner from "react-native-loading-spinner-overlay";
import * as Layout from "../constants/Layout";
import React, { Component } from "react";
import DateTimePicker from "react-native-modal-datetime-picker";
import Accordian from "./components/accordian";
import { connect } from "react-redux";
import Icon from "react-native-vector-icons/FontAwesome";
import MaterialIcons from "react-native-vector-icons/MaterialIcons";
import * as postActions from "../actions/postActions";
import { commonStyle } from "../config/commonstyles";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import * as eventActions from "../actions/eventActions";
import * as miscutils from "../utils/misc";
import Modal from "react-native-modal";
import { WebView } from "react-native-webview";
import ModalMemberListScreen from "./components/modalmemberlist";
import shallowCompare from "react-addons-shallow-compare";
import { UploadPhoto } from "../api/uploadmedia";
import {
  Card,
  Tile,
  ThemeProvider,
  ButtonGroup,
  Button,
  Divider,
} from "react-native-elements";
import { H3, Item, Input } from "native-base";
var EnvConfig = require("../config/environment");
import Picker from "react-native-picker";
import RNFetchBlob from "react-native-fetch-blob";
import { Buffer } from "buffer";
import { RNS3 } from "react-native-aws3";
import { Navigation } from "react-native-navigation";
import { throws } from "assert";
import NumberSpinner from "./components/numberspinner";
import * as eventsapi from "../api/eventsapi";

const rsvpButtons = ["Yes", "Maybe", "No"];

class AddEventScreen extends Component {
  constructor(props) {
    super(props);
    console.log(props);
    this.state = {
      _showParents: false,
      selectedUsers: {},
      formData: {},
      errorText: "",
      _alldayevent: true,
      isEditable: true,
      parentchooservisible: false,
      eventimgurl: null,
      coverimage: null,
      updatingImage: false,
      _eventname: "",
      _eventdesc: "",
      _eventstartDate: "",
      _eventendDate: "",
      _eventstartTime: "",
      _eventendTime: "",
      _eventlocation: "",
      _eventnotify: false,
      _eventstartDateObj: Date(),
      _eventendDateObj: null,

      _remindsameday: false,
      _remindsOneday: false,
      _remindsTwoday: false,
      _reminderCount: 0,
      rsvpBtnSelect: null,
      groupshash: {},

      descHeight: 100,
      startDatePickerVisible: false,
      startTimePickerVisible: false,
      endDatePickerVisible: false,
      endTimePickerVisible: false,
      _grpSelect: "",
      _grpSelectId: 0,
      rsvpIndex: 4,
      adultcount: 1,
      kidscount: 0,
      childDialogModal: false,
      childDialogModalData: [],
      childDialogModalselection: null,
      showRefreshing: false,
      type3signupSlot: null,
      type4signupSlot: null,
      event4data: null,
      loading: false,
      _eventStep: 0,
      _selectGroupParent: "Group",
      _selectNoticationDayAgo: null,
      _addAttachment: null,
      _addAttachmentUrl: null,
      _disableNextStepBtn: true,
    };
    this.updateRSVPIndex = this.updateRSVPIndex.bind(this);
    this.confirmRSVP = this.confirmRSVP.bind(this);
    this.showSpinner = this.showSpinner.bind(this);
    this.onNavigationURLChange = this.onNavigationURLChange.bind(this);
    Navigation.events().bindComponent(this);
    if (this.props.eventid !== null) {
      if (this.props.eventData === undefined) return;
      this.eventData = this.props.eventdata;
      //this.state.eventimgurl = this.props.eventdata.eventimgurl

      if (
        this.eventData.starttime !== null ||
        this.eventData.endtime !== null
      ) {
        this.state._alldayevent = false;
      }
    } else {
      this.eventData = null;
    }
  }

  showSpinner() {
    // if (Platform.OS == 'android') {
    //   return  false

    // }else{
    return this.props.events.isLoading;
    //}
  }

  componentWillMount() {
    console.log("AddEvent->CWM");
    console.log(this.props.eventid);
    if (this.props.eventid != null) {
      this.getCoverImage();
    } else {
      this.setState({
        eventimgurl: require("../img/bg.png"),
      });
    }
  }

  navigationButtonPressed({ buttonId }) {
    if (buttonId === "closebutton") {
      this.setState({
        childDialogModal: false,
        childDialogModalselection: null,
      });
      Navigation.dismissAllModals();
    }
  }

  showParentChooser() {
    this.setState({ parentchooservisible: true });
  }

  hideParentChooser() {
    this.setState({ parentchooservisible: false });
  }

  onNavigatorEvent(event) {
    if (event.id === "closePost") {
      this.props.navigator.dismissModal();
    }
  }

  componentDidUpdate(prevProps, prevState) {
    const {
      _eventname,
      _eventlocation,
      _grpSelect,
      _selectGroupParent,
      selectedUsers,
      _eventstartDate,
      _eventendDate,
      _eventstartTime,
      _eventendTime,
    } = this.state;
    if (
      this.state.childDialogModal === false &&
      prevState.childDialogModal === true &&
      this.state.childDialogModalselection != null &&
      prevState.childDialogModalselection === null
    ) {
      console.log("Sign up CHild !!!!!");
      console.log(
        (this.state.type3signupSlot, this.state.childDialogModalselection.id)
      );
      if (this.props.events.events[this.props.eventid].eventtype === 3) {
        this.postsignupSlotEvent3(
          this.state.type3signupSlot,
          this.state.childDialogModalselection.id
        );
      } else {
        this.postsignupSpotEvent4(
          this.state.type4signupSlot,
          this.state.childDialogModalselection.id
        );
      }
    }
    console.log(this.state, "---aa---");
    if (
      prevState._eventname !== _eventname ||
      prevState._grpSelect !== _grpSelect ||
      prevState._selectGroupParent !== _selectGroupParent ||
      prevState.selectedUsers !== selectedUsers
    ) {
      if (
        _eventname &&
        _selectGroupParent &&
        (_grpSelect || Object.keys(selectedUsers).length !== 0) &&
        _eventstartDate &&
        _eventendDate &&
        _eventstartTime &&
        _eventendTime
      ) {
        this.setState({ _disableNextStepBtn: false });
      } else {
        this.setState({ _disableNextStepBtn: true });
      }
    }
  }

  shouldComponentUpdate(nextProps, nextState) {
    //return shallowCompare(this, nextProps, nextState);

    return true;
  }

  renderImageHeader(isEditable) {
    if (this.state.formData !== null) {
      var headerText = (
        <Text style={styles.headerTextStyle}>
          {this.state.formData.eventname}
        </Text>
      );
    } else {
      var headerText = <Text style={styles.headerTextStyle}></Text>;
    }
    console.log(this.state.eventimgurl);
    if (this.state.eventimgurl === "null" || this.state.eventimgurl === null) {
      var coverurl = require("../img/bg.png");
    } else {
      if (
        typeof this.state.eventimgurl === "string" &&
        this.state.eventimgurl.includes("http")
      ) {
        var coverurl = { uri: this.state.eventimgurl };
      } else {
        var coverurl = this.state.eventimgurl;
      }
    }
    console.log(coverurl);
    return (
      <View>
        <ImageBackground
          style={styles.headerimage}
          resizeMethod={"auto"}
          resizeMode={"cover"}
          source={coverurl}
          // onLoad={()=>{console.log("coverurl")}}
        >
          {headerText}
          {this.renderCameraCoverPhoto()}
        </ImageBackground>
      </View>
    );
  }

  getCoverImage() {
    var coverurl = require("../img/bg.png");
    if (
      this.props.eventdata.eventimgurl === null ||
      this.props.eventdata.eventimgurl === "" ||
      this.props.eventdata.eventimgurl === "0" ||
      this.props.eventdata.eventimgurl === "null"
    ) {
      this.setState({ eventimgurl: null });
    } else {
      if (
        this.props.eventdata.eventimgurl.includes("https:") ||
        this.props.eventdata.eventimgurl.includes("http:")
      ) {
        coverurl = { uri: this.props.eventdata.eventimgurl };
        console.log(coverurl);
        this.setState({ eventimgurl: coverurl });
      } else {
        var url = Storage.get(this.props.eventdata.eventimgurl).then((url) => {
          this.setState({ eventimgurl: url });
          return url;
        });
      }
    }
  }

  renderCameraCoverPhoto() {
    if (this.state.isEditable && this.state._eventStep <= 0) {
      return (
        <View
          style={{
            alignItems: "center",
            justifyContent: "flex-end",
            flex: 1,
            marginBottom: 10,
          }}
        >
          <TouchableOpacity style={{ backgroundColor: "transparent" }}>
            <Button
              title="Change Picture"
              type="outline"
              buttonStyle={{
                backgroundColor: "transparent",
                width: 200,
                borderRadius: 20,
              }}
              onPress={this.onClickCoverCamera.bind(this)}
              titleStyle={{ color: "white" }}
            />
          </TouchableOpacity>
        </View>
      );
    } else {
      return null;
    }
  }
  // renderCameraCoverPhoto() {
  //   if (this.state.isEditable) {
  //     return (
  //       <View style={{}}>
  //         <TouchableOpacity
  //           onPress={this.onClickCoverCamera.bind(this)}
  //           style={{ backgroundColor: "transparent" }}
  //         >
  //           <Icon
  //             name="camera"
  //             style={styles.cameraicon}
  //             size={30}
  //             zIndex={10}
  //           />
  //         </TouchableOpacity>
  //       </View>
  //     );
  //   } else {
  //     return null;
  //   }
  // }

  onClickCoverCamera() {
    ImagePicker.openPicker({
      width: 800,
      height: 600,
      cropping: true,
      smartAlbums: [
        "UserLibrary",
        "PhotoStream",
        "RecentlyAdded",
        "SelfPortraits",
        "Screenshots",
        "Generic",
      ],
    }).then((image) => {
      this.setState({ coverimage: image });
      this.setState({ eventimgurl: { uri: image.path } });
    });
  }
  onClickAddAttachement() {
    ImagePicker.openPicker({
      width: 800,
      height: 600,
      cropping: true,
      smartAlbums: [
        "UserLibrary",
        "PhotoStream",
        "RecentlyAdded",
        "SelfPortraits",
        "Screenshots",
        "Generic",
      ],
    }).then((image) => {
      console.log(image, "image");
      this.setState({ _addAttachment: image });
      this.setState({ _addAttachmentUrl: { uri: image.path } });
    });
  }
  ParentChooserModal() {
    if (this.state.parentchooservisible) {
      return (
        <Modal
          onRequestClose={() => {
            console.log("Modal has been closed.");
          }}
          transparent={true}
          isVisible={this.state.parentchooservisible}
          style={styles.modal}
        >
          <ModalMemberListScreen
            modaldismissfunc={this.hideParentChooser.bind(this)}
            onSelectedList={(e) => this.onSelectedList(e)}
            initialSelectedList={this.state.selectedUsers}
          />
        </Modal>
      );
    } else {
      return null;
    }
  }
  _eventStepper() {
    const { _eventStep } = this.state;
    return (
      <>
        <View style={styles.stepperContainer}>
          <View style={styles.stepperElement}>
            <View
              style={_eventStep === 0 ? styles.hightlightRoundElement: styles.roundElement}>
              <Text
                style={_eventStep === 0
                    ? { color: "white", fontSize: 18, fontWeight: "bold" }
                    : { color: "#3C3989", fontSize: 18, fontWeight: "bold" }}>
                1
              </Text>
            </View>
            <Text style={_eventStep === 0 ? styles.highlightedStepText : styles.stepText}>
              General Details
            </Text>
          </View>
          <View style={styles.stepperElement}>
            <View
              style={_eventStep === 1? styles.hightlightRoundElement: styles.roundElement}
            >
              <Text
                style={
                  _eventStep === 1
                    ? { color: "white", fontSize: 18, fontWeight: "bold" }
                    : { color: "#3C3989", fontSize: 18, fontWeight: "bold" }
                }
              >
                2
              </Text>
            </View>
            <Text style={_eventStep === 1 ? styles.highlightedStepText : styles.stepText}>
              Preview
            </Text>
          </View>
        </View>
        <Divider style={{ backgroundColor: "blue" }} />
      </>
    );
  }
  _footerTab() {
    return (
      <View
        style={{
          flexDirection: "row",
          padding: 10,
          borderTopColor: "#ccc",
          borderTopWidth: 1,
          alignItems: "center",
          justifyContent: "center",
        }}
      >
        <Button
          type="clear"
          disabled={this.state._eventStep <= 0}
          buttonStyle={
            !this.state._eventStep <= 0
              ? {
                  backgroundColor: "#3C3989",
                  borderRadius: 50,
                }
              : {
                  backgroundColor: "#cccccc",
                  borderRadius: 50,
                }
          }
          icon={
            <Entypo
              name="chevron-left"
              style={styles.actionButtonIcon}
              size={24}
            />
          }
          onPress={this._handleNavigateBackInRsvp.bind(this)}
        />
        <View style={{ flex: 1 }}>
          <Button
            title="Cancel"
            type="clear"
            titleStyle={{ color: "#000" }}
            onPress={() => {
              this.navigationButtonPressed({ buttonId: "closebutton" });
            }}
          />
        </View>
        <Button
          type="clear"
          disabled={
            this.state._disableNextStepBtn || this.state._eventStep >= 1
          }
          buttonStyle={
            !this.state._disableNextStepBtn && !this.state._eventStep >= 1
              ? {
                  backgroundColor: "#3C3989",
                  borderRadius: 50,
                }
              : {
                  backgroundColor: "#cccccc",
                  borderRadius: 50,
                }
          }
          icon={
            <Entypo
              name="chevron-right"
              style={styles.actionButtonIcon}
              size={24}
            />
          }
          onPress={this._handleNavigateNextInRsvp.bind(this)}
        />
      </View>
    );
  }
  _handleNavigateBackInRsvp() {
    this.setState({ _eventStep: this.state._eventStep - 1 });
  }
  _handleNavigateNextInRsvp = () => {
    if (this.state._eventStep >= 1) {
      // this.navigationButtonPressed({ buttonId: "closebutton" });
    } else {
      this.setState({ _eventStep: this.state._eventStep + 1 });
    }
  };

  validateElement(elementValue, element, errtext, setFocus = false) {
    if (
      elementValue === undefined ||
      elementValue === null ||
      elementValue.length === 0
    ) {
      if (setFocus) {
        element.focus();
      }
      this.setState({ errorText: "Error, " + errtext + "!" });
      return false;
    }
    return true;
  }

  getDatefromTimeStr(timestr) {
    // Format "07:14 PM"
    var d = new Date();
    var timearray = timestr.split(":");
    var hour = parseInt(timearray[0]);
    timearray = timearray[1].split(" ");

    var minute = timearray[0];
    var ampm = timearray[1];
    if (ampm === "PM") hour = hour + 12;
    if (hour !== undefined) {
      d.setHours(hour.toString());
    }
    d.setMinutes(minute);

    return d;
  }

  componentDidMount() {
    var event = this.props.events.events[this.props.eventid];
    if (this.props.isReadonly) {
      this.setState({ isEditable: false });
    } else {
      if (
        this.props.user.roles === "parent" ||
        this.props.user.roles === "attendance"
      ) {
        this.setState({ isEditable: false });
      } else {
        this.setState({ isEditable: true });
      }
    }
    var groups = this.getGroupData();
    var hash = {};
    var firstkey = null;
    Object.keys(groups).forEach(function (key) {
      hash[groups[key]] = key;
      if (firstkey == null) firstkey = key;
    });

    console.log(this.state);
    if (
      this.props.eventid !== null ||
      (this.props.eventCloneId && this.props.eventCloneId !== null)
    ) {
      var eventprops = this.props.events.events[
        this.props.eventid || this.props.eventCloneId
      ];
      var users = {};
      for (var i in this.props.eventdata.users) {
        var username = this.props.eventdata.users[i];
        if (username !== this.props.user.username)
          users[username] = miscutils.getUserfromCache(
            this.props.login.usercache,
            username
          );
      }
      console.log(this.props.eventdata);
      if (
        this.props.eventdata.endtime === this.props.eventdata.starttime &&
        this.props.eventdata.startdate === this.props.eventdata.enddate
      ) {
        var _alldayevent = true;
      } else {
        _alldayevent = false;
      }
      console.log(_alldayevent);
      // RSVP Settings for EventType2
      var _rsvpindex = 4;
      var _adultcount = 1;
      var _kidscount = 0;
      var _selectGroupParent = null;
      var _reminderCount = 0;
      var rsvpBtnSelect = null;
      if (eventprops.eventtype === 2) {
        if (
          eventprops.userresponse !== undefined &&
          eventprops.userresponse.user_response !== null &&
          typeof eventprops.userresponse.user_response == "object" &&
          eventprops.userresponse.user_response.user ===
            this.props.user.username
        ) {
          _rsvpindex = rsvpButtons.indexOf(
            eventprops.userresponse.user_response.attend
          );
          _adultcount = eventprops.userresponse.user_response.adult;
          _kidscount = eventprops.userresponse.user_response.children;
          console.log(_adultcount);
          console.log(_kidscount);
        }
      }
      if (
        eventprops.users.length > 0 &&
        (eventprops.group_id === null || eventprops.grpid === null)
      ) {
        _selectGroupParent = "Parents";
      } else {
        _selectGroupParent = "Group";
      }
      if (eventprops.remindoneday === 1) {
        _reminderCount = _reminderCount + 1;
      }
      if (eventprops.remindtwoday === 1) {
        _reminderCount = _reminderCount + 1;
      }
      if (eventprops.remindsameday === 1) {
        _reminderCount = _reminderCount + 1;
      }
      if (event.attend !==null) {
        rsvpBtnSelect = event.attend;
      }
      this.setState({
        groupshash: hash,
        _eventname: this.props.eventdata.eventname,
        _eventdesc: this.props.eventdata.eventdesc,
        _eventstartDate: this.GetFormattedDate(this.props.eventdata.startdate),
        _eventendDate: this.GetFormattedDate(this.props.eventdata.enddate),
        _eventstartTime: miscutils.formatTimeinAMPM(
          this.props.eventdata.starttime
        ),
        _eventendTime: miscutils.formatTimeinAMPM(this.props.eventdata.endtime),
        _eventlocation: this.props.eventdata.location,
        _eventnotify: this.props.eventdata.notifyusers === 0 ? false : true,
        _eventstartDateObj: new Date(this.props.eventdata.startdate),
        _eventendDateObj: new Date(this.props.eventdata.enddate),
        eventimgurl: this.props.eventdata.eventimgurl,
        _remindsameday: this.props.eventdata.remindsameday === 0 ? false : true,
        _remindsOneday: this.props.eventdata.remindoneday === 0 ? false : true,
        _remindsTwoday: this.props.eventdata.remindtwoday === 0 ? false : true,
        _grpSelectId: this.props.eventdata.groupid,
        _grpSelect: groups[this.props.eventdata.groupid],
        _alldayevent: _alldayevent,
        _showParents: this.props.eventdata.users.length === 0 ? false : true,
        selectedUsers: Object.assign(users),
        rsvpIndex: _rsvpindex,
        adultcount: _adultcount,
        kidscount: _kidscount,
        _selectGroupParent: _selectGroupParent,
        _reminderCount: _reminderCount,
        rsvpBtnSelect: rsvpBtnSelect
      });
      //RSVP Changes
    } else {
      this.setState({
        groupshash: hash,
        _eventstartDate: this.GetFormattedDate(new Date()),
        _eventendDate: this.GetFormattedDate(new Date()),
        _eventstartTime: miscutils.formatTimeinAMPM(new Date()),
        _eventendTime: miscutils.formatTimeinAMPM(new Date()),
        _grpSelectId: firstkey,
        _grpSelect: groups[firstkey],
      });
    }
  }

  async uploadEventPhoto(image) {
    const options = {
      bucket: EnvConfig.AWS_CONFIG.S3_PROFILE_BUCKET,
      region: EnvConfig.AWS_CONFIG.S3_BUCKET_REGION,
      accessKey: EnvConfig.AWS_CONFIG.S3_PROFILE_ACCESS_KEY,
      secretKey: EnvConfig.AWS_CONFIG.S3_PROFILE_SECRET_KEY,
      successActionStatus: 201,
    };
    var filename = "Event/EventPhoto_" + Date.now() + ".jpg";
    const file = {
      uri: image.path,
      type: "image/jpeg",
      name: filename,
    };
    var response = await RNS3.put(file, options)
      .then((response) => {
        if (response.status !== 201) {
          throw new Error("Failed to upload image to S3");
        }
        var url = EnvConfig.AWS_CONFIG.S3_PROFILE_URL + filename;
        return url;
      })
      .catch((error) => {
        dispatch({ type: "UPLOAD_PROFILE_PHOTO_REJECTED", payload: err });
        return error;
      });
    return response;
  }

  getGroupData() {
    var groups = {};
    for (var key in this.props.user.groups) {
      var group = this.props.user.groups[key];
      if (
        group.is_admin === 1 ||
        group.is_mod === 1 ||
        group.is_admin === "1" ||
        group.is_mod === "1"
      ) {
        groups[group.id] = group.name.replace(/_/g, " ");
      } else {
        if (!this.state.isEditable) {
          groups[group.id] = group.name.replace(/_/g, " ");
        }
      }
    }
    return groups;
  }

  handleFormFocus(event, reactNode) {
    this.refs.scroll.scrollToFocusedInput(reactNode);
  }

  RenderErrorText() {
    if (this.state.errorText !== "") {
      return <Text style={styles.errorText}>{this.state.errorText}</Text>;
    } else {
      return null;
    }
  }

  getCalendarIcons() {
    if (Platform.OS === "ios") {
      return [
        <Icon
          key="0"
          style={{ alignSelf: "center", marginLeft: 10 }}
          name="calendar"
          size={24}
        />,
        <Icon
          key="1"
          style={{ alignSelf: "center", marginLeft: 10, color: "purple" }}
          name="calendar"
          size={24}
        />,
      ];
    } else {
      return [
        <Icon
          key="1"
          style={{
            alignSelf: "center",
            paddingBottom: 20,
            marginLeft: 10,
            color: "purple",
          }}
          name="calendar"
          size={24}
        />,
      ];
    }
  }
  renderMainWidget() {
    console.log(this.state, this.props, "renderMainWidget");
    return (
      <View style={{ flex: 1 }}>
        <Spinner
          visible={this.showSpinner()}
          textContent={"Loading..."}
          textStyle={{ color: "#FFF" }}
        />
        {this.ParentChooserModal()}
        <View style={styles.container}>
          {this.RenderErrorText()}
          <ThemeProvider theme={theme}>
            <KeyboardAwareScrollView ref="scroll">
              <View style={{ marginBottom: 50 }}>
                {this._eventStepper()}
                {this.renderImageHeader(this.props.isEditable)}
                {/* //1st Section */}
                <View style={{ marginTop: 5 }}>
                  <Text style={styles.sectionTextHeading}>General Details</Text>
                  <View style={{ padding: 10 }}>
                    <Text style={styles.inputlabel}>Event name</Text>
                    <Item rounded style={styles.inputFieldCover}>
                      <Input
                        errorStyle={{ color: "red" }}
                        ref={(component) => (this._inputeventName = component)}
                        value={this.state._eventname}
                        style={styles.inputFieldStyles}
                        onChangeText={(text) =>
                          this.setState({ _eventname: text })
                        }
                      />
                    </Item>
                  </View>
                  <View style={{ padding: 10 }}>
                    <Text style={styles.inputlabel}>Description</Text>
                    <Item rounded style={styles.inputFieldCover}>
                      <Input
                        errorStyle={{ color: "red" }}
                        multiline={true}
                        value={this.state._eventdesc}
                        onChangeText={(text) =>
                          this.setState({ _eventdesc: text })
                        }
                        inputStyle={{
                          height: this.state.descHeight,
                        }}
                        style={styles.inputFieldStyles}
                        numberOfLines={3}
                        onContentSizeChange={(e) =>
                          this.updateDescSize(e.nativeEvent.contentSize.height)
                        }
                      />
                    </Item>
                  </View>
                  <View style={{ padding: 10 }}>
                    <Text style={styles.inputlabel}>Location</Text>
                    <Item rounded style={styles.inputFieldCover}>
                      <Input
                        ref={(component) =>
                          (this._inputeventLocation = component)
                        }
                        errorStyle={{ color: "red" }}
                        value={this.state._eventlocation}
                        style={styles.inputFieldStyles}
                        onChangeText={(text) =>
                          this.setState({ _eventlocation: text })
                        }
                      />
                    </Item>
                  </View>
                  <View
                    style={{
                      justifyContent: "space-between",
                      flexDirection: "row",
                      padding: 10,
                    }}
                  >
                    <Text style={styles.generalTextView}>
                      {this.state._addAttachmentUrl && "1 file selected"}
                    </Text>
                    <Button
                      buttonStyle={{
                        width: 180,
                        backgroundColor: "#05B9A4",
                        borderRadius: 20,
                      }}
                      icon={
                        <Entypo name="attachment" size={15} color="white" />
                      }
                      title="Add Attachments"
                      onPress={this.onClickAddAttachement.bind(this)}
                    />
                  </View>
                </View>
                {/* //2nd Section */}
                <View style={{ marginTop: 5 }}>
                  <Text style={styles.sectionTextHeading}>
                    Select Date {"&"} Time
                  </Text>
                  <TouchableOpacity
                    onPress={() =>
                      this.setState({ startDatePickerVisible: true })
                    }
                  >
                    <View style={{ padding: 10 }} pointerEvents="none">
                      <Item
                        rounded
                        style={styles.inputFieldCover}
                        onPress={() =>
                          this.setState({ startDatePickerVisible: true })
                        }
                      >
                        <Input
                          placeholder="Start Date"
                          editable={false}
                          style={styles.inputFieldStyles}
                          errorStyle={{ color: "red" }}
                          value={this.state._eventstartDate}
                          onPress={() =>
                            this.setState({ startDatePickerVisible: true })
                          }
                        />
                        <Icon
                          active
                          name="calendar"
                          size={24}
                          style={{ marginRight: 10, color: "black" }}
                          onPress={() =>
                            this.setState({ startDatePickerVisible: true })
                          }
                        />
                      </Item>
                    </View>
                  </TouchableOpacity>
                  {this.renderStartTime()}
                  <TouchableOpacity
                    onPress={() =>
                      this.setState({ endDatePickerVisible: true })
                    }
                  >
                    <View style={{ padding: 10 }} pointerEvents="none">
                      <Item
                        rounded
                        style={styles.inputFieldCover}
                        onPress={() =>
                          this.setState({ endDatePickerVisible: true })
                        }
                      >
                        <Input
                          errorStyle={{ color: "red" }}
                          value={this.state._eventendDate}
                          placeholder="End Date"
                          editable={false}
                          style={styles.inputFieldStyles}
                        />
                        <Icon
                          active
                          name="calendar"
                          size={24}
                          style={{ marginRight: 10, color: "black" }}
                          onPress={() =>
                            this.setState({ endDatePickerVisible: true })
                          }
                        />
                      </Item>
                    </View>
                  </TouchableOpacity>
                  {this.renderEndTime()}
                  {this.RenderAllDaySwitch()}
                </View>
                {/* //3rd Section */}
                <View style={{ marginTop: 5 }}>
                  <Text style={styles.sectionTextHeading}>
                    Add Group/Parents
                  </Text>
                  <TouchableOpacity
                    onPress={this.onSelectGroupsParents.bind(this)}
                  >
                    <View style={{ padding: 10 }} pointerEvents="none">
                      <Item rounded style={styles.inputFieldCover}>
                        <Input
                          placeholder="Select Group"
                          editable={false}
                          style={styles.inputFieldStyles}
                          value={this.state._selectGroupParent}
                        />
                        <Entypo
                          active
                          name="chevron-down"
                          size={24}
                          style={{ marginRight: 10, color: "black" }}
                        />
                      </Item>
                    </View>
                  </TouchableOpacity>
                  {this.renderInviteSection()}
                </View>
                {/* //4rth Section */}
                <View style={{ marginTop: 5 }}>
                  <Text style={styles.sectionTextHeading}>Notifications</Text>
                  {this.RenderSetNotifyParent()}
                </View>
                {/* //5th Section */}
                <View style={{ marginTop: 5 }}>
                  <Text style={styles.sectionTextHeading}>Set Reminders</Text>
                  {this.RenderSetReminderSameDay()}
                  {this.RenderSetReminderOneDay()}
                  {this.RenderSetReminderTwoDay()}
                </View>
              </View>
              <DateTimePicker
                mode="date"
                isVisible={this.state.startDatePickerVisible}
                minimumDate={new Date()}
                onConfirm={(dt) => {
                  this.handleConfirm(dt, "startDate");
                }}
                onCancel={() =>
                  this.setState({ startDatePickerVisible: false })
                }
              />
              <DateTimePicker
                mode="date"
                minimumDate={new Date(this.state._eventstartDateObj)}
                isVisible={this.state.endDatePickerVisible}
                onConfirm={(dt) => {
                  this.handleConfirm(dt, "endDate");
                }}
                onCancel={() => this.setState({ endDatePickerVisible: false })}
              />
              <DateTimePicker
                mode="time"
                display="calendar" // 'default', 'spinner', 'calendar', 'clock' // Android Only
                minuteInterval={5}
                isVisible={this.state.startTimePickerVisible}
                onConfirm={(dt) => {
                  this.handleConfirmTime(dt, "startTime");
                }}
                onCancel={() =>
                  this.setState({ startTimePickerVisible: false })
                }
                titleIOS={"Pick a Start Time"}
              />
              <DateTimePicker
                mode="time"
                display="calendar" // 'default', 'spinner', 'calendar', 'clock' // Android Only
                minuteInterval={5}
                isVisible={this.state.endTimePickerVisible}
                onConfirm={(dt) => {
                  this.handleConfirmTime(dt, "endTime");
                }}
                onCancel={() => this.setState({ endTimePickerVisible: false })}
                titleIOS={"Pick an End Time"}
              />
              {/* {this.RenderSaveButton()} */}
              {/* {this.RenderDeleteButton()} */}
            </KeyboardAwareScrollView>
          </ThemeProvider>
          {this._footerTab()}
        </View>
      </View>
    );
  }

  renderMainPreviewWidget() {
    console.log(this.state, this.props, "renderMainPreviewWidget");
    return (
      <View style={{ flex: 1 }}>
        <Spinner
          visible={this.showSpinner()}
          textContent={"Loading..."}
          textStyle={{ color: "#FFF" }}
        />
        <View style={styles.container}>
          {this.RenderErrorText()}
          <ThemeProvider theme={theme}>
            <KeyboardAwareScrollView ref="scroll">
              <View style={{ marginBottom: 30 }}>
                {this._eventStepper()}
                {this.renderImageHeader(this.props.isEditable)}
                <View style={{ paddingLeft: 10, paddingBottom: 50 }}>
                  <Text style={styles.newSectionHeading}>
                    {this.state._eventname}
                  </Text>
                  <Text style={styles.newGeneralTextView}>
                    {this.state._eventdesc}
                  </Text>
                  <View style={{ flexDirection: "row", margin: 10 }}>
                    <Entypo
                      active
                      name="location-pin"
                      color="#05B9A4"
                      size={24}
                      style={{ width: 40 }}
                    />
                    <Text style={styles.previewTextShow}>
                      {this.state._eventlocation}
                    </Text>
                  </View>

                  {this.dateview()}
                  <View style={{ flexDirection: "row", margin: 10 }}>
                    <Icon
                      active
                      name="clock-o"
                      color="#05B9A4"
                      size={24}
                      style={{ width: 40 }}
                    />
                    <Text style={styles.previewTextShow}>
                      {`${this.state._eventstartTime} - ${this.state._eventendTime}`}
                    </Text>
                  </View>
                  {this.state._grpSelect !== "" && (
                    <View style={{ flexDirection: "row", margin: 10 }}>
                      <Icon
                        active
                        name="group"
                        color="#05B9A4"
                        size={24}
                        style={{ width: 40 }}
                      />
                      <Text style={styles.previewTextShow}>
                        {this.state._grpSelect}
                      </Text>
                    </View>
                  )}
                  {Object.keys(this.state.selectedUsers).length !== 0 && (
                    <View style={{ flexDirection: "row", margin: 10 }}>
                      <MaterialIcons
                        active
                        name="group"
                        color="#05B9A4"
                        size={24}
                        style={{ width: 40 }}
                      />
                      <Text style={styles.previewTextShow}>
                        {Object.keys(this.state.selectedUsers).length} Parents
                        Selected
                      </Text>
                    </View>
                  )}
                  <View style={{ flexDirection: "row", margin: 10 }}>
                    <Icon
                      active
                      name="bell"
                      color="#FF0000"
                      size={24}
                      style={{ width: 40 }}
                    />
                    <Text style={styles.previewTextShow}>
                      {this.state._reminderCount} Reminders
                    </Text>
                  </View>
                </View>

                <View style={{ paddingBottom: 20 }}>
                  <View
                    style={{
                      justifyContent: "center",
                      flexDirection: "row",
                      paddingBottom: 10,
                    }}
                  >
                    <Text
                      style={{
                        fontFamily: "Helvetica Neue",
                        fontSize: 20,
                        fontWeight: "bold",
                        color: "#000",
                      }}
                    >
                      Will You Attend?
                    </Text>
                  </View>
                  <View
                    style={{
                      flexDirection: "row",
                      justifyContent: "space-around",
                    }}
                  >
                    {[
                      { title: "Yes", color: "#05B9A4" },
                      { title: "Maybe", color: "#E38330" },
                      { title: "No", color: "#E11F66" },
                    ].map(({ title, color }, index) => {
                      return (
                        <Button
                          key={index + title}
                          disabled
                          disabledStyle={{
                            backgroundColor: "#fff",
                            width: 100,
                            borderColor: color,
                            height: 40,
                            borderRadius: 8,
                            backgroundColor: "#fff",
                            borderWidth: 1,
                          }}
                          disabledTitleStyle={{ color: color }}
                          title={title}
                        />
                      );
                    })}
                  </View>
                </View>
                <View
                  style={{
                    padding: 10,
                    justifyContent: "center",
                    flexDirection: "row",
                  }}
                >
                  <Button
                    buttonStyle={{
                      width: 180,
                      backgroundColor: "#3C3989",
                      borderRadius: 20,
                    }}
                    title="Save"
                    onPress={this.addSaveEvent.bind(this)}
                  />
                </View>
              </View>
            </KeyboardAwareScrollView>
          </ThemeProvider>
          {this._footerTab()}
        </View>
      </View>
    );
  }
  // renderMainWidget() {
  //   return (
  //     <View style={{ flex: 1 }}>
  //       <Spinner
  //         visible={this.showSpinner()}
  //         textContent={"Loading..."}
  //         textStyle={{ color: "#FFF" }}
  //       />
  //       {this.ParentChooserModal()}
  //       <View style={styles.container}>
  //         {this.RenderErrorText()}
  //         <ThemeProvider theme={theme}>
  //           <KeyboardAwareScrollView ref="scroll">
  //             {this.renderImageHeader(this.props.isEditable)}

  //             <Card
  //               title="EVENT DETAILS"
  //               titleStyle={styles.titleStyle}
  //               dividerStyle={styles.dividerStyle}
  //             >
  //               <Input
  //                 label="Enter Name of Event"
  //                 errorStyle={{ color: "red" }}
  //                 ref={(component) => (this._inputeventName = component)}
  //                 value={this.state._eventname}
  //                 onChangeText={(text) => this.setState({ _eventname: text })}
  //               />
  //               <Input
  //                 label="Enter Description of Event"
  //                 errorStyle={{ color: "red" }}
  //                 multiline={true}
  //                 value={this.state._eventdesc}
  //                 onChangeText={(text) => this.setState({ _eventdesc: text })}
  //                 inputStyle={{ height: this.state.descHeight }}
  //                 numberOfLines={3}
  //                 onContentSizeChange={(e) =>
  //                   this.updateDescSize(e.nativeEvent.contentSize.height)
  //                 }
  //               />
  //               <Input
  //                 label="Event Location"
  //                 ref={(component) => (this._inputeventLocation = component)}
  //                 errorStyle={{ color: "red" }}
  //                 value={this.state._eventlocation}
  //                 onChangeText={(text) =>
  //                   this.setState({ _eventlocation: text })
  //                 }
  //               />
  //               <TouchableOpacity
  //                 onPress={() =>
  //                   this.setState({ startDatePickerVisible: true })
  //                 }
  //               >
  //                 <Input
  //                   label="Starts at"
  //                   errorStyle={{ color: "red" }}
  //                   value={this.state._eventstartDate}
  //                   editable={false}
  //                   onFocus={() =>
  //                     this.setState({ startDatePickerVisible: true })
  //                   }
  //                   rightIcon={
  //                     <Icon
  //                       name="calendar-o"
  //                       size={24}
  //                       color="gray"
  //                       onPress={() =>
  //                         this.setState({ startDatePickerVisible: true })
  //                       }
  //                     />
  //                   }
  //                 />
  //               </TouchableOpacity>
  //               {this.renderStartTime()}
  //               <TouchableOpacity
  //                 onPress={() => this.setState({ endDatePickerVisible: true })}
  //               >
  //                 <Input
  //                   label="Ends at"
  //                   errorStyle={{ color: "red" }}
  //                   value={this.state._eventendDate}
  //                   editable={false}
  //                   rightIcon={
  //                     <Icon
  //                       name="calendar-o"
  //                       size={24}
  //                       color="gray"
  //                       onPress={() =>
  //                         this.setState({ endDatePickerVisible: true })
  //                       }
  //                     />
  //                   }
  //                 />
  //               </TouchableOpacity>
  //               {this.renderEndTime()}
  //               {this.RenderAllDaySwitch()}
  //             </Card>
  //             <Card
  //               title="WHO TO INVITE"
  //               titleStyle={styles.titleStyle}
  //               dividerStyle={styles.dividerStyle}
  //             >
  //               {this.renderInviteSection()}
  //               {this.RenderInviteSwitch()}
  //               {this.RenderNotifySwitch()}
  //             </Card>
  //             <Card
  //               title="SET REMINDERS"
  //               titleStyle={styles.titleStyle}
  //               dividerStyle={styles.dividerStyle}
  //             >
  //               {this.RenderSetReminderSameDay()}
  //               {this.RenderSetReminderOneDay()}
  //               {this.RenderSetReminderTwoDay()}
  //             </Card>
  //             <DateTimePicker
  //               mode="date"
  //               isVisible={this.state.startDatePickerVisible}
  //               minimumDate={new Date()}
  //               onConfirm={(dt) => {
  //                 this.handleConfirm(dt, "startDate");
  //               }}
  //               onCancel={() =>
  //                 this.setState({ startDatePickerVisible: false })
  //               }
  //             />
  //             <DateTimePicker
  //               mode="date"
  //               minimumDate={new Date(this.state._eventstartDateObj)}
  //               isVisible={this.state.endDatePickerVisible}
  //               onConfirm={(dt) => {
  //                 this.handleConfirm(dt, "endDate");
  //               }}
  //               onCancel={() => this.setState({ endDatePickerVisible: false })}
  //             />
  //             <DateTimePicker
  //               mode="time"
  //               minuteInterval={5}
  //               isVisible={this.state.startTimePickerVisible}
  //               onConfirm={(dt) => {
  //                 this.handleConfirmTime(dt, "startTime");
  //               }}
  //               onCancel={() =>
  //                 this.setState({ startTimePickerVisible: false })
  //               }
  //               titleIOS={"Pick a Start Time"}
  //             />
  //             <DateTimePicker
  //               mode="time"
  //               minuteInterval={5}
  //               isVisible={this.state.endTimePickerVisible}
  //               onConfirm={(dt) => {
  //                 this.handleConfirmTime(dt, "endTime");
  //               }}
  //               onCancel={() => this.setState({ endTimePickerVisible: false })}
  //               titleIOS={"Pick an End Time"}
  //             />
  //             {this.RenderSaveButton()}
  //             {this.RenderDeleteButton()}
  //           </KeyboardAwareScrollView>
  //         </ThemeProvider>
  //       </View>
  //     </View>
  //   );
  // }

  updateDescSize = (height) => {
    if (height < 100) return;
    this.setState({
      descHeight: height,
    });
  };

  handleConfirmTime(dt, type) {
    var time = dt.toLocaleTimeString([], {
      hour: "numeric",
      minute: "numeric",
    });

    if (type === "startTime") {
      this.setState({
        startTimePickerVisible: false,
        _eventstartTime: time,
      });
    }
    if (type === "endTime") {
      this.setState({ endTimePickerVisible: false, _eventendTime: time });
    }
  }

  handleConfirm(dt, type) {
    var newDate = new Date(dt);

    var date =
      newDate.getDate() < 10 ? "0" + newDate.getDate() : newDate.getDate();
    var month = newDate.getMonth() + 1;
    month = month < 10 ? "0" + month : month;
    var dtstr = month + "/" + date + "/" + newDate.getFullYear();
    if (type === "startDate") {
      this.setState({
        startDatePickerVisible: false,
        _eventstartDate: dtstr,
        _eventstartDateObj: newDate,
        _eventendDate: dtstr,
        _eventendDateObj: newDate,
      });
    }
    if (type === "endDate") {
      this.setState({
        endDatePickerVisible: false,
        _eventendDate: dtstr,
        _eventendDateObj: newDate,
      });
    }
  }

  getMonth(datestr) {
    const monthNames = [
      "Jan",
      "Feb",
      "Mar",
      "Apr",
      "May",
      "Jun",
      "Jul",
      "Aug",
      "Sep",
      "Oct",
      "Nov",
      "Dec",
    ];
    let dt = new Date(datestr);
    return monthNames[dt.getMonth()];
  }

  getFullDay(datestr) {
    const monthNames = [
      "Jan",
      "Feb",
      "Mar",
      "Apr",
      "May",
      "Jun",
      "Jul",
      "Aug",
      "Sep",
      "Oct",
      "Nov",
      "Dec",
    ];
    let dt = new Date(datestr);
    return (
      monthNames[dt.getMonth()] + " " + dt.getDate() + " " + dt.getFullYear()
    );
  }
  dateview = () => {
    // var alldayevent =
    //   this.state._eventstartTime === this.state._eventendTime ? true : false;

    // if (alldayevent) {
    if (
      (this.props.eventdata &&
        this.props.eventdata.startdate === this.props.eventdata.enddate) ||
      this.state._eventstartDate === this.state._eventendDate
    ) {
      return (
        <View style={{ flexDirection: "row", margin: 10 }}>
          <Icon
            active
            name="calendar"
            color="#05B9A4"
            size={24}
            style={{ width: 40 }}
          />
          <Text style={styles.previewTextShow}>
            {/* {`${this.state._eventstartDate} - ${this.state._eventendDate}`} */}
            {this.getFullDay(this.state._eventstartDate)}
          </Text>
        </View>
      );
    } else {
      return (
        <View style={{ flexDirection: "row", margin: 10 }}>
          <Icon
            active
            name="calendar"
            color="#05B9A4"
            size={24}
            style={{ width: 40 }}
          />
          <View
            style={{
              fontSize: 18,
              fontFamily: "Helvetica Neue",
              color: "#000000",
              flexDirection: "row",
            }}
          >
            <Text style={styles.previewTextShow}>
              {this.getMonth(this.state._eventstartDate) +
                " " +
                new Date(this.state._eventstartDate).getDate() +
                " " +
                new Date(this.state._eventendDate).getFullYear()}
            </Text>
            <Text style={styles.previewTextShow}> - </Text>
            <Text style={styles.previewTextShow}>
              {this.getMonth(this.state._eventendDate) +
                " " +
                new Date(this.state._eventendDate).getDate() +
                " " +
                new Date(this.state._eventendDate).getFullYear()}
            </Text>
          </View>
        </View>
        // <View
        //   style={{
        //     flexDirection: "row",
        //     justifyContent: "space-between",
        //     marginLeft: 50,
        //     marginRight: 50
        //   }}
        // >
        //   <View style={{ flexDirection: "column", alignItems: "center" }}>
        //     <Text style={styles.monthPublicText}>
        //       {this.getMonth(this.state._eventstartDate)}
        //     </Text>
        //     <Text style={styles.dateFromPublicText}>
        //       {new Date(this.state._eventstartDate).getDate()}
        //     </Text>
        //   </View>
        //   <Text
        //     style={{
        //       fontSize: 24,
        //       color: "darkgray",
        //       fontWeight: "bold",
        //       alignItems: "center",
        //       paddingTop: 20
        //     }}
        //   >
        //     {" "}
        //     -{" "}
        //   </Text>
        //   <View style={{ flexDirection: "column", alignItems: "center" }}>
        //     <Text style={styles.monthPublicText}>
        //       {this.getMonth(this.state._eventendDate)}
        //     </Text>
        //     <Text style={styles.dateToPublicText}>
        //       {new Date(this.state._eventendDate).getDate()}
        //     </Text>
        //   </View>
        // </View>
      );
    }
    // } else {
    //   return (
    //     <View
    //       style={{
    //         flexDirection: "row",
    //         justifyContent: "space-between",
    //         marginLeft: 50,
    //         marginRight: 50
    //       }}
    //     >
    //       <View
    //         style={{
    //           flexDirection: "column",
    //           textAlign: "center",
    //           alignItems: "center"
    //         }}
    //       >
    //         <Text style={styles.monthPublicText}>
    //           {this.getMonth(this.state._eventstartDate)}
    //         </Text>
    //         <Text style={styles.dateFromPublicText}>
    //           {new Date(this.state._eventstartDate).getDate()}
    //         </Text>
    //         <Text style={styles.timePublicFromText}>
    //           {this.state._eventstartTime}
    //         </Text>
    //       </View>
    //       <Text
    //         style={{
    //           fontSize: 24,
    //           color: "darkgray",
    //           fontWeight: "bold",
    //           alignItems: "center",
    //           paddingTop: 20
    //         }}
    //       >
    //         {" "}
    //         -{" "}
    //       </Text>
    //       <View style={{ flexDirection: "column", alignItems: "center" }}>
    //         <Text style={styles.monthPublicText}>
    //           {this.getMonth(this.state._eventendDate)}
    //         </Text>
    //         <Text style={styles.dateToPublicText}>
    //           {new Date(this.state._eventendDate).getDate()}
    //         </Text>
    //         <Text style={styles.timePublicToText}>
    //           {this.state._eventendTime}
    //         </Text>
    //       </View>
    //     </View>
    //   );
    // }
  };
  renderPublicDateView() {
    var alldayevent =
      this.state._eventstartTime === this.state._eventendTime ? true : false;
    if (
      this.props.events.events[this.props.eventid] !== undefined &&
      (this.props.events.events[this.props.eventid].eventtype === 1 ||
        this.props.events.events[this.props.eventid].eventtype === 2)
    ) {
      return <View style={styles.eventDateView}>{this.dateview()}</View>;
    } else {
      return null;
    }
  }

  renderReadOnlyMainWidget() {
    var event = this.props.events.events[this.props.eventid];
    if (this.state.eventimgurl === "null" || this.state.eventimgurl === null) {
      var coverurl = require("../img/bg.png");
    } else {
      if (
        typeof this.state.eventimgurl === "string" &&
        this.state.eventimgurl.includes("http")
      ) {
        var coverurl = { uri: this.state.eventimgurl };
      } else {
        var coverurl = this.state.eventimgurl;
      }
    }
    console.log("-------------6");
    console.log(this.props.eventdata.eventdesc);
    console.log("-------------6");
    return (
      <ScrollView>
        <View style={{ backgroundColor: "#fff", paddingBottom: 20 }}>
          <Spinner
            visible={this.state.loading || this.props.events.isLoading}
            textContent={"Loading..."}
            textStyle={{ color: "#FFF" }}
          />
          <View style={{}}>
            {this.renderImageHeader(this.props.isEditable)}
            <View style={{ paddingLeft: 10, paddingBottom: 20 }}>
              <Text style={styles.newSectionHeading}>
                {this.state._eventname}
              </Text>
              <Text style={styles.newGeneralTextView}>
                {this.state._eventdesc}
              </Text>
              <View style={{ flexDirection: "row", margin: 10 }}>
                <Entypo
                  active
                  name="location-pin"
                  color="#05B9A4"
                  size={24}
                  style={{ width: 40 }}
                />
                <Text style={styles.previewTextShow}>
                  {this.state._eventlocation}
                </Text>
              </View>

              {this.renderPublicDateView()}
              {(event.eventtype === 2 || event.eventtype === 1) && (
                <View style={{ flexDirection: "row", margin: 10 }}>
                  <Icon
                    active
                    name="clock-o"
                    color="#05B9A4"
                    size={24}
                    style={{ width: 40 }}
                  />
                  <Text style={styles.previewTextShow}>
                    {`${this.state._eventstartTime} - ${this.state._eventendTime}`}
                  </Text>
                </View>
              )}
              {this.state._grpSelect && (
                <View style={{ flexDirection: "row", margin: 10 }}>
                  <Icon
                    active
                    name="group"
                    color="#05B9A4"
                    size={24}
                    style={{ width: 40 }}
                  />
                  <Text style={styles.previewTextShow}>
                    {this.state._grpSelect}
                  </Text>
                </View>
              )}
              {Object.keys(this.state.selectedUsers).length !== 0 && (
                <View style={{ flexDirection: "row", margin: 10 }}>
                  <MaterialIcons
                    active
                    name="group"
                    color="#05B9A4"
                    size={24}
                    style={{ width: 40 }}
                  />
                  <Text style={styles.previewTextShow}>
                    {`${
                      Object.keys(this.state.selectedUsers).length
                    } Parents Selected`}
                  </Text>
                </View>
              )}
              <View style={{ flexDirection: "row", margin: 10 }}>
                <Icon
                  active
                  name="bell"
                  color="#FF0000"
                  size={24}
                  style={{ width: 40 }}
                />
                <Text style={styles.previewTextShow}>
                  {this.state._reminderCount} Reminders
                </Text>
              </View>
            </View>
          </View>
          {/* <Tile
          imageSrc={coverurl}
          title={this.props.eventdata.eventname}
          contentContainerStyle={{
            height: 70,
            backgroundColor: "white",
            borderRadius: 20,
            top: -20
          }}
        /> */}

          <View style={{}}>
            {/* <AutoHeightWebView
            style={[
              { width: Dimensions.get("window").width - 100 },
              { flex: 1 }
            ]}
            scrollEnabled={false}
            ref={r => (this.eventView = r)}
            useWebKit={false}
            dataDetectorTypes={"link"}
            allowsInlineMediaPlayback={true}
            source={{ html: this.props.eventdata.eventdesc }}
            scalesPageToFit={false}
            onNavigationStateChange={this.onNavigationURLChange}
            androidHardwareAccelerationDisabled
            customStyle={`
            * {
              font-family:-apple-system, BlinkMacSystemFont, sans-serif;;
              font-size: 14px;
            }
            p {
              font-size: 14px;
            }
          `}
          ></AutoHeightWebView> */}

            {this.renderVolunteerSpot()}
            {this.renderPublicParentTeacher()}

            {/* {this.renderPublicLocation()} */}
            {/* {this.renderPublicEventInvites()} */}
            {this.renderRSVPDetails()}
            {this.renderchildDialogModal()}
          </View>
        </View>
      </ScrollView>
    );
  }

  onNavigationURLChange(event) {
    if (
      !(
        event.url.startsWith("about:blank") ||
        event.url.startsWith("data:text/html") ||
        event.url.startsWith("file://")
      )
    ) {
      this.eventView.stopLoading();
      Linking.openURL(event.url).catch((err) => {});
    }
  }

  renderchildDialogModal() {
    var renderchildren = () => {
      return Object.keys(this.state.childDialogModalData).map((keyindex) => {
        var item = this.state.childDialogModalData[keyindex];
        console.log(item);
        return (
          <TouchableOpacity
            key={keyindex}
            style={{
              width: (Dimensions.get("window").width * 0.9) / 4,
              margin: Dimensions.get("window").width * 0.02,
              borderRadius: 4,
              backgroundColor: item.isSelected ? "#1B98E0" : "#fff",
            }}
            onPress={() => {
              this.setState({
                childDialogModalselection: item,
                childDialogModal: false,
              });
            }}
          >
            <View
              style={{
                padding: 5,
                alignItems: "center",
                justifyContent: "center",
              }}
            >
              {item.profile_photo == "<DEFAULT>" ||
              item.profile_photo == "" ||
              item.profile_photo == null ? (
                <Image
                  style={styles.roundImgStyle}
                  imageStyle={styles.roundImgStyle}
                  source={require("../img/child.png")}
                />
              ) : (
                <ImageLoad
                  borderRadius={(Dimensions.get("window").width * 0.9) / 8}
                  style={styles.roundImgStyle}
                  placeholderStyle={styles.roundImgStyle}
                  loadingStyle={{ size: "small", color: "grey" }}
                  source={{
                    uri: item.profile_photo,
                  }}
                />
              )}
              <Text numberOfLines={2} style={styles.groupName}>
                {item.firstname} {item.lastname}
              </Text>
            </View>
          </TouchableOpacity>
        );
      });
    };

    return (
      <Dialog
        visible={this.state.childDialogModal}
        onTouchOutside={() => {
          this.setState({
            childDialogModal: false,
            childDialogModalselection: null,
          });
        }}
      >
        <DialogContent style={{ width: 300, height: 500 }}>
          <View style={[styles.dialogItem, { justifyContent: "center" }]}>
            <Text>Please Select a Child to Signup</Text>
          </View>
          <ScrollView contentContainerStyle={{ justifyContent: "center" }}>
            <View
              style={{
                flexDirection: "row",
                flexWrap: "wrap",
                justifyContent: "center",
              }}
            >
              {renderchildren()}
            </View>
          </ScrollView>
        </DialogContent>
      </Dialog>
    );
  }

  getChildIdtoSignUp() {
    return undefined;
  }

  async getUserResponsesforEvent4() {
    var response = await eventsapi.getEventResponses(
      this.props.eventid,
      this.props.user.token
    );
    if (response.data.statusCode === 200) {
      console.log("-----------------");
      console.log(response.data);
      this.setState({
        event4data: response.data.result.userResponse,
      });
    } else {
      Alert.alert(
        "Error",
        "There was an error loading data for this event. Please try again"
      );
    }
  }

  async postsignupSpotEvent4(spot, childid) {
    console.log(spot);
    console.log(childid);
    var event = this.props.events.events[this.props.eventid];
    var response = await eventsapi.volunteerforEvent(
      this.props.eventid,
      event.eventtype,
      childid,
      spot.id,
      this.props.user.token
    );
    if (response.data.statusCode === 200) {
      //Navigation.dismissAllModals()
      //this.props.getEventsAction(this.props.user.token, this.props.user.groups)
      this.getUserResponsesforEvent4();
    } else {
      Alert.alert(
        "Error",
        "There was an error saving your Signup Preference. Please try again"
      );
    }
  }

  async postsignupSlotEvent3(slot, childid) {
    this.setState({ loading: true });
    console.log(slot, "slot");
    console.log(childid, "childid");
    var event = this.props.events.events[this.props.eventid];
    var response = await eventsapi.signupforChild(
      this.props.eventid,
      event.eventtype,
      childid,
      slot.slotId,
      this.props.user.token
    );
    if (response.data.statusCode === 200) {
      console.log(response, "response");
      //Navigation.dismissAllModals()
      await this.props.getEventsAction(
        this.props.user.token,
        this.props.user.groups
      );
      await this.setState({ loading: false });
    } else {
      await this.setState({ loading: false });
      Alert.alert(
        "Error",
        "There was an error saving your Signup Preference. Please try again"
      );
    }
  }

  signupSlotEvent3(slot) {
    console.log(slot);
    //var childid = this.getChildIdtosignupSlotEvent3()
    var children = {};
    console.log(this.props);
    console.log(this.props.children_groups);

    if (this.props.eventdata.groupid != null) {
      if (this.props.user.roles !== "parent") {
        if (this.props.eventdata.groupid in this.props.children_groups) {
          children = this.props.children_groups[this.props.eventdata.groupid];
        }
      } else {
        if (this.props.eventdata.groupid in this.props.children_groups) {
          var children_list = Object.keys(
            this.props.children_groups[this.props.eventdata.groupid]
          );
          console.log(children_list);
          this.props.user.children_array.forEach((childid) => {
            console.log(childid);
            console.log(children_list);
            if (children_list.indexOf(childid.toString()) >= 0) {
              console.log("here");
              console.log(childid);
              children[childid] = this.props.children_groups[
                this.props.eventdata.groupid
              ][childid];
            }
          });
          console.log(children);
          console.log(Object.keys(children).length);
          if (Object.keys(children).length === 1) {
            console.log("Signing up !!!!");
            this.postsignupSlotEvent3(slot, Object.keys(children)[0]);

            return;
          }
        }
      }
    } else {
      console.log("here2222");
      if (this.props.user.children_array.length === 1) {
        this.postsignupSlotEvent3(slot, this.props.user.children_array[0]);

        return;
      } else {
        this.props.user.children.forEach((child) => {
          child["id"] = child.childid; // Silly thing to do but userpref has diff key then cache
          children[child.childid] = child;
        });
      }
    }
    console.log(children, "children");
    if (Object.keys(children).length === 0) {
      Alert.alert(
        "Error Selecting a Child",
        "Hmmm ... it seems you dont have a child linked to this group. Please contact the School Administrator"
      );
      return;
    }
    this.setState({
      childDialogModalData: children,
      childDialogModal: true,
      childDialogModalselection: null,
      type3signupSlot: slot,
    });

    console.log("done");
  }
  //TODO : Need to optomize both the signup functions so code can be reused
  signupSpotEvent4(spot) {
    console.log(spot);
    //var childid = this.getChildIdtosignupSlotEvent3()
    var children = {};
    console.log(this.props);
    console.log(this.props.children_groups);

    if (this.props.eventdata.groupid != null) {
      if (this.props.user.roles !== "parent") {
        if (this.props.eventdata.groupid in this.props.children_groups) {
          children = this.props.children_groups[this.props.eventdata.groupid];
        }
      } else {
        if (this.props.eventdata.groupid in this.props.children_groups) {
          var children_list = Object.keys(
            this.props.children_groups[this.props.eventdata.groupid]
          );
          console.log(children_list);
          this.props.user.children_array.forEach((childid) => {
            console.log(childid);
            console.log(children_list);
            if (children_list.indexOf(childid.toString()) >= 0) {
              console.log("here");
              console.log(childid);
              children[childid] = this.props.children_groups[
                this.props.eventdata.groupid
              ][childid];
            }
          });
          console.log(children);
          console.log(Object.keys(children).length);
          if (Object.keys(children).length === 1) {
            console.log("Signing up !!!!");
            this.postsignupSpotEvent4(spot, Object.keys(children)[0]);
            return;
          }
        }
      }
    } else {
      if (this.props.user.children_array.length === 1) {
        this.postsignupSpotEvent4(spot, this.props.user.children_array[0]);
        return;
      } else {
        this.props.user.children.forEach((child) => {
          child["id"] = child.childid; // Silly thing to do but userpref has diff key then cache
          children[child.childid] = child;
        });
      }
    }
    console.log(children);
    if (children.length === 0) {
      Alert.alert(
        "Error Selecting a Child",
        "Hmmm ... it seems you dont have a child linked to signup. Please contact the School Administrator"
      );
      return;
    }
    this.setState({
      childDialogModalData: children,
      childDialogModal: true,
      childDialogModalselection: null,
      type4signupSlot: spot,
    });

    console.log("done");
  }

  renderVolunteerSpot() {
    var event = this.props.events.events[this.props.eventid];

    if (event !== undefined && event.eventtype === 4) {
      if (this.state.event4data === null) {
        this.getUserResponsesforEvent4();
        return (
          <Card containerStyle={styles.publicCard}>
            <Animatable.Text
              animation="pulse"
              easing="ease-out"
              iterationCount="infinite"
              style={{ fontSize: 20, textAlign: "center", color: "darkgray" }}
            >
              Loading ...
            </Animatable.Text>
          </Card>
        );
      } else {
        var eventresponses = {};
        this.state.event4data.forEach((item) => {
          eventresponses[item.spotId] = item;
        });
        console.log(eventresponses);
      }
      console.log(event);
      var items = event.volunteerDetail.volunteer.map((responseItem) => {
        console.log(responseItem);
        var dt = new Date(responseItem.event_date);
        var datestr =
          miscutils.getMonthName(dt.getUTCMonth() + 1) +
          " " +
          dt.getUTCDate() +
          " " +
          dt.getUTCFullYear();
        var slots = responseItem.slotTime.map((slot) => {
          console.log(slot);
          console.log(event);
          var starttime = miscutils.formatTimeinAMPM(slot.start_time);
          var endtime = miscutils.formatTimeinAMPM(slot.end_time);
          var spots = event.userresponse;
          console.log(spots);

          var signupdetails = slot.spots.map((spot) => {
            console.log(spot);
            var spotresponse = eventresponses[spot.id];
            console.log(spotresponse);
            var responded_count =
              spotresponse === undefined ? 0 : spotresponse.childs.length;
            var percent = responded_count / spot.person;
            var remainderspots = spot.person - responded_count;
            var mysignedupqty = 0;
            if (spotresponse !== undefined) {
              spotresponse.childs.forEach((child) => {
                if (
                  this.props.user.children_array.indexOf(child.childid) >= 0
                ) {
                  mysignedupqty = mysignedupqty + 1;
                }
              });
            }

            var renderMySpot = () => {
              if (mysignedupqty > 0) {
                return (
                  <View style={{ alignItems: "center" }}>
                    <Text
                      style={{
                        top: 5,
                        width: 60,
                        textAlign: "center",
                        fontSize: 12,
                        color: "#ED7D3A",
                      }}
                    >
                      My Spot({mysignedupqty})
                    </Text>
                  </View>
                );
              } else {
                return <Text style={{ width: 60 }}></Text>;
              }
            };

            var renderVolunteerDial = () => {
              if (percent == 1) {
                return (
                  <View
                    style={{
                      flexDirection: "row",
                      justifyContent: "center",
                      width: 120,
                      marginRight: 0,
                    }}
                  >
                    {renderMySpot()}
                    <Text
                      style={{
                        width: 60,
                        textAlign: "center",
                        fontSize: 14,
                        top: 15,
                        color: "darkgray",
                      }}
                    >
                      Full
                    </Text>
                  </View>
                );
              } else {
                return (
                  <View
                    style={{
                      flexDirection: "row",
                      justifyContent: "center",
                      width: 120,
                      marginRight: 0,
                      alignItems: "center",
                    }}
                  >
                    {renderMySpot()}
                    <TouchableOpacity
                      style={{
                        width: 60,
                        height: 30,
                        backgroundColor: "#3f48cc",
                        borderRadius: 4,
                      }}
                      onPress={this.signupSpotEvent4.bind(this, spot)}
                    >
                      <Text
                        style={{
                          textAlign: "center",
                          fontSize: 14,
                          color: "white",
                          top: 5,
                        }}
                      >
                      {this.props.user.roles === "admin" ||
                this.props.user.roles === "teacher"
                  ? "Assign"
                  : "Signup"}
                      </Text>
                    </TouchableOpacity>
                  </View>
                );
              }
            };
            var icon =
              spot.type === "Bring" ? (
                <Icon style={{ paddingTop: 20 }} name={"shopping-basket"} />
              ) : (
                <MaterialIcons
                  style={{ paddingTop: 18 }}
                  name={"person"}
                  size={16}
                />
              );

            var accordiantitle = (
              <View
                style={{
                  flexDirection: "row",
                  paddingLeft: 5,
                  paddingTop: 2,
                  margin: 1,
                }}
              >
                <View style={{ flex: 1, flexDirection: "row" }}>
                  <View style={{ flex: 1, flexDirection: "row" }}>
                    {icon}
                    <Text
                      style={{ fontSize: 16, paddingLeft: 5, paddingTop: 16 }}
                    >
                      {spot.description}
                    </Text>
                  </View>

                  <Text
                    style={{
                      marginTop: 20,
                      marginRight: -39,
                      textAlign: "center",
                      fontSize: 11,
                      width: 33,
                    }}
                  >
                    {responded_count}/{spot.person}
                  </Text>
                  <View>
                    <ProgressCircle
                      animate={true}
                      backgroundColor={"#b0c2db"}
                      progressColor={"#22b14c"}
                      strokeWidth={4}
                      style={{ height: 45, width: 45, marginTop: 5 }}
                      progress={percent}
                    />
                    <Text
                      style={{ textAlign: "center", fontSize: 10, width: 45 }}
                    >
                      Filled
                    </Text>
                  </View>
                </View>
                {renderVolunteerDial()}
              </View>
            );
            var accordiandata = () => {
              if (responded_count === 0) {
                return (
                  <View style={{ flex: 1 }}>
                    <Text style={{ textAlign: "center", color: "darkgray" }}>
                      No Volunteers Registered
                    </Text>
                  </View>
                );
              }
              var registered = () => {
                var renderRegistredUsers = spotresponse.childs.map((item) => {
                  let deleteButton = () => {
                    if (
                      this.props.user.roles === "admin" ||
                      this.props.user.roles === "teacher" ||
                      this.props.user.children_array.indexOf(item.childid) > -1
                    ) {
                      return (
                        <TouchableOpacity
                          style={styles.deleteButton}
                          onPress={this.removeChildfromType4Event.bind(
                            this,
                            spot.id,
                            item.childid
                          )}
                        >
                          <Icon
                            style={{ color: "white", marginRight: 2 }}
                            name="trash-o"
                            size={16}
                          ></Icon>
                          <Text style={{ color: "white", fontSize: 14 }}>
                            Delete
                          </Text>
                        </TouchableOpacity>
                      );
                    } else {
                      return null;
                    }
                  };

                  return (
                    <View
                      style={{
                        flexDirection: "row",
                        padding: 2,
                        justifyContent: "space-between",
                      }}
                    >
                      <Text>
                        {item.firstname} {item.lastname}
                      </Text>
                      {deleteButton()}
                    </View>
                  );
                });
                return renderRegistredUsers;
              };
              return (
                <View>
                  <Text
                    style={{
                      flex: 1,
                      textAlign: "center",
                      backgroundColor: "lightgray",
                    }}
                  >
                    PARTICIPANTS
                  </Text>
                  {registered()}
                </View>
              );
            };

            return (
              <Accordian
                key={spot.id}
                title={accordiantitle}
                data={accordiandata()}
              />
            );
          });

          return (
            <View
              style={{
                flexDirection: "column",
                marginLeft: -13,
                marginRight: -13,
              }}
            >
              <View
                style={{
                  flexDirection: "row",
                  backgroundColor: "#ABD1E9",
                  padding: 5,
                  justifyContent: "center",
                }}
              >
                <Text>{starttime}</Text>
                <Text> - </Text>
                <Text>{endtime}</Text>
              </View>

              {signupdetails}
            </View>
          );
        });

        return (
          <Card title={datestr} containerStyle={styles.publicCard}>
            {slots}
          </Card>
        );
      }, this);
      return items;
    }
    return null;
  }

  renderPublicParentTeacher() {
    console.log(this.props.events, this.props.eventid, "2222222222222222222");
    var event = this.props.events.events[this.props.eventid];
    console.log(event, "events_renderPublicParentTeacher");

    if (event !== undefined && event.eventtype === 3) {
      var items = event.parentTeacherResponse.map((responseItem) => {
        var dt = new Date(responseItem.event_date);
        var datestr =
          miscutils.getMonthName(dt.getUTCMonth() + 1) +
          " " +
          dt.getUTCDate() +
          " " +
          dt.getUTCFullYear();
        var slots = responseItem.slotTime.map((slot, idx) => {
          console.log(slot);
          console.log(event);
          var starttime = miscutils.formatTimeinAMPM(slot.start_time);
          var endtime = miscutils.formatTimeinAMPM(slot.end_time);
          var userresponse_slots = event.userresponse.user_response;
          console.log(userresponse_slots, "userresponse_slots");
          let slotIdsList = userresponse_slots.map(
            (userresponse_slot) => userresponse_slot.slotId
          );
          console.log(slotIdsList, "slotIdsList", slot.slotId);

          var signupdetails = () => {
            if (slotIdsList.indexOf(slot.slotId) >= 0) {
              let deleteButton = (userresponse_slot) => {
                if (
                  this.props.user.roles === "admin" ||
                  this.props.user.roles === "teacher" ||
                  (this.props.user.roles === "parent" &&
                    userresponse_slot.parents.indexOf(this.props.user.userid) >=
                      0)
                ) {
                  return (
                    <TouchableOpacity
                      style={{
                        paddingLeft: 5,
                        paddingTop: 2,
                      }}
                      onPress={this.removeChildfromEvent.bind(
                        this,
                        userresponse_slot
                      )}
                    >
                      <Icon name="trash-o" size={16}></Icon>
                    </TouchableOpacity>
                  );
                } else {
                  return null;
                }
              };
              return userresponse_slots.map((userresponse_slot) => {
                if (userresponse_slot.slotId === slot.slotId) {
                  return (
                    <View
                      style={{
                        flexDirection: "row",
                        paddingLeft: 2,
                      }}
                    >
                      <Text style={{ color: "blue" }}>
                        {userresponse_slot.firstname}-
                        {userresponse_slot.lastname}
                      </Text>
                      {deleteButton(userresponse_slot)}
                    </View>
                  );
                }
              });
            } else {
              let btnTitle =
                this.props.user.roles === "admin" ||
                this.props.user.roles === "teacher"
                  ? "Assign"
                  : "Signup";
              return (
                <Button
                  title={btnTitle}
                  buttonStyle={{
                    width: 100,
                    backgroundColor: "#05B9A4",
                    borderRadius: 20,
                  }}
                  titleStyle={{ fontSize: 14 }}
                  onPress={this.signupSlotEvent3.bind(this, slot)}
                ></Button>
              );
            }
            return null;
          };

          return (
            <View
              style={
                idx % 2 === 0
                  ? {
                      flexDirection: "row",
                      justifyContent: "space-between",
                      alignItems: "center",
                      padding: 10,
                    }
                  : {
                      backgroundColor: "#F5F5F5",
                      justifyContent: "space-between",
                      alignItems: "center",
                      flexDirection: "row",
                      padding: 10,
                    }
              }
            >
              <View style={{ flexDirection: "row", flex: 1 }}>
                <Text style={{ color: "#000000", fontWeight: "400" }}>
                  {starttime}
                </Text>
                <Text style={{ color: "#000000", fontWeight: "400" }}> - </Text>
                <Text style={{ color: "#000000", fontWeight: "400" }}>
                  {endtime}
                </Text>
              </View>
              <View
                style={{
                  flexDirection: "row",
                  flexWrap: "wrap",
                  justifyContent: "flex-end",
                  flex: 1,
                }}
              >
                {signupdetails()}
              </View>
            </View>
          );
        });

        return (
          <View
            style={{
              backgroundColor: "white",
              borderRadius: 10,
              elevation: 5,
              borderWidth: 1,
              margin: 10,
              borderColor: "#ededed",
              marginBottom: 20,
            }}
          >
            <View>
              <View
                style={{
                  flexDirection: "row",
                  // justifyContent: "space-between",
                  padding: 5,
                }}
              >
                <Text
                  style={{ fontSize: 24, fontWeight: "bold", color: "#05B9A4" }}
                >
                  {this.getMonth(responseItem.event_date) + " "}
                </Text>
                <Text
                  style={{ fontSize: 24, fontWeight: "bold", color: "#05B9A4" }}
                >
                  {new Date(responseItem.event_date).getDate() + " "}
                </Text>
                <Text style={{ fontSize: 24, color: "#05B9A4" }}>
                  {new Date(responseItem.event_date).getFullYear() + " "}
                </Text>
              </View>
              {slots}
            </View>
          </View>
        );
      }, this);

      return <View>{items}</View>;
    } else {
      return null;
    }
  }
  async removeChildfromEvent(slot) {
    this.setState({ loading: true });
    console.log(slot);
    var response = await eventsapi.deleteSignupforChild(
      this.props.eventid,
      slot.childid,
      slot.slotId,
      this.props.user.token
    );
    if (response.data.statusCode === 200) {
      //Navigation.dismissAllModals()
      await this.props.getEventsAction(
        this.props.user.token,
        this.props.user.groups
      );
      await this.setState({ loading: false });
    } else {
      await this.setState({ loading: false });
      Alert.alert(
        "Error",
        "There was an error saving your RSVP preference. Please try again"
      );
    }
    console.log("done");
  }

  async removeChildfromType4Event(spotid, childid) {
    var response = await eventsapi.deleteVolunteerSignupforChild(
      this.props.eventid,
      childid,
      spotid,
      this.props.user.token
    );
    if (response.data.statusCode === 200) {
      //Navigation.dismissAllModals()
      //this.props.getEventsAction(this.props.user.token, this.props.user.groups)
      this.getUserResponsesforEvent4();
    } else {
      Alert.alert(
        "Error",
        "There was an error saving your RSVP preference. Please try again"
      );
    }
    console.log("done");
  }

  updateRSVPIndex(rsvpIndex) {
    this.setState({
      rsvpIndex: rsvpIndex,
    });
  }
  async confirmRSVP() {
    var event = this.props.events.events[this.props.eventid];
    var attend = this.state.rsvpBtnSelect;
    console.log(this.adultselector);
    var response = await eventsapi.sendUserResponse(
      event.event_id,
      event.eventtype,
      attend,
      this.state.rsvpBtnSelect == "Yes"
        ? this.adultselector.state.value.toString()
        : -1,
      this.state.rsvpBtnSelect == "Yes"
        ? this.kidsselector.state.value.toString()
        : -1,
      this.props.user.token
    );

    console.log(response, "-------rrrresponse-----");
    if (response.data.statusCode === 200) {
      this.props.clearLastRefreshTime();
      Navigation.dismissAllModals();
    } else {
      Alert.alert(
        "Error",
        "There was an error saving your RSVP preference. Please try again"
      );
    }
  }

  renderRSVPDetails() {
    var event = this.props.events.events[this.props.eventid];
    if (event === undefined) return null;
    console.log(event);
    console.log(this.state);
    var bgColor = ["#7FB069", "#FFBA49", "#FF4545"];
    var yesconfirmbox = () => {
      if (this.state.rsvpBtnSelect == "Yes") {
        return (
          <View
            style={{
              flex: 1,
              flexDirection: "row",
              justifyContent: "center",
            }}
          >
            <NumberSpinner
              ref={(r) => (this.adultselector = r)}
              onIncrease={() => { }}
              onDecrease={() => { }}
              label="Adults"
              defaultValue={this.state.adultcount}
              minimumValue={0}
              maximumValue={100}
            />
            <NumberSpinner
              ref={(r) => (this.kidsselector = r)}
              onIncrease={() => { }}
              onDecrease={() => { }}
              label="Kids"
              defaultValue={this.state.kidscount}
              minimumValue={0}
              maximumValue={100}
            />
          </View>
        );
      } else {
        return null;
      }
    };
    if (event.eventtype === 2) {
      return (
        <View style={{ backgroundColor: "#fff" }}>
          <View style={{ paddingBottom: 20 }}>
            <View
              style={{
                justifyContent: "center",
                flexDirection: "row",
                paddingBottom: 10,
              }}
            >
              <Text
                style={{
                  fontFamily: "Helvetica Neue",
                  fontSize: 20,
                  fontWeight: "bold",
                  color: "#000",
                }}
              >
                {event.userresponse?.user_response===null? "Will You Attend?":"Would you like to edit response"}
              </Text>
            </View>
            <View
              style={{
                flexDirection: "row",
                justifyContent: "space-around",
              }}
            >
              <Button
                buttonStyle={
                  this.state.rsvpBtnSelect === "Yes"
                    ? {
                        backgroundColor: "#05B9A4",
                        width: 100,
                        height: 40,
                        borderRadius: 8,
                        borderWidth: 1,
                      }
                    : {
                        backgroundColor: "#fff",
                        width: 100,
                        borderColor: "#05B9A4",
                        height: 40,
                        borderRadius: 8,
                        backgroundColor: "#fff",
                        borderWidth: 1,
                      }
                }
                onPress={() => {
                  this.setState({ rsvpBtnSelect: "Yes" });
                }}
                titleStyle={
                  this.state.rsvpBtnSelect === "Yes"
                    ? { color: "#fff" }
                    : { color: "#05B9A4" }
                }
                title={"Yes"}
              />
              <Button
                buttonStyle={
                  this.state.rsvpBtnSelect === "Maybe"
                    ? {
                        backgroundColor: "#E38330",
                        width: 100,
                        height: 40,
                        borderRadius: 8,
                        borderWidth: 1,
                      }
                    : {
                        backgroundColor: "#fff",
                        width: 100,
                        borderColor: "#E38330",
                        height: 40,
                        borderRadius: 8,
                        backgroundColor: "#fff",
                        borderWidth: 1,
                      }
                }
                onPress={() => {
                  this.setState({ rsvpBtnSelect: "Maybe" });
                }}
                titleStyle={
                  this.state.rsvpBtnSelect === "Maybe"
                    ? { color: "#fff" }
                    : { color: "#E38330" }
                }
                title={"Maybe"}
              />
              <Button
                buttonStyle={
                  this.state.rsvpBtnSelect === "No"
                    ? {
                        backgroundColor: "#E11F66",
                        width: 100,
                        height: 40,
                        borderRadius: 8,
                        borderWidth: 1,
                      }
                    : {
                        backgroundColor: "#fff",
                        width: 100,
                        borderColor: "#E11F66",
                        height: 40,
                        borderRadius: 8,
                        backgroundColor: "#fff",
                        borderWidth: 1,
                      }
                }
                onPress={() => {
                  this.setState({ rsvpBtnSelect: "No" });
                }}
                titleStyle={
                  this.state.rsvpBtnSelect === "No"
                    ? { color: "#fff" }
                    : { color: "#E11F66" }
                }
                title={"No"}
              />
            </View>
          </View>
          {yesconfirmbox()}
          <View
            style={{
              padding: 10,
              justifyContent: "center",
              flexDirection: "row",
            }}
          >
            <Button
              buttonStyle={{
                width: 180,
                backgroundColor: "#3C3989",
                borderRadius: 20,
              }}
              title="Save"
              onPress={this.confirmRSVP}
            />
          </View>
        </View>
      );
    } else {
      return null;
    }
  }
  // <View>
  //   <Card title="Will You Attend ?" containerStyle={styles.publicCard}>
  //     <ButtonGroup
  //       selectedIndex={this.state.rsvpIndex}
  //       onPress={this.updateRSVPIndex}
  //       buttons={rsvpButtons}
  //       containerStyle={{ height: 45 }}
  //       selectedButtonStyle={{
  //         backgroundColor: bgColor[this.state.rsvpIndex]
  //       }}
  //     />
  //
  //   </Card>

  //   <Button
  //     style={{ paddingBottom: 20, paddingTop: 10 }}
  //     title="Save"
  //     onPress={this.confirmRSVP}
  //   ></Button>
  // </View>
  RenderGroupIcon(grpid) {
    if (this.props.user.groupphotohash === null) {
      return null;
    }
    if (grpid !== "") {
      if (
        this.props.user.groupphotohash[grpid] === undefined ||
        this.props.user.groupphotohash[grpid] === ""
      ) {
        var photourl = require("../img/groups.png");
        return (
          <Image
            style={styles.groupicon}
            imageStyle={{
              width: 40,
              height: 40,
              borderRadius: 20,
              backgroundColor: "white",
              paddingBottom: 20,
            }}
            source={photourl}
          ></Image>
        );
      } else {
        var photourl = {
          uri: this.props.user.groupphotohash[grpid].replace("http:", "https:"),
        };
        return (
          <CustomCachedImage
            component={Image}
            defaultSource={require("../img/groups.png")}
            imageStyle={{
              width: 40,
              height: 40,
              borderRadius: 20,
              backgroundColor: "white",
            }}
            style={styles.groupicon}
            source={photourl}
          ></CustomCachedImage>
        );
      }
    } else {
      return null;
    }
  }
  renderPublicEventInvites() {
    if (
      this.props.eventdata.groupid > 0 &&
      this.props.user.groups[this.props.eventdata.groupid] !== undefined
    ) {
      return (
        <View
          style={{
            paddingTop: 10,
            alignContent: "center",
            alignItems: "center",
          }}
        >
          <View style={{ flexDirection: "row" }}>
            {this.RenderGroupIcon(this.props.eventdata.groupid)}
            <Text style={{ marginLeft: 45, paddingTop: 12, color: "gray" }}>
              Posted to{" "}
              {this.props.user.groups[
                this.props.eventdata.groupid
              ].name.replace(/_/g, " ")}
            </Text>
          </View>
        </View>
      );
    }
  }

  renderPublicLocation() {
    if (this.props.eventdata.location === "") {
      return null;
    } else {
      return (
        <Card containerStyle={styles.publicCard}>
          <TouchableOpacity
            style={{ flexDirection: "row" }}
            onPress={() => {
              var url = this.props.eventdata.location;
              Linking.canOpenURL(url).then((supported) => {
                if (!supported) {
                  console.log("Can't handle url: " + url);
                } else {
                  Linking.openURL(url);
                }
              });
            }}
          >
            <MaterialIcons
              size={30}
              style={{ color: "red" }}
              name={"location-on"}
            />
            <Text style={{ paddingTop: 5, fontSize: 16 }}>
              {this.props.eventdata.location}
            </Text>
          </TouchableOpacity>
        </Card>
      );
    }
  }

  render() {
    console.log("---- IN RENDER -----");
    console.log(this.props);
    console.log(this.state);

    if (this.state.isEditable || this.props.openReadOnly) {
      switch (this.state._eventStep) {
        case 0:
          return <View style={{ flex: 1 }}>{this.renderMainWidget()}</View>;
        case 1:
          return (
            <View style={{ flex: 1 }}>{this.renderMainPreviewWidget()}</View>
          );
      }
      // return <View style={{ flex: 1 }}>{this.renderMainWidget()}</View>;
    } else {
      return <View style={{ flex: 1 }}>{this.renderReadOnlyMainWidget()}</View>;
    }
  }

  readFile(filePath) {
    console.log(filePath);
    RNFetchBlob.fs.exists(filePath).then((exist) => {
      console.log(`file ${exist ? "" : "not"} exists`);
    });
    return RNFetchBlob.fs
      .readFile(filePath, "base64")
      .then((data) => new Buffer(data, "base64"));
  }

  async addSaveEvent() {
    console.log(this.props);
    console.log(this.state);
    let retval = this.formisValid();

    if (retval) {
      console.log("here");
      console.log(this.state.coverimage);
      this.setState({ updatingImage: true });
      this.setState({ errorText: "" });
      if (this.state.coverimage !== null) {
        var response = await this.readFile(this.state.coverimage.path).then(
          (buffer) => {
            Storage.put(Date.now(), buffer).then((response) => {
              var url = response["key"];
              if (url !== undefined) {
                if (this.props.eventid === null) {
                  this.props.addEventAction(
                    this.state,
                    this.state.selectedUsers,
                    this.props.user.display_name,
                    this.props.user.token,
                    url,
                    this.props.user.groups,
                    this.props.eventtype,
                    null
                  );
                } else {
                  this.props.addEventAction(
                    this.state,
                    this.state.selectedUsers,
                    this.props.user.display_name,
                    this.props.user.token,
                    url,
                    this.props.user.groups,
                    this.props.eventtype,
                    this.props.eventid
                  );
                }
                this.setState({ updatingImage: false });

                Navigation.dismissAllModals();
              }
            }, this);
          },
          this
        );
      } else {
        if (this.props.eventid === null) {
          this.props.addEventAction(
            this.state,
            this.state.selectedUsers,
            this.props.user.display_name,
            this.props.user.token,
            null,
            this.props.user.groups,
            this.props.eventtype,
            null
          );
        } else {
          this.props.addEventAction(
            this.state,
            this.state.selectedUsers,
            this.props.user.display_name,
            this.props.user.token,
            this.props.eventdata.eventimgurl,
            this.props.user.groups,
            this.props.eventtype,
            this.props.eventid
          );
        }
        this.setState({ updatingImage: false });
        Navigation.dismissAllModals();
      }
    }
  }

  RenderDeleteButton() {
    if (this.state.isEditable && this.props.eventid !== null) {
      return (
        <TouchableOpacity onPress={this.deleteEvent.bind(this)}>
          <View style={[commonStyle.buttonAcross, { backgroundColor: "red" }]}>
            <Text style={commonStyle.buttonTextAcross}>Delete Event</Text>
          </View>
        </TouchableOpacity>
      );
    } else {
      return null;
    }
  }

  formisValid() {
    //**** Event Name ****
    console.log(this.state);
    var retval = this.validateElement(
      this.state._eventname,
      this._inputeventName,
      "Please enter an Event Name",
      true
    );
    retval =
      retval &&
      this.validateElement(
        this.state._eventstartDate,
        null,
        "Please enter an Event Start Date",
        false
      );
    retval =
      retval &&
      this.validateElement(
        this.state._eventendDate,
        null,
        "Please enter an Event End Date",
        false
      );
    if (!this.state._alldayevent) {
      retval =
        retval &&
        this.validateElement(
          this.state._eventstartTime,
          null,
          "Please enter an Event Start Time",
          false
        );
      retval =
        retval &&
        this.validateElement(
          this.state._eventendTime,
          null,
          "Please enter an Event End Time",
          false
        );
    }
    // Ensure end date is after start date
    var sdate = new Date(this.state._eventstartDateObj).getTime();
    var edate = new Date(this.state._eventendDateObj).getTime();
    // if (edate - sdate <= -86400000) {
    //   retval = false;
    //   this.setState({
    //     errorText: "Please Ensure End Date of Event is after the Start Date",
    //   });
    // }

    if (retval && this.state._showParents) {
      if (Object.keys(this.state.selectedUsers).length === 0) {
        retval = false;
        this.setState({ errorText: "Please Select atleast 1 Parent" });
      }
    } else {
      retval =
        retval &&
        this.validateElement(
          this.state._grpSelectId,
          null,
          "Please enter a Group",
          false
        );
    }
    console.log(retval);
    return retval;
  }

  RenderSaveButton() {
    if (this.state.isEditable && this.props.eventid !== undefined) {
      return (
        <TouchableOpacity
          onPress={this.addSaveEvent.bind(this)}
          style={{ marginBottom: 5, marginTop: 5 }}
        >
          <View style={commonStyle.buttonAcross}>
            <Text style={commonStyle.buttonTextAcross}>Save Event</Text>
          </View>
        </TouchableOpacity>
      );
    } else {
      return null;
    }
  }

  deleteEvent(event) {
    this.props.deleteEventAction(
      this.props.eventid,
      this.props.user.token,
      this.props.user.groups
    );
    Navigation.dismissAllModals();
  }

  RenderSetNotifyParent() {
    if (this.state.isEditable) {
      return (
        <View
          style={{
            padding: 10,
            flexDirection: "row",
            justifyContent: "space-between",
          }}
        >
          <Text style={styles.switchText}>
            Notify Parents once the event is created
          </Text>
          <Switch
            style={styles.switchStyle}
            trackColor={{ true: "#777CF7", false: "grey" }}
            thumbColor={this.state._eventnotify ? "#3C3989" : null}
            editable={this.state.isEditable}
            value={this.state._eventnotify}
            onValueChange={(val) => {
              this.setState({ _eventnotify: val });
            }}
          />
        </View>
      );
    } else {
      return null;
    }
  }
 
  RenderInviteSwitch() {
    if (this.state.isEditable) {
      return (
        <View style={{ flexDirection: "row" }}>
          <Text style={styles.switchText}>Invite Parents Individually ...</Text>
          <Switch
            style={styles.switchStyle}
            editable={this.state.isEditable}
            value={this.state._showParents}
            onValueChange={(val) => {
              this.setState({ _showParents: val });
              if (!val) {
                var grp = Object.keys(this.state.groupshash)[0];
                this.setState({
                  selectedUsers: {},
                  _grpSelect: grp,
                  _grpSelectId: this.state.groupshash[grp],
                });
              }
            }}
          />
        </View>
      );
    } else {
      return null;
    }
  }

  RenderAllDaySwitch() {
    if (this.state.isEditable) {
      return (
        <View
          style={{
            padding: 10,
            flexDirection: "row",
            justifyContent: "space-between",
          }}
        >
          <Text style={styles.switchText}>All Day Event</Text>
          <Switch
            style={styles.switchStyle}
            trackColor={{ true: "#777CF7", false: "grey" }}
            thumbColor={this.state._alldayevent ? "#3C3989" : null}
            editable={this.state.isEditable}
            label="All Day Event"
            value={this.state._alldayevent}
            onValueChange={this.onAllDayValueChange.bind(this)}
          />
        </View>
      );
    } else {
      return null;
    }
  }

  RenderSetReminderSameDay() {
    if (this.state.isEditable) {
      return (
        <View
          style={{
            padding: 10,
            flexDirection: "row",
            justifyContent: "space-between",
          }}
        >
          <Text style={styles.switchText}>Remind Same Day </Text>
          <Switch
            style={styles.switchStyle}
            trackColor={{ true: "#777CF7", false: "grey" }}
            thumbColor={this.state._remindsameday ? "#3C3989" : null}
            editable={this.state.isEditable}
            value={this.state._remindsameday}
            onValueChange={(val) => {
              if (val)
                this.setState({
                  _remindsameday: val,
                  _reminderCount: this.state._reminderCount + 1,
                });
              else {
                this.setState({
                  _remindsameday: val,
                  _reminderCount: this.state._reminderCount - 1,
                });
              }
            }}
          />
        </View>
      );
    } else {
      return null;
    }
  }

  RenderSetReminderOneDay() {
    if (this.state.isEditable) {
      return (
        <View
          style={{
            padding: 10,
            flexDirection: "row",
            justifyContent: "space-between",
          }}
        >
          <Text style={styles.switchText}>Remind one day before</Text>
          <Switch
            style={styles.switchStyle}
            trackColor={{ true: "#777CF7", false: "grey" }}
            thumbColor={this.state._remindsOneday ? "#3C3989" : null}
            editable={this.state.isEditable}
            value={this.state._remindsOneday}
            onValueChange={(val) => {
              if (val)
                this.setState({
                  _remindsOneday: val,
                  _reminderCount: this.state._reminderCount + 1,
                });
              else {
                this.setState({
                  _remindsOneday: val,
                  _reminderCount: this.state._reminderCount - 1,
                });
              }
            }}
          />
        </View>
      );
    } else {
      return null;
    }
  }

  RenderSetReminderTwoDay() {
    if (this.state.isEditable) {
      return (
        <View
          style={{
            padding: 10,
            flexDirection: "row",
            justifyContent: "space-between",
          }}
        >
          <Text style={styles.switchText}>Remind Two Days Before</Text>
          <Switch
            style={styles.switchStyle}
            trackColor={{ true: "#777CF7", false: "grey" }}
            thumbColor={this.state._remindsTwoday ? "#3C3989" : null}
            editable={this.state.isEditable}
            value={this.state._remindsTwoday}
            onValueChange={(val) => {
              if (val)
                this.setState({
                  _remindsTwoday: val,
                  _reminderCount: this.state._reminderCount + 1,
                });
              else {
                this.setState({
                  _remindsTwoday: val,
                  _reminderCount: this.state._reminderCount - 1,
                });
              }
            }}
          />
        </View>
      );
    } else {
      return null;
    }
  }

  renderStartTime() {
    if (!this.state._alldayevent) {
      return (
        <View style={{ padding: 10 }}>
          <Item
            rounded
            style={styles.inputFieldCover}
            onPress={() => this.setState({ startTimePickerVisible: true })}
          >
            <Input
              placeholder="Start Time"
              errorStyle={{ color: "red" }}
              value={this.state._eventstartTime}
              editable={false}
              style={styles.inputFieldStyles}
            />
            <Icon
              active
              name="clock-o"
              size={24}
              style={{ marginRight: 10, color: "black" }}
              onPress={() => this.setState({ startTimePickerVisible: true })}
            />
          </Item>
        </View>
      );
    } else {
      return null;
    }
  }
  renderEndTime() {
    if (!this.state._alldayevent) {
      return (
        <View style={{ padding: 10 }}>
          <Item
            rounded
            style={styles.inputFieldCover}
            onPress={() => this.setState({ endTimePickerVisible: true })}
          >
            <Input
              placeholder="End Time"
              errorStyle={{ color: "red" }}
              value={this.state._eventendTime}
              editable={false}
              style={styles.inputFieldStyles}
            />
            <Icon
              active
              name="clock-o"
              size={24}
              style={{ marginRight: 10, color: "black" }}
              onPress={() => this.setState({ endTimePickerVisible: true })}
            />
          </Item>
        </View>
      );
    } else {
      return null;
    }
  }

  DateTimeFormat(date, mode) {
    if (!date) return "";
    let value = date.toLocaleTimeString([], {
      day: "2-digit",
      month: "2-digit",
      year: "2-digit",
      hour: "2-digit",
      minute: "2-digit",
    });
    return value;
  }

  TimeFormat(date, mode) {
    if (!date) return "";
    let value = date.toLocaleTimeString([], {
      hour: "numeric",
      minute: "numeric",
    });
    return value;
  }

  onAllDayValueChange(value) {
    this.setState({ _alldayevent: value });
    if (value == true) {
      this.setState({ _eventstartTime: "", _eventendTime: "" });
    }
  }

  renderInviteSection() {
    var userids = Object.keys(this.state.selectedUsers);
    var selectedMemberList = userids.map(function (userKey, i) {
      var user = this.state.selectedUsers[userKey];
      return (
        <Text key={user.username} style={styles.selectedlisttext}>
          {user.display_name}
        </Text>
      );
    }, this);
    var grps = this.getGroupData();
    if (this.props.user.groups !== undefined) {
      if (this.state._selectGroupParent === "Group") {
        return (
          <TouchableOpacity
            onPress={this.onGroupPicker.bind(this)}
            // style={{ flexDirection: "row" }}
          >
            {/* <Text style={styles.groupSelectText}>Select Group</Text>
            <Text style={styles.groupSelectTextSelected}>
              {this.state._grpSelect}
            </Text> */}
            <View style={{ padding: 10 }} pointerEvents="none">
              <Item rounded style={styles.inputFieldCover}>
                <Input
                  placeholder="Select Group"
                  editable={false}
                  style={styles.inputFieldStyles}
                  value={this.state._grpSelect}
                />
                <Entypo
                  active
                  name="chevron-down"
                  size={24}
                  style={{ marginRight: 10, color: "black" }}
                />
              </Item>
            </View>
          </TouchableOpacity>
        );
      } else if (this.state._selectGroupParent === "Parents") {
        // console.log(selectedMemberList, "selectedMemberList");
        return (
          <View>
            <TouchableOpacity onPress={() => this.showParentChooser()}>
              <View style={{ padding: 10 }} pointerEvents="none">
                <Item rounded style={styles.inputFieldCover}>
                  <Input
                    placeholder=" Select Parents to Invite ..."
                    editable={false}
                    style={styles.inputFieldStyles}
                    value={`Select Parents (${selectedMemberList.length} Selected)`}
                  />
                  <Entypo
                    active
                    name="chevron-down"
                    size={24}
                    style={{ marginRight: 10, color: "black" }}
                  />
                </Item>
              </View>
            </TouchableOpacity>
            <View style={styles.addcontainer}>{selectedMemberList}</View>
          </View>
        );
      }
    }
  }

  onGroupPicker(event) {
    var groupsdict = this.state.groupshash;
    Picker.init({
      pickerData: Object.keys(groupsdict),
      pickerTitleText: "Select a Group",
      pickerCancelBtnText: "Cancel",
      pickerConfirmBtnText: "Select",
      selectedValue: [this.state.selectedGroup],
      onPickerConfirm: (data) => {
        this.setState({ selectedGroup: data[0] });
        this.setState({
          _grpSelect: data[0],
          _grpSelectId: this.state.groupshash[data[0]],
        });
        //this.props.setGroupid(this.getSeletedGroupID(), data[0])
      },
    });
    Picker.show();
  }
  onSelectGroupsParents(event) {
    Picker.init({
      pickerData: ["Group", "Parents"],
      pickerTitleText: "",
      pickerCancelBtnText: "Cancel",
      pickerConfirmBtnText: "Select",
      selectedValue: [this.state._selectGroupParent],
      onPickerConfirm: (data) => {
        if (data[0] === "Parents") {
          this.setState({ _showParents: true, _selectGroupParent: data[0] });
        } else {
          var grp = Object.keys(this.state.groupshash)[0];
          this.setState({
            _selectGroupParent: data[0],
            selectedUsers: {},
            _grpSelect: grp,
            _grpSelectId: this.state.groupshash[grp],
          });
        }
      },
    });
    Picker.show();
  }
  GetFormattedDate(dt) {
    var newDate = new Date(dt);
    var date =
      newDate.getUTCDate() < 10
        ? "0" + newDate.getUTCDate()
        : newDate.getUTCDate();
    var month = newDate.getUTCMonth() + 1;
    month = month < 10 ? "0" + month : month;
    var dtstr = month + "/" + date + "/" + newDate.getUTCFullYear();
    return dtstr;
  }

  onSelectedList(selectedUsers) {
    this.setState({
      selectedUsers: selectedUsers,
    });
  }

  onValueChange(value) {
    this.setState({ _showParents: !value });
    if (value === false) {
      this.setState({ selectedUsers: {} });
    }
  }

  validateEndDate(value) {
    return new Date();
  }

  handleFormChange(formData) {
    this.setState({ formData: formData });
    this.props.onFormChange && this.props.onFormChange(formData);
  }
}

var styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: "column",
    justifyContent: "center",
    //padding: 10,
    backgroundColor: "white",
  },
  eventDateView: {
    marginTop: 10,
  },
  errorText: {
    color: "red",
    backgroundColor: "white",
    height: 20,
    alignSelf: "center",
    paddingLeft: 5,
    paddingRight: 5,
    borderRadius: 10,
    marginTop: 2,
  },
  headerimage: {
    height: 300,
    paddingTop: 0,
    flex: 1,
    zIndex: 1,
    flexDirection: "column",
    flexWrap: "wrap",
  },
  headerTextStyle: {
    color: "white",
    textAlign: "center",
    backgroundColor: "transparent",
    justifyContent: "flex-start",
    fontSize: 20,
  },
  addcontainer: {
    flex: 1,
    flexDirection: "row",
    flexWrap: "wrap",
    padding: 10,
  },
  selectedlisttext: {
    backgroundColor: "blue",
    color: "white",
    padding: 5,
    height: 25,
    margin: 3,
    textAlign: "center",
    borderRadius: 10,
  },
  cameraicon: {
    ...Platform.select({
      ios: {
        color: "white",
        alignItems: "flex-end",
        opacity: 0.8,
        paddingTop: 200,
        paddingLeft: 10,
        zIndex: 20,
      },
      android: {
        color: "white",
        alignItems: "flex-end",
        opacity: 0.8,
        paddingTop: 200,
        paddingLeft: 10,
        zIndex: 20,
      },
    }),
  },
  deleteButton: {
    paddingLeft: 10,
    paddingRight: 10,
    marginTop: 3,
    backgroundColor: "#FF4545",
    borderRadius: 2,
    flexDirection: "row",
    height: 18,
  },
  formContainer: {
    //   backgroundColor : 'purple',
    //   color : 'white'
  },
  modal: {
    margin: 0, // This is the important style you need to set
    alignItems: undefined,
    justifyContent: undefined,
  },
  titleStyle: {
    fontSize: 14,
    backgroundColor: "lightgray",
    padding: 3,
    top: 0,
  },
  dividerStyle: {
    width: 0,
  },
  actionButtonIcon: {
    // fontSize: 20,
    // height: 22,
    // width:22,
    color: "white",
  },
  switchText: {
    color: "#000",
    fontSize: 16,
    ...Platform.select({
      android: {},
      default: {},
    }),
    // paddingLeft: 10
  },
  switchStyle: {},
  groupSelectText: {
    color: "#86939e",
    fontSize: 16,
    ...Platform.select({
      android: {
        fontWeight: "bold",
      },
      default: {
        fontWeight: "bold",
      },
    }),
    paddingLeft: 10,
  },
  roundImgStyle: {
    width: (Dimensions.get("window").width * 0.9) / 4,
    height: (Dimensions.get("window").width * 0.9) / 4,
    marginHorizontal: 5,
    padding: 0,
    borderRadius: (Dimensions.get("window").width * 0.9) / 8.2,
    borderColor: "#d3d3d3",
    borderWidth: 0,
  },
  dialogItem: {
    height: 50,
    flexDirection: "row",
    alignItems: "center",
    borderBottomWidth: 1,
    borderBottomColor: "#d3d3d3",
  },

  groupSelectTextSelected: {
    marginLeft: 20,
    paddingTop: 5,
    paddingLeft: 10,
    paddingBottom: 5,
    paddingRight: 10,
    backgroundColor: "#3fb0ac",
    marginTop: -5,
    color: "white",
  },
  monthPublicText: {
    fontSize: 16,
    color: "#545E75",
    fontWeight: "bold",
    textAlign: "left",
    alignSelf: "center",
  },
  dateFromPublicText: {
    fontSize: 32,
    color: "darkgray",
    fontWeight: "bold",
    textAlign: "center",
  },
  dateToPublicText: {
    fontSize: 32,
    color: "darkgray",
    fontWeight: "bold",
    alignContent: "center",
    textAlign: "center",
  },
  timePublicFromText: {
    fontSize: 18,
    color: "blue",
    fontWeight: "bold",
    paddingLeft: 5,
  },
  timePublicToText: {
    fontSize: 18,
    color: "blue",
    fontWeight: "bold",
  },
  publicCard: { marginLeft: -5, marginRight: -5, flex: 1 },
  stepperContainer: {
    flexDirection: "row",
    justifyContent: "space-around",
    padding: 10,
  },
  stepperElement: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },

  roundElement: {
    width: 50,
    height: 50,
    borderRadius: 50,
    alignItems: "center",
    borderWidth: 2,
    borderColor: "#3C3989",
    justifyContent: "center",
  },
  hightlightRoundElement: {
    width: 50,
    height: 50,
    borderRadius: 50,
    backgroundColor: "#3C3989",
    alignItems: "center",
    borderWidth: 2,
    borderColor: "#3C3989",
    justifyContent: "center",
  },
  highlightedStepText: {
    fontSize: 16,
    fontWeight: "bold",
    color: "#3C3989",
  },
  stepText: {
    fontSize: 16,
    color: "#3C3989",
  },
  textContainerStyle: {
    width: 100,
    textAlign: "center",
  },
  inputFieldCover: {
    backgroundColor: "#F4F4F4",
    paddingLeft: 10,
    paddingRight: 10,
  },
  inputFieldStyles: { fontFamily: "Helvetica Neue", fontSize: 16 },
  sectionTextHeading: {
    padding: 10,
    fontWeight: "bold",
    fontSize: 18,
    fontFamily: "Helvetica Neue",
    color: "#000000",
  },
  generalTextView: {
    padding: 10,
    fontSize: 16,
    fontFamily: "Helvetica Neue",
    color: "#000000",
  },
  newGeneralTextView: {
    margin: 10,
    fontSize: 18,
    fontFamily: "Helvetica Neue",
    color: "#000000",
  },
  newSectionHeading: {
    margin: 10,
    fontWeight: "bold",
    fontSize: 22,
    fontFamily: "Helvetica Neue",
    color: "#000000",
  },
  previewTextShow: {
    fontSize: 18,
    fontFamily: "Helvetica Neue",
    color: "#000000",
  },
  inputlabel: {
    fontFamily: "Helvetica Neue",
    padding: 2,
    color: "#000000",
  },
});

const theme = {
  Input: {
    inputStyle: {
      color: "blue",
    },
  },
};

var mapStateToProps = function (store) {
  return {
    user: store.user,
    login: store.login,
    events: store.events,
    children_groups: store.attendance.groups,
  };
};

var mapDispatchToProps = function (dispatch) {
  return {
    addEventAction: function (
      formData,
      selectedUsers,
      displayName,
      token,
      eventimgurl,
      groups,
      eventtype,
      eventid
    ) {
      dispatch(
        eventActions.addEventAction(
          formData,
          selectedUsers,
          displayName,
          token,
          eventimgurl,
          groups,
          eventtype,
          eventid
        )
      );
    },
    deleteEventAction: function (eventid, token, groups) {
      dispatch(eventActions.deleteEventAction(eventid, token, groups));
    },
    deleteSignupforChild: function (event_id, child_id, slotId, token) {
      dispatch(
        eventActions.deleteSignupforChild(event_id, child_id, slotId, token)
      );
    },
    getEventsAction: function (token, groups) {
      dispatch(eventActions.getEventsAction(token, groups));
    },
    clearLastRefreshTime: function () {
      dispatch(eventActions.clearLastRefreshTime());
    },
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(AddEventScreen);
