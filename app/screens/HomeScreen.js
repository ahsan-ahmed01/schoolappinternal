import React from "react";
import {
  Platform,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  Image, Alert
} from "react-native";
import { connect } from "react-redux";
import * as loginActions from "../actions/loginActions";
import * as attendanceActions from "../actions/attendanceActions";
import * as newsfeedaction from "../actions/newsfeedActions";
import Icon from "react-native-vector-icons/Entypo";
import { Navigation } from "react-native-navigation";
import { commonStyle } from "../config/commonstyles";

var EnvConfigs = require("../config/environment");
import timer from "react-native-timer";
import NotificationsUtil from "../utils/notifications";
var EnvConfig = require("../config/environment");
//import { GoogleAnalyticsTracker } from "react-native-google-analytics-bridge";
import VersionNumber from "react-native-version-number";
import codePush from "react-native-code-push";
//import firebase from 'react-native-firebase'
import ProfileButton from "../components/ProfileButton";
import HMSActionButton from "../navigation/HMSActionButton";
import * as userActions from '../actions/userActions'
import * as userapi from '../api/userapi'
var bugsnag = require('../config/bugsnag')
import CodePushComponent from "../components/codepushcomponent";
class HomeScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      showAbout: false
    };
    this.onLoadNewsFeed = this.onLoadNewsFeed.bind(this);
    this.onMoreNewsFeed = this.onMoreNewsFeed.bind(this);
    console.log(this.props);
  }

  async checkResetVersion(){
    console.log('Checking Reset Version !')
    await userapi.getResetVerson().then((data)=> {
        console.log(data)
        var current_version = data.data.data.reset_version
        console.log('Current-Version-> ' + this.props.reset_version)
        console.log('Checked-Version-> ' + current_version)
        bugsnag.leaveBreadcrumb('Current-Version-> ' + this.props.reset_version)
        bugsnag.leaveBreadcrumb('Checked-Version-> ' + current_version)
        if (current_version > this.props.reset_version){
            
            console.log('PROFILEBUTTON->')
            Alert.alert('Error', 'Hmmm .... There seems to be an error with your application. We have logged you off the application to reset its state')
            this.props.resetUser(current_version)
        }
    }).catch((error)=>{
        console.log(error)
         // this.props.resetUser(current_version)
    })
  }

  componentWillMount(){
    this.checkResetVersion()
  }



  shouldComponentUpdate(nextProps, nextState) {
    console.log("HomeScreen");
    console.log(this.props.usergroups)
    console.log(nextProps.groups)
    if(nextProps.usergroups != this.props.usergroups){
        if (this.props.usergroups !== null) {
            if (Object.keys(this.props.usergroups).length > 0) {
              let groups = [];
              for (var key in this.props.usergroups) {
                groups.push(parseInt(key));
              }
              console.log(groups);
              this.props.initAttendanceList(
                groups,
                new Date().yyyymmdd(),
                this.props.token
              );
            }
          }
    }

    if (nextProps.refreshNewFeed && !this.props.refreshNewFeed) {
      console.log("on Loading news Feed");
      this.onLoadNewsFeed();
    }
    if (
      this.props.newsfeed.isNewsFeedLoading === false &&
      nextProps.newsfeed.isNewsFeedLoading
    ) {
      return false;
    }
    if (this.props.token !== nextProps.token) {
      return false;
    }
    if (this.props.feed_nextpg === 0 && nextProps.feed_nextpg === 1) {
      return false;
    }
    return true;
  }

  componentDidMount() {
    console.log("DidMount");
    codePush.getUpdateMetadata().then(metadata => {
      console.log(metadata);
      if (metadata !== null && metadata !== undefined) {
        var versionString =
          VersionNumber.appVersion +
          " " +
          VersionNumber.buildVersion +
          " (" +
          metadata.label +
          ") ";
        console.log("Version ->" + versionString);
      }
    });
  }

//   componentWillMount() {
//     if (this.props.groups != null) {
//       if (Object.keys(this.props.usergroups).length > 0) {
//         let groups = [];
//         for (var key in this.props.usergroups) {
//           groups.push(parseInt(key));
//         }
//         console.log(groups);
//         this.props.initAttendanceList(
//           groups,
//           new Date().yyyymmdd(),
//           this.props.token
//         );
//       }
//     }
//   }

  render() {
    console.log("RENDER->HOMESCREEN");
    return (
      <View style={styles.container}>
        <CodePushComponent />
        <NotificationsUtil />
        <HMSActionButton
          userroles={this.props.userroles}
          parent={"nav.Three60Memos.HomeScreen"}
        />
       <Text>News Feed Screen</Text>
      </View>
    );
  }

  onShowSignIn() {}

  onLoadNewsFeed() {
    console.log("Resetting to Page 0");
    this.props.getNewsFeedAll(
      0,
      this.props.token,
      this.props.authobj,
      this.props.usergroups
    );
  }

  onMoreNewsFeed() {
    console.log("Loading Page # " + this.props.feed_nextpg);
    this.props.getNewsFeedAll(
      this.props.feed_nextpg,
      this.props.token,
      this.props.authobj,
      this.props.usergroups
    );
  }

  _maybeRenderDevelopmentModeWarning() {
    if (__DEV__) {
      const learnMoreButton = (
        <Text onPress={this._handleLearnMorePress} style={styles.helpLinkText}>
          Learn more
        </Text>
      );

      return (
        <Text style={styles.developmentModeText}>
          Development mode is enabled, your app will be slower but you can use
          useful development tools. {learnMoreButton}
        </Text>
      );
    } else {
      return (
        <Text style={styles.developmentModeText}>
          You are not in development mode, your app will run at full speed.
        </Text>
      );
    }
  }
}

var mapStateToProps = function(store) {
  return {
    authobj: store.login.authobj,
    newsfeed: store.newsfeed,
    token: store.user.token,
    usergroups: store.user.groups,
    userroles: store.user.roles,
    refreshNewFeed: store.newsfeed.refreshNewFeed,
    feed_nextpg: store.newsfeed.feed_nextpg,
    reset_version : store.user.reset_version
  };
};

var mapDispatchToProps = function(dispatch) {
  return {
    authenticate: function(username, password) {
      dispatch(loginActions.authenticateUser(username, password));
    },
    getNewsFeedAll: function(page, token, authobj, groups) {
      dispatch(newsfeedaction.getNewsFeedAll(page, token, authobj, groups));
    },
    setNavigationTab: function(tabName) {
      dispatch(userActions.setNavigationTab(tabName));
    },
    checkTokenValidity: function(token) {
      dispatch(loginActions.checkTokenValidity(token));
    },
    setLoginModalStatus: function(modalStatus) {
      dispatch(loginActions.setLoginModalStatus(modalStatus));
    },
    initAttendanceList: function(groups, attendancedate, token) {
      dispatch(
        attendanceActions.initAttendanceList(groups, attendancedate, token)
      );
    },
        resetUser: function () {
            dispatch(userActions.resetUser())
        },
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(HomeScreen);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff"
  },
  composeIcon: {
    paddingLeft: 10
  },
  MenuIcon: {
    paddingRight: 10
  },
  developmentModeText: {
    marginBottom: 20,
    color: "rgba(0,0,0,0.4)",
    fontSize: 14,
    lineHeight: 19,
    textAlign: "center"
  },
  contentContainer: {
    paddingTop: 0
  },
  welcomeContainer: {
    alignItems: "center",
    marginTop: 10,
    marginBottom: 20
  },
  welcomeImage: {
    width: 100,
    height: 80,
    resizeMode: "contain",
    marginTop: 3,
    marginLeft: -10
  },
  getStartedContainer: {
    alignItems: "center",
    marginHorizontal: 50
  },
  homeScreenFilename: {
    marginVertical: 7
  },
  codeHighlightText: {
    color: "rgba(96,100,109, 0.8)"
  },
  codeHighlightContainer: {
    backgroundColor: "rgba(0,0,0,0.05)",
    borderRadius: 3,
    paddingHorizontal: 4
  },
  getStartedText: {
    fontSize: 17,
    color: "rgba(96,100,109, 1)",
    lineHeight: 24,
    textAlign: "center"
  },
  tabBarInfoContainer: {
    position: "absolute",
    bottom: 0,
    left: 0,
    right: 0,
    ...Platform.select({
      ios: {
        shadowColor: "black",
        shadowOffset: { height: -3 },
        shadowOpacity: 0.1,
        shadowRadius: 3
      },
      android: {
        elevation: 20
      }
    }),
    alignItems: "center",
    backgroundColor: "#fbfbfb",
    paddingVertical: 20
  },
  tabBarInfoText: {
    fontSize: 17,
    color: "rgba(96,100,109, 1)",
    textAlign: "center"
  },
  navigationFilename: {
    marginTop: 5
  },
  helpContainer: {
    marginTop: 15,
    alignItems: "center"
  },
  helpLink: {
    paddingVertical: 15
  },
  helpLinkText: {
    fontSize: 14,
    color: "#2e78b7"
  }
});
