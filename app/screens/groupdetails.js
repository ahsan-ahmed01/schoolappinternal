'use strict'

import {
    Platform,
    AppRegistry,
    RefreshControl,
    Text,
    ActivityIndicator,
    View,
    StyleSheet,
    Image, ImageBackground,Dimensions,
    ScrollView
} from 'react-native'
import React, {Component} from 'react'


import {connect} from 'react-redux'

import GroupMemberList from './groupdetailsmembers'
import GroupPhotos from './components/groupphotos'
import * as groupdetailactions from '../actions/groupdetailsActions'
import GroupActivity from './components/groupactivity'
import * as newsfeedaction from '../actions/newsfeedActions'
import ScrollableTabView, { ScrollableTabBar, } from 'react-native-scrollable-tab-view';
import GroupTabBar from './components/grouptabbar'
var EnvConfigs = require('../config/environment')
import timer from 'react-native-timer'

class GroupDetailScreen extends Component {





    constructor(props) {
        super(props);
        this.state = {
            covert_photo : null,
            profile_photo : null,

        }

    }


    componentWillUnmount() {
        if (Platform.OS === 'ios') {
            //timer.clearInterval('ClassRefresh')
        }

    }


    _renderHeaderNew2 = () => {
        console.log(this.props.selectedgroup)
        if (this.props.selectedgroup.is_admin === 1 ||
            this.props.selectedgroup.is_mod === 1){
            var is_admin = true;
        }else{
            var is_admin = false;
        }

        console.log(this.props)


        return (

                <GroupTabBar style={styles.grouptabbarcontainer}
                             is_admin={is_admin}
                             user_token = {this.props.user.token}
                             group_id = {this.props.selectedgroup.id}
                             group_name={this.props.selectedgroup.name}
                              group_desc={this.props.selectedgroup.description}/>)
    }

    _onRefresh() {
        console.log('inRefresh')
        var group = {}
        group[this.props.selectedgroup.id] = this.props.selectedgroup
        this.props.getNewsFeedByGroup(0, group, this.props.user.token)
    }


    shouldComponentUpdate(nextProps, nextState) {
        console.log(this.props.selectedgroup.cover_photo)
        console.log(nextProps.selectedgroup.cover_photo)
        if ((nextProps.rootNavprops.currentTab !== "nav.Three60Memos.EventsScreen") &&
            (nextProps.rootNavprops.currentTab !== "nav.Three60Memos.GroupDetailScreen"))
        {
            console.log('No Need to Render Events')
            return false
        }
        return true
    }

    componentDidUpdate(prevProps, prevState){
        console.log(this.props)

        if (this.props.rootNavprops.currentTab === "nav.Three60Memos.GroupDetailScreen"){
            // If tab is switched immediately refresh
            if (this.props.rootNavprops.currentTab !== prevProps.rootNavprops.currentTab){
                this.props.showNewFeedsisLoading()
                this._onRefresh()
            }
            // if (Platform.OS === 'ios') {
            //     if (!timer.intervalExists('ClassRefresh')) {
            //         timer.setInterval('ClassRefresh', () => this._onRefresh(),
            //             EnvConfigs.HOMESCREEN_INTERVAL);
            //     }
            // }
        }
    }

    _onScroll (e) {
        var windowHeight = Dimensions.get('window').height,
            height = e.nativeEvent.contentSize.height,
            offset = e.nativeEvent.contentOffset.y
        console.log(windowHeight + '-' + height + '-' + offset)
        if (windowHeight + offset >= height) {
            console.log('End ScrollView')
            console.log(this.props)
            var group = {}
            group[this.props.selectedgroup.id] = this.props.selectedgroup

            if (!this.props.newsfeed.isNewsFeedLoading)
                this.props.getNewsFeedByGroup(this.props.newsfeed.group_nextpg, group, this.props.user.token)
        }
    }

    render() {
        console.log(this.props)
        console.log('RENDER-> IN GROUP DETAIL ->' + this.props.selectedgroup.desc)
        if (this.props.selectedgroup.errorLoadingGroupDetails) {
            return (<View style={[styles.tabcontainer, styles.errcontainer]}>
                <Text style={styles.errText}>Error Loading Data</Text>
                <Text style={styles.errText}>Check your network connection and try again</Text>
            </View>)
        }
        else {
            var showGroupMembers = () => {
                console.log(this.props.selectedgroup)
                if (this.props.selectedgroup.desc.toLowerCase().includes('public'))
                {
                    return null
                }else {


                    return (<GroupMemberList selectedgroup={this.props.selectedgroup} tabBarTextStyle={{}} parentprops={this.props}  tabLabel="Members"/>)
                }
            }

            return (

                <ScrollableTabView initialPage={0} renderTabBar={this._renderHeaderNew2}>
                    <ScrollView ref="scrollView" style={styles.container} tabLabel="Activity"
                                onScroll={this._onScroll.bind(this)}
                                refreshControl={
                                    <RefreshControl
                                        refreshing={this.props.newsfeed.isNewsFeedLoading}
                                        onRefresh={this._onRefresh.bind(this)}
                                        tintColor="#d3d3d3"
                                        title="Loading..."
                                        titleColor="black"
                                        colors={['#ff0000', '#00ff00', '#0000ff']}
                                        progressBackgroundColor="#ffff00"/>}>
                        <GroupActivity selectedgroup={this.props.selectedgroup}
                                       componentId={this.props.componentId}
                                       navigation={this.props.navigation} />
                    </ScrollView>
                    {showGroupMembers()}

                    {/*<GroupPhotos  tabLabel="Photos" />*/}

                </ScrollableTabView>

            )
        }
    }
}


const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    body: {
        flex: 3
    },
    grouptabbarcontainer:{
      marginBottom: 100
    },

    tabcontainer: {
        flex: 1,
        backgroundColor: 'white'
    },
    tabmenu: {
        backgroundColor: 'darkgreen',
        height: 35
    },
    tabbar: {
        backgroundColor: 'lightgreen',
    },
    page: {
        flex: 1,

    },
    centering: {
        alignItems: 'center',
        justifyContent: 'center',
        padding: 0
    },
    errcontainer: {
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F24949'
    },
    errText: {
        color: 'white',
        textAlign: "right",
    }
})
var mapStateToProps = function (store) {
    return {
        login: store.login,
        user: store.user,
        newsfeed: store.newsfeed,
        rootNavprops : store.rootNavprops,
    };
};
var mapDispatchToProps = function (dispatch) {
    return {
        getNewsFeedByGroup: function (page, groupid, token) {
            dispatch(newsfeedaction.getNewsFeedByGroup(page, groupid, token))
        },
        getGroupPhotos: function (groupid, token) {
            dispatch(groupdetailactions.getGroupPhotos(groupid, token));
        },
        showNewFeedsisLoading : function () {
            dispatch(newsfeedaction.showNewFeedsisLoading())
        }
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(GroupDetailScreen)