const React = require('react');
const { Component } = require('react');
const { View, Text, ScrollView, Platform, TouchableHighlight, RefreshControl } = require('react-native');
import { connect } from 'react-redux'
import BriefCaseTabBar from './components/briefcasetabbar'
const { Navigation } = require('react-native-navigation');
import ScrollableTabView, { DefaultTabBar, ScrollableTabBar } from 'react-native-scrollable-tab-view';
import * as briefcaseActions from '../actions/briefcaseActions'
import Spinner from 'react-native-loading-spinner-overlay'
import BriefCaseNewsList from './components/briefcasenewslist'
import { Card } from 'react-native-elements'

class BriefCaseScreen extends Component {
    constructor(props) {
        super(props)
        this.state = {
            refreshing: false
        }
        this._onRefresh = this._onRefresh.bind(this)
    }

    componentDidUpdate(prevProps, prevState) {
        if (this.props.currentTab === 'nav.Three60Memos.BriefcaseScreen') {
            if (this.props.currentTab !== prevProps.currentTab) {
                this.props.refreshBriefcase(this.props.usergroups)
            }
        }
    }
    showDrafts() {
        console.log(this.props.briefcase.draftfeed)
        console.log(typeof this.props.briefcase.draftfeed)
        if (((this.props.briefcase.draftfeed === undefined)
            || (this.props.briefcase.draftfeed === null))
            || (typeof this.props.briefcase.draftfeed === "object" && Object.keys(this.props.briefcase.draftfeed).length === 0)) {
                console.log('No Drafts !!!')
            return (
                <Card title=" No Posts Found">
                    <Text style={{ marginBottom: 10 }}>
                        There are no Posts in Draft at the moment !
                    </Text>
                </Card>
            )
        } else {
            return (
                <View>
                    <BriefCaseNewsList  isReviewPost={false} isDraftPost={true} isScheduledPost={false}  componentId={this.props.componentId} feeddata={this.props.briefcase.draftfeed} />
                </View>

            )
        }
    }

    showScheduled() {
        console.log(this.props.briefcase.scheduledfeed)
        if ((this.props.briefcase.scheduledfeed === undefined)
            || (this.props.briefcase.scheduledfeed === null)
            || (Object.keys(this.props.briefcase.scheduledfeed).length === 0)) {
            return (
                <Card title=" No Posts Found">
                    <Text style={{ marginBottom: 10 }}>
                        There are no Scheduled Posts at the moment !
                    </Text>
                </Card>
            )
        } else {
            return (
                <View>
                    <BriefCaseNewsList  isReviewPost={false} isDraftPost={false} isScheduledPost={true} componentId={this.props.componentId} feeddata={this.props.briefcase.scheduledfeed} />
                </View>

            )
        }
    }

    shouldComponentUpdate(nextProps, nextState) {
        if (nextProps.currentTab === 'nav.Three60Memos.BriefcaseScreen') {
            return true
        }else{
            return false
        }
    }

    showReview() {
        console.log(this.props.briefcase.reviewfeed)
        if ((this.props.briefcase.reviewfeed === undefined)
            || (this.props.briefcase.reviewfeed === null)
            || (Object.keys(this.props.briefcase.reviewfeed).length === 0)) {
            return (
                <Card title=" No Posts Found">
                    <Text style={{ marginBottom: 10 }}>
                        There are no Posts for Review at the moment !
                    </Text>
                </Card>
            )
        } else {
            return (
                <View>
                    <BriefCaseNewsList  isReviewPost={true} isDraftPost={false} isScheduledPost={false} componentId={this.props.componentId} feeddata={this.props.briefcase.reviewfeed} />
                </View>

            )
        }
    }

    _onRefresh() {
        console.log('In Refresh')
        this.props.refreshBriefcase(this.props.usergroups)
    }

    render() {
        console.log(this.props.briefcase)
        return (
            <View style={{ flex: 1, padding : 0 }}>
                <Spinner visible={false} textContent={'Loading...'} textStyle={{ color: '#FFF' }} />
                <ScrollableTabView
                    style={{  }}
                    initialPage={0}
                    renderTabBar={() => <BriefCaseTabBar  briefcase={this.props.briefcase}/>}>
                    <ScrollView  refreshControl={
                        <RefreshControl
                            onRefresh={this._onRefresh}
                            refreshing={Platform.OS === 'ios' ? this.props.briefcase.isLoading : false} 
                        />
                    }
                        tabLabel="My Drafts" style={styles.tabView}>
                        {this.showDrafts()}
                    </ScrollView>
                    <ScrollView tabLabel="Scheduled" badgecount={1} style={styles.tabView} refreshControl={
                        <RefreshControl
                            onRefresh={this._onRefresh}
                            refreshing={Platform.OS === 'ios' ? this.props.briefcase.isLoading : false} 
                        />
                    }
                    >
                        {this.showScheduled()}
                    </ScrollView>
                    <ScrollView tabLabel="Review" badgecount={1} style={styles.tabView} refreshControl={
                        <RefreshControl
                            onRefresh={this._onRefresh}
                            refreshing={Platform.OS === 'ios' ? this.props.briefcase.isLoading : false} 
                        />
                    }
                    >
                        {this.showReview()}
                    </ScrollView>
                </ScrollableTabView>
            </View>
        )
    }
}

var mapStateToProps = function (store) {
    return {
        currentTab: store.rootNavprops.currentTab,
        briefcase: store.briefcase,
        usergroups: store.user.groups,

    }
}

var mapDispatchToProps = function (dispatch) {
    return {
        refreshBriefcase: function (usergroups) {
            dispatch(briefcaseActions.refreshBriefcase(usergroups))
        },
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(BriefCaseScreen)

const styles = {
    root: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#e8e8e8',
    },
    message: {
        textAlign: 'center',
        fontSize: 32
    },
    message2: {
        textAlign: 'center',
        fontSize: 20,
        color: 'blue'
    },
    bar: {
        flex: 1,
        marginTop: 200,
        flexDirection: 'column',
        backgroundColor: '#e8e8e8',
        height: 100,


    },
    h1: {
        fontSize: 24,
        textAlign: 'center',
        margin: 30
    },
    footer: {
        fontSize: 10,
        color: '#888',
        marginTop: 10
    },
    tabView: {
        flex: 1,

        backgroundColor: 'lightgray'
    },
    card: {
        flex: 1,
        backgroundColor: 'pink',
        borderWidth: 1,
        backgroundColor: '#fff',
        borderColor: 'rgba(0,0,0,0.1)',
        margin: 5,
        height: 150,
        padding: 15,
        shadowColor: '#ccc',
        shadowOffset: { width: 2, height: 2, },
        shadowOpacity: 0.5,
        shadowRadius: 3,
    },
};
